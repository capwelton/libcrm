<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */






class crm_PortletDefinition_MyTasks extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{
    /**
     * (non-PHPdoc)
     * @see portlet_PortletDefinitionInterface::getName()
     *
     * @return string
     */
    public function getName()
    {
        $Crm = $this->Crm();
        return sprintf($Crm->translate('My incoming tasks'), $Crm->getAddonName());
    }


    public function getDescription()
    {
        $Crm = $this->Crm();
        return $Crm->translate('My pending CRM Tasks ' . $Crm->getAddonName());
    }
    
    public function getPreferenceFields()
    {
        $Crm = $this->Crm();
        return array(
            1 => array(
                'type' => 'boolean',
                'label' => $Crm->translate('Show personal tasks'),
                'name' => 'personalTask'
            ),
            2 => array(
                'type' => 'boolean',
                'label' => $Crm->translate('Show production tasks'),
                'name' => 'productionTask'
            )
        );
    }
}






class crm_PortletUi_MyTasks extends crm_PortletUi implements portlet_PortletInterface
{
    protected $configuration = array();

    protected $portletId = null;


    /**
     * @param Func_Crm $crm
     */
    public function __construct(Func_Crm $crm)
    {
        $W = bab_Widgets();
        parent::__construct($crm);
        $this->setInheritedItem($W->VBoxLayout());
    }


    /**
     * {@inheritDoc}
     * @see crm_PortletUi::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new crm_PortletDefinition_MyTasks($this->Crm());
    }


    /**
     * {@inheritDoc}
     * @see crm_PortletUi::setPortletId()
     */
    public function setPortletId($id)
    {
        $this->portletId = $id;
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see crm_PortletUi::setPreferences()
     */
    public function setPreferences(Array $configuration)
    {
        $this->configuration = $configuration;
    }


    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        $ctrl = $this->Crm()->Controller()->Task(false);

        $itemId = get_class($this) . $this->portletId;

        $tableview = $ctrl->myTasks($this->configuration['personalTask'], $this->configuration['productionTask'], $itemId);

        return $tableview->display($canvas);
    }
}
