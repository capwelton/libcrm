<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */








class crm_PortletDefinition_MailBox extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Mail box');
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Mail box') . ' ' . $Crm->getAddonName();
	}

	/**
	* @return array
	*/
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();

		$prefs = array();

		$modesList = array(
			array('value' => crm_HistoryElement::MODE_COMPACT, 'label' => $Crm->translate('Compact')),
			array('value' => crm_HistoryElement::MODE_SHORT, 'label' => $Crm->translate('Short')),
			array('value' => crm_HistoryElement::MODE_FULL, 'label' => $Crm->translate('Full'))
		);


		$prefs[] = array(
			'label' => $Crm->translate('Title'),
			'type' => 'string',
			'name' => 'title',
			'default' => ''
		);
		$prefs[] = array(
			'label' => $Crm->translate('Number of emails displayed'),
			'type' => 'integer',
			'name' => 'nbItems',
			'default' => 5
		);
		$prefs[] = array(
			'label' => $Crm->translate('Display mode'),
			'type' => 'list',
			'name' => 'mode',
			'options' => $modesList
		);
		return $prefs;
	}

}




class crm_PortletUi_MailBox extends crm_PortletUi implements portlet_PortletInterface
{

	/**
	 * @param Func_Crm $crm
	 */
	public function __construct(Func_Crm $Crm)
	{
		$W = bab_Widgets();
		parent::__construct($Crm);

		$this->setInheritedItem(
			$W->VBoxItems()
		);
	}




	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
	    $W = bab_Widgets();

		if (empty($this->options['nbItems'])) {
			$this->options['nbItems'] = 2;
		}
		if (empty($this->options['mode'])) {
			$this->options['mode'] = crm_HistoryElement::MODE_SHORT;
		}
		if (empty($this->options['title'])) {
			$this->options['title'] = '';
		}

		$mailBoxCtrl = $this->Crm()->Controller()->MailBox();

		if (method_exists($mailBoxCtrl, 'recentEmails')) {
		    $this->addItem(
		        $W->DelayedItem($mailBoxCtrl->recentEmails($this->options['nbItems'], $this->options['mode'], $this->options['title']))
		    );
		}

		return parent::display($canvas);
	}
}
