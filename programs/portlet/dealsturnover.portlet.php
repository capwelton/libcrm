<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */


require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';


class crm_PortletDefinition_DealsTurnOver extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{



	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('Deals'), $Crm->getAddonName());
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('Deals turnover (%s).'), $Crm->getAddonName());
	}




	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();

		$modesList = array(
			array('value' => 'sales_volume', 'label' => $Crm->translate('Volume')),
			array('value' => 'sales_amount', 'label' => $Crm->translate('Amount'))
		);
		$nbMonthsList = array(
			array('value' => '2', 'label' => $Crm->translate('2')),
			array('value' => '3', 'label' => $Crm->translate('3')),
			array('value' => '4', 'label' => $Crm->translate('4')),
			array('value' => '5', 'label' => $Crm->translate('5')),
			array('value' => '6', 'label' => $Crm->translate('6')),
			array('value' => '7', 'label' => $Crm->translate('7')),
			array('value' => '8', 'label' => $Crm->translate('8')),
			array('value' => '9', 'label' => $Crm->translate('9')),
			array('value' => '10', 'label' => $Crm->translate('10')),
			array('value' => '11', 'label' => $Crm->translate('11')),
			array('value' => '12', 'label' => $Crm->translate('12')),
			array('value' => '18', 'label' => $Crm->translate('18')),
			array('value' => '24', 'label' => $Crm->translate('24')),
		);



		$statusLevels = array('1' => 0);
		$offset = 1;

		$statusList = array();
		$statusSet = $Crm->StatusSet();
		$statuses = $statusSet->select($statusSet->id->greaterThan(1))->orderAsc($statusSet->lf);
		foreach ($statuses as $status) {
			if ($status->name == '') {
				continue;
			}
			if ($status->id_parent) {
				$statusLevel = $statusLevels[$status->id_parent] + 1;
			} else {
				$statusLevel = 0;
			}
			$statusLevels[$status->id] = $statusLevel;
			$statusName = str_repeat(bab_nbsp(), 4 * ($statusLevel - $offset)) . $status->name;
			$statusList[] = array('value' => $status->id, 'label' => $statusName);
		}

		$prefs = array(
			array('label' => $Crm->translate('Mode'), 'type' => 'list', 'name' => 'mode', 'options' => $modesList),
			array('label' => $Crm->translate('Number of months to display'), 'type' => 'list', 'name' => 'nbMonths', 'options' => $nbMonthsList),
			array('label' => $Crm->translate('Status'), 'type' => 'list', 'name' => 'status', 'options' => $statusList),
			array('label' => $Crm->translate('Start date'), 'type' => 'date', 'name' => 'start'),
			array('label' => $Crm->translate('Compare with previous year'), 'type' => 'boolean', 'name' => 'previousYear')
		);

		return $prefs;
	}
}




class crm_PortletUi_DealsTurnOver extends crm_PortletUi implements portlet_PortletInterface
{

    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm)
    {
        $W = bab_Widgets();
        parent::__construct($Crm);
        $this->setInheritedItem($W->Frame());
    }


    /**
     *
     * @param array $monthlyValues
     * @return Widget_TableView
     */
    protected function tableView($periodValues, $periodDeals)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $tableView = $W->TableView();
        $tableView->setView(Widget_TableView::VIEW_CHART_BAR);

        $periodLabels = array(
            0 => $Crm->translate('Current year'),
            1 => $Crm->translate('Previous year')
        );

        $tableView->addSection('header', null, 'widget-table-header');
        $tableView->setCurrentSection('header');

        $row = 0;
        $col = 0;
        $tableView->addItem($W->Label(''), $row, $col);
        $col++;
        $previousYear = null;
        foreach ($periodValues as $monthlyValues) {
            foreach ($monthlyValues as $yearMonth => $value) {
                list($year) = explode('-', $yearMonth);
                $time = bab_mktime($yearMonth . '-01');
                if ($previousYear !== $year) {
                    $monthLabel = date('M Y', $time);
                    $monthLabel = bab_formatDate('%m' . "\n". '%y', $time);
                    $previousYear = $year;
                } else {
                    $monthLabel = date('M', $time);
                    $monthLabel = bab_formatDate('%m', $time);
                }

                $tableView->addItem($W->Label($monthLabel), $row, $col++);
            }
            break;
       }

        $tableView->addSection('body', null, 'widget-table-body');
        $tableView->setCurrentSection('body');

        $row++;

        foreach ($periodValues as $periodId => $monthlyValues) {

            $monthlyDeals = $periodDeals[$periodId];

            $col = 0;
            $tableView->addItem($W->Label($periodLabels[$periodId]), $row, $col);
            $col++;

            foreach ($monthlyValues as $yearMonth => $value) {

                if (count($monthlyDeals[$yearMonth]) > 0) {
                    $item = $W->Link(
                        '' . $value,
                        $Crm->Controller()->Deal()->displayList(
                            array(
                                'filter' => array(
                                    'fullNumber' => implode(',', $monthlyDeals[$yearMonth])
                                )
                            )
                        )
                    )->setTitle(count($monthlyDeals[$yearMonth]));
                } else {
                    $item = $W->Label('' . $value);
                }

                $tableView->addItem($item, $row, $col);
                $col++;
            }
            $row++;
        }

        return $tableView;
    }



	/**
	 * @return
	 */
	public function volume($nbMonths, $status, $startDate = null, $nbYears = 1)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		if (!isset($startDate)) {
			$start = bab_DateTime::now();
		}

		$statusSet = $Crm->StatusSet();
		/* @var $status crm_Status */
		$status = $statusSet->get($status);

		if (!isset($status)) {
		    $status = $statusSet->get(1);
		}


		$statusHistorySet = $Crm->DealStatusHistorySet();
		$statusHistorySet->status();
		$statusHistorySet->deal();
		$statusHistorySet->deal->addFullNumberField();

		$statusHistorySet->addFields(
//		    $statusHistorySet->deal->amount->round()->setName('sales'),
		    $statusHistorySet->date->left(7)->setName('yearMonth')
		);

		$box = $W->VBoxItems();
		$tableviews = array();
		$maxValue = 0;

		$periodValues = array();
		$periodDeals = array();

		for ($year = $nbYears - 1; $year >= 0; $year--) {
    		$firstOfMonth = $startDate->cloneDate();
    		$firstOfMonth->add(1 - $firstOfMonth->getDayOfMonth(), BAB_DATETIME_DAY);
    		$firstOfMonth->add(-$nbMonths + 1, BAB_DATETIME_MONTH);
    		$firstOfMonth->add(-1 * $year, BAB_DATETIME_YEAR);

    		$monthlyValues = array();
    		$monthlyDeals = array();
    		for ($i = 0; $i < $nbMonths; $i++) {
    			$yearMonth = substr($firstOfMonth->getIsoDate(), 0, 7);
    			$monthlyValues[$yearMonth] = 0;
    			$monthlyDeals[$yearMonth] = array();
    			$firstOfMonth->add(1, BAB_DATETIME_MONTH);
    		}

    		$descendants = $status->getDescendants();

    		$allStatusIds = array($status->id => $status->id);

    		foreach ($descendants as $descendant) {
    			$allStatusIds[$descendant->id] = $descendant->id;
    		}


    		$end = $firstOfMonth->getIsoDate();
    		$firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);
    		$start = $firstOfMonth->getIsoDate();

    		$statusHistories = $statusHistorySet->select(
    			$statusHistorySet->date->greaterThanOrEqual($start)
    				->_AND_($statusHistorySet->date->lessThan($end))
    				->_AND_($statusHistorySet->status->in($allStatusIds))
    		);
    		$statusHistories->orderAsc($statusHistorySet->yearMonth);

    		foreach ($statusHistories as $statusHistory) {
    			$previousStatusHistories = $statusHistorySet->select(
    				$statusHistorySet->deal->id->is($statusHistory->deal->id)
    					->_AND_($statusHistorySet->date->lessThan($statusHistory->date))
    			);
    			$previousStatusHistories->orderDesc($statusHistorySet->date);
    			$previousStatusHistory = null;
    			foreach ($previousStatusHistories as $previousStatusHistory) {
    				break;
    			}
    			if (!isset($previousStatusHistory) || !isset($allStatusIds[$previousStatusHistory->status->id])) {
    				$monthlyValues[$statusHistory->yearMonth] += 1;
    				$monthlyDeals[$statusHistory->yearMonth][$statusHistory->deal->id] = $statusHistory->deal->fullNumber;
    			}
    		}

            foreach ($monthlyValues as $monthlyValue) {
                if ($monthlyValue > $maxValue) {
                    $maxValue = $monthlyValue;
                }
            }

            $periodValues[$year] = $monthlyValues;
            $periodDeals[$year] = $monthlyDeals;
    	}

    	$tableview = $this->tableView($periodValues, $periodDeals);
    	$tableviews[] = $tableview;
    	$box->addItem($tableview);

		foreach ($tableviews as $tableview) {
		    $tableview->setMetadata('verticalScale', $maxValue);
		}

		return $box;
	}





	/**
	 * @return
	 */
	public function amount($nbMonths, $status, $startDate = null, $nbYears = 1)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		if (!isset($startDate)) {
			$startDate = bab_DateTime::now();
		}

		$statusSet = $Crm->StatusSet();
		/* @var $status crm_Status */
		$status = $statusSet->get($status);

		if (!isset($status)) {
		    $status = $statusSet->get(1);
		}

		$statusHistorySet = $Crm->DealStatusHistorySet();
		$statusHistorySet->status();
		$statusHistorySet->deal();
		$statusHistorySet->deal->addFullNumberField();

		$statusHistorySet->addFields(
		    $statusHistorySet->deal->amount->round()->setName('sales'),
		    $statusHistorySet->date->left(7)->setName('yearMonth')
		);

		$box = $W->VBoxItems();
		$tableviews = array();
		$maxValue = 0;

		$periodValues = array();
		$periodDeals = array();

        for ($year = $nbYears - 1; $year >= 0; $year--) {
            $firstOfMonth = $startDate->cloneDate();
            $firstOfMonth->add(1 - $firstOfMonth->getDayOfMonth(), BAB_DATETIME_DAY);
            $firstOfMonth->add(-$nbMonths + 1, BAB_DATETIME_MONTH);
            $firstOfMonth->add(-1 * $year, BAB_DATETIME_YEAR);
            $monthlyValues = array();
            $monthlyDeals = array();
            for ($i = 0; $i < $nbMonths; $i++) {
                $yearMonth = substr($firstOfMonth->getIsoDate(), 0, 7);
                $monthlyValues[$yearMonth] = 0;
                $monthlyDeals[$yearMonth] = array();
                $firstOfMonth->add(1, BAB_DATETIME_MONTH);
            }

            $descendants = $status->getDescendants();

            $allStatusIds = array($status->id => $status->id);

            foreach ($descendants as $descendant) {
                $allStatusIds[$descendant->id] = $descendant->id;
            }

            $end = $firstOfMonth->getIsoDate();
            $firstOfMonth->add(-$nbMonths, BAB_DATETIME_MONTH);
            $start = $firstOfMonth->getIsoDate();

            $statusHistories = $statusHistorySet->select(
                $statusHistorySet->date->greaterThanOrEqual($start)
                    ->_AND_($statusHistorySet->date->lessThan($end))
                    ->_AND_($statusHistorySet->status->in($allStatusIds))
            );
            $statusHistories->orderAsc($statusHistorySet->yearMonth);

            foreach ($statusHistories as $statusHistory) {
                $previousStatusHistories = $statusHistorySet->select(
                    $statusHistorySet->deal->id->is($statusHistory->deal->id)
                    ->_AND_($statusHistorySet->date->lessThan($statusHistory->date))
                );
                $previousStatusHistories->orderDesc($statusHistorySet->date);

                $previousStatusHistory = null;
                foreach ($previousStatusHistories as $previousStatusHistory) {
                    break;
                }
                if (!isset($previousStatusHistory) || !isset($allStatusIds[$previousStatusHistory->status->id])) {
                    $monthlyValues[$statusHistory->yearMonth] += $statusHistory->sales;
                    $monthlyDeals[$statusHistory->yearMonth][$statusHistory->deal->id] = $statusHistory->deal->fullNumber;
                }
            }

            foreach ($monthlyValues as $monthlyValue) {
                if ($monthlyValue > $maxValue) {
                    $maxValue = $monthlyValue;
                }
            }

            $periodValues[$year] = $monthlyValues;
            $periodDeals[$year] = $monthlyDeals;
    	}

    	$tableview = $this->tableView($periodValues, $periodDeals);
    	$tableviews[] = $tableview;
    	$box->addItem($tableview);

		foreach ($tableviews as $tableview) {
		    $tableview->setMetadata('verticalScale', $maxValue);
		}

		return $box;
	}










	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		if (empty($this->options['mode'])) {
			$this->options['mode'] = 'sales_volume';
		}
		if (empty($this->options['nbMonths'])) {
			$this->options['nbMonths'] = 12;
		}
		if (empty($this->options['status'])) {
			$this->options['status'] = 9;
		}
		if (empty($this->options['previousYear'])) {
			$this->options['previousYear'] = false;
		}
		if (empty($this->options['start'])) {
			$start = date('Y-m-d');
		} else {
			$start = ORM_DateField('')->input($this->options['start']);
		}

		$start = BAB_DateTime::fromIsoDateTime($start . ' 00:00:00');

		if ($this->options['previousYear']) {
		    $nbYears = 2;
		} else {
		    $nbYears = 1;
		}
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$statusName = '';

		$statusSet = $Crm->StatusSet();
		/* @var $status crm_Status */
		if ($status = $statusSet->get($this->options['status'])) {
		    $statusName = $status->name;
		}

		switch ($this->options['mode'])
		{
			case 'sales_volume':
				$this->addItem($W->Title(sprintf($Crm->translate('Number of deals entering status %s during last %d months'), $statusName, $this->options['nbMonths']), 4));
				$this->addItem($this->volume($this->options['nbMonths'], $this->options['status'], $start, $nbYears));
				break;

			case 'sales_amount':
				$this->addItem($W->Title(sprintf($Crm->translate('Amount of deals entering status %s during last %d months'), $statusName, $this->options['nbMonths']), 4));
				$this->addItem($this->amount($this->options['nbMonths'], $this->options['status'], $start, $nbYears));
				break;
		}

		return parent::display($canvas);
	}
}
