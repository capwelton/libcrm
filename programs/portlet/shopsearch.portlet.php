<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */





class crm_PortletDefinition_ShopSearch extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('Online shop search keywords (%s)'), $Crm->getAddonName());
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Online shop search best keywords with related number of results');
	}

	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();

		return  array(
				array('label' => $Crm->translate('Number of search keywords'), 'type' => 'text', 'name' => 'limit')
		);
	}
}




class crm_PortletUi_ShopSearch extends crm_PortletUi implements portlet_PortletInterface
{

	/**
	 * @param Func_Crm $Crm
	 */
	public function __construct(Func_Crm $Crm)
	{
		$W = bab_Widgets();
		parent::__construct($Crm);
		$this->setInheritedItem($W->Frame());




	}

	/**
	 * @return
	 */
	public function keywords()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();

		$tableView = $Ui->OrderTableView();
		
		if (!$Crm->SearchHistory()) {
		    return null;
		}

		$set = $Crm->SearchHistorySet();
		$set->addFields(
			$set->id->count()->setName('usage')		
		);
		
		$tableView->addColumn(widget_TableModelViewColumn($set->keyword, $Crm->translate('Keywords')));
		$tableView->addColumn(widget_TableModelViewColumn($set->usage, $Crm->translate('Usages')));
		$tableView->addColumn(widget_TableModelViewColumn($set->results, $Crm->translate('Number of results')));
		$res = $set->select($set->keyword->is('')->_NOT());
		$res->groupBy($set->keyword);
		$res->orderDesc($set->usage);

		$tableView->setDataSource($res);
		$tableView->setLimit($this->options['limit']);

		return $tableView;
	}




	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{

		if (empty($this->options['limit']))
		{
			$this->options['limit'] = 10;
		}

		$Crm = $this->Crm();
		$W = bab_Widgets();

		$this->addItem($W->Title(sprintf($Crm->translate('The %d best search keywords'), $this->options['limit']), 4));
		$this->addItem($this->keywords());


		return parent::display($canvas);
	}
}