<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */





class crm_Portlet extends crm_Object
{
	public function includeBase()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'base.portlet.php';
	}


	/**
	 * @param string $category
	 * @return portlet_PortletDefinitionInterface[]
	 */
	public function select($category = null)
	{
		$return = array();

		$Crm = $this->Crm();


		if (isset($Crm->Task) && (null === $category))
		{
			$mytasks = $this->MyTasks();
			$return[$mytasks->getId()] = $mytasks;
		}


		if ($Crm->onlineShop && (null === $category || $category === $Crm->translate('Online shop statistics')))
		{
			$shopContact = $this->ShopContact();
			$return[$shopContact->getId()] = $shopContact;

			$shopOrder = $this->ShopOrder();
			$return[$shopOrder->getId()] = $shopOrder;

			$shopOrderItem = $this->ShopOrderItem();
			$return[$shopOrderItem->getId()] = $shopOrderItem;

			$shopSale = $this->ShopSale();
			$return[$shopSale->getId()] = $shopSale;

			$shopSearch = $this->ShopSearch();
			$return[$shopSearch->getId()] = $shopSearch;
		}


		$mailbox = $this->MailBox();
		$return[$mailbox->getId()] = $mailbox;


		return $return;
	}



	public function includeRecentActivity()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'recentactivity.portlet.php';
	}


	public function RecentActivity()
	{
		$this->includeRecentActivity();
		return new crm_PortletDefinition_RecentActivity($this->Crm());
	}




	public function includeDealsSummary()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'dealssummary.portlet.php';
	}


	public function DealsSummary()
	{
		$this->includeDealsSummary();
		return new crm_PortletDefinition_DealsSummary($this->Crm());
	}



	public function includeTimeline()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'timeline.portlet.php';
	}


	public function Timeline()
	{
		$this->includeTimeline();
		return new crm_PortletDefinition_Timeline($this->Crm());
	}



	public function includeDealsTurnover()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'dealsturnover.portlet.php';
	}


	public function DealsTurnover()
	{
		$this->includeDealsTurnover();
		return new crm_PortletDefinition_DealsTurnover($this->Crm());
	}



	public function includeMyTasks()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'mytasks.portlet.php';
	}


	public function MyTasks()
	{
		$this->includeMyTasks();
		return new crm_PortletDefinition_MyTasks($this->Crm());
	}



	public function includeShopContact()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'shopcontact.portlet.php';
	}

	/**
	 * Portlet with a list of contacts from online shop
	 */
	public function ShopContact()
	{
		$this->includeShopContact();
		return new crm_PortletDefinition_ShopContact($this->Crm());
	}



	public function includeShopOrder()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'shoporder.portlet.php';
	}

	/**
	 * Portlet with a list of orders from online shop
	 */
	public function ShopOrder()
	{
		$this->includeShopOrder();
		return new crm_PortletDefinition_ShopOrder($this->Crm());
	}



	public function includeShopOrderItem()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'shoporderitem.portlet.php';
	}

	public function ShopOrderItem()
	{
		$this->includeShopOrderItem();
		return new crm_PortletDefinition_ShopOrderItem($this->Crm());
	}



	public function includeShopSale()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'shopsale.portlet.php';
	}

	public function ShopSale()
	{
		$this->includeShopSale();
		return new crm_PortletDefinition_ShopSale($this->Crm());
	}



	public function includeShopSearch()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'shopsearch.portlet.php';
	}

	public function ShopSearch()
	{
		$this->includeShopSearch();
		return new crm_PortletDefinition_ShopSearch($this->Crm());
	}


	public function includeMailBox()
	{
		require_once FUNC_CRM_PORTLET_PATH . 'mailbox.portlet.php';
	}


	public function MailBox()
	{
		$this->includeMailBox();
		return new crm_PortletDefinition_MailBox($this->Crm());
	}


}