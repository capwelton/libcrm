<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */





class crm_PortletDefinition_ShopOrderItem extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('Online shop sold products (%s)'), $Crm->getAddonName());
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Online shop sold products, with configurable number of products');
	}

	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();

		return  array(
				array('label' => $Crm->translate('Number of products'), 'type' => 'text', 'name' => 'limit')
		);
	}
}




class crm_PortletUi_ShopOrderItem extends crm_PortletUi implements portlet_PortletInterface
{

	/**
	 * @param Func_Crm $Crm
	 */
	public function __construct(Func_Crm $Crm)
	{
		$W = bab_Widgets();
		parent::__construct($Crm);
		$this->setInheritedItem($W->Frame());




	}

	/**
	 * @return
	 */
	public function products()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();

		$tableView = $Ui->OrderTableView();

		$set = $Crm->OrderItemSet();

		$set->addFields(
			$set->quantity->sum()->round()->setName('sales')
		);
		
		$tableView->addColumn(widget_TableModelViewColumn($set->reference, $Crm->translate('Reference')));
		$tableView->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
		$tableView->addColumn(widget_TableModelViewColumn($set->sales, $Crm->translate('Sales')));
		$res = $set->select();
		$res->groupBy($set->reference);
		$res->orderDesc($set->sales);

		$tableView->setDataSource($res);
		$tableView->setLimit($this->options['limit']);

		return $tableView;
	}




	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{

		if (empty($this->options['limit']))
		{
			$this->options['limit'] = 10;
		}

		$Crm = $this->Crm();
		$W = bab_Widgets();

		$this->addItem($W->Title(sprintf($Crm->translate('The %d best sold products'), $this->options['limit']), 4));
		$this->addItem($this->products());
		
		if ($Crm->Access()->listOrderItem())
		{
			$this->addItem($W->Link($Crm->translate('View all sold products'), $Crm->Controller()->OrderItem()->displayList())->addClass('crm-dialog-button'));
		}

		return parent::display($canvas);
	}
}