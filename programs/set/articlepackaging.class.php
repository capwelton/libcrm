<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/shoppingcartitemsource.class.php';

/**
 * ArticlePackaging unit used of sellable items (crm_Article)
 *
 * @property	ORM_IntField		$id
 * @property 	ORM_Decimalfield	$weight
 * @property	ORM_DecimalField	$unit_cost
 * @property 	ORM_DecimalField	$reduction
 * @property	ORM_DateField		$reduction_end
 * @property	ORM_BoolField		$main
 * @property	crm_ArticleSet		$article
 * @property	crm_PackagingSet	$packaging
 * 
 * @method crm_ArticlePackaging get()
 * @method crm_ArticlePackaging newRecord()
 * @method crm_ArticlePackaging[] select()
 */
class crm_ArticlePackagingSet extends crm_RecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);
	
		$this->setDescription('Article packaging');
		$this->setPrimaryKey('id');
		
		
		$this->addFields(
				
			ORM_Decimalfield('weight', 4)
				->setDescription('Weight in Kg with packaging'),
		
			ORM_DecimalField('unit_cost', 3)
				->setDescription('price tax excl.'),
		
			ORM_DecimalField('reduction', 2)
				->setDescription('%'),
		
			ORM_DateField('reduction_end')
				->setDescription('the reduction apply until the date'),
				
			ORM_BoolField('main')
				
		);
	
		$this->hasOne('article', $Crm->ArticleSetClassName());
		$this->hasOne('packaging', $Crm->PackagingSetClassName());
	}
	
	
	public function save(ORM_Record $record)
	{
		if (1 === (int) $record->main)
		{
			$article = $record->article();	
		}
		
		$return = parent::save($record);
		
		if (isset($article) && $article->mainpackaging != $record->id)
		{
			$article->mainpackaging = $record->id;
			$article->save(true);
		}
		
		return $return;
	}
	
	
	public function delete(ORM_Criteria $criteria = null)
	{
		$removedMainArticlePackaging = $this->get($criteria->_AND_($this->main->is(1)));
		
		
		// if the main packaging is removed, set the main flag on another

		
		$return = parent::delete($criteria);
		
		if (null !== $removedMainArticlePackaging)
		{
			if ($removedMainArticlePackaging->article instanceof crm_Article)
			{
				$otherPackagingCriteria = $this->article->id->is($removedMainArticlePackaging->article->id);
			} else {
				$otherPackagingCriteria = $this->article->is($removedMainArticlePackaging->article);
			}
		
			$res = $this->select($otherPackagingCriteria);
			foreach($res as $articlePackaging)
			{
				$articlePackaging->main = 1;
				$articlePackaging->save();
				break;
			}
		}
		
		return $return;
	}

}

/**
 * @property	int				$id
 * @property 	float			$weight
 * @property	float			$unit_cost
 * @property 	float			$reduction
 * @property	string			$reduction_end
 * @property	int				$main
 * @property	crm_Article		$article
 * @property	crm_Packaging	$packaging
 *
 */
class crm_ArticlePackaging extends crm_Record implements crm_ShoppingCartItemSourceInterface 
{
	/**
	 * Unit cost displayed on product page
	 * the cost is tax excluded without reduction
	 * @return float
	 */
	public function getRawUnitCostDF()
	{
		return (float) $this->unit_cost;
	}
	
	/**
	 * Get reduction amount for this product
	 * positive value
	 * @return float
	 */
	public function getReduction()
	{
		$reduction = $this->getReductionPercentage();
	
		if (null === $reduction)
		{
			return 0.0;
		}
	
		return (($reduction/100) * $this->getRawUnitCostDF());
	}
	
	/**
	 * get reduction in percentage on the product
	 * @return float | null
	 */
	public function getReductionPercentage()
	{
		if ('0.00' === $this->reduction)
		{
			return null;
		}
	
		if ('0000-00-00' !== $this->reduction_end && $this->reduction_end < date('Y-m-d'))
		{
			return null;
		}
	
		return (float) $this->reduction;
	}
	
	
	/**
	 * Unit cost displayed on product page
	 * the cost is tax excluded with reduction, this cost will be duplicated in shopping cart Item
	 * @return float
	 */
	public function getUnitCostDF()
	{
		return ($this->getRawUnitCostDF() - $this->getReduction());
	}
	
	
	/**
	 * Unit cost without reduction displayed on product page, this is the unavailable price if a reduction exists on product
	 * the cost is without reduction and tax included
	 * @return float
	 */
	public function getRawUnitCostTI()
	{
		return ($this->getRawUnitCostDF() + $this->getRawVat());
	}
	
	
	/**
	 * Unit cost displayed on product page, the real product cost for customer
	 * the cost is with reduction and tax included
	 * @return float
	 */
	public function getUnitCostTI()
	{
		return ($this->getUnitCostDF() + $this->getVat());
	}
	
	/**
	 * default quantity to use when the product is added to shopping cart
	 * @return float
	 */
	public function getDefaultQuantity()
	{
		return 1.0;
	}
	
	/**
	 * Return VAT rate
	 * @example 0.196
	 * @return float
	 */
	public function getVatRate()
	{
		$Crm = $this->Crm();
		
		$Crm->includeVatSet();
		$vat = $this->article->vat();
		
		if (!($vat instanceof crm_Vat))
		{
			return 0;
		}
	
		if ($vat->id)
		{
			// use the article VAT if defined
	
			$vat = (float) $vat->value;
			return ($vat /100);
		}

		return 0;
	}
	
	/**
	 * Get raw VAT amount for this product, computed on price without reduction
	 * return float
	 */
	public function getRawVat()
	{
		return ($this->getRawUnitCostDF() * $this->getVatRate());
	}
	
	/**
	 * Get VAT amount for this product
	 * return float
	 */
	public function getVat()
	{
	
		return ($this->getUnitCostDF() * $this->getVatRate());
	}
	
	
	/**
	 * Get packaging name
	 * @return string
	 */
	public function getPackagingName()
	{
		return $this->packaging->name;
	}
	
	
	
	/**
	 * Update properties with CSV row
	 * @param	Widget_CsvRow	$row	A CSV row
	 * @return 	int						number of updated properties
	 */
	public function import(Widget_CsvRow $row)
	{
		$up_prop = 0;

	
		if (isset($row->packaging)) {
			if (is_numeric($row->packaging)) {
	
				$up_prop += $this->importProperty('packaging', $row->packaging);
	
			} else {
	
				$PSet = $this->Crm()->PackagingSet();
				$packaging = $PSet->get($PSet->name->like($row->packaging));
	
				if (isset($packaging)) {
					$up_prop += $this->importProperty('packaging', $packaging->id);
				} else {
					// no packaging specified
				}
			}
		}
	
		if (isset($row->weight)) {
			$up_prop += $this->importProperty('weight', str_replace(',', '.', $row->weight));
		}
		
		if (isset($row->unit_cost)) {
			$up_prop += $this->importProperty('unit_cost', str_replace(',', '.', $row->unit_cost));
		}
		
		if (isset($row->reduction)) {
			$up_prop += $this->importProperty('reduction', str_replace(',', '.', $row->reduction));
		}
		
		if (isset($row->reduction_end)) {
			$up_prop += $this->importDate('reduction_end', $row->reduction_end);
		}
		
		if (isset($row->main)) {
			$up_prop += $this->importProperty('main', $row->main);
		}
		
	
		return $up_prop;
	}
	
}