<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/shoppingcartitemsource.class.php';



/**
 * A crm_Catalog is a catalog of items.
 *
 * @property crm_CatalogSet	$catalog
 * @property crm_ArticleSet 	$article
 */
class crm_CatalogItemSet extends crm_RecordSet
{

	public function __construct($crm = null)
	{
		parent::__construct($crm);

		$this->setDescription('Catalog product');
		$this->setPrimaryKey('id');


		$this->hasOne('article', $this->Crm()->ArticleSetClassName());
		$this->hasOne('catalog', $this->Crm()->CatalogSetClassName());
	}


	/**
	 * Get access rights verification default criteria for catalog items
	 * @return ORM_Criteria
	 */
	public function getAccessibleItems()
	{
		return $this->catalog->getAccessibleCatalogs()->_AND_($this->article->disabled->is(0));
	}
	
	
	/**
	 * Get criteria from search array
	 * 
	 * @see crm_CtrlCatalogItem::displayList()
	 * @see crm_SearchCatalog::getVisibleArticles()
	 * 
	 * @param	array	$search
	 * 
	 * @return ORM_Criteria
	 */
	public function getSearchCriteria(Array $search)
	{
		$Crm = $this->Crm();
		
		// base criteria for accessibles articles
		
		$criteria = $this->getAccessibleItems();
		
		$set = $this;
		
		// filter criteria
		
		if (!empty($search['keywords']))
		{
			$criteria = $criteria->_AND_(
					$this->article->reference->matchAll($search['keywords'])
					->_OR_($this->article->name->matchAll($search['keywords']))
					->_OR_($this->article->subtitle->matchAll($search['keywords']))
					->_OR_($this->article->description->matchAll($search['keywords']))
			);
		}
		
		if (!empty($search['catalog']))
		{
			$criteria = $criteria->_AND_(
					$set->catalog->id->is($search['catalog'])
			);
		}
		
		if (!empty($search['reduction']))
		{
			$subset = $Crm->ArticlePackagingSet();
			$subcriteria = $subset->reduction->greaterThan(0)
			->_AND_($subset->reduction_end->greaterThanOrEqual(date('Y-m-d'))->_OR_($subset->reduction_end->is('0000-00-00')))
			->_AND_($subset->main->is(1));
	
			$criteria = $criteria->_AND_($set->article->id->in($subcriteria));
		}
		
		return $criteria;
	}
}


/**
 * A crm_CatalogItem is an item from a catalog
 * linked to an article, the catalog item add access rights verification and pricing control to the main collection of articles
 *
 * 
 * @property crm_Catalog	$catalog
 * @property crm_Article	$article
 */
class crm_CatalogItem extends crm_Record 
{

	/**
	 * default quantity when the catalog item is added into an order
	 * method used by manager to add item in an order
	 * @return float
	 */
	public function getOrderDefaultQuantity(crm_Order $order)
	{
		return 1.0;
	}

	/**
	 * default unit cost when the catalog item is added into an order
	 * method used by manager to add item in an order
	 * @return float
	 */
	public function getOrderDefaultUnitCost(crm_Order $order)
	{
		return 0.0;
	}

	/**
	 * default VAT when the catalog item is added into an order
	 * method used by manager to add item in an order
	 * @return float
	 */
	public function getOrderDefaultVat(crm_Order $order)
	{
		return 0.0;
	}



	/**
	 * Get similar catalog item iterator
	 *
	 * @return ORM_Iterator		<crm_CatalogItem>
	 */
	public function similarCatalogItems()
	{
		return null;
	}
	
	
	/**
	 * @return string
	 */
	public function getPageTitle()
	{
		if ($this->article->page_title)
		{
			return $this->article->page_title;
		}
		
		return $this->article->name;
	}
	
	/**
	 * @return string
	 */
	public function getPageDescription()
	{
		if ($this->article->page_description)
		{
			return $this->article->page_description;
		}
		
		return bab_abbr($this->article->getShortDesc(), BAB_ABBR_FULL_WORDS, 400);
	}
	
	/**
	 * @return string
	 */
	public function getPageKeywords()
	{
		return $this->article->page_keywords;
	}
	
	
	
	
	/**
	 * get url to use in front office, contain the domain name
	 * @return string
	 */
	public function getRewritenUrl()
	{
		$Crm = $this->Crm();
		
		
		if (!($this->article instanceof crm_Article))
		{
			throw new ErrorException('missing join on article');
		}
		
		if (!$Crm->rewriting || empty($this->article->name))
		{
			return $GLOBALS['babUrl'].$Crm->Controller()->CatalogItem()->display($this->id)->url();
		}
	
		if (empty($this->article->rewritename))
		{
			$this->article->rewritename = crm_getRewriteName($this->article->name);
			$this->article->save(true);
		}
		
		return $GLOBALS['babUrl'].'Shop/'.$this->article->rewritename;
	
	}

}