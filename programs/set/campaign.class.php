<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */




class crm_CampaignSet extends crm_TraceableRecordSet {

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Campaign');
		$this->setPrimaryKey('id');

		$options = array();


		foreach($this->getAllTypes() as $type) {
			$options[$type] = $this->getFromType($type)->getDescription();
		}


		$this->addFields(
			ORM_StringField('name')
					->setDescription($Crm->translate('Name')),
			ORM_TextField('description')
					->setDescription($Crm->translate('Description')),
			ORM_EnumField('type', $options)
					->setDescription($Crm->translate('Campaign type')),
			ORM_DateField('period_begin')
					->setDescription($this->Crm()->translate('Begin')),
			ORM_DateField('period_end')
					->setDescription($Crm->translate('End'))
		);
	}

	/**
	 * Get list of available campaign types
	 *
	 * @return array
	 */
	public function getAllTypes()
	{
		return array(
 			'Mailing',
 			'Phoning'
		);
	}



	/**
	 * Get campaign type object from string
	 * @param	string		$type
	 * @return	crm_CampaignType
	 */
	public function getFromType($type)
	{
		$classname = 'crm_CampaignType'.$type;

		switch($classname) {
			case 'crm_CampaignTypeMailing':
				require_once dirname(__FILE__).'/mailing.php';
				break;

			case 'crm_CampaignTypePhoning':
				require_once dirname(__FILE__).'/phoning.php';
				break;

			default:
				throw new Exception('Unsupported campaign type '.$classname);
				return null;
		}

		$obj = new $classname($this->Crm());
//		$obj->setCrm($this->Crm());

		return $obj;
	}

}






/**
 *
 * @author paul
 *
 */
class crm_Campaign extends crm_TraceableRecord {

	/**
	 * Get campaign type object
	 * @return	crm_CampaignType
	 */
	public function getFromType()
	{
		return $this->getParentSet()->getFromType($this->type);
	}
}
