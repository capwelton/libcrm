<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * A class used to store emblems.
 *
 * @property	ORM_StringField	$icon			The emblem icon name (@see Func_Icons)
 * @property	ORM_BoolField	$description	The emblem short description
 */
class crm_EmblemSet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->addFields(

			ORM_StringField('icon')
				->setDescription('Emblem icon'),
			ORM_StringField('label')
				->setDescription('Emblem label'),
			ORM_StringField('description')
				->setDescription('Emblem description')
		);

	}





	/**
	 * Returns an iterator of emblems associated to the specified object,
	 * optionally filtered on the specified link type.
	 *
	 * @return ORM_Iterator
	 */
	public function selectFor(crm_Record $object)
	{
		return parent::selectLinkedTo($object, 'hasEmblem');
	}





	/**
	 * Replaces all links to $oldEmblemId by links to $newEmblemId.
	 *
	 * @return crm_emblemSet
	 */
	public function replace($oldEmblemId, $newEmblemId, $linkType = 'hasEmblem')
	{
		$Crm = $this->Crm();
		$linkSet = $Crm->LinkSet();

		$links = $linkSet->select(
			$linkSet->targetClass->is($Crm->EmblemClassName())
			->_AND_($linkSet->targetId->is($oldEmblemId))
			->_AND_($linkSet->type->is($linkType))
		);

		foreach ($links as $link) {
			$link->targetId = $newEmblemId;
			$link->save();
		}

		return $this;
	}
}






/**
 * @property	string	$icon			The emblem icon name (@see Func_Icons)
 * @property	string	$description	The emblem short description
 */
class crm_Emblem extends crm_TraceableRecord
{

	/**
	 * Adds this emblem to the specified crm object.
	 * 
	 * @param crm_Record $object
	 *
	 * @return crm_Emblem
	 */
	public function linkTo(crm_Record $object, $linkType = '')
	{
		return parent::linkTo($object, 'hasEmblem');
	}





	/**
	 * Removes this emblem from the specified crm object.
	 * 
	 * @param crm_Record $object
	 *
	 * @return crm_Emblem
	 */
	public function unlinkFrom(crm_Record $object, $linkType = null)
	{
		return parent::unlinkFrom($object, 'hasEmblem');
	}





	/**
	 * Replaces all links with this emblem by links to the one specified.
	 *
	 * @param crm_Emblem|int	$emblem		An emblem object or id.
	 *
	 * @return crm_Emblem
	 */
	public function replaceBy($emblem)
	{
		if ($emblem instanceof crm_Emblem) {
			$emblem = $emblem->id;
		}
		$this->getParentSet()->replace($this->id, $emblem);
		return $this;
	}

}
