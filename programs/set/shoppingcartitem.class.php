<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A shopping cart item set
 *
 * @property ORM_StringField			$name
 * @property ORM_StringField			$subtitle
 * @property ORM_TextField				$description
 * @property ORM_StringField			$packaging
 * @property ORM_DecimalField			$quantity
 * @property ORM_DecimalField			$unit_cost
 * @property ORM_DecimalField			$reduction
 * @property ORM_DecimalField			$vat
 * @property ORM_BoolField				$giftcard
 * @property crm_ShoppingCartSet		$cart					The parent shopping cart
 * @property crm_CatalogItemSet		$catalogitem			The source article in a catalog
 * @property crm_ArticlePackagingSet	$articlepackaging
 */
class crm_ShoppingCartItemSet extends crm_RecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Product in shopping cart');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name of product'),
			ORM_StringField('subtitle')
					->setDescription('Product subtitle'),
			ORM_TextField('description')
					->setDescription('Description of product'),
			ORM_StringField('packaging')
					->setDescription('Packaging name'),
			ORM_DecimalField('quantity', 4)
					->setDescription('Quantity'),
			ORM_DecimalField('unit_cost', 4)
					->setDescription('Unit cost tax excl, reduction incl'),
			ORM_DecimalField('reduction', 2)
					->setDescription('percentage, not used in amount, allready included in unit cost'),
			ORM_DecimalField('vat', 2)
					->setDescription('VAT %'),
			ORM_BoolField('giftcard')
					->setDescription('This product is a gift card, a coupon will be generated after payment')
		);

		$this->hasOne('cart', $Crm->ShoppingCartSetClassName());
		$this->hasOne('catalogitem', $Crm->CatalogItemSetClassName());
		$this->hasOne('articlepackaging', $Crm->ArticlePackagingSetClassName());
	}


	public function delete(ORM_Criteria $criteria = null)
	{
		foreach($this->select($criteria) as $itemRecord) {
			$itemRecord->onBeforeDelete();
		}

		return parent::delete($criteria);
	}
	
	
	/**
	 * Select the schopping cart item in shopping cart from the shopping source
	 * @param	int										$cartId
	 * @param	crm_ShoppingCartItemSourceInterface 	$shoppingSource
	 * @return ORM_Iterator
	 */
	public function selectFromSource($cartId, crm_ShoppingCartItemSourceInterface $shoppingSource)
	{
		if ($shoppingSource instanceof crm_ArticlePackaging)
		{
			return $this->select($this->cart->is($cartId)->_AND_($this->articlepackaging->is($shoppingSource->id)));
		}
		
		if ($shoppingSource instanceof crm_CatalogItem)
		{
			return $this->select($this->cart->is($cartId)->_AND_($this->catalogitem->is($shoppingSource->id)));
		}
		
		throw new ErrorException(sprintf('the shoppingSource %s is not implemented', get_class($shoppingSource)));
	}

}


/**
 * A shopping cart item
 *
 * @property string					$name
 * @property string					$subtitle
 * @property string					$description
 * @property string					$packaging
 * @property float						$quantity
 * @property float						$unit_cost
 * @property float						$reduction
 * @property float						$vat		
 * @property int						$giftcard
 * @property crm_ShoppingCart			$cart
 * @property crm_CatalogItem			$catalogitem
 * @property crm_ArticlePackaging		$articlepackaging
 */
class crm_ShoppingCartItem extends crm_Record
{
    
    
    /**
     * Return true if the shopping cart item accept decimals for the quantity field
     * @return boolean
     */
    public function canOrderWithDecimals()
    {
        return false;
    }
    

	/**
	 * this method is called before a shopping cart item delete
	 */
	public function onBeforeDelete()
	{
		$this->deleteAttachements();
	}



	/**
	 * Delete the attachements files
	 * return null if there are no attachements
	 *
	 * @throw bab_FolderAccessRightsException
	 *
	 * @return bool
	 */
	public function deleteAttachements()
	{
		$attachements = $this->getAttachements();
		if ($attachements instanceOf bab_Path) {
			return $attachements->deleteDir();
		}

		return null;
	}

	/**
	 * Get attachement in a folder
	 * return null if there are no attachements
	 * @return bab_Path
	 */
	protected function getAttachements()
	{
		return null;
	}
	
	/**
	 * get unit cost as float, as stored in shopping cart item
	 * the price is tax excluded, reduction included
	 * 
	 * @return float
	 */
	public function getUnitCostDF()
	{
		return (float) $this->unit_cost;
	}
	
	/**
	 * get unit cost as float, as stored in shopping cart item
	 * the price is tax included, reduction included
	 *
	 * @return float
	 */
	public function getUnitCostTI()
	{
		$df = $this->getUnitCostDF();
		$taxes = ($df * ($this->vat / 100));
		
		return $df + $taxes;
	}
	
	
	/**
	 * Get the real unit cost as displayed on product
	 * the price is tax excluded, reduction included
	 * 
	 * @return float;
	 */
	public function getRealUnitCostDF()
	{
		if (!($this->catalogitem instanceof crm_CatalogItem))
		{
			throw new Exception('Missing join on catalogItem');
		}
		
		if (!$this->catalogitem->id)
		{
			return null;
		}
		
		if (!($this->articlepackaging instanceof crm_ArticlePackaging))
		{
			throw new Exception('Missing join on articlePackaging');
		}
		
		if (!$this->articlepackaging->id)
		{
			return null;
		}
		
		
		if ($this->catalogitem->article instanceof crm_Article)
		{
			$id_article1 = $this->catalogitem->article->id;
		} else {
			$id_article1 = $this->catalogitem->article;
		}
		
		
		if ($this->articlepackaging->article instanceof crm_Article)
		{
			$id_article2 = $this->articlepackaging->article->id;
		} else {
			$id_article2 = $this->articlepackaging->article;
		}
		
		if ($id_article1 !== $id_article2)
		{
			bab_debug('the packaging recorded in shopping cart item does not match the real article');
			return null;
		}
		
		return $this->articlepackaging->getUnitCostDF();
	}
	


	/**
	 * Get total tax excluded, reduction included
	 * @return float
	 */
	public function getTotalDF()
	{
		return ($this->unit_cost * $this->quantity);
	}

	/**
	 * Get total taxes included, reduction included
	 * @return float
	 */
	public function getTotalTI()
	{
		$df = $this->getTotalDF();
		$taxes = ($df * ($this->vat / 100));

		return $df + $taxes;
	}
	

	
	/**
	 * Update the unit cost and VAT from article packaging if price has been modified from shoppping cart values
	 * return true if the price has been modified
	 *
	 * @return bool
	 */
	public function updatePriceFromArticlePackaging()
	{
		$Crm = $this->Crm();
		$articlePackaging = $this->articlepackaging;
		
		if (!($articlePackaging instanceof crm_ArticlePackaging))
		{
			throw new ErrorException('Missing join on articlepackaging');
		}
		
		if (empty($articlePackaging->article->id) || empty($this->catalogitem->article->id))
		{
			// the item is not linked to a product in database (a gift card?)
			return true;
		}
		
		if ($articlePackaging->article->id !== $this->catalogitem->article->id)
		{
			throw new ErrorException('Error in Article packaging linked to shopping cart item');
		}
		
		$sc_unit_cost = (int) round((float) $this->unit_cost * 10000);
		$real_unit_cost = (int) round($articlePackaging->getUnitCostDF() * 10000);
		
		$sc_vat = (int) round((float) $this->vat * 10000);
		$real_vat = (int) round($articlePackaging->getVatRate() * 1000000);
		
		if ($sc_unit_cost === $real_unit_cost && $sc_vat === $real_vat)
		{
			// the shopping cart item is up-to-date
			return false;
		}
		
		$this->unit_cost = $articlePackaging->getUnitCostDF();
		$this->vat = ($articlePackaging->getVatRate() * 100);
		
		return $this->save();
	}
	
	
	/**
	 * Test if the product is shippable (need a shipping cost)
	 * @return boolean
	 */
	public function isShippable()
	{
		if ($this->giftcard)
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * Get reference used for creating an order item from the shopping cart item
	 * @return string
	 */
	public function getReference()
	{
		if ($this->giftcard)
		{
			return 'GIFTCARD';
		}
		
		return $this->catalogitem->article->reference;
	}

	
	
	
	/**
	 * Create order item after payment
	 * @see crm_ShoppingCart::createOrder()
	 */
	public function createOrderItem(crm_OrderItemSet $oiSet, crm_Order $order, $sortkey)
	{
		$Crm = $this->Crm();
		
		if (!($this->catalogitem instanceof crm_CatalogItem)) {
			throw new crm_Exception('missing join on catalogItem');
		}
		
		if (!$this->catalogitem->id)
		{
			$catalogitem = 0; // the entry in shopping cart is not an item in catalog
		} else {
			$catalogitem = $this->catalogitem->id;
		}
		
		
		if (!($this->articlepackaging instanceof crm_ArticlePackaging))
		{
			throw new crm_Exception('missing join on articlepackaging');
		}
		
		
		if (!$this->articlepackaging->id)
		{
			$articlepackaging = 0; // the entry in shopping cart is not an item in catalog
		} else {
			$articlepackaging = $this->articlepackaging->id;
		}
		
		/*@var $orderItem crm_OrderItem */
		$orderItem = $oiSet->newRecord();
		$orderItem->parentorder			= $order->id;
		$orderItem->reference			= $this->getReference();
		$orderItem->name				= $this->name;
		$orderItem->subtitle			= $this->subtitle;
		$orderItem->description			= $this->description;
		$orderItem->quantity			= $this->quantity;
		$orderItem->unit_cost			= $this->unit_cost;
		$orderItem->reduction			= $this->reduction;
		$orderItem->vat					= $this->vat;
		$orderItem->catalogitem			= $catalogitem;
		$orderItem->articlepackaging	= $articlepackaging;
		$orderItem->packaging			= $this->packaging;
		$orderItem->sortkey				= $sortkey;
		$orderItem->save();
		
		// generate a giftcard if necessary
		
		if ($this->giftcard)
		{
			
			static $giftcard_validity = null;
			
			if (null === $giftcard_validity)
			{
				$addonname = $Crm->getAddonName();
				$registry = bab_getRegistryInstance();
				$registry->changeDirectory("/$addonname/configuration/");
				$giftcard_validity = $registry->getValue('giftcard_validity', 60);
			}
			
			$cSet = $Crm->CouponSet();
			$coupon = $cSet->createRandom($giftcard_validity);
			$coupon->description = sprintf($Crm->translate('Gift cards offered by %s'), $order->billingperson);
			$coupon->value = $orderItem->getTotalTI();
			$coupon->orderitem = $orderItem->id;
			$coupon->unique = 1;
			$coupon->save();
		}
		
		
		return $orderItem;
	}
	
	/**
	 * Get displayable quantity
	 * @return string
	 */
	public function getDispQuantity()
	{
	    
	    
	    $qte = rtrim($this->quantity, '0');
	    if ('.' === substr($qte, -1)) {
	        $qte = rtrim($qte, '.');

	        return $qte;
	    }
	    
	    if ('en' === bab_getLanguage()) {
	        
	        return $qte;
	    }
	    
	    return str_replace('.', ',', $qte);
	}
}