<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */






/**
 * A crm_Currency
 *
 * @property ORM_PkField		$id
 * @property ORM_StringField	$code
 * @property ORM_StringField	$name_en
 * @property ORM_StringField	$name_fr
 * @property ORM_StringField	$symbol
 *
 */
class crm_CurrencySet extends crm_RecordSet
{
	public function __construct($crm = null)
	{
		parent::__construct($crm);

		$this->setDescription('Currency');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('code', 3)
					->setDescription('ISO 4217 code'),
			ORM_StringField('name_en', 255)
					->setDescription('Name (english)'),
			ORM_StringField('name_fr', 255)
					->setDescription('Name (french)'),
			ORM_StringField('symbol', 3)
					->setDescription('Symbol')
		);
	}


	/**
	 *
	 *
	 * @return string
	 */
	public function getNameColumn()
	{

		$colname = 'name_en';
		if ('fr' === $GLOBALS['babLanguage']) {
			$colname = 'name_fr';
		}

		return $colname;
	}


	/**
	 * Get currencies as array for select widget
	 *
	 */
	public function getOptions()
	{
		$colname = $this->getNameColumn();

		$options = array('' => '');
		$iterator = $this->select()->orderAsc($this->$colname);

		foreach($iterator as $country) {
			$options[$country->id] = $country->$colname;
		}

		return $options;
	}
}

/**
 * A crm_Currency
 *
 * @property int				$id
 * @property string				$code
 * @property string				$name_fr
 * @property string				$name_en
 * @property string				$symbol
 */
class crm_Currency extends crm_Record
{
	/**
	 * Name of currency according to user language
	 *
	 * @return string
	 */
	public function getName()
	{
		$colname = 'name_en';

		if ('fr' === $GLOBALS['babLanguage']) {
			$colname = 'name_fr';
		}

		return $this->$colname;
	}
}
