<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * password token are unique ID used to create an account on site for a contact, or allow a contact to change his forgotten password
 * 
 * 
 * @property	ORM_DateTimeField		$createdOn
 * @property	ORM_StringField			$uuid
 * @property	crm_ContactSet			$contact
 */
class crm_PasswordTokenSet extends crm_RecordSet
{
	function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);
		
		$this->setPrimaryKey('id');
		
		$this->addFields(
			ORM_DateTimeField('createdOn')
					->setDescription('Created on'),
			ORM_StringField('uuid')
		);
		
		$this->hasOne('contact', $this->Crm()->ContactSetClassName());
	}

	
	/**
	 * Saves a record. init uuid and createdOn on creation
	 *
	 * @param crm_PasswordToken		$record			The record to save.
	 *
	 * @return boolean				True on success, false otherwise
	 */
	public function save(ORM_Record $record)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
		
		
		$primaryKey = $this->getPrimaryKey();
		if (!$record->{$primaryKey}) {
			$record->initValue('uuid', bab_uuid());
			$record->initValue('createdOn', date('Y-m-d H:i:s'));
		}

		return parent::save($record);
	}
	
}


/**
 * @property	string				$createdOn
 * @property	string				$uuid
 * @property	crm_Contact			$contact
 */
class crm_PasswordToken extends crm_Record
{
	
	const VALIDITY = 7;
	
	
	/**
	 * @return bool
	 */
	public function isValid()
	{
		$age = time() - bab_mktime($this->createdOn);
		$age = round($age / 3600);
		
		return ($age <= (24 * self::VALIDITY));
	}
	
	
	/**
	 * @return string
	 */
	public function getUrl()
	{
		$action = $this->Crm()->Controller()->LostPassword()->editPasswordToken($this->uuid);
		return $GLOBALS['babUrl'].$action->url();
	}
}
