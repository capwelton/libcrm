<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Article type
 * @param	ORM_StringField		$name
 * @param	ORM_TextField		$description
 */
class crm_ArticleTypeSet extends crm_TraceableRecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);
	
		$this->setDescription('Article type');
		$this->setPrimaryKey('id');
	
		$this->addFields(
				ORM_StringField('name')
					->setDescription('Name of article type'),
	
				ORM_TextField('description')
					->setDescription('Generic description field')
		);
	}
	
	
	
	public function getOptions()
	{
		$return = array('' => '');
		
		$res = $this->select();
		$res->orderAsc($this->name);
		foreach($res as $articletype)
		{
			$return[$articletype->id] = $articletype->name;
		}
		
		return $return;
	}
}

/**
 * @param	string		$name
 * @param	string		$description
 * 
 *
 */
class crm_ArticleType extends crm_TraceableRecord
{
	
}