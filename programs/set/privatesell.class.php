<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_PrivateSellSet is a Private sell.
 * @see crm_ArticlePrivateSellSet
 *
 * @property ORM_StringField	$name
 * @property ORM_TextField		$description
 * @property ORM_DatetimeField	$startedOn
 * @property ORM_DatetimeField  $expireOn
 */
class crm_PrivateSellSet extends crm_RecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();
		
		$this->setDescription('Private sell');

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('name'),
			ORM_TextField('description')
					->setDescription('Generic description field'),
			ORM_DateField('startedOn')
					->setDescription('Start date'),
			ORM_DateField('expireOn')
					->setDescription('Expiration date')

		);
	}
	
	
	
	
	
	/**
	 * Add catalogs to sitemap
	 * @param bab_eventBeforeSiteMapCreated $event
	 * @param array $path
	 */
	public function addSitemapNodes(bab_eventBeforeSiteMapCreated $event, Array $path)
	{
		$Crm = $this->Crm();
	
		$res = $this->select();
		$res->orderAsc($this->name);
		
		if ($res->count() > 0)
		{
			$nodeId = $Crm->classPrefix.'PrivateSales';
			
			$link = $event->createItem($nodeId);
			$link->setLabel($Crm->translate('Private sales'));
			$link->setLink($Crm->Controller()->CatalogItem()->listPrivateSell()->url());
			$link->setPosition($path);
			$link->addIconClassname(Func_Icons::PLACES_FOLDER_RED);
			
			$event->addFolder($link);
			
			$path[] = $nodeId;
		}
		
		
		foreach($res as $privatesell)
		{
			$nodeId = $Crm->classPrefix.'PrivateSell'.$privatesell->id;
	
			$link = $event->createItem($nodeId);
			$link->setLabel($privatesell->name);
			$link->setDescription($privatesell->description);
			$link->setLink($Crm->Controller()->CatalogItem()->privateSellDisplay($privatesell->id)->url());
			$link->setPosition($path);
			$link->addIconClassname(Func_Icons::PLACES_FOLDER_BOOKMARKS);
	
			$event->addFunction($link);
		}
	
	
	}
	
}



/**
 * A private sell
 *
 *
 * @see crm_ArticlePrivateSell
 *
 * @property string			$name
 * @property string			$description
 * @property string			$startedOn
 * @property string			$expireOn
 */
class crm_PrivateSell extends crm_Record
{
	
	/**
	 * Test access validity
	 */
	public function isAccessValid()
	{
		$today = date('Y-m-d');
		
		if ($today >= $this->startedOn && $today <= $this->expireOn)
		{
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 * Get the main photo upload path
	 * @return bab_Path
	 */
	public function getPhotoUploadPath()
	{
		$up = $this->uploadPath();
		if (!isset($up))
		{
			return null;
		}
	
		$up->push('photo');
	
		return $up;
	}


	/**
	 * Return the full path of the main photo file
	 * this method return null if there is no photo to display
	 *
	 * @return bab_Path | null
	 */
	public function getPhotoPath()
	{


		$uploadpath = $this->getPhotoUploadPath();

		if (!isset($uploadpath)) {
			return null;
		}

		$W = bab_Widgets();

		$uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

		if (!isset($uploaded)) {
			return null;
		}

		foreach($uploaded as $file) {
			return $file->getFilePath();
		}

		return null;
	}


	/**
	 * Move photo to photo upload path
	 * this method is used to import a photo from a temporary directory of the filePicker widget or another file
	 * warning, the default behavior remove the source file and existing article photo file
	 *
	 * @param	bab_Path 	$sourcefile
	 * @param	bool		$temporary			default is true, if the sourcefile is a filePicker temporary file
	 * @return 	bool
	 */
	public function importPhoto(bab_Path $sourcefile, $temporary = true)
	{
		$uploadpath = $this->getPhotoUploadPath();

		if (!isset($uploadpath)) {
			return false;
		}

		$uploadpath->createDir();


		if (null !== $photo = $this->getPhotoPath()) {
			unlink($photo->toString());
		}

		if (!$temporary) {
			$W = bab_Widgets();
			return $W->imagePicker()->setFolder($uploadpath)->importFile($sourcefile);
		}


		$original = $sourcefile->toString();
		$uploadpath->push(basename($original));

		return rename($original, $uploadpath->toString());
	}
}