<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Bank account details set.
 *
 * @property ORM_PkField			$id
 * @property ORM_StringField		$name
 * @property ORM_TextField			$street
 * @property ORM_StringField		$postalCode
 * @property ORM_StringField		$city
 * @property ORM_StringField		$cityComplement
 * @property ORM_StringField		$country
 * @property ORM_StringField		$bankName
 * @property ORM_StringField		$bankCode
 * @property ORM_StringField		$guichetCode
 * @property ORM_StringField		$account
 * @property ORM_StringField		$controlKey
 * @property ORM_StringField		$iban
 * @property ORM_StringField		$swift
 */
class crm_BankAccountSet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);
		
		$this->setDescription('Bank account');

		$this->addFields(
			ORM_StringField('name')
				->setDescription('Name of drawee'),
			ORM_TextField('street')
				->setDescription('Street'),
			ORM_StringField('postalCode', 10)
				->setDescription('Zip code'),
			ORM_StringField('city', 60)
				->setDescription('City'),
			ORM_StringField('cityComplement', 60)
				->setDescription('City complement (CEDEX...)'),
			ORM_StringField('country', 60)
				->setDescription('Country'),
			
			ORM_StringField('bankName', 255)
				->setDescription('Bank name'),
			ORM_StringField('bankCode', 6)
				->setDescription('Bank code'),
			ORM_StringField('guichetCode', 5)
				->setDescription('Guichet code'),
			ORM_StringField('account', 20)
				->setDescription('Account number'),
			ORM_StringField('controlKey', 2)
				->setDescription('Control key'),
			ORM_StringField('iban', 255)
				->setDescription('IBAN'),
			ORM_StringField('swift', 255)
				->setDescription('SWIFT')				
			
		);

	}

}


/**
 * Bank account details set.
 *
 * @property int		$id
 * @property string		$name
 * @property string		$street
 * @property string		$postalCode
 * @property string		$city
 * @property string		$cityComplement
 * @property string		$country
 * @property string		$bankName
 * @property string		$bankCode
 * @property string		$guichetCode
 * @property string		$account
 * @property string		$controlKey
 * @property string		$iban
 * @property string		$swift
 */
class crm_BankAccount extends crm_TraceableRecord
{
	
}