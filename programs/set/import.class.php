<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * A crm_Import
 *
 * @property ORM_StringField	$title
 * @proporty ORM_EnumField		$objecttype
 * @property ORM_IntField		$itemcount
 */
class crm_ImportSet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('filename')
					->setDescription('File name'),
			ORM_StringField('title')
					->setDescription('import title'),
			ORM_EnumField('objecttype', crm_translateArray(crm_Import::$objecttypes))
					->setDescription('Type of imported objects'),
			ORM_IntField('itemcount')
					->setDescription('Number of imported items'),
			ORM_BoolField('finished')
		);

	}





	public function delete(ORM_Criteria $criteria = null, $definitive = false)
	{
		$Crm = $this->Crm();
		require_once dirname(__FILE__).'/link.class.php';
		$linkSet = $Crm->LinkSet();

		// delete notes and links

		foreach($this->select($criteria) as $import) {
			$linkSet->deleteForSource($import, $Crm->NoteClassName(), true);
			$linkSet->deleteForSource($import);

			if ($attachment = $import->getAttachment())
			{
				/* @var $attachment Widget_FilePickerItem */
				$attachment->delete();
			}
		}



		parent::delete($criteria);
	}
}






/**
 * A crm_Contact is a person
 *
 * @property ORM_StringField	$title
 * @proporty ORM_EnumField		$objecttype
 * @property ORM_IntField		$itemcount
 */
class crm_Import extends crm_TraceableRecord
{
	const TYPE_ORGANIZATION 		= 1;
	const TYPE_CONTACT				= 2;
	const TYPE_CAMPAIGN_RECIPIENT	= 3;
	const TYPE_ARTICLE				= 4;
	const TYPE_DEAL    				= 5;


	public static $objecttypes = array(
		crm_Import::TYPE_ORGANIZATION 			=> 'Organizations',
		crm_Import::TYPE_CONTACT 				=> 'Contacts',
		crm_Import::TYPE_CAMPAIGN_RECIPIENT		=> 'Campaign recipients',
		crm_Import::TYPE_ARTICLE				=> 'Articles',
	    crm_Import::TYPE_DEAL      				=> 'Deals'
	);



	/**
	 * add an import Note
	 */
	public function addNote(crm_Note $note)
	{
		$note->linkTo($this);
	}


	/**
	 * add a contact to import
	 */
	public function addContact(crm_Contact $contact)
	{
		$contact->linkTo($this);
	}

	/**
	 * add an organization to import
	 */
	public function addOrganization(crm_Organization $organization)
	{
		$organization->linkTo($this);
	}


	/**
	 * add an deal to import
	 */
	public function addDeal(crm_Deal $deal)
	{
	    $deal->linkTo($this);
	}

	/**
	 * add an article to import
	 */
	public function addArticle(crm_Article $article)
	{
		$article->linkTo($this);
	}



	/**
	 * @return ORM_Iterator
	 */
	public function selectNotes()
	{
		return $this->Crm()->NoteSet()->selectLinkedTo($this);
	}



	/**
	 * Attach a temporary file to import record and set filename in record
	 * @see Widget_FilePickerIterator
	 * @param	Widget_FilePickerItem $file
	 *
	 * @return bool
	 */
	public function attachFile(Widget_FilePickerItem $file)
	{
		$uploadPath = $this->uploadPath();
		if ($uploadPath->isDir())
		{
			$uploadPath->deleteDir(); // remove existing file if exists
		}
		$uploadPath->createDir(); // create new empty directory
		$original = $file->getFilePath()->toString();
		$uploadPath->push(basename($original));

		if (rename($original, $uploadPath->toString()))
		{
			$this->filename = $file->toString();
			$this->save();
			return true;
		}

		return false;
	}



	/**
	 * @return Widget_FilePickerItem
	 */
	public function getAttachment()
	{
		$I = bab_Widgets()->FilePicker()->getFolderFiles($this->uploadPath());
		if ($I)
		{
			foreach($I as $filePickerItem)
			{
				return $filePickerItem;
			}
		}
		return null;
	}




	/**
	 * Select imported items
	 * @return ORM_Iterator
	 */
	public function selectItems()
	{
		switch($this->objecttype) {

			case self::TYPE_ORGANIZATION:
				return $this->Crm()->OrganizationSet()->selectLinkedTo($this);

			case self::TYPE_CONTACT:
				return $this->Crm()->ContactSet()->selectLinkedTo($this);

			case self::TYPE_ARTICLE:
				return $this->Crm()->ArticleSet()->selectLinkedTo($this);
		}

		throw new Exception('unknown link type '.$this->objecttype);
	}



	/**
	 * Get main title
	 *
	 */
	public function getMainTitle()
	{
		$Crm = $this->Crm();
		switch($this->objecttype) {

			case self::TYPE_ORGANIZATION:

				if (1 === (int) $this->itemcount) {

					return sprintf($Crm->translate('1 organization'), $this->itemcount);
				} else {
					return sprintf($Crm->translate('%d organizations'), $this->itemcount);
				}
				break;

			case self::TYPE_CONTACT:

				if (1 === (int) $this->itemcount) {

					return sprintf($Crm->translate('1 contact'), $this->itemcount);
				} else {
					return sprintf($Crm->translate('%d contacts'), $this->itemcount);
				}

				break;

			case self::TYPE_ARTICLE:

				if (1 === (int) $this->itemcount) {

					return sprintf($Crm->translate('1 article'), $this->itemcount);
				} else {
					return sprintf($Crm->translate('%d articles'), $this->itemcount);
				}

				break;

			case self::TYPE_CAMPAIGN_RECIPIENT:

				$name = '???';
				$campaign = $this->getCampaign();
				if ($campaign) {
					$name = $campaign->name;
				}

				if (1 === (int) $this->itemcount) {

					return sprintf($Crm->translate('1 recipient (campaign %s)'), $this->itemcount, $name);
				} else {
					return sprintf($Crm->translate('%d recipients (campaign %s)'), $this->itemcount, $name);
				}

				break;

			default:

				if (1 === (int) $this->itemcount) {

					return sprintf($Crm->translate('1 item'), $this->itemcount);
				} else {
					return sprintf($Crm->translate('%d items'), $this->itemcount);
				}

				break;
		}
	}

	/**
	 * Get the optional campaign linked to record
	 *
	 * @return crm_Campaign | null
	 */
	public function getCampaign()
	{
		$this->Crm()->includeCampaignSet();

		$linkSet = $this->Crm()->LinkSet();
		$I = $linkSet->selectForSource($this, $this->Crm()->CampaignClassName());

		foreach($I as $link) {
			return $link->targetId;
		}

		return null;
	}



	/**
	 * Set the optional campaign ID linked to record
	 *
	 * @param	int	$id_campaign
	 * @return bool
	 */
	public function setCampaign($id_campaign)
	{
		$linkSet = $this->Crm()->LinkSet();
		$I = $linkSet->selectForSource($this, $this->Crm()->CampaignClassName());

		foreach($I as $link) {
			// link exists, do nothing
			return false;

		}

		/* @var $link crm_Link */
		$link = $linkSet->newRecord();
		$link->sourceClass = get_class($this);
		$link->sourceId = $this->id;
		$link->targetClass = $this->Crm()->CampaignClassName();
		$link->targetId = $id_campaign;
		$link->type = 'CampaignImport';
		$link->save();

		return true;
	}





	/**
	 * Import a CSV row or add a note in case of error
	 * The set must have an import method
	 * return the same value as the import method
	 *
	 * @return bool | int
	 */
	public function importRow(ORM_RecordSet $set, Widget_CsvRow $row, $notrace = false)
	{
		bab_setTimeLimit(5);

		try {
			if ($row->checkMandatory()) {
				if ($result = $set->import($this, $row, $notrace)) { /* See import() in contact.class.php */
					$this->itemcount++;
					return $result;
				}
			}

		} catch(Widget_ImportException $e) {

			// record import exception as notes, linked to the importRecord

			$note = $this->Crm()->noteSet()->newRecord();
			$note->summary = $e->getMessage();
			$note->save();

			$this->addNote($note);

			$note->__destruct();
			unset($note);
		}

		return false;
	}
}
