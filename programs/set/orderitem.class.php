<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * An order item set.
 *
 * @property ORM_StringField		$reference
 * @property ORM_StringField		$name
 * @property ORM_StringField		$subtitle
 * @property ORM_TextField			$description
 * @property ORM_StringField		$packaging
 * @property ORM_DecimalField		$quantity
 * @property ORM_DecimalField		$unit_cost
 * @property ORM_DecimalField		$reduction
 * @property ORM_DecimalField		$vat
 * @property ORM_IntField			$sortkey
 * @property ORM_BoolField			$commentrequest
 *
 * @property crm_OrderSet			$parentorder		The parent order
 * @property crm_CatalogItemSet		$catalogitem		The source article in a catalog
 * @property crm_ArticlePackagingSet $articlepackaging
 */
class crm_OrderItemSet extends crm_RecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $this->setDescription('Order item');
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('reference')
                ->setDescription('unique reference in order'),
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_StringField('subtitle')
                ->setDescription('Subtitle'),
            ORM_TextField('description')
                ->setDescription('Description'),
            ORM_StringField('packaging')
                ->setDescription('Packaging name'),
            ORM_DecimalField('quantity')
                ->setPrecision(11, 4)
                ->setDescription('Quantity'),
            ORM_DecimalField('unit_cost')
                ->setPrecision(11, 4)
                ->setDescription('Unit cost excluding tax, reduction incl'),
            ORM_DecimalField('reduction', 2)
                ->setDescription('percentage, not used in amount, allready included in unit cost'),
            ORM_DecimalField('vat', 2)				// TVA
                ->setDescription('value added tax (percentage)'),
            ORM_IntField('sortkey')
                ->setDescription('Item sort key in order'),
            ORM_BoolField('commentrequest')
                ->setDescription('A comment request has been sent or the item is allready commented')
        );

        if (isset($Crm->Order)) {
            $this->hasOne('parentorder', $Crm->OrderSetClassName());
        }

        if (isset($Crm->CatalogItem)) {
            $this->hasOne('catalogitem', $Crm->catalogItemSetClassName());
        }

        if (isset($Crm->ArticlePackaging)) {
            $this->hasOne('articlepackaging', $Crm->ArticlePackagingSetClassName());
        }
    }
}





/**
 * An order item.
 *
 * @property string					$reference
 * @property string					$name
 * @property string					$subtitle
 * @property string					$description
 * @property string					$packaging
 * @property string					$quantity
 * @property string					$unit_cost
 * @property string					$reduction
 * @property string					$vat
 * @property int					$sortkey
 * @property bool					$commentrequest
 *
 * @property 	 crm_Order				$parentorder
 * @property	 crm_CatalogItem		$catalogitem
 * @property	 crm_ArticlePackaging	$articlepackaging
 */
class crm_OrderItem extends crm_Record
{
	/**
	 * Get total duty-free
	 * @return float
	 */
	public function getTotalDF()
	{
		return $this->unit_cost * $this->quantity;
	}



	/**
	 * Get total for order item taxes included
	 * @return float
	 */
	public function getTotalTI()
	{
		$df = $this->getTotalDF();
		$total = $df + ($df * $this->vat) / 100;

		return $total;
	}
}