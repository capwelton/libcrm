<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * crm_Classification represents hierachical classification used for deals (project) and contacts abilities.
 *
 *
 * @property ORM_StringField	$name
 * @property self				$lf
 * @property self				$lr
 * @property self				$parent
 */
class crm_ClassificationSet extends crm_RecordSet
{
	function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);

		$this->setDescription('Classification');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name')
		);

		$this->hasOne('lf', get_class($this));
		$this->hasOne('lr',  get_class($this));
		$this->hasOne('id_parent',  get_class($this));
	}


	/**
	 *
	 * @return ORM_Criterion
	 */
	public function isChildOf($node)
	{
		if (!is_int($node)) {
			$node = $node->id;
		}
		return $this->id_parent->is($node);
	}


	/**
	 *
	 * @return ORM_Criterion
	 */
	public function isDescendantOf($node)
	{
		if (is_int($node)) {
			$set = new self();
			$node = $set->get($node);
		}
		return $this->lf->greaterThan($node->lf)->_AND_($this->lr->lessThan($node->lr));
	}


	/**
	 * Updates lf and lr on tree nodes.
	 *
	 * @internal
	 */
	public function update($lr, $offset = 1)
	{
		global $babDB;

		$offset *= 2;
		$operation = ($offset > 0 ? '+' : '-');
		$offset = abs($offset);
		$lr = (int)$lr;
		$babDB->db_query('UPDATE ' . $this->getTableName() . ' SET lr = lr' . $operation . $offset . ' WHERE lr > ' . $lr);
		$babDB->db_query('UPDATE ' . $this->getTableName() . ' SET lf = lf' . $operation . $offset . ' WHERE lf > ' . $lr);
	}


	/**
	 * Delete records, the number of records depends on criteria
	 *
	 * @param ORM_Criteria	$oCriteria	Criteria for selecting records to delete
	 *
	 * @return boolean					True on success, False otherwise
	 */
	public function delete(ORM_Criteria $criteria = null)
	{
		$nodes = $this->select($criteria);
		foreach ($nodes as $node) {

			$lf = $node->lf;
			$lr = $node->lr;

			if ($lr - $lf > 1) {
				$subTreeCriteria = $this->lf->greaterThanOrEqual($lf)->_AND_($this->lf->lessThanOrEqual($lr));
				$subTreeNodes = $this->select($subTreeCriteria);
				parent::delete($subTreeCriteria);
				$this->update($lr, ($lr - $lf + 1) / 2);
			} else {
				parent::delete($this->id->is($node->id));
			}
		}
		return true;
	}

}


/**
 * crm_Classification represents hierachical classification used for deals (project), organizations and contacts abilities.
 *
 * @property int							$id
 * @property string							$name
 * @property crm_Classification			$lf
 * @property crm_Classification			$lr
 * @property crm_Classification			$id_parent
 */
class crm_Classification extends crm_Record
{
	/**
	 * Returns an iterator on the classification's descendants.
	 *
	 * @return self[]
	 */
	public function getDescendants()
	{
		$set = $this->getParentSet();
		return $set->select($set->isDescendantOf($this))->orderAsc($set->lf);
	}


	/**
	 * Returns an array of the classification's ascendants.
	 *
	 * @return self[]
	 */
	public function getAscendants()
	{
		$set = $this->getParentSet();

		$ascendants = array();
		$parentId = $this->id_parent;
		while ($parentId != 1 && $parent = $set->get($parentId)) {
			$ascendants[$parentId] = $parent;
			$parentId = $parent->id_parent;
		}
		return $ascendants;
	}


	/**
	 * Returns an iterator on the classification's children.
	 *
	 * @return self[]
	 */
	public function getChildren()
	{
		$set = $this->getParentSet();
		return $set->select($set->isChildOf($this))->orderAsc($set->lf);
	}

	/**
	 *
	 *
	 * @return string[]
	 */
	public function getPathname()
	{
		$pathname = array();
		$ascendants = $this->getAscendants();
		foreach ($ascendants as $classification) {
			$pathname[] = $classification->name;
		}
		$pathname[] = $this->name;
		return $pathname;
	}


	/**
	 * Returns the first child of the node or null.
	 *
	 * @return self|null
	 */
	public function getFirstChild()
	{
		$set = $this->getParentSet();
		$children = $set->select($set->id_parent->is($this->id));
		$children->orderAsc($set->lf);
		foreach ($children as $child) {
			return $child;
		}
		return null;
	}

	/**
	 * Returns the last child of the node or null.
	 *
	 * @return self|null
	 */
	public function getLastChild()
	{
		$set = $this->getParentSet();
		$children = $set->select($set->id_parent->is($this->id));
		$children->orderDesc($set->lf);
		foreach ($children as $child) {
			return $child;
		}
		return null;
	}


	/**
	 * Saves the specified classification as the last child.
	 *
	 * @return bool
	 */
	public function appendChild(ORM_Record $classification)
	{
		$classification->id_parent = $this->id;
		if ($lastChild = $this->getLastChild()) {
			$lr = $lastChild->lr;
		} else {
			$lr = $this->lf;
		}
		if($lr == null){
			$lr = 0;
		}
		$this->getParentSet()->update($lr);
		$classification->lf = $lr + 1;
		$classification->lr = $lr + 2;

		return $classification->save();
	}

}


/**
 * @return crm_Classification
 */
function crm_Classification($classification = null)
{
	$classificationSet = new crm_ClassificationSet();

	if (isset($classification)) {
		return $classificationSet->get($classification);
	} else {
		return $classificationSet->newRecord();
	}
	return $classification;
}
