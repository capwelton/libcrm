<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_PaymentErrorSet
 * 
 * @property ORM_PkField			$id
 * @property ORM_StringField		$paymentToken
 * @property ORM_DatetimeField		$createdOn
 * @property ORM_DatetimeField		$transmission_date
 * @property ORM_StringField		$code
 * @property ORM_StringField		$message
 * @property ORM_StringField		$bank_response_code
 * @property ORM_StringField		$payment_means
 * @property ORM_StringField		$card_number
 * @property crm_ShoppingCartSet	$cart
 */
class crm_PaymentErrorSet extends crm_RecordSet
{
	public function __construct($Crm = null)
	{
		parent::__construct($Crm);
		
		$this->setDescription('Failed payment attempts');
		
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('paymentToken'),
			ORM_DatetimeField('createdOn'),
			ORM_DatetimeField('transmission_date')->setDescription('Date and hour of the payment transaction request given by the gateway'),
			ORM_StringField('code')->setDescription('Gateway error code'),
			ORM_StringField('message')->setDescription('Gateway error message'),
			ORM_StringField('bank_response_code')->setDescription('Bank response code forwarded by gateway'),
			ORM_StringField('payment_means'),
			ORM_StringField('card_number')
		);
		
		$this->hasOne('cart', $Crm->ShoppingCartSetClassName());
	}
	

}

/**
 * A crm_PaymentError
 * 
 * @property int				$id
 * @property string				$paymentToken
 * @property string				$createdOn
 * @property string				$transmission_date
 * @property string				$code
 * @property string				$message
 * @property string				$bank_response_code
 * @property string				$payment_means
 * @property string				$card_number
 * @property crm_ShoppingCart	$cart
 */
class crm_PaymentError extends crm_Record
{
	
}
