<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * A crm_Deal
 *
 * @property ORM_StringField        $name
 * @property ORM_TextField          $description
 * @property crm_OrganizationSet    $lead           The deal's lead organization.
 * @property crm_StatusSet          $status         The status of the deal.
 * @property crm_ContactSet         $responsible    The collaborator responsible for this deal.
 * @method crm_OrganizationSet      lead()          The deal's lead organization.
 * @method crm_StatusSet            status()        The status of the deal.
 * @method crm_ContactSet           responsible()   The collaborator responsible for this deal.
 */
class crm_DealSet extends crm_TraceableRecordSet
{
    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setDescription('Deal');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_TextField('description')
                ->setDescription('Description')
        );

        $this->hasOne('lead', $Crm->OrganizationSetClassName())
            ->setDescription('Lead');

        $this->hasOne('status', $Crm->StatusSetClassName())
            ->setDescription('Status');

        $this->hasOne('responsible', $Crm->ContactSetClassName())
            ->setDescription('Responsible collaborator');

        $Crm->includeTeamSet();
        $this->hasMany('teams', $Crm->TeamSetClassName(), 'deal')
            ->setOnDeleteMethod(ORM_ManyRelation::ON_DELETE_CASCADE);
    }



    /**
     * Adds a fullNumber field containing the year and number (YYYY-NNN).
     *
     * @return crm_DealSet
     */
    public function addFullNumberField()
    {
        $this->addFields(
            $this->year->concat('-')->concat($this->number->leftPad(3, '0'))
            ->setName('fullNumber')
        );

        return $this;
    }


    /**
     * @return ORM_Criteria
     */
    public function hasImpliedClassification($classification)
    {
        return $this->hasClassification($classification);
    }



    /**
     * @return ORM_Criteria
     */
    public function hasClassification($classification)
    {
        if (empty($classification)) {
            return $this->all();
        }
        $Crm = $this->Crm();
        $dealClassificationSet = $Crm->DealClassificationSet();
        $dealClassificationSet->classification();

        return $this->id->in($dealClassificationSet->classification->name->is($classification), 'deal');
    }



    /**
     * @param int|int[]     $classification ids.
     * @return ORM_Criteria
     */
    public function hasClassificationId($classifications)
    {
        if (empty($classifications)) {
            return $this->all();
        }
        $Crm = $this->Crm();

        $dealClassificationSet = $Crm->DealClassificationSet();

        return $this->id->in($dealClassificationSet->classification->in($classifications), 'deal');
    }


    /**
     * @param int   $status
     * @return ORM_Criteria
     */
    public function hasStatus($status)
    {
        $statusSet = $this->Crm()->StatusSet();
        $status = $statusSet->get($statusSet->id->is($status));

        return $this->status->in($statusSet->isDescendantOf($status)->_OR_($statusSet->id->is($status->id)), 'id');
    }


    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }

    /**
     *
     * {@inheritDoc}
     * @see crm_RecordSet::isUpdatable()
     */
    public function isUpdatable()
    {
        return $this->Crm()->Access()->canPerformActionOnSet($this, 'deal:update');
    }

    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isDeletable()
     */
    public function isDeletable()
    {
        return $this->Crm()->Access()->canPerformActionOnSet($this, 'deal:delete');
    }

    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isReadable()
     */
    public function isReadable()
    {
        return $this->Crm()->Access()->canPerformActionOnSet($this, 'deal:read');
    }
}


/**
 * A crm_Deal
 *
 * @property string             $name
 * @property string             $description
 * @property crm_Organization   $lead           The deal's lead organization.
 * @property crm_Status         $status         The status of the deal.
 * @property crm_Contact        $responsible    The collaborator responsible for this deal.
 * @method crm_Organization     lead()          The deal's lead organization.
 * @method crm_Status           status()        The status of the deal.
 * @method crm_Contact          responsible()   The collaborator responsible for this deal.
 */
class crm_Deal extends crm_TraceableRecord
{

    const SUBFOLDER = 'deals';
    const PHOTOSUBFOLDER = 'photo';
    const ATTACHMENTSSUBFOLDER = 'attachments';


    /**
     * Returns the formatted number for the deal.
     *
     * @return string
     */
    public function getFullNumber()
    {
        return $this->id;
    }

    /**
     * Initialize a unique numer for the deal.
     *
     * @return crm_Deal
     */
    public function initFullNumber()
    {
        return $this;
    }


    public function save($notrace = false)
    {
        $this->initFullNumber();
        return parent::save($notrace);
    }


    /**
     * Get the upload path for files related to this deal.
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

        $path = $this->Crm()->uploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }


    /**
     * Get the upload path for the photo file
     * this method can be used with the image picker widget
     *
     * @return bab_Path
     */
    public function getPhotoUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::PHOTOSUBFOLDER);
        return $path;
    }


    /**
     * Get the upload path for attached files.
     *
     * @return bab_Path
     */
    public function getAttachmentsUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::ATTACHMENTSSUBFOLDER);
        return $path;
    }


    /**
     * Return the full path of the photo file
     * The photo field contain a file name or a description of the photo
     * this method return null if the is no photo to display
     *
     * @return bab_Path | null
     */
    public function getPhotoPath()
    {
        $uploadpath = $this->getPhotoUploadPath();

        if (!isset($uploadpath)) {
            return null;
        }

        $W = bab_Widgets();

        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

        if (!isset($uploaded)) {
            return null;
        }

        foreach($uploaded as $file) {
            return $file->getFilePath();
        }

        return null;
    }


    /**
     * Move photo to photo upload path
     * this method is used to import a photo from a temporary directory of the filePicker widget or another file
     * warning, the default behavior remove the source file
     *
     * @param	bab_Path 	$sourcefile
     * @param	bool		$temporary			default is true, if the sourcefile is a filePicker temporary file
     * @return 	bool
     */
    public function importPhoto(bab_Path $sourcefile, $temporary = true)
    {
        $uploadpath = $this->getPhotoUploadPath();

        if (!isset($uploadpath)) {
            return false;
        }

        $uploadpath->createDir();

        if (!$temporary) {
            $W = bab_Widgets();
            return $W->imagePicker()->setFolder($uploadpath)->importFile($sourcefile);
        }

        $original = $sourcefile->toString();
        $uploadpath->push(basename($original));

        return rename($original, $uploadpath->toString());
    }


    /**
     * Return the status as a human-readable string.
     *
     * @return string
     */
    public function getStatusName()
    {
        $this->Crm()->includeStatusSet();
        if ($status = $this->status()) {
            return $status->name;
        }
        return '';
    }


    /**
     * Updates the status of the deal and keep track of the status updates history.
     *
     * @param	int		$status				The new status id.
     * @param	string	$comment			A comment to associate to the status change.
     * @param	string	$newStatusDateTime	The iso-formatted datetime of the status change.
     *
     * @return crm_Deal
     */
    public function updateStatus($status, $comment = null, $newStatusDateTime = null)
    {
        $dealStatusHistorySet = $this->Crm()->DealStatusHistorySet();
        $dealStatusHistory = $dealStatusHistorySet->newRecord();
        $dealStatusHistory->deal = $this->id;
        $dealStatusHistory->status = $status;
        if (!isset($newStatusDateTime) || empty($newStatusDateTime) || $newStatusDateTime === '0000-00-00 00:00:00') {
            require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
            $newStatusDateTime = BAB_DateTime::now()->getIsoDate();
        }
        $dealStatusHistory->date = $newStatusDateTime;
        $dealStatusHistory->comment = $comment;

        $latestStatuses = $dealStatusHistorySet->select($dealStatusHistorySet->deal->is($this->id))
            ->orderDesc($dealStatusHistorySet->date)
            ->orderDesc($dealStatusHistorySet->id);

        foreach ($latestStatuses as $latestStatus) {
            break;
        }
        $dealStatusHistory->save();

        if (isset($latestStatus)) {
            // Check if the new status becomes the new latest status.
            if ($dealStatusHistory->date >= $latestStatus->date) {
                $this->status = $dealStatusHistory->status;

                // If the new latest status is the same as the previous latest status, we remove the previous one.
                if ($dealStatusHistory->status == $latestStatus->status) {
                    $dealStatusHistorySet->delete($dealStatusHistorySet->id->is($latestStatus->id));
                }
            } else {
                $this->status = $latestStatus->status;
            }
        } else {
            $this->status = $dealStatusHistory->status;
        }

        $this->save();

        return $this;
    }

    /**
     * Updates the status of the deal and keep track of the status updates history.
     *
     * @param	string	$key				The new status reference id.
     * @param	string	$comment			A comment to associate to the status change.
     * @param	string	$newStatusDateTime	The iso-formatted datetime of the status change.
     *
     * @return crm_Deal
     */
    public function updateStatusByReference($key, $comment = null, $newStatusDateTime = null)
    {
        $statusSet = $this->Crm()->StatusSet();
        $status = $statusSet->get($statusSet->reference->is($key));
        if (!isset($status)) {
            return $this;
        }

        $result = $this->updateStatus($status->id, $comment, $newStatusDateTime);

        return $result;
    }

    /**
     * Updates the deal status with the latest in the deal status history.
     */
    public function updateLatestStatus()
    {
        $Crm = $this->Crm();
        $dealStatusHistorySet = $Crm->DealStatusHistorySet();

        $latestStatuses = $dealStatusHistorySet->select($dealStatusHistorySet->deal->is($this->id))
           ->orderDesc($dealStatusHistorySet->date)
           ->orderDesc($dealStatusHistorySet->id);

        $this->status = 0;
        foreach ($latestStatuses as $latestStatus) {
            $this->status = $latestStatus->status;
            break;
        }
        $this->save();
    }


    /**
     * (non-PHPdoc)
     * @see programs/crm_Record::delete()
     */
    public function delete()
    {
        parent::delete();

        return $this;
    }
}
