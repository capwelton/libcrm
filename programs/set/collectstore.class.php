<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * Collect store for delivery (online shop)
 * 
 * @property ORM_StringField $name
 * @property ORM_TextField $openinghours
 * @property crm_AddressSet $address
 * 
 * @method crm_CollectStore newRecord()
 * @method crm_CollectStore get()
 * @method crm_CollectStore[] select()
 */
class crm_CollectStoreSet extends crm_TraceableRecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);
    
        $Crm = $this->Crm();
        
        $this->setDescription('Collect store');
        
        $this->addFields(
            ORM_StringField('name'),
            ORM_TextField('openinghours')
        );
        
        $this->hasOne('address', $Crm->AddressSetClassName());
        
    }
}


/**
 *
 * @property string $name
 * @property string $openinghours
 * @property crm_Address $address
 */
class crm_CollectStore extends crm_TraceableRecord
{
    
}