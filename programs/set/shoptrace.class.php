<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Trace online shop users from first connexion to payment
 *
 * @property	ORM_StringField		$trace
 * @property	ORM_StringField		$session
 * @property	ORM_StringField		$shoppingcart_cookie
 * @property	ORM_StringField		$user_agent
 * @property	ORM_StringField		$remote_ip
 * @property	ORM_StringField		$url
 * @property	ORM_StringField		$message
 * @property	ORM_DateTimeField	$createdOn
 * @property	ORM_DateTimeField	$traceCreatedOn
 * @property	crm_ShoppingCartSet	$cart
 * @property	crm_OrderSet		$paymentorder
 */
class crm_ShopTraceSet extends crm_RecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('trace')->setDescription('unique id for trace'),
            ORM_StringField('session'),
            ORM_StringField('shoppingcart_cookie'),
            ORM_StringField('user_agent'),
            ORM_StringField('remote_ip'),
            ORM_StringField('url'),
            ORM_StringField('message'),
            ORM_DatetimeField('createdOn'),
            ORM_DatetimeField('traceCreatedOn')
        );

        $this->hasOne('cart', $Crm->ShoppingCartSetClassName());
    }


    /**
     * Get a trace unique identifier
     * @param	bool	$createtrace
     * @return array(identifier, datetime) | null
     */
    public function getTrace($createtrace)
    {
        $Crm = $this->Crm();
        $sesskey = $Crm->classPrefix.'ShopTraceId';

        if (isset($_SESSION[$sesskey]) && is_array($_SESSION[$sesskey]) && !empty($_SESSION[$sesskey][0]))
        {
            return $_SESSION[$sesskey];
        }

        // aucune trace existe dans la session, trouver la trace associee au panier en cours

        $scSet = $Crm->ShoppingCartSet();
        $shoppingcart_cookie = $scSet->getMyCookie();

        if (null !== $shoppingcart_cookie)
        {
            $shoptrace = $this->get($this->shoppingcart_cookie->is($shoppingcart_cookie));
            if (!empty($shoptrace->trace))
            {
                $_SESSION[$sesskey] = array($shoptrace->trace, $shoptrace->traceCreatedOn);
                return $_SESSION[$sesskey];
            }
        }


        if (false === $createtrace)
        {
            return null;
        }

        // on ne retrouve pas de trace a continuer, demarrer une nouvelle trace

        require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
        $_SESSION[$sesskey] = array(bab_uuid(), date('Y-m-d H:i:s'));

        return $_SESSION[$sesskey];
    }


    /**
     * End trace
     * warning : cannot end trace if the shopping cart cookie exists and a trace exists with it
     */
    public function endTrace()
    {
        if (isset($_SESSION[$sesskey]))
        {
            unset($_SESSION[$sesskey]);
        }
    }



    private function addLogToTrace(Array $traceInfos, $message)
    {
        list($trace, $traceCreatedOn) = $traceInfos;

        $Crm = $this->Crm();
        $shoptrace = $this->newRecord();

        $scSet = $Crm->ShoppingCartSet();
        $shoppingCart = $scSet->getMyCart(false);

        $shoptrace->shoppingcart_cookie		= $scSet->getMyCookie();
        $shoptrace->cart 					= null;

        if (isset($shoppingCart))
        {
            $shoptrace->cart 				= $shoppingCart->id;
        }



        $shoptrace->trace 					= $trace;
        $shoptrace->traceCreatedOn 			= $traceCreatedOn;
        $shoptrace->session 				= session_id();

        if (isset($_SERVER['HTTP_USER_AGENT']))
        {
            $shoptrace->user_agent			= $_SERVER['HTTP_USER_AGENT'];
        }

        if (isset($_SERVER['REMOTE_ADDR']))
        {
            $shoptrace->remote_ip			= $_SERVER['REMOTE_ADDR'];
        }

        if (!empty($_SERVER['REQUEST_URI']))
        {
            $shoptrace->url					= $_SERVER['REQUEST_URI'];

        } else if (isset($_SERVER['SCRIPT_NAME'])) {

            $shoptrace->url = $_SERVER['SCRIPT_NAME'];

            if (!empty($_SERVER['QUERY_STRING']))
            {
                $shoptrace->url .= '?' . $_SERVER['QUERY_STRING'];
            }
        }

        $shoptrace->message 				= $message;
        $shoptrace->createdOn 				= date('Y-m-d H:i:s');



        return $shoptrace;
    }



    /**
     * Add a message into trace
     * @param string 	$message
     * @param bool		$createtrace	create trace if not exists
     *
     * @return crm_ShopTrace
     */
    public function log($message, $createtrace = false)
    {
        $arr = $this->getTrace($createtrace);

        if (null === $arr)
        {
            return null;
        }

        $shoptrace = $this->addLogToTrace($arr, $message);
        $shoptrace->save();

        return $shoptrace;
    }


    /**
     * Insert log message into trace associated to the shopping cart
     *
     * @param crm_ShoppingCart $cart
     * @param string $message
     */
    public function cartLog(crm_ShoppingCart $cart, $message)
    {
        // get trace from shopping cart

        $firstlog = $this->get($this->cart->is($cart->id));
        /*@var $firstlog crm_ShopTrace */

        $shoptrace = $this->addLogToTrace(array($firstlog->trace, $firstlog->traceCreatedOn), $message);
        $shoptrace->cart = $cart->id;
        $shoptrace->save();

        return $shoptrace;
    }
}


/**
 * Trace message
 *
 * @property	string				$trace
 * @property	string				$session
 * @property	string				$shoppingcart_cookie
 * @property	string				$user_agent
 * @property	string				$remote_ip
 * @property	string				$url
 * @property	string				$message
 * @property	string				$createdOn
 * @property	string				$traceCreatedOn
 * @property	crm_ShoppingCart	$cart
 * @property	crm_Order			$paymentorder
 *
 */
class crm_ShopTrace extends crm_Record
{

}