<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * A crm_ShippingScaleSet
 * 
 * @property ORM_PkField		$id
 * @property ORM_DecimalField	$shipping_amount
 * @property ORM_DecimalField	$weight
 * @property ORM_StringField	$department
 * @property crm_CountrySet		$country
 */
class crm_ShippingScaleSet extends crm_RecordSet
{
	public function __construct($Crm = null)
	{
		parent::__construct($Crm);
		
		$this->setDescription('Shipping scale');
		$this->setPrimaryKey('id');

		$this->addFields(
		    ORM_StringField('comment'),
			ORM_DecimalField('shipping_amount', 4)	->setDescription('Amount added in shopping cart, incl tax'),
			ORM_DecimalField('weight', 4)			->setDescription('Weight in Kg'),
			ORM_StringField('department')			->setDescription('2 first chars of postal code')
		);
		
		$this->hasOne('country', $Crm->CountrySetClassName());
	}
	

	
	/**
	 * Get shipping amount from the shipping scale
	 * 
	 * @param string 	$department
	 * @param int	 	$country
	 * @param float 	$weight
	 * 
	 * @return string		as stored in shopping cart (decimal)
	 */
	public function getAmount($department, $country, $weight)
	{
		bab_debug("Shipping cost for department=$department, country=$country, weight=$weight Kg");
		
		// chercher le plus petit poids dans le bareme supperieur au poids du panier
		// pour deux poinds egaux, utiliser le plus petit montant
		
		$res = $this->select($this->department->is($department)->_AND_($this->country->is($country))->_AND_($this->weight->greaterThanOrEqual($weight)));
		$res->orderAsc($this->weight)->orderAsc($this->shipping_amount);
		
		if ($res->count() == 0)
		{
			// nothing found for a specific department, use other department
		
			$department = '';
		
			$res = $this->select($this->department->is($department)->_AND_($this->country->is($country))->_AND_($this->weight->greaterThanOrEqual($weight)));
			$res->orderAsc($this->weight)->orderAsc($this->shipping_amount);
		
			if ($res->count() == 0)
			{
				// nothing found for a specific country, use other countries
		
				$department = '';
				$country = 0;
		
				$res = $this->select($this->department->is($department)->_AND_($this->country->is($country))->_AND_($this->weight->greaterThanOrEqual($weight)));
				$res->orderAsc($this->weight)->orderAsc($this->shipping_amount);
		
				if ($res->count() == 0)
				{
					return null;
				}
			}
		}
		
		
		
		foreach($res as $shippingscale)
		{
			return $shippingscale->shipping_amount;
		}
		
		return null;
	}

}

/**
 * A crm_ShippingScale
 * 
 * @property int				$id
 * @property string				$shipping_amount
 * @property string				$weight
 * @property string				$department
 * @property crm_Country		$country
 */
class crm_ShippingScale extends crm_Record
{
	/**
	 * get weight with unit for display
	 * @return string
	 */
	public function getWeight()
	{
		$decimal = $this->weight;
	
		if (!preg_match('/[^\.0]/', $decimal))
		{
			return '0';
		}
	
		$weight = (float) $decimal;
	
		if ($weight < 0.09)
		{
			// get weight in grammes
	
			$g = round($weight * 1000);
	
			return $g.' g';
	
		} else {
			// get weight in Kg
	
			return round($weight, 4). ' Kg';
		}
	}
}
