<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * A crm_Catalog is a category of articles (products).
 *
 * @property ORM_StringField	$name
 * @property ORM_TextField		$description
 * @property ORM_BoolField		$disabled
 * @property ORM_StringField	$page_title
 * @property ORM_EnumField		$catalog_displaymode
 * @property ORM_EnumField		$catalogitem_displaymode
 * @property ORM_IntField		$sortkey
 * @property ORM_TextField		$page_description
 * @property ORM_TextField		$page_keywords
 * @property ORM_StringField	$image_alt
 * @property crm_CatalogSet		$parentcatalog
 */
class crm_CatalogSet extends crm_TraceableRecordSet
{

	public function __construct($crm = null)
	{
		parent::__construct($crm);

		$this->setDescription('Catalog');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name'),
			ORM_TextField('description')
					->setDescription('Description'),
			ORM_BoolField('disabled')
					->setDescription('Disabled'),
			ORM_EnumField('catalog_displaymode', crm_CatalogSet::getDisplayModes()),
			ORM_EnumField('catalogitem_displaymode', crm_CatalogSet::getDisplayModes()),
				
			ORM_IntField('sortkey'),
			ORM_IntField('refcount')->setDescription('Number of product in catalog and sub-catalogs'),
				
			ORM_StringField('page_title')
					->setDescription('Page title used for referencing'),
			ORM_TextField('page_description')
					->setDescription('Page description used for referencing'),
			ORM_TextField('page_keywords')
					->setDescription('Page keywords used for referencing'),
			ORM_StringField('image_alt')
					->setDescription('Alt text for the main article image')
		);
		
		$this->hasOne('parentcatalog', $this->Crm()->CatalogSetClassName());
	}
	
	
	public static function getDisplayModes()
	{
		
		return array(
			0 => crm_translate('Icon view'),
			1 => crm_translate('List view')
		);
	}
	


	/**
	 * Get access rights verification default criteria for catalogs
	 * @return ORM_Criteria
	 */
	public function getAccessibleCatalogs()
	{
		return $this->disabled->is(0);
	}
	
	
	/**
	 * 
	 * @param int 		$parentcatalog
	 * @param bool 		$all				false : display only accessibles catalogs
	 * @param string 	$padding
	 * 
	 * @return array
	 */
	public function getCatalogLevel($parentcatalog, $all = false, $padding = '')
	{
		$options = array();
		
		$criteria = $this->parentcatalog->is($parentcatalog);
		
		if ($all === false)
		{
			$criteria->_AND_($this->getAccessibleCatalogs());
		}
		
		$res = $this->select($criteria);
		$res->orderAsc($this->sortkey);
		
		foreach($res as $catalog)
		{
			$options[$catalog->id] = $padding.$catalog->name;
			$options += $this->getCatalogLevel($catalog->id, $all, str_repeat(bab_nbsp(), 5).$padding);
		}
		
		return $options;
	}


	/**
	 * Get accessible catalog as an options array for a widget_select
	 * @see Widget_Select
	 * @return array
	 */
	public function getOptions()
	{
		$options = array('' => '');
		$options += $this->getCatalogLevel(0, false);
		return $options;
	}


	/**
	 * Get all catalog as an options array for a widget_select
	 * @see Widget_Select
	 * @return array
	 */
	public function getAllOptions()
	{
		$options = array('' => '');
		$options += $this->getCatalogLevel(0, true);
		return $options;
	}
	
	
	
	/**
	 * Saves a record and keeps traces of the user doing it.
	 *
	 * @param crm_Catalog	$record			The record to save.
	 * @param bool			$noTrace		True to bypass the tracing of modifications.
	 * 
	 * @throws crm_SaveException
	 *
	 * @return boolean				True on success, false otherwise
	 */
	public function save(ORM_Record $record, $noTrace = false)
	{
		// the parentcatalog cannot be under or equal to id
		
		if (0 !== (int) $record->parentcatalog && !empty($record->id))
		{
			$subtree = $this->getCatalogLevel($record->id, true);
			$subtree[$record->id] = $record->name;
			
			if (isset($subtree[$record->parentcatalog]))
			{
				$e = new crm_SaveException($this->Crm()->translate('The parent category cannot be under the current category'));
				$e->redirect = false;
				
				throw $e;
			}
		}
		
		
		// auto define sortkey
		
		if (0 === (int) $record->sortkey)
		{
			$sortkey = $this->getMaxSortkeyInCatalog($record->parentcatalog);
			$record->sortkey = 1 + $sortkey;
			
			// pad other records
			
			global $babDB;
			$Crm = $this->Crm();
			$babDB->db_query('UPDATE '.$babDB->backTick($Crm->classPrefix.'catalog').' SET sortkey=sortkey+1 WHERE sortkey > '.$babDB->quote($sortkey));
		}
		
		
		
		$savereturn = parent::save($record, $noTrace);
		
		if (false === $noTrace)
		{
			$this->updateRefCount();
		}

		return $savereturn;
	}
	
	
	
	/**
	 * Update all catalogs refcount
	 * return true if refcount has been modified in one of the first-level catalogs
	 * @return bool
	 */
	public function updateRefCount()
	{
		$mod = false;
		$res = $this->select($this->parentcatalog->is(0));
		foreach($res as $catalog)
		{
			/*@var $catalog crm_Catalog */
			
			$old_value = $catalog->refcount;
			$new_value = $catalog->updateRefCount();
			
			if (!$mod && $old_value !== $new_value)
			{
				$mod = true;
			}
		}
		
		return $mod;
	}
	
	
	
	
	/**
	 * Get the max sortkey in the same parentcatalog, used to insert a new catalog at the end of parentcatalog
	 * 
	 * @param int $parentcatalog
	 * 
	 * @return int
	 */
	private function getMaxSortkeyInCatalog($parentcatalog)
	{
		$res = $this->select($this->parentcatalog->is($parentcatalog));
		$res->orderDesc($this->sortkey);
		$res->rewind();
		
		if ($res->valid())
		{
			$previous_sibbling = $res->current();
			
			// auto fix incorrect database :
			
			if (!$previous_sibbling->sortkey)
			{
				if (0 === (int) $parentcatalog)
				{
					return 0;
				}
				
				$parent = $this->get($parentcatalog);
				return $parent->sortkey;
			}
			
			
			// check if the node have children
			
			return $this->getMaxSortkeyInCatalog($previous_sibbling->id);
			
		} elseif (0 === (int) $parentcatalog) {
			
			return 0;
			
		} else {
			// no sibbling, get the parentcatalog sortkey
			
			$parent = $this->get($parentcatalog);
			return $parent->sortkey;
		}

	}
	
	
	/**
	 * Test if an empty catalog should be displayed on page
	 * @return bool
	 */
	public function displayEmpty()
	{
		static $display_empty_categories = null;
		
		if (null === $display_empty_categories)
		{
		
			$Crm = $this->Crm();
			$addonname = $Crm->getAddonName();
			
			$registry = bab_getRegistryInstance();
			$registry->changeDirectory("/$addonname/configuration/");
			
			$display_empty_categories = (bool) $registry->getValue('display_empty_categories');
		}
		
		return $display_empty_categories;
	}
	
	
	
	
	/**
	 * Select catalog and searchcatalog in a sorted array
	 * @return array
	 */
	public function selectFromParent($parentcatalog, $empty_catalogs = null)
	{
		if (null === $empty_catalogs)
		{
			$empty_catalogs = $this->displayEmpty();
		}
		
		
		$Crm = $this->Crm();
		$list = array();
		
		$criteria = $this->parentcatalog->is($parentcatalog)->_AND_($this->getAccessibleCatalogs());
		if (!$empty_catalogs)
		{
			$criteria = $criteria->_AND_($this->refcount->greaterThan(0));
		}
		
		$res = $this->select($criteria);
		
		foreach($res as $catalog)
		{
			/*@var $catalog crm_Catalog  */
			$list[] = $catalog;
		}
		
		
		
		
		if (isset($Crm->SearchCatalog))
		{
			$vSet = $Crm->SearchCatalogSet();
			$res = $vSet->select($vSet->parentcatalog->is($parentcatalog));
			
			foreach($res as $searchcatalog)
			{
				if (!$empty_catalogs && $searchcatalog->isEmpty())
				{
					continue;
				}
				
				
				$list[] = $searchcatalog;
			}
		}
		
		bab_Sort::sortObjects($list, 'getSortKey');
		
		
		return $list;
	}
	
	
	/**
	 * Add catalogs to sitemap
	 * @param bab_eventBeforeSiteMapCreated $event
	 * @param int $parentcatalog
	 */
	public function addSitemapNodes(bab_eventBeforeSiteMapCreated $event, $parentcatalog, $path)
	{
		$Crm = $this->Crm();
			
		try {
			$res = $this->selectFromParent($parentcatalog);
		} catch(ORM_BackEndSelectException $e)
		{
			// do not add to sitemap if select failed
			return;
		}
		
		foreach($res as $catalog)
		{
			$nodeId = get_class($catalog).$catalog->id;
			
			$link = $event->createItem($nodeId);
			$link->setLabel($catalog->name);
			$link->setDescription($catalog->description);
			$link->setLink($Crm->Controller()->CatalogItem()->catalog($catalog->id)->url());
			$link->setPosition($path);
			$link->addIconClassname(Func_Icons::PLACES_FOLDER);
			
			$event->addFunction($link);
			
			if ($catalog instanceof crm_Catalog)
			{
				$nextpath = $path;
				$nextpath[] = $nodeId;
				$this->addSitemapNodes($event, $catalog->id, $nextpath);
			}
		}
	}
	

}


/**
 * A crm_Catalog is acatalog of items
 *
 * @property string		$name
 * @property string		$description
 * @property int		$disabled
 * @property int		$catalog_displaymode
 * @property int		$catalogitem_displaymode
 * @property int		$sortkey
 * @property string		$page_title
 * @property string		$page_description
 * @property string		$page_keywords
 * @property string		$image_alt
 * @property crm_Catalog	$parentcatalog
 */
class crm_Catalog extends crm_TraceableRecord
{

	/**
	 * @return bab_Path
	 */
	public function getPhotoFolder()
	{
		$ul = $this->uploadPath();
		$ul->push('photo');
		$ul->createDir();
		return $ul;
	}
	
	/**
	 * get photo path or null if no photo
	 * @return bab_Path
	 */
	public function getPhotoPath()
	{
		$uploadpath = $this->getPhotoFolder();

		if (!isset($uploadpath)) {
			return null;
		}

		$W = bab_Widgets();

		$uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

		if (!isset($uploaded)) {
			return null;
		}

		foreach($uploaded as $file) {
			return $file->getFilePath();
		}

		return null;
	}
	
	
	/**
	 * @return ORM_Iterator
	 */
	public function getCatalogs()
	{
		$set = $this->Crm()->CatalogSet();
		$res = $set->select($set->parentcatalog->is($this->id));
		
		$res->orderAsc($set->sortkey);
		
		return $res;
	}
	
	
	/**
	 * Get Accessibles catalogs using access rights, including empty catalogs
	 * @return ORM_Iterator
	 */
	public function getVisibleCatalogs()
	{
		$set = $this->getParentSet();
		$res = $set->select($set->getAccessibleCatalogs()->_AND_($set->parentcatalog->is($this->id)));
		
		$res->orderAsc($set->sortkey);
		
		
		
		return $res;
	}
	
	

	/**
	 * @return ORM_Iterator
	 */
	public function getArticles()
	{
		$this->Crm()->includeArticleSet();
		$set = $this->Crm()->CatalogItemSet();
		$set->article();
		return $set->select($set->catalog->is($this->id));
	}
	
	/**
	 * @return ORM_Iterator
	 */
	public function getVisibleArticles($sortcol = null, $sortmethod = null)
	{
		$this->Crm()->includeArticleSet();
		$set = $this->Crm()->CatalogItemSet();
		$set->catalog();
		$set->article();
		$set->article->joinHasOneRelations();
		if(isset($this->Crm()->Vat)){
			$set->article->addUnitCostField();
		}
		$res = $set->select($set->catalog->id->is($this->id)->_AND_($set->catalog->getAccessibleCatalogs())->_AND_($set->article->disabled->is(0)));
		
		if (null !== $sortcol && null !== $sortmethod)
		{
			$res->$sortmethod($set->article->$sortcol);
		}
		
		return $res;
	}
	
	
	/**
	 * Test if there are no article and catalogs
	 * @return boolean
	 */
	public function isEmpty()
	{
		if (0 === (int) $this->refcount)
		{
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Get an array of catalogs from root to current catalog
	 * @return array
	 */
	public function getAncestors()
	{
		$arr = array();
		$catalog = $this;
		
		do {
			array_unshift($arr, $catalog);
			$catalog = $catalog->parentcatalog();
		} while ($catalog && $catalog->id);
		
		return $arr;
	}
	
	/**
	 * Get textual representation of a catalog with ancestors
	 * @return string
	 */
	public function asText()
	{
		$text = '';
		$arr = $this->getAncestors();
		foreach($arr as $catalog)
		{
			$text .= $catalog->name.' > ';
		}
		
		return rtrim($text, '> ');
	}
	
	
	
	
	/**
	 * @return string
	 */
	public function getPageTitle()
	{
		if ($this->page_title)
		{
			return $this->page_title;
		}
	
		return $this->name;
	}
	
	/**
	 * @return string
	 */
	public function getPageDescription()
	{
		if ($this->page_description)
		{
			return $this->page_description;
		}
	
		return bab_abbr($this->description, BAB_ABBR_FULL_WORDS, 400);
	}
	
	/**
	 * @return string
	 */
	public function getPageKeywords()
	{
		return $this->page_keywords;
	}
	
	/**
	 * @return string
	 */
	public function getRewritenUrl()
	{
		$Crm = $this->Crm();
		
		if (!$Crm->rewriting)
		{
			return $Crm->Controller()->CatalogItem()->catalog($this->id)->url();
		}
		$nodeId = $Crm->classPrefix.'Catalog'.$this->id;
		return bab_siteMap::rewrittenUrl($nodeId);
	}
	
	
	/**
	 * Set sortkey recursively
	 * @param 	int 	$sortkey
	 * @return int	last sortkey
	 */
	public function setSortKey($sortkey)
	{
		$this->sortkey = $sortkey;
		$res = $this->getCatalogs();
		foreach($res as $childnode)
		{
			$sortkey++;
			$sortkey = $childnode->setSortKey($sortkey);
		}
		
		$this->save();
		return $sortkey;
	}
	
	
	public function getSortKey()
	{
		return $this->sortkey;
	}
	
	/**
	 * Update refcount recursively
	 * @return int
	 */
	public function updateRefCount()
	{
		$Crm = $this->Crm();
		$refcount = 0;
		
		
		static $ciSet = null;
		if (null === $ciSet)
		{
			$ciSet = $Crm->CatalogItemSet();
			$ciSet->article();
		}
		
		$res = $ciSet->select($ciSet->article->disabled->is('0')->_AND_($ciSet->catalog->is($this->id)));
		// bab_debug($res->getSelectQuery());
		$refcount += $res->count();
	
		$res = $this->getVisibleCatalogs();
		foreach($res as $catalog)
		{
			$refcount += $catalog->updateRefCount();
		}
		
		
		//bab_debug($this->name.' '.$this->refcount.' '.$refcount);
		
		if ($refcount !== (int) $this->refcount)
		{
			$this->refcount = $refcount;
			$this->save(true);
			
			bab_debug('Save refcount modification in catalog : '.$this->name);
		}
		
		return $refcount;
	}
}