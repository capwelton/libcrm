<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * @property	ORM_EnumField		$criterion
 * @property	ORM_StringField		$action
 * @property	crm_RoleSet		$role
 */
class crm_RoleAccessSet extends crm_RecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_EnumField('criterion', $Crm->translateArray(crm_RoleAccess::$conditions))
                ->setDescription('Condition'),
            ORM_StringField('action')
                ->setDescription('Access to what') // 'contact:create'
        );

        $this->hasOne('role', $Crm->RoleSetClassName());
    }
}



/**
 * @property	string			$criterion
 * @property	string			$action
 * @property	crm_Role		$role
 */
class crm_RoleAccess extends crm_Record
{
    const ACCESS_ALWAYS = 'always';
    const ACCESS_NEVER = 'never';
    const ACCESS_OWN = 'own';
    const ACCESS_DEPARTMENT = 'department';
    const ACCESS_ORGANIZATION = 'organization';
    const ACCESS_PARENT_ORGANIZATION = 'parentorganization';
    const ACCESS_GRAND_PARENT_ORGANIZATION = 'grandparentorganization';
    const ACCESS_GREAT_GRAND_PARENT_ORGANIZATION = 'greatgrandparentorganization';
    const ACCESS_SECTOR = 'sector';

    public static $conditions =	array(
        crm_RoleAccess::ACCESS_ALWAYS => 'Always',
        crm_RoleAccess::ACCESS_NEVER => 'Never',
        crm_RoleAccess::ACCESS_OWN => 'Own',
        crm_RoleAccess::ACCESS_DEPARTMENT => 'Own department',
        crm_RoleAccess::ACCESS_ORGANIZATION => 'Own organization',
        crm_RoleAccess::ACCESS_PARENT_ORGANIZATION => 'Own parent organization',
        crm_RoleAccess::ACCESS_GRAND_PARENT_ORGANIZATION => 'Own grand parent organization',
        crm_RoleAccess::ACCESS_GREAT_GRAND_PARENT_ORGANIZATION => 'Own great grand parent organization',
        crm_RoleAccess::ACCESS_SECTOR => 'Own sector',
    );
}

