<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Order set
 *
 * @property ORM_PkField			$id
 * @property ORM_StringField		$name
 * @property ORM_StringField		$billingperson
 * @property ORM_StringField		$shippingperson
 * @property ORM_StringField		$deliveryperson
 * @property ORM_EnumField			$type
 * @property ORM_EnumField			$recipienttype
 * @property ORM_DateField          $request_date
 * @property ORM_DecimalField		$total_df
 * @property ORM_DecimalField		$total_ti
 * @property ORM_TextField			$gift_comment
 * @property ORM_TextField			$shipped_comment
 *
 * @property ORM_TextField			$header
 * @property ORM_TextField			$footer
 *
 * @property ORM_StringField 		$billingorganizationname
 * @property ORM_StringField 		$shippingorganizationname
 * @property ORM_StringField 		$deliveryorganizationname
 *
 * @property crm_OrganizationSet	$billingorganization
 * @property crm_OrganizationSet	$shippingorganization
 * @property crm_OrganizationSet	$deliveryorganization
 *
 * @property crm_AddressSet			$billingaddress
 * @property crm_AddressSet			$shippingaddress
 * @property crm_AddressSet			$deliveryaddress
 * @property crm_ContactSet			$contact
 * @property crm_CurrencySet        $currency
 * @property crm_ShoppingCartSet 	$shoppingcart
 * @property crm_CollectStoreSet    $collectstore
 */
class crm_OrderSet extends crm_TraceableRecordSet
{

    /**
     * @var string
     */
	private $orderNamePrefix = null;

	/**
	 * @var int
	 */
	private $nextInvoiceNumber = null;



	public function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);


		$this->setDescription('Order');

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name or invoice number'),			// this value must be unique


			ORM_StringField('billingorganizationname')
					->setDescription('Billing organization name'),

			ORM_StringField('shippingorganizationname')
					->setDescription('Shipping organization name'),

			ORM_StringField('deliveryorganizationname')
					->setDescription('Delivery organization name'),

			ORM_StringField('billingperson')
					->setDescription('Bill recipient title'),

			ORM_StringField('shippingperson')
					->setDescription('sender title'),

			ORM_StringField('deliveryperson')
					->setDescription('Delivery recipient'),

			ORM_EnumField('type', crm_Order::getTypes())
					->setDescription('Order type'),

			ORM_EnumField('recipienttype', crm_Order::getRecipientTypes())
					->setDescription('Recipient type'),

			ORM_DateField('request_date')
				->setDescription('Invoice request date'),

			ORM_DateField('formal_notice_date')
				->setDescription('Formal notice date for non payment'),

			ORM_BoolField('ignorevalue')
				->setDescription('Ignore amount in sales (not for value)'),		// ex : facture a titre gratuit

			ORM_DecimalField('total_df', 2)
					->setDescription('Total excluding taxes (HT), including reductions on products, excluding shipping cost, excluding global reduction'),

			ORM_DecimalField('total_ti', 2)
					->setDescription('Total including taxes (TTC)'),

			ORM_TextField('header')->setDescription('Comment to display on top of order'),
			ORM_TextField('footer')->setDescription('Comment to display at the bottom of order'),

			ORM_StringField('autorization_id')->setDescription('Autorization ID of associated payment'),
			ORM_StringField('transaction_id')->setDescription('Transaction ID of associated payment'),

			ORM_StringField('delivery')
				->setDescription('delivery method, empty if no delivery method selected'),
			ORM_DatetimeField('shipped_date'),
			ORM_TextField('gift_comment')
				->setDescription('A comment set by customer, this message will be joined to the package for the final recipient'),
			ORM_TextField('shipped_comment')
				->setDescription('A comment set by manager to provide informations to the customer about the package associated to the shopping cart')
		);

		if (isset($Crm->Organization))
		{
			$this->hasOne('billingorganization', $Crm->OrganizationSetClassName());			// facturation
			$this->hasOne('shippingorganization', $Crm->OrganizationSetClassName());		// expedition
			$this->hasOne('deliveryorganization', $Crm->OrganizationSetClassName());		// reception
		}

		$this->hasOne('billingaddress', $Crm->AddressSetClassName());
		$this->hasOne('shippingaddress', $Crm->AddressSetClassName());
		$this->hasOne('deliveryaddress', $Crm->AddressSetClassName());

		if (isset($Crm->CollecStore)) {
		    // click and collect
		    $this->hasOne('collectstore', $Crm->AddressSetClassName());
		}

		if (isset($Crm->ShoppingCart))
		{
			$this->hasOne('shoppingcart', $Crm->ShoppingCartSetClassName());
		}

		if (isset($Crm->Contact))
		{
			$this->hasOne('contact', $Crm->ContactSetClassName());	// the main customer contact, inherited from shopping cart if the order has been created from a shopping cart
		}

		if (isset($Crm->Currency))
		{
			$this->hasOne('currency', $Crm->CurrencySetClassName());
		}


		$this->hasMany('orderitems', $Crm->OrderItemSetClassName(), 'parentorder')
			->setOnDeleteMethod(ORM_ManyRelation::ON_DELETE_NO_ACTION);

		$this->hasMany('ordertaxs', $Crm->OrderTaxSetClassName(), 'parentorder')
			->setOnDeleteMethod(ORM_ManyRelation::ON_DELETE_NO_ACTION);
	}


	/**
	 * Set prefix for order name
	 *
	 * @param	string	$prefix
	 */
	public function setOrderNamePrefix($prefix)
	{
		$this->orderNamePrefix = $prefix;
		return $this;
	}


	/**
	 * Delete accounting document
	 * delete the associated shopping cart if exists
	 *
	 * @see crm_TraceableRecordSet::delete($criteria, $definitive)
	 *
	 * @param ORM_Criteria	$criteria		The criteria for selecting the records to delete.
	 * @param bool			$definitive		True to delete permanently the record.
	 *
	 * return boolean
	 */
	public function delete(ORM_Criteria $criteria = null, $definitive = false)
	{
		if (false !== $definitive)
		{
			throw new Exception('Definitive delete of order is not allowed');
			// en cas ne suppression definitive, il faut supprimer les orderitem, ordertax, order addresses
		}

		$Crm = $this->Crm();
		if (isset($Crm->ShoppingCart)) {
			$Crm->includeShoppingCartSet();

			$res = $this->select($criteria);
			foreach ($res as $order) {
				/*@var $order crm_Order */
				if ($order->shoppingcart) {
					if ($order->shoppingcart instanceof crm_ShoppingCart) {
						$shoppingCart = $order->shoppingcart;
					} else {
						$shoppingCart = $order->shoppingcart();
					}

					if (!empty($shoppingCart->id)) {
						$shoppingCart->emptyCart();
						$shoppingCart->delete();
					}
				}
			}
		}

		parent::delete($criteria, $definitive);
	}




	/**
	 * save order, generate name if empty or throw exception if duplicate
	 *
	 * @param ORM_Record   $record     The record to save.
	 * @param bool         $noTrace    True to bypass the tracing of modifications.
	 * @return boolean     True on success, false otherwise
	 */
	public function save(ORM_Record $record, $noTrace = false)
	{
		$name = (string) $record->name;

		if ('' === $name)
		{
			$record->name = $this->getNewName($record, $this->orderNamePrefix);
		}

		// check for duplicate for number manually set on documents

		if (null !== $this->get($this->name->trim('0', ORM_TrimOperation::LEADING)->like(ltrim($record->name,'0'))->_AND_($this->id->isNot($record->id))))
		{
			throw new crm_OrderDuplicateException(sprintf('The entry %s already exists', $record->name));
		}


		$return = parent::save($record, $noTrace);

		if ($return) {
		    $this->saveNextInvoiceNumber();
		}

		return $return;
	}


	protected function saveNextInvoiceNumber()
	{
	    if (isset($this->nextInvoiceNumber)) {
	        $addonname = $this->Crm()->getAddonName();

	        $registry = bab_getRegistryInstance();
	        $registry->changeDirectory("/$addonname/configuration/");

	        $registry->setKeyValue('nextInvoiceNumber', $this->nextInvoiceNumber);
	    }
	}


	/**
	 * Generate new name for order
	 * @param	crm_Order	$order 	this order is not created in table
	 * @param	string		$prefix	prefix used in order name
	 * @return string
	 */
	protected function getNewName(crm_Order $order, $prefix = null)
	{
	    $addonname = $this->Crm()->getAddonName();

	    $registry = bab_getRegistryInstance();
	    $registry->changeDirectory("/$addonname/configuration/");
		$this->nextInvoiceNumber = $registry->getValue('nextInvoiceNumber', 1);

		do {
			$name = date('Ymd').'_'.$this->nextInvoiceNumber;
			$duplicate = $this->get($this->name->like($name));
			$this->nextInvoiceNumber++;

		} while($duplicate);

		return $name;
	}
}


/**
 * Order
 *
 * @property string					$name
 * @property string					$billingperson
 * @property string					$shippingperson
 * @property string					$deliveryperson
 * @property int					$type
 * @property int					$recipienttype
 * @property string                 $request_date
 * @property string					$total_df
 * @property string					$total_ti
 * @property string					$gift_comment
 * @property string					$shipped_comment
 *
 * @property string                 $header
 * @property string                 $footer

 * @property crm_Organization		$billingorganization
 * @property crm_Organization		$shippingorganization
 * @property crm_Organization		$deliveryorganization
 *
 * @property crm_Address			$billingaddress
 * @property crm_Address			$shippingaddress
 * @property crm_Address			$deliveryaddress
 *
 * @property crm_Contact			$contact
 * @property crm_Currency			$currency
 *
 * @property crm_Contact			$contact
 * @property crm_ShoppingCart		$shoppingcart
 * @property crm_CollectStore       $collectstore
 *
 */
class crm_Order extends crm_TraceableRecord
{
    /**
     * @deprecated use crm_Order::TYPE_DELIVERY_ORDER
     */
    const DELIVERY_ORDER        = 1;
    /**
     * @deprecated use crm_Order::TYPE_PURCHASE_ORDER
     */
    const SUPPLY_ORDER          = 2;
    /**
     * @deprecated use crm_Order::TYPE_PURCHASE_ORDER
     */
    const PURCHASE_ORDER        = 2;
    /**
     * @deprecated use crm_Order::TYPE_PRO_FORMA_REQUEST
     */
    const PRO_FORMA_REQUEST     = 3;
    /**
     * @deprecated use crm_Order::TYPE_PRO_FORMA
     */
    const PRO_FORMA             = 4;
    /**
     * @deprecated use crm_Order::TYPE_ORDER_REQUEST
     */
    const ORDER_REQUEST         = 5;
    /**
     * @deprecated use crm_Order::TYPE_ORDER
     */
    const ORDER                 = 6;
    /**
     * @deprecated use crm_Order::TYPE_INVOICE
     */
    const INVOICE               = 7;
    /**
     * @deprecated use crm_Order::TYPE_STORE_TRANSFER_IN
     */
    const STORE_TRANSFER_IN     = 8;
    /**
     * @deprecated use crm_Order::TYPE_STORE_TRANSFER_OUT
     */
    const STORE_TRANSFER_OUT    = 9;
    /**
     * @deprecated use crm_Order::TYPE_ESTIMATE
     */
    const ESTIMATE              = 10;
    /**
     * @deprecated use crm_Order::TYPE_QUOTATION
     */
    const QUOTATION             = 11;
    /**
     * @deprecated use crm_Order::TYPE_PRODUCTION_ORDER
     */
    const PRODUCTION_ORDER      = 13;
    /**
     * @deprecated use crm_Order::TYPE_CREDIT_NOTE
     */
    const CREDIT_NOTE           = 14;


    /**
     * @var integer
     * @since 1.0.37
     */
    const TYPE_DELIVERY_ORDER       = 1;
    const TYPE_PURCHASE_ORDER       = 2;
    const TYPE_PRO_FORMA_REQUEST    = 3;
    const TYPE_PRO_FORMA            = 4;
    const TYPE_ORDER_REQUEST        = 5;
    const TYPE_ORDER                = 6;
    const TYPE_INVOICE              = 7;
    const TYPE_STORE_TRANSFER_IN    = 8;
    const TYPE_STORE_TRANSFER_OUT   = 9;
    const TYPE_ESTIMATE             = 10;
    const TYPE_QUOTATION            = 11;
    const TYPE_PRODUCTION_ORDER     = 13;
    const TYPE_CREDIT_NOTE          = 14;



    const CUSTOMER              = 1;
    const SUPPLIER              = 2;


    /**
     * Types of orders
     * @return array
     */
    public static function getTypes()
    {
        return array(
            self::TYPE_DELIVERY_ORDER       => crm_translate('Delivery order'),
            self::TYPE_PURCHASE_ORDER       => crm_translate('Purchase order'),
            self::TYPE_PRO_FORMA_REQUEST    => crm_translate('Pro forma request'),
            self::TYPE_PRO_FORMA            => crm_translate('Pro forma'),
            self::TYPE_ORDER_REQUEST        => crm_translate('Order request'),
            self::TYPE_ORDER                => crm_translate('Sales order'),
            self::TYPE_INVOICE              => crm_translate('Invoice'),
            self::TYPE_STORE_TRANSFER_IN    => crm_translate('Store transfer in'),
            self::TYPE_STORE_TRANSFER_OUT   => crm_translate('Store transfer out'),
            self::TYPE_ESTIMATE             => crm_translate('Estimate'),
            self::TYPE_QUOTATION            => crm_translate('Quotation'),
            self::TYPE_PRODUCTION_ORDER     => crm_translate('Production order'),
            self::TYPE_CREDIT_NOTE          => crm_translate('Credit note')
        );
    }



    /**
     * Types of orders
     * @return array
     */
    public static function getListTitle()
    {
        return array(
            self::TYPE_DELIVERY_ORDER       => crm_translate('Delivery orders'),
            self::TYPE_PURCHASE_ORDER       => crm_translate('Purchase orders'),
            self::TYPE_PRO_FORMA_REQUEST    => crm_translate('Pro forma requests'),
            self::TYPE_PRO_FORMA            => crm_translate('Pro forma'),
            self::TYPE_ORDER_REQUEST        => crm_translate('Order requests'),
            self::TYPE_ORDER                => crm_translate('Orders'),
            self::TYPE_INVOICE              => crm_translate('Invoices'),
            self::TYPE_STORE_TRANSFER_IN    => crm_translate('Store transfers in'),
            self::TYPE_STORE_TRANSFER_OUT   => crm_translate('Store transfers out'),
            self::TYPE_ESTIMATE             => crm_translate('Estimates'),
            self::TYPE_QUOTATION            => crm_translate('Quotations'),
            self::TYPE_PRODUCTION_ORDER     => crm_translate('Production orders'),
            self::TYPE_CREDIT_NOTE          => crm_translate('Credit notes')
        );
    }


    public function getRecordTitle()
    {
        if ($this->name) {
            return $this->name;
        }

        $types = $this->getTypes();
        if (!isset($types[$this->type])) {
            return crm_translate('Unknown document');
        }

        return $types[$this->type];
    }



	/**
	 * recipients types for orders
	 * used to set items prices, customer order will use customer tarif and supplier order will use supplier prices
	 *
	 * @return array
	 */
	public static function getRecipientTypes()
	{
		return array(
			self::CUSTOMER => crm_translate('Customer'),
			self::SUPPLIER => crm_translate('Supplier')
		);
	}

	/**
	 * @return crm_OrderItem[]
	 */
	public function getItems()
	{
		$set = $this->Crm()->OrderItemSet();
		$I = $set->select($set->parentorder->is($this->id));

		$I->orderAsc($set->sortkey)->orderAsc($set->name);

		return $I;
	}


	/**
	 * Get order items with a reference in an array with reference as key
	 * @return Array
	 */
	public function getItemsByReference()
	{
		$return = array();
		foreach($this->getItems() as $orderItem)
		{
			if ($orderItem->reference)
			{
				$return[$orderItem->reference] = $orderItem;
			}
		}
		return $return;
	}


	/**
	 * @return ORM_Iterator <crm_OrderTax>
	 */
	public function getTaxes()
	{
		$set = $this->Crm()->OrderTaxSet();
		$I = $set->select($set->parentorder->is($this->id));

		$I->orderAsc($set->sortkey)->orderAsc($set->name);

		return $I;
	}





	/**
	 * Create billing address from organization
	 */
	public function copyBillingAddress()
	{
		if (null === $this->billingorganization || empty($this->billingorganization)) {
			return;
		}


		if ($this->billingorganization instanceOf crm_Organization) {
			$org = $this->billingorganization;
			/*@var $org crm_Organization */
			$billing = $org->getBestBillingAddress($this);
		} else {
			$oSet = $this->Crm()->OrganizationSet();
			$org = $oSet->get($this->billingorganization);
			if (!$org)
			{
				$oSet->__destruct();
				unset($oSet);
				return;
			}

			$billing = $org->getBestBillingAddress($this);
		}

		if (isset($org) && empty($this->billingorganizationname)) {
			$this->billingorganizationname = $org->getName();
		}

		if (isset($billing)) {

			if ($this->billingaddress instanceOf crm_Address) {

				$address = clone $billing;
				$address->id = null;
				$this->billingaddress = $address;

			} else {

				$address = clone $billing;
				$address->id = null;
				$address->save();

				$this->billingaddress = $address->id;
				$address->__destruct();
				unset($address);
			}
		}

		if (isset($oSet)) {
			$oSet->__destruct();
			unset($oSet);
		}
	}






	/**
	 * Create delivery address from delivery organization
	 */
	public function copyDeliveryAddress()
	{
		if (null === $this->deliveryorganization || empty($this->deliveryorganization)) {
			return;
		}


		if ($this->deliveryorganization instanceOf crm_Organization) {
			$org = $this->deliveryorganization;
			$mainaddress = $org->getMainAddress();
		} else {
			$oSet = $this->Crm()->OrganizationSet();
			$org = $oSet->get($this->deliveryorganization);
			if (!$org)
			{
				$oSet->__destruct();
				unset($oSet);
				return;
			}
			$mainaddress = $org->getMainAddress();
		}

		if (isset($org) && empty($this->deliveryorganizationname)) {
			$this->deliveryorganizationname = $org->getName();
		}

		if (isset($mainaddress)) {

			if ($this->deliveryaddress instanceOf crm_Address) {

				$address = clone $mainaddress;
				$address->id = null;
				$this->deliveryaddress = $address;

			} else {

				$address = clone $mainaddress;
				$address->id = null;
				$address->save();

				$this->deliveryaddress = $address->id;

				$address->__destruct();
				unset($address);
			}
		}

		if (isset($oSet)) {
			$oSet->__destruct();
			unset($oSet);
		}
	}



	/**
	 * Set billing organization and address from contact
	 *
	 */
	public function setBillingFromContact(crm_Contact $contact)
	{
		$org = $contact->getMainOrganization();
		if (null !== $org)
		{
			$this->billingorganization = $org->id;
		}

		$contactaddress = $contact->getBillingAddress();
		if (isset($contactaddress))
		{
			$address = clone $contactaddress;
			$address->id = null;
			$this->billingaddress = $address;
		}

		if (!empty($this->billingaddress->recipient))
		{
			$this->billingperson = $this->billingaddress->recipient;
		} else {
			$this->billingperson = $contact->getFullName();
		}

		return $this;
	}



	/**
	 * Set delivery organization and address from contact
	 *
	 */
	public function setDeliveryFromContact(crm_Contact $contact)
	{
	    $org = $contact->getMainOrganization();
	    if (null !== $org)
	    {
	        $this->deliveryorganization = $org->id;
	    }

	    $contactaddress = $contact->getDeliveryAddress();
	    if (isset($contactaddress))
	    {
	        $address = clone $contactaddress;
	        $address->id = null;
	        $this->deliveryaddress = $address;
	    }

	    if (!empty($this->deliveryaddress->recipient))
	    {
	        $this->deliveryperson = $this->deliveryaddress->recipient;
	    } else {
	        $this->deliveryperson = $contact->getFullName();
	    }

	    return $this;
	}




	/**
	 * set delivery organization and address from a contact
	 *
	 */
	public function setDeliveryFromShoppingCart(crm_ShoppingCart $cart)
	{
	    $Crm = $this->Crm();

	    $deliveryaddress = $cart->getDeliveryAddress();
	    $contact = $cart->getContact();

		$org = $contact->getMainOrganization();
		if (null !== $org)
		{
			$this->deliveryorganization = $org->id;
		}

		$contactaddress = $contact->getDeliveryAddress();
		$deliveryperson = $contact->getFullName();

		if (isset($deliveryaddress))
		{
			$address = clone $deliveryaddress;
			$address->id = null;
			$this->deliveryaddress = $address;

			if (!empty($address->recipient))
			{
				$deliveryperson = $address->recipient;
			}
		}
		$this->deliverycontact = $contact->id;
		$this->deliveryperson = $deliveryperson;

		return $this;
	}




	/**
	 * refresh computed values of order
	 *
	 */
	public function refreshTotal()
	{
		$sub_total_df = 0;
		$sub_total_vat = 0;
		$sub_total_taxes = 0;

		if ($this->id)
		{

			$oSet = $this->Crm()->OrderItemSet();
			$res = $oSet->select($oSet->parentorder->is($this->id));
			foreach($res as $item)
			{
				$quantity 	= (float) $item->quantity;
				$unit_cost 	= (float) $item->unit_cost;
				$vat 		= (float) $item->vat;

				$df			= $quantity * $unit_cost;

				$sub_total_df 	+= $df;

				if ('0.0000' !== $item->vat) {
					$sub_total_vat	+= ($df * $vat / 100);
				}

				$item->__destruct();
				unset($item);
			}

			$oSet->__destruct();
			unset($oSet);

			$taxSet = $this->Crm()->OrderTaxSet();
			$vatRecord = $taxSet->get($taxSet->parentorder->is($this->id)->_AND_($taxSet->reference->is('vat')));


			// update VAT tax
			if ($sub_total_vat) {

				if (null === $vatRecord)
				{
					$vatRecord = $taxSet->newRecord();
					$vatRecord->reference = 'vat';
					$vatRecord->sortkey = 1; // place la TVA avant le frais de port
					$vatRecord->name = $this->Crm()->translate('Value added tax');
					$vatRecord->description = '';
					$vatRecord->parentorder = $this->id;
					$vatRecord->readonly = true;
				}

				if ($sub_total_vat !== (float) $vatRecord->amount)
				{
					$vatRecord->amount = $sub_total_vat;
					$vatRecord->save();
				}

				$vatRecord->__destruct();
				unset($vatRecord);

			} else {

				if (null !== $vatRecord)
				{
					$taxSet->delete($taxSet->id->is($vatRecord->id));

					$vatRecord->__destruct();
					unset($vatRecord);
				}

			}


			$res = $taxSet->select($taxSet->parentorder->is($this->id));
			foreach($res as $tax)
			{
				$sub_total_taxes += (float) $tax->amount;

				$tax->__destruct();
				unset($tax);
			}

			$taxSet->__destruct();
			unset($taxSet);
		}

		$this->total_df = $sub_total_df;
		$this->total_ti = $sub_total_df + $sub_total_taxes;
	}






	/**
	 * Format number as money with currency
	 *
	 * @param	float | string numeric		$money
	 * @return	string
	 */
	public function moneyFormat($money)
	{
		$str = $this->Crm()->numberFormat($money);

		if (null === $this->currency)
		{
			return $str.bab_nbsp().$this->Crm()->Ui()->Euro();
		}

		if (!($this->currency instanceof crm_Currency))
		{
			throw new Exception('missing join on currency');
			return $str;
		}

		$str .= bab_nbsp(). $this->currency->symbol;

		return $str;
	}


    /**
     * @return bab_Path
     */
    protected function createTmpPdfFilePath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $tmpfile = $this->name.'.pdf';

        $addon = bab_getAddonInfosInstance('LibCrm');
        $path = new bab_Path($addon->getUploadPath());
        $path->push('tmp');
        $path->createDir();

        $path->push($tmpfile);

        // unlink this file at the end of page

        register_shutdown_function('unlink', $path->toString());

        return $path;
    }


	/**
	 * Record PDF to disk
	 * need join on billingaddress, deliveryaddress, shippingaddress
	 *
	 * @param	bab_Path	$path		full path name of file to save
	 * 									if null, the file will be a temporary file deleted at the end of page
	 * @return bab_Path
	 */
	public function exportPdf(bab_Path $path = null)
	{
		$Ui = $this->Crm()->Ui();

		if (null === $path) {
			$path = $this->createTmpPdfFilePath();
		}

		$pdf = $Ui->OrderPdf($this);
		$pdf->Output($path->toString(), 'F');

		return $path;
	}


	/**
	 * Test if the order contain only a gift card / a mix of gift card and products / no gift card
	 *
	 * if return null : no gift card
	 * if return true : only a gift card
	 * if return false : gift cart + other product(s)
	 *
	 * @return bool | null
	 */
	public function isGiftCardOnly()
	{
		$Crm = $this->Crm();

		$cSet = $Crm->CouponSet();
		$cSet->orderitem();

		$giftcards = $cSet->select($cSet->orderitem->parentorder->is($this->id));

		if (0 === $giftcards->count())
		{
			// no gift card
			return null;
		}

		$items = $this->getItems();

		if ($items->count() === $giftcards->count())
		{
			return true;
		}

		return false;
	}
}





class crm_OrderDuplicateException extends UnexpectedValueException
{

}
