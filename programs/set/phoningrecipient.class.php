<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/campaignrecipient.class.php';


/**
 * Phoning recipient
 *
 */
class crm_PhoningRecipientSet extends crm_CampaignRecipientSet {

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->addFields(
			ORM_DateTimeField('calldate')
				->setDescription($Crm->translate('Phone call date')),

			ORM_StringField('calluser')
				->setDescription($Crm->translate('Phone call user')),

			ORM_TextField('comment')
				->setDescription($Crm->translate('Phone call comment'))
		);

		$Crm->includeOrganizationSet();
		$Crm->includeContactSet();

		$this->hasOne('organization', $Crm->OrganizationSetClassName());
		$this->hasOne('contact', $Crm->ContactSetClassName());
	}





	public function import(crm_Import $import, Widget_CsvRow $row)
	{
		$campaign = $import->getCampaign();

		$organizationSet = $this->Crm()->OrganizationSet();
		$contactSet = $this->Crm()->ContactSet();

		// prepare data

		if (isset($row->organization)) {
			$organization = $organizationSet->getRecordByUuid($row->organization);
		}

		if (isset($row->contact)) {
			$contact = $contactSet->getRecordByUuid($row->contact);
		}



		if (!isset($organization)) {
			$message = sprintf($this->Crm()->translate('Error on line %d, there is no organization with the universaly unique identifier found in the organization UUID column but the organization is mandatory to create or update a recipient'), $row->line());

			$exception = new Widget_ImportException($message);
			$exception->setCsvRow($row);

			throw $exception;

			return false;
		}



		if (!isset($contact)) {
			$message = sprintf($this->Crm()->translate('Error on line %d, there is no contact with the universaly unique identifier found in the contact UUID column but the contact is mandatory to create or update a recipient'), $row->line());

			$exception = new Widget_ImportException($message);
			$exception->setCsvRow($row);

			throw $exception;

			return false;
		}


		if ('' !== (string) $row->calldate) {

			if (!preg_match('/(\d+)-(\d+)-(\d{4}) (\d+):(\d+)/', $row->calldate, $datematch)) {

				$message = sprintf($this->Crm()->translate('Error on line %d, the date format in the sent date column  must be DD/MM/YYYY HH:MM'), $row->line());

				$exception = new Widget_ImportException($message);
				$exception->setCsvRow($row);

				throw $exception;

				return false;
			}

			$calldate = sprintf('%04d-%02d-%02d %02d:%02d',
				(int) $datematch[3],
				(int) $datematch[2],
				(int) $datematch[1],
				(int) $datematch[4],
				(int) $datematch[5]
			);
		} else {
			$calldate = '';
		}






		if (isset($row->uuid)) {

			// update recipient

			$recipient = $this->getRecordByUuid($row->uuid);

			if (!isset($recipient)) {
				$message = sprintf($this->Crm()->translate('Error on line %d, there is a value in the unique identifier column but the recipient does not exists in the database.
To create the recipient, you must remove the value in the universally unique identifier column'), $row->line());

				$exception = new Widget_ImportException($message);
				$exception->setCsvRow($row);

				throw $exception;

				return false;
			}

			$recipient->organization = $organization->id;
			$recipient->contact = $contact->id;
			$recipient->calldate = $calldate;
			$recipient->comment = $row->comment;
			$recipient->save();

		} else {

			// insert recipient

			$recipient = $this->newRecord();

			$recipient->campaign = $campaign->id;
			$recipient->contact = $contact->id;
			$recipient->calldate = $calldate;
			$recipient->comment = $row->comment;
			$recipient->save();
		}

		return true;
	}

}




class crm_PhoningRecipient extends crm_CampaignRecipient {


}
