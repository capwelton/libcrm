<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_SearchHistory
 *
 * @property ORM_StringField	$keyword
 * @property ORM_TextField		$criteria
 * @property ORM_IntField		$results
 */
class crm_SearchHistorySet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('keyword')
					->setDescription('mai search text field'),
			ORM_TextField('criteria')
					->setDescription('All search parameters in a serialized array'),
			ORM_IntField('results')
					->setDescription('Number of results')

		);

	}

	
	/**
	 * Add a search query to history only if this is the first search query (do not log other pages or refresh)
	 * @param string $keyword
	 * @param array $criteria
	 * 
	 * @return bool
	 */
	public function log($keyword, $criteria, $results)
	{
		// cleanup $criteria
		
		foreach($criteria as $filter => $value)
		{
			if (empty($value))
			{
				unset($criteria[$filter]);
			}
		}
		
		
		// search identifier :
		
		$arr = $criteria;
		
		if (isset($arr['pageNumber']))
		{
			unset($arr['pageNumber']);
		}
		
		$identifier = abs(crc32(serialize($arr)));
		
		
		if (isset($_SESSION['crm_Search_Identifier']))
		{
			if ($_SESSION['crm_Search_Identifier'] === $identifier)
			{
				return false;
			}
		}
		
		
		$_SESSION['crm_Search_Identifier'] = $identifier;
		
		// add to history
		
		$record = $this->newRecord();
		$record->setCriteria($criteria);
		$record->keyword = $keyword;
		$record->results = $results;
		$record->save();
	}
}


/**
 * A crm_SearchHistory
 *
 * @property string	$keyword
 * @property string	$criteria
 * @property int		$results
 */
class crm_SearchHistory extends crm_TraceableRecord
{
	
	/**
	 * 
	 * @param array $criteria
	 * @return crm_SearchHistory
	 */
	public function setCriteria(Array $criteria)
	{
		$this->criteria = serialize($criteria);
		return $this;
	}
	
	
	/**
	 * 
	 * @return array
	 */
	public function getCriteria()
	{
		if ('' === $this->criteria)
		{
			return array();
		}
		
		return unserialize($this->criteria);
	}

}
