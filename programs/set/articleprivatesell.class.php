<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_ArticlePrivateSell is a private sell item.
 *
 * @property crm_PrivateSellSet		$privatesell
 * @property crm_ArticleSet			$article
 */
class crm_ArticlePrivateSellSet extends crm_RecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();
		
		$this->setDescription('Shopping carts');

		$this->setPrimaryKey('id');
		
		$this->hasOne('privatesell', $Crm->PrivateSellSetClassName());
		$this->hasOne('article', $Crm->ArticleSetClassName());
	}
}


/**
 * A private sell item
 *
 *
 * @property crm_PrivateSell	$privatesell
 * @property crm_Article		$article
 */
class crm_ArticlePrivateSell extends crm_Record
{
	
}