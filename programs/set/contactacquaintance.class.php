<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This set stores information about who knows who.
 *
 * @property ORM_EnumField  $level          The level of acquaintance 0 = min
 * @property crm_ContactSet $contact        The contact who knows
 * @property crm_ContactSet $acquaintance   The contact who is known
 */
class crm_ContactAcquaintanceSet extends crm_TraceableRecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->addFields(
            ORM_EnumField('level', $Crm->translateArray(crm_ContactAcquaintance::$levels))
                ->setDescription('Acquaintance level')
        );

        $this->hasOne('contact', $Crm->ContactSetClassName());      // The contact who knows
        $this->hasOne('acquaintance', $Crm->ContactSetClassName()); // The contact who is known
    }



    /**
     * Select all acquaintances of a contact.
     *
     * @param int   $contact        The contact id
     *
     * @return ORM_Iterator         <crm_Contact>
     */
    public function selectForContact($contact, $minLevel = null, $maxLevel = null)
    {
        $criteria = $this->contact->is($contact);
        if (isset($minLevel)) {
            $criteria = $criteria->_AND_($this->level->greaterThanOrEqual($minLevel));
        }
        if (isset($maxLevel)) {
            $criteria = $criteria->_AND_($this->level->lessThanOrEqual($maxLevel));
        }
        return $this->select($criteria)->orderDesc($this->level);
    }



    /**
     * Select contacts knowing an a specified other contact (acquaintance).
     *
     * @param int | crm_Contact $acquaintance       The contact
     * @param int               $minLevel           The minimum level of acquaintance the contact should have
     * @param int               $maxLevel           The maximum level of acquaintance the contact should have
     *
     * @return ORM_Iterator		<crm_Contact>
     */
    public function selectKnowing($acquaintance, $minLevel = null, $maxLevel = null)
    {
        if ($acquaintance instanceof crm_Contact) {
            $acquaintance = $acquaintance->id;
        }
        $criteria = $this->acquaintance->is($acquaintance);
        if (isset($minLevel)) {
            $criteria = $criteria->_AND_($this->level->greaterThanOrEqual($minLevel));
        }
        if (isset($maxLevel)) {
            $criteria = $criteria->_AND_($this->level->lessThanOrEqual($maxLevel));
        }
        return $this->select($criteria)->orderDesc($this->level);
    }
}




/**
 * @property int            $level          The level of acquaintance 0 = min
 * @property crm_Contact    $contact        The contact who knows
 * @property crm_Contact    $acquaintance   The contact who is known
 */
class crm_ContactAcquaintance extends crm_TraceableRecord
{
    const LEVEL_1 = 1;
    const LEVEL_2 = 2;
    const LEVEL_3 = 3;

    public static $levels =	array(
        self::LEVEL_1 => '1',
        self::LEVEL_2 => '2',
        self::LEVEL_3 => '3',
    );
    /**
     * {@inheritDoc}
     * @see ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->level;
    }
}

