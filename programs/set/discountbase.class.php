<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * Base discount set
 *
 * @property ORM_TextField					$description
 * @property ORM_DecimalField				$value
 * @property ORM_BoolField                  $shipping
 * @property ORM_BoolField                  $percentage
 * @property ORM_DecimalField               $threshold
 * @property ORM_EnumField                  $threshold_type
 * @property ORM_DateField					$start_date
 * @property ORM_DateField					$end_date
 */
abstract class crm_DiscountBaseSet extends crm_TraceableRecordSet
{

	

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setPrimaryKey('id');
		

		$this->addFields(
			ORM_TextField('description')
					->setDescription('Description'),
			ORM_DecimalField('value', 2)
					->setDescription('Value'),
		    ORM_BoolField('shipping')
		            ->setDescription('If the base value to discount is the shipping amount in shopping cart'),
		    ORM_BoolField('percentage')
		            ->setDescription('Value is percentage or euros'),
		    ORM_DecimalField('threshold', 2)
		            ->setDescription('will be applicable only if shopping cart amount is at least this value'),
		    ORM_EnumField('threshold_type', $this->getThresholdTypes()),
			ORM_DateField('start_date')
					->setDescription('Start date'),
			ORM_DateField('end_date')
					->setDescription('End date')
		);
	}
	
	
	/**
	 * @since 1.0.45
	 */
	public function getDateCriteria()
	{
	    $now = date('Y-m-d H:i:s');
	    
	    return $this->start_date->lessThanOrEqual($now)
	       ->_AND_($this->end_date->greaterThanOrEqual($now));
	}
	    
	/**
	 * @since 1.0.45
	 * @return boolean
	 */
	public function containItems()
	{
	    $res = $this->select($this->getDateCriteria());
	    return ($res->count() > 0);
	}
	
	
	public function getThresholdTypes()
	{
	    return array(
	        'amount' => crm_translate('from amount in euro'),
	        'weight' => crm_translate('from weight in kilogam')
	    );
	}
}


/**
 * Base discount
 *
 * @property string					  $description
 * @property float				      $value
 * @property bool                     $shipping
 * @property bool                     $percentage
 * @property float                    $threshold
 * @property string                   $threshold_type
 * @property string					  $start_date
 * @property string				      $end_date
 */
abstract class crm_DiscountBase extends crm_TraceableRecord
{
    /**
     * Get value of discount relative to max discount value (shopping cart total)
     * @param	float					$max_discount_value		Total amount in shopping cart, preceding discounts and reductions allready included
     *
     * @return float    In euros
     */
    protected function getInDiscountValue($max_discount_value)
    {
        $value = (float) $this->value;
    
        if (!$this->percentage) {
            return $value;
        }
    
        return round($value/100 * $max_discount_value, 2);
    }
    
    
    
    
    
    /**
     * Get the discouted value as string
     * @return string
     */
    public function getDiscount()
    {
        $Crm = $this->Crm();
    
    
        if(0 !== (int) round(100 * $this->value)) {
    
            $type = crm_translate('%s %s on total');
            if ($this->shipping) {
                $type = crm_translate('%s %s on shipping');
            }
    
            $set = $this->getParentSet();
            $v = $set->value->output($this->value);
    
            $unit = '%';
            if (!$this->percentage) {
                $unit = $Crm->Ui()->Euro();
            }
    
            return sprintf($type, $v, $unit);
        }
    
        return null;
    }
    
    
    
    abstract public function getAmount(crm_ShoppingCart $cart, stdClass $max);
    
    
    

    
    
    
    
    /**
     * Test on threshold value
     * 
     * @param crm_ShoppingCart $cart        The tested shopping cart
     * @param stdClass $max                 Max value for amount and shipping
     * 
     * @return boolean
     */
    protected function testThreshold(crm_ShoppingCart $cart, stdClass $max)
    {
        if (empty($this->threshold_type)) {
            $this->threshold_type = 'amount';
        }
        
        //var_dump($this->description.' '.$max->total.' '.$this->threshold);
        
        if ('amount' === $this->threshold_type && $max->total < (float) $this->threshold) {
            return false;
        }
        
        if ('weight' === $this->threshold_type && $cart->getWeight() < (float) $this->threshold) {
            return false;
        }
        
        return true;
    }
    
    
    
    public function getThresholdUnit()
    {
        if ('weight' === $this->threshold_type) {
            return 'Kg';
        }
        
        return $this->Crm()->Ui()->Euro();
    }
    
    
    
    
    /**
     * Apply amount from discount to the max object
     * the max object will by passed to the next discount
     *
     * @param stdClass     $max
     * @param float        $amount     The negative value from the getAmount() method
     */
    public function applyAmount(stdClass $max, $amount)
    {
        if ($this->shipping) {
            $max->shipping += $amount;
            return;
        }
         
        $max->total += $amount;
    }
}





/**
 * 
 */
class crm_DiscountBaseException extends Exception
{
	
}