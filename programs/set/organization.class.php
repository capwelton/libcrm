<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




class crm_OrganizationAfterSaveEvent extends bab_event
{
    /**
     * @var crm_Organization
     */
    public $organization;
}

class crm_OrganizationBeforeSaveEvent extends bab_event
{
    /**
     * @var crm_Organization
     */
    public $organization;
}


/**
 * A crm_Organization may be a company, association...
 *
 * @property ORM_StringField            $name
 * @property ORM_TextField              $description
 * @property ORM_StringField            $activity
 * @property ORM_EmailField             $email
 * @property ORM_StringField            $phone
 * @property ORM_StringField            $fax
 * @property ORM_UrlField               $website
 * @property crm_OrganizationSet        $parent
 * @property crm_OrganizationTypeSet    $type
 * @property crm_AddressSet             $address
 *
 * @method crm_Organization                  get()
 * @method crm_Organization                  request()
 * @method crm_Organization[]|\ORM_Iterator  select()
 * @method crm_Organization                  newRecord()
 */
class crm_OrganizationSet extends crm_TraceableRecordSet
{
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');

        $this->setDescription('Organization');

        $this->addFields(
            ORM_StringField('name')
                    ->setDescription('Name'),
            ORM_TextField('description')
                    ->setDescription('Description'),
            ORM_TextField('logo')
                    ->setDescription('Logo'),
            ORM_StringField('activity')
                    ->setDescription('Main activity code'),
            ORM_EmailField('email')
                ->setDescription('Email'),
            ORM_StringField('phone')
                ->setDescription('Phone'),
            ORM_StringField('fax')
                ->setDescription('Fax'),
            ORM_UrlField('website')
                ->setDescription('Web site')
        );

        if (isset($Crm->Contact))
        {
            $this->hasOne('responsible', $Crm->ContactSetClassName())
                ->setDescription('The collaborator responsible for this organization');
        }

        $this->hasOne('parent', $Crm->OrganizationSetClassName())
            ->setDescription('Parent organization');

        if (isset($Crm->OrganizationType))
        {
            $this->hasOne('type', $Crm->OrganizationTypeSetClassName());
        }

        $this->hasOne('address', $Crm->AddressSetClassName())
            ->setDescription('Address');

        $this->hasOne('replacedBy', $Crm->OrganizationSetClassName()); // If the organization has been deleted and replaced by another organization.

        if (isset($Crm->Deal))
        {
            $Crm->includeDealSet();
            $this->hasMany('deals', $Crm->DealSetClassName(), 'lead')
                ->setOnDeleteMethod(ORM_ManyRelation::ON_DELETE_SET_NULL);
        }
    }


    /**
     * {@inheritDoc}
     * @see crm_TraceableRecordSet::save()
     */
    public function save(ORM_Record $record, $noTrace = false)
    {
        $event = new crm_OrganizationBeforeSaveEvent();
        $event->organization = $record;
        bab_fireEvent($event);

        $result = parent::save($record);

        $event = new crm_OrganizationAfterSaveEvent();
        $event->organization = $record;
        bab_fireEvent($event);

        return $result;
    }



    /**
     * Matches organizations whose parent, grand-parent, great-grand-parent or
     * great-great-grand-parent is $organization.
     *
     * @since 1.0.23
     * @param crm_Organization $organization
     * @return ORM_Criterion
     */
    public function isDescendantOf(crm_Organization $organization)
    {
        $Crm = $this->Crm();
        $organizationSet = $Crm->OrganizationSet();
        $isDescendant = $organizationSet->parent()->id->is($organization->id)->_OR_(
            $organizationSet->parent()->parent()->id->is($organization->id)
        )->_OR_(
            $organizationSet->parent()->parent()->parent()->id->is($organization->id)
        )->_OR_(
            $organizationSet->parent()->parent()->parent()->parent()->id->is($organization->id)
        );

        return $this->id->in($isDescendant, 'id');
    }


    /**
     * Creates the specified number of random organizations.
     *
     * @param int	$nbOrganizations
     * @return array	The ids of organizations actually created
     */
    public function populateRandomly($nbOrganizations)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = bab_functionality::get('Populator');
        if (!($Pop instanceof bab_functionality)) {
            return 0;
        }

        // if joined with address we also fill addresses
        $withAddress = ($this->address instanceof ORM_RecordSet);

        $CountrySet = $this->Crm()->CountrySet();
        $country = $CountrySet->get($CountrySet->code->is('FR'));

        $recordIds = array();
        for ($nbCreatedOrganizations = 0; $nbCreatedOrganizations < $nbOrganizations; $nbCreatedOrganizations++) {
            $organization = $this->newRecord();
            $organization->name = $Pop->getRandomBusinessName();

            if ($withAddress) {
                $userinfo = $Pop->getRandomUserInfo();
                $organization->address->street = $userinfo['bstreetaddress'];
                $organization->address->postalCode = $userinfo['bpostalcode'];
                $organization->address->city = $userinfo['bcity'];
                $organization->address->country = $country->id;
            }
            $organization->save();
            $recordIds[] = $organization->id;
        }
        return $recordIds;
    }




    /**
     * Creates the specified number of random organizations.
     *
     * @return array	The ids of organizations actually created
     */
    public function renameRandomly($nbOrganizations)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $Pop = bab_functionality::get('Populator');
        if (!($Pop instanceof bab_functionality)) {
            return 0;
        }

        $organizationSet = $Crm->OrganizationSet();
        $organizations = $organizationSet->select();

        foreach ($organizations as $organization) {
            $organization->name = $Pop->getRandomBusinessName();
            $organization->save();
        }

    }


    /**
     * Returns an iterator of organization linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = 'hasOrganization')
    {
        return parent::selectLinkedTo($source, $linkType);
    }





    private function importTag($organization, $rowTag)
    {
        $Crm = $this->Crm();
        $tagSet = $Crm->TagSet();

        $tag = trim($rowTag);

        /* Verify if the tag exist */
        $tagRecord = $tagSet->get($tagSet->label->is($tag));
        if (!isset($tagRecord)) {
            $tagRecord = $tagSet->newRecord();
            $tagRecord->label = $tag;
            $tagRecord->save();

            $hasTag = false;
            $tagRecord->linkTo($organization); /* see tag.class in LibCrm */

        } else {

            $tagRecord->linkTo($organization); /* see tag.class in LibCrm */
            //$linkSet = $Crm->LinkSet();

            //$links = $linkSet->select(
            //      $linkSet->sourceClass->is('Contact')
            //      ->_AND_($linkSet->destClass->is('Tag'))
            //      ->_AND_($linkSet->sourceId->is($contact->id))
            //      ->_AND_($linkSet->destId->is($tagRecord->id))
            //);

            //$hasTag = ($links->count() > 0);
        }

        $tagRecord->__destruct();
        unset($tagRecord);
    }


    /**
     * Import a CSV row
     * return false on error
     *
     *
     * @throw crm_ImportException
     *
     * @param	crm_Import			$import
     * @param	Widget_CsvRow		$row
     *
     * @return 	bool
     */
    public function import(crm_Import $import, Widget_CsvRow $row, $notrace = false)
    {
        $Crm = $this->Crm();
        if (!isset($row->name) || '' === trim($row->name)) {
            $message = sprintf($Crm->translate('Error on line %d, the organization name must not be empty'), $row->line());

            $exception = new Widget_ImportException($message);
            $exception->setCsvRow($row);

            throw $exception;

            return false;
        }

        if (isset($row->uuid) && $row->uuid) {

            // update row if uuid is present


            $organization = $this->get($this->uuid->is($row->uuid));

            if (!$organization) {

                $message = sprintf($Crm->translate('Error on line %d, there is a value in the unique identifier column but the organization does not exists in the database.
To create the organization, you must remove the value in the universally unique identifier column'), $row->line());

                $exception = new Widget_ImportException($message);
                $exception->setCsvRow($row);

                throw $exception;

                return false;
            }

        } else {

            // organization creation

            $orgWithSameName = $this->get($this->name->like($row->name));

            if ($orgWithSameName) {

                if (trim($orgWithSameName->entity) === trim($row->entity)) {

                    $message = sprintf($Crm->translate('Error on line %d, this organization name "%s" and entity "%s" already exists.
To update the organization, there must be a value in the universally unique identifier column'), $row->line(), $row->name, $row->entity);

                    $exception = new Widget_ImportException($message);
                    $exception->setCsvRow($row);

                    throw $exception;

                    return false;
                }



            } else {

                /* @var $organization crm_Organization */
                $organization = $this->newRecord();
            }
        }


        $nb_prop = $organization->import($row);
        if (0 < $nb_prop) {
            $organization->save($notrace);
            $import->addOrganization($organization);
        }


        $id = $organization->id;


        /* Create a tag in TagSet if value exist in CSV row : tag for contact */
        if (isset($row->tag1) && $row->tag1 != '') {
            $this->importTag($organization, $row->tag1);
        }
        if (isset($row->tag2) && $row->tag2 != '') {
            $this->importTag($organization, $row->tag2);
        }
        if (isset($row->tag3) && $row->tag3 != '') {
            $this->importTag($organization, $row->tag3);
        }
        if (isset($row->tag4) && $row->tag4 != '') {
            $this->importTag($organization, $row->tag4);
        }
        if (isset($row->tag5) && $row->tag5 != '') {
            $this->importTag($organization, $row->tag5);
        }
        if (isset($row->tag6) && $row->tag6 != '') {
            $this->importTag($organization, $row->tag6);
        }
        if (isset($row->tag7) && $row->tag7 != '') {
            $this->importTag($organization, $row->tag7);
        }

        $organization->__destruct();
        unset($organization);

        return $id;
    }

}

/**
 * Organization object
 *
 * @property string					$name
 * @property string					$description
 * @property string					$activity
 * @property string                 $email
 * @property string                 $phone
 * @property string                 $fax
 * @property string                 $website
 * @property crm_Organization		$parent
 * @property crm_OrganizationType	$type
 * @property crm_Address			$address
 */
class crm_Organization extends crm_TraceableRecord
{
    const SUBFOLDER = 'organizations';
    const PHOTOSUBFOLDER = 'logo';
    const ATTACHMENTSSUBFOLDER = 'attachments';

    /**
     * Main organization address
     * @return crm_Address
     */
    public function getMainAddress()
    {
        $this->Crm()->includeAddressSet();
        return $this->address();
    }





    public function updateMainAddress()
    {
        $Crm = $this->Crm();
        $addressSet = $Crm->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this);

        if ($this->address instanceof crm_Address) {
            $thisAddressId = $this->address->id;
        } else {
            $thisAddressId = $this->address;
        }

        $firstAddressId = null;
        $firstAddressRecord = null;
        $thisAddressOk = false;
        foreach ($addressLinks as $addressLink) {
            if ($addressLink->targetId->id == $thisAddressId) {
                // We found the current main address in the linked addresses.
                $thisAddressOk = true;
                break;
            }
            if (!isset($firstAddressId)) {
                $firstAddressId = $addressLink->targetId->id;
                $firstAddressRecord = $addressLink->targetId;
            }
        }

        if (!$thisAddressOk) {
            // If the current main address is not part of the linked addresses,
            // we replace it with the first linked address.
            if ($this->address instanceof crm_Address) {
                $this->address = $firstAddressRecord;
            } else {
                $this->address = $firstAddressId;
            }
            $this->save();
        }
    }


    /**
     * Adds a typed address to the contact.
     *
     * @param crm_Address $address
     * @param string $type
     */
    public function addAddress(crm_Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }
        if (!$address->isLinkedTo($this, 'hasAddressType:' . $type)) {
            $address->linkTo($this, 'hasAddressType:' . $type);
            $this->updateMainAddress();
        }

        return $this;
    }


    /**
     * Removes a typed address from the organization.
     *
     * @param crm_Address $address
     * @param string $type
     *
     * @return crm_Organization
     */
    public function removeAddress(crm_Address $address, $type)
    {
        if (!isset($this->id)) {
            return;
        }
        $address->unlinkFrom($this, 'hasAddressType:' . $type);

        $this->updateMainAddress();

        return $this;
    }



    /**
     * Update the type of a linked address for the organization.
     *
     * @param crm_Address $address
     * @param string $previousAddressType
     * @param string $newAddressType
     *
     * @return crm_Organization
     */
    public function updateAddress(crm_Address $address, $previousAddressType, $newAddressType)
    {
        if (!isset($this->id)) {
            return;
        }

        $Crm = $this->Crm();
        $addressSet = $Crm->AddressSet();
        $addressLinks = $addressSet->selectLinkedTo($this, 'hasAddressType:' . $previousAddressType);
        foreach ($addressLinks as $addressLink) {
            $addressLink->type = 'hasAddressType:' . $newAddressType;
            $addressLink->save();
        }

        return $this;
    }




    /**
     *
     * @param string $type
     *
     * @return array  array('Type 1' => array(address1), 'Type 2' => array(address2, address3 ), ...)
     */
    public function getAddresses($type = null)
    {
        $Crm = $this->Crm();

        $addressSet = $Crm->AddressSet();

        if (isset($type)) {
            $type = 'hasAddressType:' . $type;
        }

        $addresses = $addressSet->selectLinkedTo($this, $type);

        $typedAddresses = array();

        foreach ($addresses as $address) {
            $type = $address->type;
            if (substr($type, 0, strlen('hasAddressType:')) === 'hasAddressType:') {
                $type = substr($type, strlen('hasAddressType:'));
            }
            if (!isset($typedAddresses[$type])) {
                $typedAddresses['' . $type] = array();
            }
            $typedAddresses['' . $type][] = $address->targetId;
        }

        return $typedAddresses;
    }





    /**
     * Get billing address or null if no billing address
     * @return crm_Address
     */
    public function getBillingAddress()
    {
        return null;
    }


    /**
     * Get billing address or null if no billing address
     *
     * @param	crm_Order $order
     *
     * @return crm_Address
     */
    public function getBestBillingAddress(crm_Order $order = null)
    {
        $address = $this->getBillingAddress();
        if (!$address || $address->isEmpty())
        {
            return $this->getMainAddress();
        } else {
            return $address;
        }
    }

    /**
     * Get delivery address or null if no delivery address
     *
     * @param	crm_Order $order
     *
     * @return crm_Address
     */
    public function getBestDeliveryAddress(crm_Order $order = null)
    {
        $address = $this->getDeliveryAddress();
        if (!$address || $address->isEmpty())
        {
            return $this->getMainAddress();
        } else {
            return $address;
        }
    }



    /**
     * Get bill recipient displayed before the billing address
     * @return string
     */
    public function getBillingAddressRecipient()
    {
        if ($contact = $this->getBillingContact())
        {
            return $contact->getFullName();
        }

        return null;
    }



    /**
     * Get billing contact
     * @return crm_Contact
     */
    public function getBillingContact()
    {
        return null;
    }




    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get main website http URL or null if no website
     * @return string | null
     */
    public function getMainWebsite()
    {
        return $this->website;
    }

    /**
     * Get main organization email or null if no email
     * @return string | null
     */
    public function getMainEmail()
    {
        return $this->email;
    }

    /**
     * Get main organization phone number or null if no phone
     * @return string | null
     */
    public function getMainPhone()
    {
        return $this->phone;
    }

    /**
     * Get main organization mobile phone number or null if no phone
     * @return string | null
     */
    public function getMainMobile()
    {
        return null;
    }

    /**
     * Get main organization fax number or null if no fax
     * @return string | null
     */
    public function getMainFax()
    {
        return $this->fax;
    }

    /**
     * Get additional information for organization displayed in a widget organization suggest as a second line after informations from the getName() method
     * @see crm_Organization::getName()
     * @return string
     */
    public function getSuggestInfos()
    {
        if (!$this->address || $this->address->isEmpty()) {
            return null;
        }

        return $this->address->postalCode . ' ' . $this->address->city;
    }



    public function getChildren()
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        return $children;
    }



    /**
     * Get the upload path for files related to this organization.
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

        $path = $this->Crm()->uploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }


    /**
     * Get the upload path for attached files.
     *
     * @return bab_Path
     */
    public function getAttachmentsUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::ATTACHMENTSSUBFOLDER);
        return $path;
    }

    /**
     * Get the upload path for the logo file
     * this method can be used with the image picker widget
     *
     * @return bab_Path
     */
    public function getLogoUploadPath()
    {
        $path = $this->uploadPath();
        if (!isset($path)) {
            return null;
        }

        $path->push(self::PHOTOSUBFOLDER);
        return $path;
    }


    /**
     * Return the full path of the photo file
     * this method return null if there is no logo to display
     *
     * @return bab_Path | null
     */
    public function getLogoPath()
    {
        $uploadpath = $this->getLogoUploadPath();

        if (!isset($uploadpath)) {
            return null;
        }

        $W = bab_Widgets();
        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

        if (!isset($uploaded)) {
            return null;
        }

        foreach($uploaded as $file) {
            return $file->getFilePath();
        }

        return null;
    }


    /**
     * Move logo to logo upload path
     * this method is used to import an image from a temporary directory of the filePicker widget or another file
     * warning, the default behavior remove the source file
     *
     * @param	bab_Path 	$sourcefile
     * @param	bool		$temporary			default is true, if the sourcefile is a filePicker temporary file
     * @return 	bool
     */
    public function importLogo(bab_Path $sourcefile, $temporary = true)
    {
        $uploadpath = $this->getLogoUploadPath();

        if (!isset($uploadpath)) {
            return false;
        }

        $uploadpath->createDir();



        if (!$temporary) {
            $W = bab_Widgets();
            return $W->imagePicker()->setFolder($uploadpath)->importFile($sourcefile);
        }


        $original = $sourcefile->toString();
        $uploadpath->push(basename($original));

        return rename($original, $uploadpath->toString());
    }




    /**
     * Method used to get user info for the bab_updateUserById function
     * only the organization part
     *
     * @see crm_Contact::getUserInfos()
     *
     *
     * @return array
     */
    public function getUserOrganizationInfos()
    {
        $output = array();

        $output['btel']				= $this->getMainPhone();
        $output['bfax']				= $this->getMainFax();
        $output['organisationname']	= $this->getName();

        if ($orgaddress = $this->getMainAddress()) {
            /* @var $orgaddress crm_Address */

            $output['bstreetaddress']	= $orgaddress->street;
            $output['bcity']			= $orgaddress->city;

            if ($orgaddress->cityComplement) {
                $output['bcity'] .= ' '.$orgaddress->cityComplement;
            }

            $output['bpostalcode']	= $orgaddress->postalCode;
            $output['bstate']		= $orgaddress->state;

            if ($orgcountry = $orgaddress->getCountry()) {
                /* @var $orgcountry crm_Country */
                $output['bcountry']	= $orgcountry->getName();
            } else {
                $output['bcountry']	= '';
            }

        } else {

            $output['bstreetaddress']	= '';
            $output['bcity']			= '';
            $output['bpostalcode']		= '';
            $output['bstate']			= '';
            $output['bcountry']			= '';
        }

        return $output;
    }





    /**
     * Checks whether the organization is a descendant of the specified organization.
     *
     * @param int|crm_Organization $organization
     *
     * @return bool
     */
    public function isDescendantOf($organization)
    {
        $organizationId = (($organization instanceof crm_Organization) ? $organization->id : $organization);

        $org = $this;
        while ($org->parent != 0) {
            if ($org->parent == $organizationId) {
                return true;
            }
            $org = $org->parent();
        }
        return false;
    }





    /**
     *
     * @return  array of org id => org.
     */
    public function getAscendants()
    {
        $Crm = $this->Crm();
        $organizationSet = $Crm->OrganizationSet();

        $ancestors = array();

        $org = $this;
        while ($org) {
            if (isset($ancestors[$org->id])) {
                break;
            }
            $ancestors[$org->id] = $org;
            $org = $org->parent();
        }

        return $ancestors;
    }



    /**
     * @return bool
     */
    public function hasDescendants()
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        $hasDescendants = ($children->count() > 0);
        return $hasDescendants;
    }



    /**
     *
     * @param array $descendants 	Contains the result of descendants ids.
     */
    public function getDescendants(&$descendants)
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        foreach ($children as $child) {
            if ($child->id != $this->id && !isset($descendants[$child->id]))
            {
                $descendants[$child->id] = $child->id;
                $child->getDescendants($descendants);
            }
        }
    }





    /**
     * Select contacts linked to this organization.
     * the crm_Organization::selectContacts() method return an iterator of crm_OrganizationPerson
      * witch are crm_ContactOrganization objects or crm_Contact objects in case of an implementation
      * of crm with one organization per contact
     *
     * @return ORM_Iterator	<crm_OrganizationPerson>
     */
    public function selectContacts($includeHistory = false)
    {
        $this->Crm()->includeContactSet();
        $set = $this->Crm()->ContactOrganizationSet();
        $set->contact();

        $criteria = $set->organization->is($this->id)->_AND_($set->contact->deleted->is(0));
        if (!$includeHistory) {
            $criteria = $criteria->_AND_($set->history_to->is('0000-00-00')->_OR_($set->history_to->greaterThan(date('Y-m-d'))));
        }

        $contacts = $set->select($criteria);

        $contacts->orderAsc($set->contact->lastname);

        return $contacts;
    }





    /**
     * Select existing organizations that are possible duplicates
     * of this one.
     *
     * @return ORM_Iterator <crm_Organization>
     */
    public function selectPossibleDuplicates()
    {
        $organizationSet = $this->getParentSet();

        $isDuplicationCandidate = $organizationSet->name->like($this->name);
        if (isset($this->id)) {
            $isDuplicationCandidate = $isDuplicationCandidate->_AND_($organizationSet->id->notIn($this->id));
        }
        $organizations = $organizationSet->select(
            $organizationSet->name->like($this->name)
        );

        return $organizations;
    }





    /**
     * (non-PHPdoc)
     * @see crm_TraceableRecord::linkTo($source, $linkType)
     */
    public function linkTo(crm_Record $source, $linkType = 'hasOrganization')
    {
        return parent::linkTo($source, $linkType);
    }





    /**
     * Update properties with CSV row
     * @param	Widget_CsvRow	$row	A CSV row
     * @return 	int						number of updated properties
     */
    public function import(Widget_CsvRow $row)
    {
        $up_prop = 0;

        if (isset($row->name)) {
            $up_prop += $this->importProperty('name', $row->name);
        }

        if (isset($row->description)) {
            $up_prop += $this->importProperty('description', $row->description);
        }

        if (isset($row->logo)) {
            $up_prop += $this->importProperty('logo', $row->logo);
        }

        if (isset($row->street) && isset($row->city)) {

            $ASet = $this->Crm()->AddressSet();
            $address = $this->getMainAddress();

            if (!isset($address)) {
                /* @var $address crm_Address */
                $address = $ASet->newRecord();
            }

            $up_prop_address = $address->import($row);
            if (0 < $up_prop_address) {

                $address->save();
                $up_prop += $up_prop_address;
            }

            $up_prop += $this->importProperty('address', $address->id);
        }


        if (isset($row->type)) {

            if (is_numeric($row->type)) {
                $up_prop += $this->importProperty('type', $row->type);
            } else {

                $OTset = $this->Crm()->OrganizationTypeSet();
                if ($OType = $OTset->get($OTset->name->like($row->type)))
                {
                    $up_prop += $this->importProperty('type', $OType->id);
                }
            }
        }

        return $up_prop;
    }




    /**
     * (non-PHPdoc)
     * @see crm_Record::delete()
     */
    public function delete()
    {
        return parent::delete();
    }





    /**
     * (non-PHPdoc)
     * @see crm_Record::getRelatedRecords()
     */
    public function getRelatedRecords()
    {
        $Crm = $this->Crm();

        $relatedRecords = parent::getRelatedRecords();

        $contacts = $this->selectContacts();
        if ($contacts->count() > 0) {
            $relatedRecords[$Crm->ContactSetClassName()] = $contacts;
        }

        return $relatedRecords;
    }





    /**
     * Returns the teams to which this organization has been linked as a member.
     *
     * @return ORM_Iterator	of crm_Team
     */
    public function getTeams()
    {
        $Crm = $this->Crm();

        $teamClassName = $Crm->TeamClassName();
        $organizationClassName = $Crm->OrganizationClassName();

        $linkSet = $Crm->LinkSet();
        $links = $linkSet->select(
            $linkSet->sourceClass->is($teamClassName)
            ->_AND_($linkSet->targetClass->is($organizationClassName))
            ->_AND_($linkSet->targetId->is($this->id))
        );

        $arrLink = array();
        foreach ($links as $link) {
            $arrLink[$link->sourceId] = $link->sourceId;
        }

        $teamSet = $Crm->TeamSet();
        $teams = $teamSet->select($teamSet->id->in($arrLink));

        return $teams;
    }





    /**
     * Reassociates all data associated to this organization to another
     * specified one.
     *
     * @param	int		$id		The id of the organization that will replace this one.
     *
     * @return crm_Organization
     */
    public function replaceWith($id)
    {
        parent::replaceWith($id);


        $Crm = $this->Crm();


        // We reassociate the contacts to the new organization.
        $Crm->includeContactSet();
        $contactOrganizationSet = $Crm->ContactOrganizationSet();

        $contactOrganizations = $contactOrganizationSet->select(
            $contactOrganizationSet->organization->is($this->id)
        );
        foreach ($contactOrganizations as $contactOrganization) {
            $contactOrganization->organization = $id;
            $contactOrganization->save();
        }


        // Keep track of the replacement.
        $this->replacedBy = $id;

        $this->save();

        return $this;
    }
}
