<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * A notification
 * with a subject/body message
 * 
 */
class crm_NotifyMessage extends crm_Object
{
	protected $recipients = array();
	
	protected $subject = null;
	
	protected $body = null;
	
	/**
	 * 
	 * @var crm_Email
	 */
	private $email = null;
	
	
	public function addContactRecipient(crm_Contact $contact)
	{
		$this->recipients[] = $contact;
		return $this;
	}
	
	public function addUserRecipient($id_user)
	{
		$this->recipients[] = $id_user;
		return $this;
	}
	
	public function addRecipient($email)
	{
		$this->recipients[] = $email;
		return $this;
	}
	
	
	public function setSubject($subject)
	{
		$this->subject = $subject;
		return $this;
	}
	
	public function setBody($body)
	{
		$this->body = $body;
		return $this;
	}
	
	
	/**
	 * Get a not saved crm_Email object
	 * return null if no recipients
	 * @return crm_Email
	 */
	public function getEmail()
	{
	    $Crm = $this->Crm();
	    
	    foreach($Crm->disabledNotifications as $className) {
	        if ($this instanceof $className) {
	            return null;
	        }
	    }
	    
		if (isset($this->email)) {
			return $this->email;
		}
		
		
		$set = $Crm->EmailSet();
		$emailRecord = $set->newRecord();
	
		/*@var $email crm_Email */
	
		$email_recipients = array();
	
		foreach($this->recipients as $recipient)
		{
			if ($recipient instanceof crm_Contact)
			{
				$email = $recipient->getMainEmail();
				if (!empty($email))
				{
					$email_recipients[] = $email;
				}
			} else if (is_numeric($recipient)) {
				$email = bab_getUserEmail($recipient);
				if (!empty($email))
				{
					$email_recipients[] = $email;
				}
			} else {
				$email_recipients[] = $recipient;
			}
		}
		
		if (empty($email_recipients))
		{
			return null;
		}
		
	
		$emailRecord->recipients = implode(',', $email_recipients);
	
		$emailRecord->subject = $this->subject;
		$emailRecord->body = $this->body;
	
		$emailRecord->setSiteSkin();
	
		$this->email = $emailRecord;
		return $emailRecord;
	}
	
	
	/**
	 * Save the email to database
	 * @param crm_Email $email
	 */
	protected function save(crm_Email $email)
	{
		$email->save();
	}
	
	/**
	 * Attach files to the email allready saved
	 * @param crm_Email $email
	 */
	protected function attachFiles(crm_Email $email)
	{
		
	}
	
	/**
	 * Link email to others CRM objets
	 * @param crm_Email $email
	 */
	protected function linkRecords(crm_Email $email)
	{
		foreach($this->recipients as $recipient)
		{
			if ($recipient instanceof crm_Contact)
			{
				$email->linkTo($recipient, 'referencedBy');
			}
		}
	}
	
	
	/**
	 * Send email
	 * this method will return null if no recipients or email notification disabled (no mail created)
	 * this method will return false if the mail is created but not sent because of a mail error
	 * this method will return true if email was sent correctly
	 * @return bool
	 */
	public function send()
	{
		$email = $this->getEmail();
		
		if (null === $email)
		{
			return null;
		}
		
		
		$this->save($email);
		$this->attachFiles($email);
		$this->linkRecords($email);

		return $email->send();
	}
	
	
	/**
	 * Get mail error message as string
	 * @return string
	 */
	public function getError()
	{
		$email = $this->getEmail();
		$Crm = $this->Crm();
		
		if (null === $email)
		{
			return $Crm->translate('No email created because the are no recipients');
		}
		
		if (!empty($email->statusMessages))
		{
			return implode('\n', $email->statusMessages);
		}
	}
}