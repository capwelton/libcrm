<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



class crm_LostPasswordNotify extends crm_NotifyMessage
{
	protected $contact;
	
	public function __construct(Func_Crm $Crm, crm_Contact $contact)
	{
		$this->contact = $contact;
		parent::__construct($Crm);
	}
}


/**
 * Send an password token to a contact
 *
 */
class crm_lostPassword_sendToken extends crm_LostPasswordNotify
{
	
	public function __construct(Func_Crm $Crm, crm_Contact $contact, $init = false)
	{
		parent::__construct($Crm, $contact);
		global $babUrl;

		$this->addContactRecipient($contact);
		$this->sitename = $_SERVER['HTTP_HOST'];
		
		if ($contact->user)
		{
			$this->setSubject(sprintf($Crm->translate('Reset your password on site %s'), $this->sitename));
			$this->setBody(sprintf($Crm->translate('To get access to your account, simply click on the link %s to reset your password and log on the site.'), $this->getTokenLink($init)));
		} else {
			
			$this->setSubject(sprintf($Crm->translate('Create your account on site %s'), $this->sitename));
			$this->setBody(sprintf($Crm->translate('This is an invitation for %s. To get your private access, simply click on the link %s to create your account and log on the site.'), $this->sitename, $this->getTokenLink($init)));
		}
		
		
	}
	

	/**
	 *
	 * @param bool $init
	 * @return string HTML
	 */
	private function getTokenLink($init)
	{
		$Crm = $this->Crm();
		$contact = $this->contact;
		
		return sprintf(
				'<a href="%s">%s</a>',
				bab_toHtml($contact->getPasswordTokenUrl($init)), bab_toHtml($Crm->translate('set your password'))
		);
	}
}
