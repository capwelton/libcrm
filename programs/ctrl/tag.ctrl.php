<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

$Crm = crm_Crm();
$Crm->includeRecordController();

/**
 * This controller manages actions that can be performed on tags.
 */
class crm_CtrlTag extends crm_RecordController
{

    /**
     * @param widget_TableModelView $tableView
     * @return crm_Toolbar
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $toolbar = new crm_Toolbar();
        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Add a tag'), $this->proxy()->edit()
            )->addClass('icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        return $toolbar;
    }



    /**
     *
     * @param array	$data
     * @return bool
     */
    public function save($data = null)
    {
        $Crm = $this->Crm();

        $tagSet = $Crm->TagSet();

        if (isset($data['id']) && !empty($data['id'])) {

            // This is an update

            $editedTag = $tagSet->request($data['id']);

            $sameTag = $tagSet->get($tagSet->label->is($data['label']));

            if (isset($sameTag) && $sameTag->id != $editedTag->id) {
                // When we update with an existing tag, we use this existing tag
                // to replace the one being edited

                $this->addMessage(sprintf($Crm->translate('The "%s" tag has been replaced by the "%s" tag.'), $editedTag->label, $sameTag->label));

                $tagSet->replaceLinks($editedTag->id, $sameTag->id);

                $editedTag->delete();
                return true;
            }

        } else {

            // This is a creation
            $editedTag = $tagSet->newRecord();

        }

        $editedTag->setFormInputValues($data);
        $editedTag->save();

        $this->addMessage($Crm->translate('The tag has been saved.'));
        return true;
        //crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The tag has been saved.'));
    }



    /**
     * @param int $id
     * @return boolean
     */
    public function validate($id)
    {
        $tagSet = $this->getRecordSet();
        $tag = $tagSet->request($id);
        if (!$tag->isUpdatable()) {

        }
        $tag->checked = true;
        $tag->save();

        return true;
    }


    /**
     * Unlinks the specified tag from the specified crm object.
     *
     * @param int		$tag	The tag id.
     * @param string	$from	The crm object string reference (e.g. Contact:12).
     * @return Widget_Action
     */
    public function unlink($tag = null, $from = null)
    {
        $Crm = $this->Crm();
        $tagSet = $Crm->TagSet();
        $tag = $tagSet->get($tag);

        $record = $Crm->getRecordByRef($from);

        $tag->unlinkFrom($record);

        return true;
    }





    /**
     * Links the specified tag to the specified crm object.
     *
     * @param int		$tag	The tag id.
     * @param string	$to		The crm object string reference (e.g. Contact:12).
     * @return Widget_Action
     */
    public function link($tag = null, $to = null)
    {
        $Crm = $this->Crm();
        $tagSet = $Crm->TagSet();

        $newTag = $tagSet->get($tagSet->label->is($tag['label']));

        if (!isset($newTag)) {
            if (trim($tag['label']) === '') {
                return true;
            }
            $newTag = $tagSet->newRecord();
            $newTag->label = $tag['label'];
            $newTag->save();
        }

        $record = $Crm->getRecordByRef($to);

        if (!$newTag->isLinkedTo($record)) {
            $newTag->linkTo($record);
        }

        return true;
    }






    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggest($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $suggestTag = $Ui->SuggestTag();
        $suggestTag->setMetadata('suggestparam', 'search');
        $suggestTag->suggest();

        die;
    }




    /**
     * Return a visual display of tags associated to the specified object.
     * The selector allows to remove tags and displays an input field to add more.
     *
     * @param string $ref	The crm object string reference (e.g. Contact:12)
     * @return Widget_FlowLayout
     */
    public function selector($ref)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $record = $Crm->getRecordByRef($ref);
        if (!isset($record)) {
            return null;
        }

        $Ui->includeTag();

        $tagsDisplay = crm_displayTags($record);

        $addTagForm = $W->Form()
            ->setLayout($W->FlowLayout()->setVerticalAlign('middle')->setSpacing(2, 'px'))
            ->setName('tag');

        $addTagForm->addItem($Ui->SuggestTag()->setName('label'));
        $addTagForm->addItem(
            $W->SubmitButton()
                ->setAjaxAction($Crm->Controller()->Tag()->link(), $addTagForm)
                ->setLabel($Crm->translate('Add'))
        );
        $addTagForm->setHiddenValue('tg', bab_rp('tg'));
        $addTagForm->setHiddenValue('to', $ref);

        $selector = $W->FlowItems(
            $tagsDisplay,
            $addTagForm
        );

        return $selector;
    }
}
