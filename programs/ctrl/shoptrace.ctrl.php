<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * 
 */
class crm_CtrlShopTrace extends crm_Controller implements crm_ShopAdminCtrl
{
	
	/**
	 * list of shop traces
	 * @return Widget_Action
	 */
	public function displayList($shoptrace = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($shoptrace), $Crm->translate('Trace list'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('Trace list'));
		
		
		$filter = isset($shoptrace['filter']) ? $shoptrace['filter'] : array('sort' => 'createdOn:down');
		
		$set = $Crm->ShopTraceSet();

		$table = $Crm->Ui()->ShopTraceTableView();
		$table->addDefaultColumns($set);

		$criteria = $table->getFilterCriteria($filter);
		$res = $set->select($criteria);
		$res->groupBy($set->trace);
		
		$table->setDataSource($res);
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'shoptrace');
		$page->addItem($filterPanel);
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function display($trace = null, $log = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($log), $Crm->translate('Message log in trace'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('Message log in trace'));
		
		
		$filter = isset($log['filter']) ? $log['filter'] : array();
		
		$set = $Crm->ShopTraceSet();
		
		$table = $Crm->Ui()->ShopTraceLogTableView();
		$table->addDefaultColumns($set);
		
		$criteria = $table->getFilterCriteria($filter)->_AND_($set->trace->is($trace));
		$res = $set->select($criteria);
		
		$table->setDataSource($res);
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'log');
		$filterPanel->getFilter()->addItem($W->Hidden()->setName('trace')->setValue($trace));
		$page->addItem($filterPanel);
		
		return $page;
	}
	
}
