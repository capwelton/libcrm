<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on tags.
 */
class crm_CtrlShippingScale extends crm_Controller implements crm_ShopAdminCtrl
{
	
	public function getLinkTitle()
	{
		return $this->Crm()->translate('Shipping scale');
	}
	
	/**
	 * @return Widget_Action
	 */
	public function displayList($shippingscale = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($shippingscale), $Crm->translate('Shipping scale'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('Shipping scale'));
		
		
		$filter = isset($shippingscale['filter']) ? $shippingscale['filter'] : array();
		
		$Crm->includeCountrySet();
		$set = $Crm->ShippingScaleSet();
		$set->country();

		$table = $Crm->Ui()->ShippingScaleTableView();
		
		$table->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		$table->addColumn(widget_TableModelViewColumn($set->comment, $Crm->translate('Comment')));
		$table->addColumn(widget_TableModelViewColumn($set->shipping_amount, $Crm->translate('Shipping amount'))->setSearchable(false));
		$table->addColumn(widget_TableModelViewColumn($set->weight, $Crm->translate('Weight'))->setSearchable(false));
		$table->addColumn(widget_TableModelViewColumn($set->department, $Crm->translate('Department')));
		$table->addColumn(widget_TableModelViewColumn($set->country->getNameField(), $Crm->translate('Country')));
		
		
		$criteria = $table->getFilterCriteria($filter);
		$table->setDataSource($set->select($criteria));
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'shippingscale');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add an shipping amount'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function display()
	{
		// Not used
	}
	
	/**
	 * @return Widget_Action
	 */
	public function edit($shippingscale = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($shippingscale), $Crm->translate('Edit shipping cost'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit shipping cost'));
		
		if (null !== $shippingscale)
		{
			$set = $Crm->ShippingScaleSet();
			$shippingscale = $set->get($shippingscale);
		
		}
		
		
		
		$form = $Ui->ShippingScaleEditor($shippingscale);
		
		$page->addItem($form);
		
		if ($shippingscale instanceof crm_ShippingScale)
		{
			$actionsFrame = $page->ActionsFrame();
			$page->addContextItem($actionsFrame);
			
			$actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($shippingscale->id)));
		}
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function save($shippingscale = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->ShippingScaleSet();
		
		if (empty($shippingscale['id']))
		{
			$record = $set->newRecord();
		} else {
			$record = $set->get($shippingscale['id']);
			if (null == $record)
			{
				throw new crm_AccessException($Crm->translate('This shipping cost does not exists'));
			}
		}
		
		$record->comment = $set->comment->input($shippingscale['comment']);
		$record->shipping_amount = $set->shipping_amount->input($shippingscale['shipping_amount']);
		$record->weight = $set->weight->input($shippingscale['weight']);
		$record->department = $shippingscale['department'];
		$record->country = $shippingscale['country'];
		
		$record->save();
		
		return true;
	}
	
	
	
	public function delete($shippingscale = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		if (!$shippingscale)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->ShippingScaleSet();
		$set->delete($set->id->is($shippingscale));
		
		crm_redirect($this->proxy()->displayList());
	}
}
