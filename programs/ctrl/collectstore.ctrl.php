<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on collect store.
 */
class crm_CtrlCollectStore extends crm_Controller implements crm_ShopAdminCtrl
{


	/**
	 *
	 * @param int $collectstore
	 * @return Widget_Action
	 */
	public function edit($collectstore = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Access = $Crm->Access();
		
		if (!$Access->editCollectStore()) {
		    throw new crm_AccessException('Acces denied');
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit(), $Crm->translate('Edit collect store'));

		$set = $Crm->CollectStoreSet();
		$set->address();

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');
		


		if (isset($collectstore)) {
			$collectstore = $set->get($collectstore);
			if (!isset($collectstore)) {
				throw new crm_Exception($Crm->translate('Trying to access a collect store with a wrong (unknown) id.'));
			}
			$page->addItem($W->Title($Crm->translate('Edit collect store'), 1));
			
		} else {
			$page->addItem($W->Title($Crm->translate('Create a new collect store'), 1));
		}
		
		$editor = $Ui->CollectStoreEditor($collectstore);
		$page->addItem($editor);

		return $page;
	}




	/**
	 * Saves the collect store
	 *
	 * @param array	$collectstore
	 * @return Widget_Action
	 */
	public function save(Array $collectstore = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$set = $Crm->CollectStoreSet();
		$set->address();
		
		if (!$Access->editCollectStore()) {
		    throw new crm_AccessException('Acces denied');
		}

		if (isset($collectstore['id'])) {
			$record = $set->get($collectstore['id']);
			if (!isset($record)) {
				throw new crm_SaveException($Crm->translate('Trying to access a collect store with a wrong (unknown) id.'));
			}
		} else {
			$record = $set->newRecord();
		}
		
		$record->setFormInputValues($collectstore);
		$record->save();

		
		return true;
	}
	
	
	
	/**
	 * Display list of waiting comments to confirm
	 */
	public function displayList(Array $collectstore = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Access = $Crm->Access();
		$set = $Crm->CollectStoreSet();
		$set->address();
		$set->address->country();
		
		if (!$Access->editCollectStore()) {
			throw new crm_AccessException($Crm->translate('Acces denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Collect stores'));
		
		
		$page = $Ui->Page();
		$page->addClass('crm-page-editor');
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a collect store'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		
		$page->addToolbar($toolbar);
		
		$res = $set->select();
		
		$table = $Ui->CollectStoreTableView();
		$table->addClass(Func_Icons::ICON_LEFT_16);
		$table->addDefaultColumns($set);
		$table->setDataSource($res);
		
		$filter = array();
		if (isset($collectstore['filter'])) {
		    $filter = $collectstore['filter'];
		}
		
		$filterPanel = $table->filterPanel($filter, 'collectstore');
		$page->addItem($filterPanel);
		
		return $page;
	}
	


	/**
	 * Deletes the specified collect store
	 *
	 * @param int	$collectstore
	 * @return Widget_Action
	 */
	public function delete($collectstore)
	{
	    $Crm = $this->Crm();
	    $Access = $Crm->Access();
	    
	    if (!$Access->editCollectStore())
	    {
	        throw new crm_AccessException($Crm->translate('Acces denied'));
	    }

		$set = $this->Crm()->CollectStoreSet();
		$set->delete($set->id->is($collectstore));

		crm_redirect($this->proxy()->displayList(), $this->Crm()->translate('The collect store has been deleted.'));
	}



}
