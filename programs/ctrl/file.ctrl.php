<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';


/**
 * This controller manages actions that can be performed on files attachments.
 */
class crm_CtrlFile extends crm_Controller
{



	/**
	 * Display the content of a path into the upload folder of a crm object
	 *
	 *
	 * @param 	string	$for 		A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param	string 	$path		The path relative to the object upload folder
	 *
	 * @see	crm_TracableRecord::uploadPath()
	 *
	 * @return Widget_Action
	 */
	public function display($for = null, $path = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		$W->FilePicker();

		$showLinkedElement = true;

		if (!empty($path)) {
			$pathElements = explode('/', $path);
			$pageLabel = array_pop($pathElements);
			if ($pageLabel === 'attachments') {
				$pageLabel = $Crm->translate('Attached files');
			} else {
				$pageLabel = Widget_FilePicker::decode($pageLabel);
			}
		} else {
			$pageLabel = $Crm->translate('Attached files');
		}
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($for, $path), $pageLabel, $path);


		$page = $Crm->Ui()->Page();


		$Crm->Ui()->includeFile();

		$folderView = crm_folderView($Crm, $for, $path);
		$folderView->addClass('icon-top-48 icon-top icon-48x48');
		$folderView->setUploadAction($Crm->Controller()->File()->display($for, $path));

		$page->addToolbar($folderView->toolbar());
		$page->addItem($folderView);

		$record = $Crm->getRecordByRef($for);

		if ($record && $showLinkedElement) {
			if ($record instanceof crm_Organization) {
				$page->addContextItem($Crm->Ui()->OrganizationCardFrame($record));
			}
			if ($record instanceof crm_Contact) {
				$page->addContextItem($Crm->Ui()->ContactCardFrame($record));
			}
			if ($record instanceof crm_Deal) {
				$page->addContextItem($Crm->Ui()->DealCardFrame($record));
			}
		}

		return $page;
	}


	/**
	 * Create a folder
	 *
	 * @param 	string	$for 		A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param	string 	$path		The path relative to the object upload folder
	 * @param	string	$folderName	New folder name
	 *
	 * @return Widget_Action
	 */
	public function createFolder($for = null, $path = null, $folderName = null)
	{
		$errorMessage = null;
		$Crm = $this->Crm();
		$W = bab_Widgets();

		try {

			$record = $Crm->getRecordByRef($for);
			$file = $Crm->FileAttachment($record, $path);
			$p = $file->getPath();

			$filePicker = $W->FilePicker();
			$filePicker->setFolder($p);
			$filePicker->push($folderName);
			$p = $filePicker->getFolder();


			if (!is_dir($p->toString())) {
				$p->createDir();
			} else {
				throw new Exception($Crm->translate('This folder already exists.'));
			}
		} catch (Exception $e) {
			crm_redirect(crm_BreadCrumbs::last(), $e->getMessage());
		}

		return crm_redirect(crm_BreadCrumbs::last());
	}



	public function cleanFolder($for = null, $path = null)
	{
	    $errorMessage = null;
	    $Crm = $this->Crm();
	    $W = bab_Widgets();
	
	    try {
	        $record = $Crm->getRecordByRef($for);
	        $file = $Crm->FileAttachment($record, $path);
	        $p = $file->getPath();
	
	        $filePicker = $W->FilePicker();
	        $filePicker->setFolder($p);
	        $p->deleteDir();
	    } catch (Exception $e) {
	        crm_redirect(crm_BreadCrumbs::last(), $e->getMessage());
	    }
	
	    return crm_redirect(crm_BreadCrumbs::last());
	}
	
	/**
	 * Delete folder or file
	 *
	 * @param 	string	$for 		A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param	string 	$pathname	The path relative to the object upload folder (contain the file name to delete)
	 *
	 * @return Widget_Action
	 */
	public function delete($for = null, $pathname = null)
	{
		$Crm = $this->Crm();

		$record = $Crm->getRecordByRef($for);
		$file = $Crm->FileAttachment($record, $pathname);

		if ($file->isDir() && $file->delete()) {
			crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The folder has been deleted'));
		} elseif ($file->delete()) {
			crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The file has been deleted'));
		}
	}




	/**
	 * Uncompress a zip file
	 *
	 * @param 	string	$for 		A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param	string 	$pathname	The path relative to the object upload folder (contain the file name to uncompress)
	 *
	 * @return Widget_Action
	 */
	public function uncompress($for = null, $pathname = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$record = $Crm->getRecordByRef($for);
		$file = $Crm->FileAttachment($record, $pathname);
		$p = $file->getPath();

		if (strtolower(substr($pathname, -4)) !== '.zip') {
			throw new crm_Exception('Not a zip file.');
		}

		/* @var $Zip Func_Archive_Zip */
		$Zip = bab_Functionality::get('Archive/Zip');

		$tmpPath = new bab_Path($GLOBALS['babUploadPath'],'tmp',session_id());
		if ($tmpPath->isDir()){
			$tmpPath->deleteDir();
		}
		$tmpPath->createDir();

		$result = $Zip->open($p->toString());
		$Zip->extractTo($tmpPath->toString());
		$Zip->close();

		if (0 === $tmpPath->count()) {
			crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('Nothing in zip archive.'));
		}
		else if (1 === $tmpPath->count())
		{
			$p->pop();
		} else
		{
			$basename = mb_substr($p->pop(), 0, -4);
			$p->push($basename);
		}

		// import tmp path into folder
		$W->FilePicker()->setFolder($p)->importPath($tmpPath, 'ASCII'); // IBM437 zip content charset for windows only ?
		$tmpPath->deleteDir();

		crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The file has been uncompressed.'));
	}






	/**
	 * Displays an form to edit the file/folder name.
	 *
	 * @param string $for      A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param string $pathname The full pathname of the file/folder to edit.
	 *
	 * @return crm_Page
	 */
	public function edit($for = null, $pathname = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$Crm->Ui()->includeBase();

		$filePicker = $W->FilePicker();


		$record = $Crm->getRecordByRef($for);
		$file = $Crm->FileAttachment($record, $pathname);

		$p = $file->getPath();

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');

		$page->addItem($W->Title($Crm->translate('Rename')));

		$filename = $p->pop();

		$filename = bab_Path::decode($filename);

		$editor = new crm_Editor($Crm);

		$editor->addItem(
			$editor->labelledField(
				$Crm->translate('Name'),
				$W->LineEdit(),
				'name'
			)
		);

		$editor->setValue('name', $filename);
		$editor->setHiddenValue('for', $for);
		$editor->setHiddenValue('src', $pathname);
		$editor->setHiddenValue('tg', bab_rp('tg'));

		$editor->setCancelAction($this->proxy()->cancel());
		$editor->setSaveAction($this->proxy()->rename());

		$page->addItem($editor);


		return $page;
	}


	/**
	 * Renames a folder or a file.
	 *
	 * @param string $for  A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param string $src  The source path relative to the object upload folder (contain the file name to rename)
	 * @param string $name The new name.
	 *
	 * @return Widget_Action
	 */
	public function rename($for = null, $src = null, $name = null)
	{
		$Crm = $this->Crm();

		$record = $Crm->getRecordByRef($for);
		$srcFile = $Crm->FileAttachment($record, $src);
		$srcPath = $srcFile->getPath();

		$srcPathString = $srcPath->toString();

		$srcPath->pop();
		$srcPath->push(bab_Path::encode($name));


		rename($srcPathString, $srcPath->toString());

		crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The file has been renamed.'));
	}



	/**
	 * Move folder or file
	 *
	 * @param 	string	$for 		A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param	string 	$src		The source path relative to the object upload folder (contain the file name to move)
	 * @param	string 	$dest		The destination path relative to the object upload folder (contain the file name to move)
	 *
	 * @return Widget_Action
	 */
	public function move($for = null, $src = null, $dest = null)
	{
		$Crm = $this->Crm();

		$record = $Crm->getRecordByRef($for);
		$srcFile = $Crm->FileAttachment($record, $src);
		$srcPath = $srcFile->getPath();

		$destFile = $Crm->FileAttachment($record, $dest);
		$destPath = $destFile->getPath();

		$destPath->push($srcPath->getBasename());

		rename($srcPath->toString(), $destPath->toString());

		crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The file has been moved.'));
	}





	/**
	 * Open file for download
	 *
	 * @param 	string	$for 		A reference to the object where path is attached to, a string like Contact:123 or Organization:654
	 * @param	string 	$pathname	The path relative to the object upload folder (contain the file name to download)
	 *
	 * @return Widget_Action
	 */
	public function open($for = null, $pathname = null, $inline = true)
	{
		$Crm = $this->Crm();

		$record = $Crm->getRecordByRef($for);
		$file = $Crm->FileAttachment($record, $pathname);

		$p = $file->getPath();

		if ($p->isDir()) {
			return $this->display($for, $file->getRelativePath()->toString());
		}

		$filename = $p->pop();

		$W = bab_Widgets();
		$file = $W->FilePicker()->getByName($p, $filename);
		$file->download($inline);
		die;
	}


	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}

}
