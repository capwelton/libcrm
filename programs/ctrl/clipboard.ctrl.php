<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on th clipboard.
 */
class crm_CtrlClipboard extends crm_Controller
{

    /**
     *
     * @param string $reference
     *
     * @return Widget_DislayableInterface
     */
    public function getReferenceCard($reference)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $crmRecord = $Crm->getRecordByRef($reference);

        /* @var $foldableCard Widget_Section */
        $foldableCard = null;

        if ($crmRecord instanceof crm_Email) {
            $objectClass = 'Email';
            $foldableCard = $W->Section(
                $crmRecord->subject,
                $Ui->HistoryElement($crmRecord),
                6
            )->setFoldable(true, false)
            ->addClass('crm-cardframe');
        }

        if ($crmRecord instanceof crm_Contact) {
        	$objectClass = 'Contact';
            $foldableCard = $W->Section(
                $crmRecord->getFullName(),
                $Ui->ContactCardFrame($crmRecord),
                6
            )->setFoldable(true, false)
            ->addClass('crm-cardframe');
        }

        if ($crmRecord instanceof crm_Order) {
            $objectClass = 'Order';
            if ($crmRecord->type == crm_Order::QUOTATION) {
                $typeName = $Crm->translate('Quotation');
            } else {
                $typeName = $Crm->translate('Estimate');
            }

            $foldableCard = $W->Section(
                $typeName . ' ' . $crmRecord->name,
                $Ui->OrderCardFrame($crmRecord),
                6
            )->setFoldable(true, false)
            ->addClass('crm-cardframe');
        }

        $acceptedClipboardObjects = $this->getAcceptedClipboardObjects();

        if (isset($foldableCard)) {
            $foldableCard->setId('crm-clipboard-' . $crmRecord->getRef());
            $menu = $foldableCard->addContextMenu('popup');

            if (isset($acceptedClipboardObjects[$objectClass])) {
                foreach ($acceptedClipboardObjects[$objectClass] as $action) {
                    $menu->addItem(
                        $W->Link(
                            $W->Icon($Crm->translate($action['label']), Func_Icons::ACTIONS_EDIT_PASTE),
                            str_replace('__1__', $reference, $action['action'])
                        )
                    );
                }
                $foldableCard->addClass('accepted');
                $foldableCard->setFoldable(true, false);
            } else {
                $foldableCard->setFoldable(true, true);
            }

            $menu->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('Remove from clipboard'), Func_Icons::ACTIONS_LIST_REMOVE),
                    $Crm->Controller()->Clipboard()->remove($reference)
                )
            );
        }
        return $foldableCard;
    }



    /**
     * Displays the content of the clipboard.
     *
     * @param array $acceptedClipboardObjects
     * @return Widget_Frame
     */
    public function displayContent($acceptedClipboardObjects = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $this->acceptedClipboardObjects = $acceptedClipboardObjects;

        $clipboardReferences = $Crm->getUserClipboardContent();

        $content = $W->Frame()->setLayout(
            $W->FlowItems()->setSpacing(1, 'em')
        );
        foreach ($clipboardReferences as $clipboardReference => $dummy) {
            if ($card = $this->getReferenceCard($clipboardReference)) {
                $content->addItem($card);
            }
        }

        return $content;
    }



    /**
     * Adds the referenced object to the current user's clipboard.
     *
     * @param string $reference
     */
    public function add($reference)
    {
        $Crm = $this->Crm();
        $Crm->addToUserClipboard($reference);

        return true;
    }



    /**
     * Removes the referenced object from the current user's clipboard.
     *
     * @param string $reference
     */
    public function remove($reference)
    {
        $Crm = $this->Crm();
        $Crm->removeFromUserClipboard($reference);

        return true;
    }


    /**
     * Does nothing and return to the previous page.
     *
     * @param array	$message
     * @return Widget_Action
     */
    public function cancel()
    {
    	crm_redirect(crm_BreadCrumbs::last());
    }
}
