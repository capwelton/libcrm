<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

$Crm = crm_Crm();
$Crm->includeRecordController();


/**
 * This controller manages actions that can be performed on articles.
 */
class crm_CtrlNote extends crm_RecordController implements crm_ShopAdminCtrl
{
    /**
     * {@inheritDoc}
     * @see crm_RecordController::getModifedMessage()
     */
    protected function getModifedMessage()
    {
        $Crm = $this->Crm();
        return $Crm->translate('The note has been saved.');
    }

    /**
     *
     * {@inheritDoc}
     * @see crm_RecordController::getCreatedMessage()
     */
    protected function getCreatedMessage()
    {
        $Crm = $this->Crm();
        return $Crm->translate('The note has been saved.');
    }

    /**
     * {@inheritDoc}
     * @see crm_RecordController::getDeletedMessage()
     */
    protected function getDeletedMessage(crm_Record $record)
    {
        $Crm = $this->Crm();
        return $Crm->translate('The note has been deleted.');
    }

    /**
     *
     * @param $note
     * @return Widget_Action
     */
    public function edit($note = null)
    {
        $Crm = $this->Crm();
        $Crm->includeStatusSet();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $noteSet = $Crm->NoteSet();
        $note = $noteSet->request($note);

        $page->setTitle($Crm->translate('Edit note'));

        $editor = $Crm->Ui()->NoteEditor();
        $editor->setName('data');
        $editor->setRecord($note);

        $editor->isAjax = bab_isAjaxRequest();

        $page->addItem($editor);

        if (isset($note)) {
            $page->addContextItem(crm_noteLinkedObjects($note));
        }

        return $page;
    }


    /**
     *
     * @param string $for       Reference of a crm object to which the note will be linked
     * @throws crm_AccessException
     * @return crm_Page
     */
    public function add($for = null)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Create a new note'));

        $editor = $this->Crm()->Ui()->NoteEditor();
        $editor->setName('data');

        $editor->addItem($W->Hidden()->setName('for')->setValue($for));

        $editor->isAjax = bab_isAjaxRequest();

        $page->addItem($editor);

        return $page;
    }

	/**
	 *
	 * @param int $note 		The note id
	 * @return Widget_Action
	 */
	public function display($note)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$noteSet = $Crm->NoteSet();
		$note = $noteSet->request($note);
		if (!isset($note)) {
			throw new crm_Exception($Crm->translate('Trying to access a note with a wrong (unknown) id.'));
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($note->id), $Crm->translate('Note'));

		$page = $this->Crm()->Ui()->Page();
		$page->addClass('crm-page-editor');

		if ($note->private) {
			$page->addItem($W->Title($Crm->translate('Private note'), 1));
		} else {
			$page->addItem($W->Title($Crm->translate('Note'), 1));
		}

		$noteFrame = $Crm->Ui()
			->HistoryElement($note, crm_HistoryElement::MODE_FULL)
			->addComments();

		$page->addItem($noteFrame);




		$actionsFrame = $page->ActionsFrame();

		$page->addContextItem($actionsFrame);


		if ($Crm->Access()->updateNote($note)) {
			$actionsFrame->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Edit this note'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
					$Crm->Controller()->Note()->edit($note->id)
				)
			);
		}

		if ($Crm->Access()->deleteNote($note)) {
			$actionsFrame->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Delete this note'), Func_Icons::ACTIONS_EDIT_DELETE),
					$Crm->Controller()->Note()->delete($note->id)
				)->setConfirmationMessage($Crm->translate('Are you sure you want to delete this note? It will be definitely lost!'))
			);
		}



		$page->addContextItem(crm_noteLinkedObjects($note));

		return $page;
	}


	/**
	 *
	 * {@inheritDoc}
	 * @see crm_RecordController::postSave()
	 */
	protected function postSave(crm_Record $record, $data)
	{
	    $Crm = $this->Crm();
	    $W = bab_Widgets();
	    if (isset($data['for'])) {
	        $subject = $Crm->getRecordByRef($data['for']);
	        $record->linkTo($subject);
	    }

	    if ($iterator = $W->FilePicker()->getTemporaryFiles('attachments')) {
	        foreach ($iterator as $file) {
	            $record->attachFile($file);
	        }
	        $W->FilePicker()->cleanup();
	    }
	}


	/**
	 * Downloads the selected attached file.
	 *
	 * @param int		$note		The note id
	 * @param string	$filename
	 * @return Widget_Action
	 */
	public function downloadAttachment($note = null, $filename = null, $inline = 1)
	{
		$Crm = $this->Crm();
		$note = $Crm->NoteSet()->get($note);
		if (!isset($note)) {
			$this->error($Crm->translate('The specified note does not seem to exist.'));
		}
		$file = bab_Widgets()->FilePicker()->getByName($note->uploadPath(), $filename);
		if (!isset($file)) {
			$this->error($Crm->translate('The file you requested does not seem to exist.'));
		}
		$file->download($inline == 1);
	}


	/**
	 * Does nothing and returns to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}


	/**
	 * Returns to the previous page and displays the specified error message.
	 *
	 * @param string	$errorMessage
	 * @return Widget_Action
	 */
	protected function error($errorMessage)
	{
		crm_redirect(crm_BreadCrumbs::last(), $errorMessage);
	}


	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
