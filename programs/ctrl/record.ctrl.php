<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


$Crm = new Func_Crm();
$Crm->includeController();


abstract class crm_RecordController extends crm_Controller
{

    protected function getClass()
    {
        return get_class($this);
    }


    /**
     * This methods returns the Record classname managed by this controller.
     * The default method guess the record classname based on the controller name.
     * It can be overriden by inherited controllers.
     *
     * @return string
     */
    protected function getRecordClassName()
    {
        list(, $recordClassname) = explode('_Ctrl', $this->getClass());
        return $recordClassname;
    }


    /**
     * This methods returns the Record set managed by this controller.
     * The default method guess the record set based on the controller name.
     * It can be overriden by inherited controllers.
     *
     * @return crm_RecordSet
     */
    protected function getRecordSet()
    {
        $Crm = $this->Crm();
        $recordClassname = $this->getRecordClassName();
        $recordSetClassname = $recordClassname . 'Set';

        $recordSet = $Crm->$recordSetClassname();
        return $recordSet;
    }


    /**
     * @return crm_RecordSet
     */
    protected function getEditRecordSet()
    {
        return $this->getRecordSet();
    }


    /**
     * This methods returns the Record set used to save records
     *
     * @return crm_RecordSet
     */
    protected function getSaveRecordSet()
    {
        return $this->getRecordSet();
    }


    protected function getDeleteRecordSet()
    {
        return $this->getSaveRecordSet();
    }



    /**
     *
     * @return string[]
     */
    protected function getAvailableModelViewTypes()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $types = array();

        $recordClassname = $this->getRecordClassName();

        $viewClassname =  $recordClassname . 'TableView';
        if (method_exists($Ui, $viewClassname)) {
            $types['table'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::ACTIONS_VIEW_LIST_TEXT,
                'label' => $Crm->translate('Detailed list')
            );
        }

        $viewClassname =  $recordClassname . 'CardsView';
        if (method_exists($Ui, $viewClassname)) {
            $types['cards'] = array(
                'classname' => $viewClassname,
                'icon' => 'actions-view-list-cards', //Func_Icons::ACTIONS_VIEW_LIST_CARDS
                'label' => $Crm->translate('Cards')
            );
        }

        $viewClassname =  $recordClassname . 'MapView';
        if (method_exists($Ui, $viewClassname)) {
            $types['map'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::APPS_PREFERENCES_SITE,
                'label' => $Crm->translate('Map')
            );
        }

        return $types;
    }



    /**
     * Returns an array of field names and descriptions that can be used in Full/CardFrames.
     *
     * @return string[]
     */
    public function getAvailableDisplayFields()
    {
        $Crm = $this->Crm();
        $recordSet = $this->getRecordSet();

        $availableFields = array();

        foreach ($recordSet->getFields() as $name => $field) {
            $fieldName = $field->getName();
            $fieldDescription = $field->getDescription();
            if (substr($fieldName, 0, 1) !== '_') {
                $fieldDescription = $Crm->translate($fieldDescription);
            }
            if (empty($fieldDescription)) {
                $fieldDescription = $fieldName;
            }
            $availableFields[$name] = array(
                'name' => $fieldName,
                'description' => $fieldDescription
            );
        }

        return $availableFields;
    }



    /**
     * @param string $itemId
     * @return Widget_VBoxLayout
     */
    public function availableDisplayFields($section = null, $itemId = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->newRecord();

        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $allViewSections = $customSectionSet->select(
            $customSectionSet->view->is($customSection->view)->_AND_(
                $customSectionSet->object->is($record->getClassName())
            )
        );

        $allViewFieldNames = array();
        foreach ($allViewSections as $viewSection) {
            $fields = $viewSection->getFields();
            foreach ($fields as $field) {
                $allViewFieldNames[$field['fieldname']] = $viewSection->view . ',' . $viewSection->rank . ',' . $viewSection->id;
            }
        }

        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }


        $box->addItem(
            $W->Link(
                $Crm->translate('Create new field'),
                $Crm->Controller()->CustomField()->add($record->getClassName())
            )->setOpenMode(widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC)
        );

        $availableFields = $this->getAvailableDisplayFields();

        bab_Sort::asort($availableFields, 'description', bab_Sort::CASE_INSENSITIVE);

        foreach ($availableFields as $field) {
            $fieldName =  $field['name'];
            $fieldDescription = $field['description'];

            $used = isset($allViewFieldNames[$fieldName]);
            $box->addItem(
                $W->VBoxItems(
                    $W->Link(
                        $fieldDescription,
                        $this->proxy()->saveDisplayField($section, $fieldName)
                    )->addClass('widget-close-dialog', ($used ? 'widget-strong' : ''))
                    ->setTitle($fieldName)
                    ->setAjaxAction(),
                    $W->Hidden()->setName(array('sections', $field['name']))
                )->setSizePolicy('widget-list-element')
            );
        }

        $box->addClass('widget-3columns');

        $box->setReloadAction($this->proxy()->availableDisplayFields($section, $box->getId()));

        return $box;
    }



    /**
     *
     */
    public function addDisplayField($section = null, $filter = null)
    {
        $Crm = $this->Crm();

        $page = $Crm->Ui()->Page();
        $page->setTitle($Crm->translate('Available fields'));

        $box = $this->availableDisplayFields($section);

        $page->addItem($box);
        return $page;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function saveDisplayField($section = null, $fieldName = null, $parameters = null)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));
        $customSection->removeField($fieldName);
        $customSection->addField($fieldName, $parameters);
        $customSection->save();

        return true;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function editDisplayField($section, $fieldName)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $field = $customSection->getField($fieldName);

        if (!isset($field)) {
            return null;
        }

        $W = bab_Widgets();

        $fieldName = $field['fieldname'];
        $params = $field['parameters'];

        $availableFields = $this->getAvailableDisplayFields();
        $field = $availableFields[$fieldName];
        $fieldDescription = $field['description'];
        if (substr($fieldName, 0, 1) !== '_') {
            $fieldDescription = $Crm->translate($fieldDescription);
        }

        if (empty($field)) {
            //continue;
        }

        $page = $Crm->Ui()->Page();
        $page->setTitle($fieldDescription);

        $editor = new crm_Editor($Crm);
        $editor->setHiddenValue('tg', $Crm->controllerTg);
        $editor->setHiddenValue('section', $section);
        $editor->setHiddenValue('fieldName', $fieldName);

        $box = $W->VBoxItems(
            $W->LabelledWidget(
                $Crm->translate('Type'),
                $W->Select()
                    ->setName(array('parameters', 'type'))
                    ->addOption('', '')
                    ->addOption('title1', $Crm->translate('Title 1'))
                    ->addOption('title2', $Crm->translate('Title 2'))
                    ->addOption('title3', $Crm->translate('Title 3'))
            ),
            $W->LabelledWidget(
                $Crm->translate('Label'),
                $W->LineEdit()
                    ->setName(array('parameters', 'label'))
            ),
            $W->LabelledWidget(
                $Crm->translate('Class'),
                $W->LineEdit()
                    ->setName(array('parameters', 'classname'))
            )
        )->setVerticalSpacing(1, 'em');

        $editor->setValues($params, array('parameters'));

        $editor->addItem($box);

        $editor->setSaveAction($this->proxy()->saveDisplayField());

        $page->addItem($editor);

        return $page;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function removeDisplayField($section, $fieldName)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $customSection->removeField($fieldName);
        $customSection->save();

        return true;
    }


    /**
     *
     * @return crm_Page
     */
    public function sections($view, $itemId = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $availableFields = $this->getAvailableDisplayFields();

        $sectionsSection = $W->VBoxItems();

        $customSectionCtrl = $Crm->Controller()->CustomSection();

        $customSectionSet = $Crm->CustomSectionSet();
        $customSections = $customSectionSet->select(
            $customSectionSet->object->is($this->getRecordClassName())->_AND_(
                $customSectionSet->view->is($view)
            )
        );
        $customSections->orderAsc($customSectionSet->rank);

        $sectionsBoxes = array();
        /* @var $rows Widget_Layout[] */
        $rows = array();

        $currentColumn = 0;
        $row = $W->Items()->addClass('connectedSortable')->setSizePolicy('row');
        $rows[] = $row;

        $nbCol = 0;
        foreach ($customSections as $customSection) {

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

//             if ($currentColumn + $nbCol > 12) {
//                 $sectionsSection->addItem($row);
//                 $row = $W->Items()->addClass('connectedSortable')->setSizePolicy('row');
//                 $rows[] = $row;
//                 $currentColumn = 0;
//             }
            $currentColumn += $nbCol;

            $sectionSection = $W->Section(
                $customSection->name . ' (' . $customSection->rank . ')',
                $sectionBox = $W->VBoxItems($W->Hidden()->setName('#' . $customSection->id))
//                     ->addClass('widget-dockable-content')
            );
            $sectionSection->addClass('crm-custom-section');
            $sectionSection->addClass($customSection->classname);
            $sectionSection->setSizePolicy($customSection->sizePolicy);

            $row->addItem($sectionSection);

            $menu = $sectionSection->addContextMenu('inline');
            $menu->addItem(
                $W->FlowItems(
                    $W->Link('', $this->proxy()->addDisplayField($customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_LIST_ADD)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Link('', $customSectionCtrl->edit($view, $customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Link('', $customSectionCtrl->editVisibility($customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Link('', $customSectionCtrl->confirmDelete($customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG)
                )
            );
            $fields = $customSection->getFields();
            foreach ($fields as $fieldId => $field) {
                if (empty($field)) {
                    continue;
                }

                $fieldName = $field['fieldname'];

                $field = $availableFields[$fieldName];
                $fieldDescription = $field['description'];
                if (substr($fieldName, 0, 1) !== '_') {
                    $fieldDescription = $Crm->translate($fieldDescription);
                }

                if (empty($field)) {
                    continue;
                }

                $fieldItem = $W->FlowItems(
                    $W->Label($fieldDescription)->setSizePolicy(Func_Icons::ICON_LEFT_16 . ' widget-80pc' ),
                    $W->FlowItems(
                        $W->Hidden()->setName($customSection->id . '.' . $fieldId)->setValue($field['name']),
                        $W->Link('', $this->proxy()->editDisplayField($customSection->id, $field['name']))
                            ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                            ->setOpenMode(Widget_Link::OPEN_DIALOG),
                        $W->Link('', $this->proxy()->removeDisplayField($customSection->id, $field['name']))
                            ->addClass('icon', Func_Icons::ACTIONS_LIST_REMOVE)
                            ->setAjaxAction()
                    )->setSizePolicy(Func_Icons::ICON_LEFT_16 . ' widget-20pc widget-align-right widget-actions' )
                )->setSizePolicy('widget-list-element  widget-actions-target');

                $sectionBox->addItem($fieldItem);
            }

            $sectionsBoxes[] = $sectionBox;
        }

        if ($currentColumn + $nbCol > 0) {
            $sectionsSection->addItem($row);
        }

        foreach ($sectionsBoxes as $sectionsBox) {
            $sectionsBox->sortable(true, $sectionsBoxes);
            $sectionsBox->setMetadata('samePlaceholderSize', true);
        }

        foreach ($rows as $row) {
            $row->sortable(true, $rows);
            $row->setMetadata('samePlaceholderSize', true);
        }

        $form = $W->Form();

        if (isset($itemId)) {
            $form->setId($itemId);
        }
        $form->setHiddenValue('tg', $Crm->controllerTg);
        $form->setName('sections');
        $form->addItem($sectionsSection);
        $form->setAjaxAction($this->proxy()->saveSections(), $sectionsSection, 'sortstop');
        $form->setReloadAction($this->proxy()->sections($view, $form->getId()));

        $form->addClass('depends-custom-sections');

        return $form;
    }



    /**
     *
     * @param string $view
     * @return crm_Page
     */
    public function exportSections($view)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();


        $objectName = $this->getRecordClassName();

        $customSections = $customSectionSet->select(
            $customSectionSet->view->is($view)->_AND_($customSectionSet->object->is($objectName))
        );

        $sectionsValues = array();
        foreach ($customSections as $customSection) {
            $values = $customSection->getValues();
            unset($values['id']);
            unset($values['createdBy']);
            unset($values['createdOn']);
            unset($values['modifiedBy']);
            unset($values['modifiedOn']);
            unset($values['deletedBy']);
            unset($values['deletedOn']);
            unset($values['deleted']);
            $sectionsValues[] = $values;
        }

        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename="' . $objectName . '.' . ($view === '' ? 'default' : $view) . '.json"'."\n");

        $json = bab_json_encode($sectionsValues);
        $json = bab_convertStringFromDatabase($json, bab_charset::UTF_8);

        die($json);
    }




    /**
     * @param string $view
     * @return crm_Page
     */
    public function importSectionsConfirm($view = '')
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $page = $Crm->Ui()->Page();

        $page->setTitle($Crm->translate('Import sections layout'));

        $editor = new crm_Editor($Crm);
        $editor->setHiddenValue('tg', $Crm->controllerTg);

        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('View layout name'),
                $W->LineEdit(),
                'view'
            )
        );

        $editor->addItem(
            $W->LabelledWidget(
                $Crm->translate('File'),
                $W->FilePicker(),
                'sectionsfile'
            )
        );

        $editor->setSaveAction($this->proxy()->importSections());

        $page->addItem($editor);

        return $page;
    }


    /**
     * @param string $view
     * @param string $sectionsfile
     * @return bool
     */
    public function importSections($view = null, $sectionsfile = null)
    {
        $W = bab_Widgets();

        $files = $W->FilePicker()->getFolder();
        $files->push('sectionsfile');
        $files->push($sectionsfile['uid']);

        foreach ($files as $f) {
            $data = file_get_contents($f->toString());
            break;
        }

        $sectionsValues = json_decode($data, true);

        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $objectName = $this->getRecordClassName();

        $customSectionSet->delete(
            $customSectionSet->view->is($view)->_AND_($customSectionSet->object->is($objectName))
        );

        foreach ($sectionsValues as $sectionValues) {
            $customSection = $customSectionSet->newRecord();
            $customSection->setValues($sectionValues);
            $customSection->view = $view;
            $customSection->save();
        }

        return true;
    }



    /**
     *
     * @param string $view
     * @return bool
     */
    public function deleteSections($view)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $objectName = $this->getRecordClassName();

        $customSectionSet->delete(
            $customSectionSet->view->is($view)->_AND_($customSectionSet->object->is($objectName))
        );

        $this->addMessage(sprintf($Crm->translate('%s view has been deleted.'), $view));
        return true;
    }


    /**
     *
     * @param string $view
     * @return crm_Page
     */
    public function editSections($view)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $page = $Crm->Ui()->Page();

        $page->setTitle(sprintf($Crm->translate('Edit view layout %s'), $view));

        $toolbar = new crm_Toolbar();
        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Delete view layout'),
                $this->proxy()->confirmDeleteSections($view)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD_PAGE)
        );

        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Export view layout'),
                $this->proxy()->exportSections($view)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
        );

        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Add a section'),
                $Crm->Controller()->CustomSection()->edit($view, null, $this->getRecordClassName())
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG)
        );

        $form = $this->sections($view);

        $page->addItem($toolbar);
        $page->addItem($form);

        return $page;
    }



    public function saveSections($sections = null)
    {
        $Crm = $this->Crm();
        $customSectionSet = $Crm->CustomSectionSet();

        $sectionsFields = array();
        $sectionsRawFields = array();

        $rank = 0;
        foreach (array_keys($sections) as $sectionFieldId) {

            if (substr($sectionFieldId, 0, 1) === '#') {
                $currentSectionId = substr($sectionFieldId, 1);

                $customSection = $customSectionSet->request($customSectionSet->id->is($currentSectionId));

                $sectionsFields[$currentSectionId] = array();
                $sectionsRawFields[$currentSectionId] = $customSection->getRawFields();

                $customSection->rank = $rank;
                $rank++;
                $customSection->save();
                continue;
            }

            $sectionsFields[$currentSectionId][] = $sectionFieldId;
        }

        foreach ($sectionsFields as $sectionId => $sectionFieldIds) {
            foreach ($sectionFieldIds as $sectionFieldId) {
                list($srcSectionId, $srcfieldId) = explode('.', $sectionFieldId);
                if (!isset($sectionsRawFields[$srcSectionId][$srcfieldId])) {
                    return true;
                }
            }
        }

//        var_dump($sectionsRawFields);

        foreach ($sectionsFields as $sectionId => $sectionFieldIds) {
            $newRawFields = array();
            foreach ($sectionFieldIds as $sectionFieldId) {
                list($srcSectionId, $srcfieldId) = explode('.', $sectionFieldId);
                $newRawFields[] = $sectionsRawFields[$srcSectionId][$srcfieldId];
            }

            $customSection = $customSectionSet->request($customSectionSet->id->is($sectionId));
            $customSection->fields = implode(',', $newRawFields);
            $customSection->save();
        }

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }


    /**
     * Displays an editor of the record values corresponding to the fields of a custom section.
     *
     * @param int $id                   The id of the record to edit
     * @param int $customSectionId      The id of the section used to create the editor
     * @param string $itemId
     *
     * @throws crm_AccessException
     * @return crm_Page
     */
    public function editSection($id, $customSectionId, $itemId = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();

        $customSectionSet = $Crm->CustomSectionSet();
        $customSection = $customSectionSet->get(
            $customSectionSet->id->is($customSectionId)
        );

        $page->setTitle($customSection->name);

        $recordSet = $this->getEditRecordSet();

        $recordClassname = $this->getRecordClassName();
        $editorClassname =  $recordClassname . 'SectionEditor';
        /* @var $editor crm_RecordEditor */
        $editor = $Ui->$editorClassname();
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $editor->setId($itemId);
        $editor->setHiddenValue('tg', $Crm->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');
        $editor->addItem($W->Hidden()->setName('id'));

        $editor->recordSet = $recordSet;
        $section = $editor->sectionContent($customSectionId);
        $section->setSizePolicy('widget-60em');

        $editor->addItem($section);

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new crm_AccessException($Crm->translate('You do not have access to this page.'));
            }
            $editor->setRecord($record);
        } else {
            if (!$recordSet->isCreatable()) {
                throw new crm_AccessException($Crm->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
            $editor->setRecord($record);
        }

        $editor->isAjax = bab_isAjaxRequest();


        $page->addItem($editor);
        return $page;
    }


    /**
     * @param string $itemId
     * @return string
     */
    public function getModelViewDefaultId($itemId = null)
    {
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_modelView';
        }

        return $itemId;
    }

    /**
     * Returns the xxxModelView associated to the RecordSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return crm_TableModelView
     */
    protected function modelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordClassname = $this->getRecordClassName();

        $itemId = $this->getModelViewDefaultId($itemId);

        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }

        switch ($type) {
            case 'cards':
                $tableviewClassname =  $recordClassname . 'CardsView';
                break;

            case 'map':
                $tableviewClassname =  $recordClassname . 'MapView';
                break;

            case 'table':
            default:
                $tableviewClassname =  $recordClassname . 'TableView';
                break;
        }

        /* @var $tableview widget_TableModelView */
        $tableview = $Ui->$tableviewClassname();

        $tableview->setRecordController($this);

        $tableview->setId($itemId);

        return $tableview;
    }




    /**
     * @return crm_Editor
     */
    protected function recordEditor($itemId = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordClassname = $this->getRecordClassName();
        $editorClassname =  $recordClassname . 'Editor';
        $editor = $Ui->$editorClassname();
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $editor->setId($itemId);
        $editor->setHiddenValue('tg', $Crm->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');

        $editor->isAjax = bab_isAjaxRequest();

        return $editor;
    }



    /**
     * @param widget_TableModelView $tableView
     * @return crm_Toolbar
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $proxy = $this->proxy();

        $filter = $tableView->getFilterValues();

        $toolbar = new crm_Toolbar();

        $viewTypes = $this->getAvailableModelViewTypes();

        if (count($viewTypes) > 1) {
            $viewsBox = $W->Items();
            $viewsBox->setSizePolicy('pull-right');
//            $viewsBox->addClass('btn-group', Func_Icons::ICON_LEFT_16);
            $toolbar->addItem($viewsBox);

            $filteredViewType = $this->getFilteredViewType($tableView->getId());

            foreach ($viewTypes as $viewTypeId => $viewType) {
                $viewsBox->addItem(
                    $W->Link(
                        '',
                        $proxy->setFilteredViewType($tableView->getId(), $viewTypeId)
                    )->addClass('icon', $viewType['icon'] . ($filteredViewType=== $viewTypeId ? ' active' : ''))
//                    ->setSizePolicy('btn btn-xs btn-default ' . ($filteredViewType === $viewTypeId ? 'active' : ''))
                    ->setTitle($viewType['label'])
                    ->setAjaxAction()
                );
            }
        }

        if (method_exists($proxy, 'exportSelect')) {
            $toolbar->addItem(
                $W->Link(
                    $Crm->translate('Export'),
                    $proxy->exportSelect($filter)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
                ->setOpenMode(Widget_Link::OPEN_DIALOG)
            );
        }

        if (method_exists($proxy, 'printList')) {
            $toolbar->addItem(
                $W->Link(
                    $Crm->translate('Print'),
                    $proxy->printList($filter)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PRINT)
                ->setOpenMode(Widget_Link::OPEN_POPUP)
            );
        }

        return $toolbar;
    }



    /**
     * Returns the current filtered view type (table, cards...)
     * @param string        $itemId     The model view widget id
     * @return string
     */
    protected function getFilteredViewType($itemId)
    {
        $W = bab_Widgets();
        $type = $W->getUserConfiguration($itemId . '/viewType');
        if (!isset($type)) {
            $type = 'table';
        }
        return $type;
    }

    /**
     * Sets the current filtered view type (table, cards...)
     * @param string        $itemId     The model view widget id
     * @param string        $type       'table, 'cards'...
     */
    public function setFilteredViewType($itemId, $type = null)
    {
        $W = bab_Widgets();
        $W->setUserConfiguration($itemId . '/viewType', $type);

        return true;
    }

    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function filteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $view = $this->modelView($filter, $type, null, $itemId);
        $view->setAjaxAction();

        if (isset($filter)) {
            $view->setFilterValues($filter);
        }
        $filter = $view->getFilterValues();

        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $view->displaySubTotalRow(true);
        }


        $recordSet = $this->getRecordSet();
        $view->setRecordSet($recordSet);
        $view->addDefaultColumns($recordSet);

        $conditions = $view->getFilterCriteria($filter);
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $records = $recordSet->select($conditions);

        $view->setDataSource($records);

        $view->allowColumnSelection();

        $filterPanel = $view->advancedFilterPanel($filter);

        $toolbar = $this->toolbar($view);

        $box = $W->VBoxItems(
            $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->filteredView(null, null, $view->getId()));

        return $box;
    }



    /**
     * @param array $filter
     * @param string $type
     * @return array
     */
    protected function getListItemMenus($filter = null, $type = null)
    {
        $Crm = $this->Crm();
        $itemMenus = array();

//         $tabs = $this->getTabs();

//         if (count($tabs) > 0) {
//             $itemMenus['all'] = array (
//                 'label' => $Crm->translate('All'),
//                 'action' => $this->proxy()->setCurrentTab('all')
//             );
//         }
//         foreach ($tabs as $tab) {
//             $itemMenus[$tab] = array(
//                 'label' => $tab,
//                 'action' => $this->proxy()->setCurrentTab($tab)
//             );
//         }

        return $itemMenus;
    }



    /**
     * @param array	$filter
     * @param string $type
     * @return Widget_Page
     */
    public function displayList($filter = null, $type = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordSet = $this->getRecordSet();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($filter), $Crm->translate($recordSet->getDescription()));

//         if (!$recordSet->isBrowsable()) {
//             throw new crm_AccessException($Crm->translate('Access denied'));
//         }

        $page = $Ui->Page();
        $page->addClass('crm-page-list');

        //$page->addItemMenu('list', $Crm->translate($recordSet->getDescription()), $this->proxy()->displayList($filter));
        $itemMenus = $this->getListItemMenus();

        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }
//         $tab = $this->getCurrentTab();
//         if ($tab) {
//             $page->setCurrentItemMenu($tab);
//         }

        $filteredView = $this->filteredView($filter, $type);

        $page->addItem($filteredView);

        return $page;
    }



    /**
     * This method existed in contact controller before creation of exportSelect
     * @deprecated Use exportSelect
     */
    public function exportOptions($filter = null)
    {
        return $this->exportSelect($filter);
    }


    /**
     *
     * @param array $filter
     * @return Widget_Page
     */
    public function exportSelect($filter = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Export list'));

        $tableview = $this->modelView($filter, 'table');

        $recordSet = $this->getRecordSet();
        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);

        $conditions = $tableview->getFilterCriteria($filter);
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $records = $recordSet->select($conditions);

        $tableview->setDataSource($records);

        $editor = $Ui->ExportSelectEditor(__METHOD__, $tableview, $filter);

        $editor->setSaveAction(
            $this->proxy()->exportList(),
            $Crm->translate('Export')
        );
        $page->addItem($editor);

        return $page;
    }





    /**
     * @param	array	$filter
     */
    public function exportList($filter = null, $format = 'csv', $columns = null, $inline = true, $filename = 'export')
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $Ui->includeContact();

        $page = $Ui->Page();
        $page->addClass('crm-page-list');

        $tableview = $this->modelView($filter, 'table');

        $recordSet = $this->getRecordSet();
        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);

        $conditions = $tableview->getFilterCriteria($filter);
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $records = $recordSet->select($conditions);

        $tableview->setDataSource($records);

        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();
            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }

        $tableview->allowColumnSelection(false);

        switch ($format) {
            case 'xlsx':
                $tableview->downloadXlsx($filename . '.xlsx');
            case 'xls':
                $tableview->downloadExcel($filename . '.xls');
            case 'ssv':
                $tableview->downloadCsv($filename . '.csv', ';', $inline, 'Windows-1252');
            case 'csv':
            default:
                $tableview->downloadCsv($filename . '.csv', ',', $inline, bab_charset::getIso());
        }
    }



    protected function getTabs()
    {
        $id = $this->getModelViewDefaultId();

        $tabs = array();

        $registry = bab_getRegistryInstance();

        $userId = '0';
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $id. '/tabs');

        while ($filterName = $registry->fetchChildKey()) {
            $tabs[] = $filterName;
        }

        $userId = bab_getUserId();
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $id. '/tabs');

        while ($filterName = $registry->fetchChildKey()) {
            $tabs[] = $filterName;
        }

        return $tabs;
    }



    /**
     * @return string[]
     */
    public function getFilterNames()
    {
        $id = $this->getModelViewDefaultId();

        $filters = array();

        $registry = bab_getRegistryInstance();

        $userId = '0';
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $id. '/filters');

        while ($filterName = $registry->fetchChildKey()) {
            $filters[] = $filterName;
        }

        $userId = bab_getUserId();
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $id. '/filters');

        while ($filterName = $registry->fetchChildKey()) {
            $filters[] = $filterName;
        }

        return $filters;
    }



    /**
     * Sets the currently used filter for the page.
     *
     * @param string $filterName
     * @return boolean
     */
    public function setCurrentFilterName($filterName)
    {
        $W = bab_Widgets();
        $tableview = $this->modelView(null, 'table');
        $W->setUserConfiguration($tableview->getId(). '/currentFilterName', $filterName, 'widgets', false);

        $registry = bab_getRegistryInstance();
        $userId = '0';
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $tableview->getId() . '/filters');
        $filterValues = $registry->getValue($filterName);

        if (!$filterValues) {
            $filterValues = $W->getUserConfiguration($tableview->getId() . '/filters/' . $filterName, 'widgets', false);
        }

        $tableview = $this->modelView($filterValues, 'table');
        $tableview->setFilterValues($filterValues['values']);

        return true;
    }



    /**
     *
     * @return string|null
     */
    protected function getCurrentFilterName()
    {
        $W = bab_Widgets();
        $id = $this->getModelViewDefaultId();
        $filter = $W->getUserConfiguration($id. '/currentFilterName', 'widgets', false);

        return $filter;
    }



    /**
     * @return boolean
     */
    public function resetFilters($name = null)
    {
        if (isset($name)) {
            $W = bab_Widgets();
            $filter = $W->getUserConfiguration($tableview->getId(). '/filters/' . $name, 'widgets', false);
            $filterValues = $filter['values'];
        } else {
            $filterValues = null;
        }

        $tableview = $this->modelView($filterValues, 'table');
        $tableview->setFilterValues($filterValues);

        return true;
    }



    /**
     * Saves the values of the current view filter under the specified name.
     *
     * @param string $name
     * @return boolean
     */
    public function saveCurrentFilter($name = null, $description = null)
    {
        $W = bab_Widgets();
        $tableview = $this->modelView(null, 'table');
        $filterValues = $tableview->getFilterValues();

        $data = array(
            'description' => $description,
            'values' => $filterValues,
        );

        $W->setUserConfiguration($tableview->getId(). '/filters/' . $name, $data, 'widgets', false);

        return true;
    }


    /**
     * Display a dialog to propose saving the current view filter under a specific name.
     *
     * @return crm_Page
     */
    public function confirmSaveCurrentFilter()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $currentFilterName = $this->getCurrentFilterName();
        $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $currentFilterName, 'widgets', false);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Save current view'));

        $form = new crm_Editor($Crm);


        $form->addItem(
            $W->Html(bab_toHtml($Crm->translate('Save the current filter values so that you can reuse them later.')))
        );

        $form->addItem(
            $W->LabelledWidget(
                $Crm->translate('Name'),
                $W->LineEdit()->setValue($currentFilterName),
                'name'
            )
        );
        $form->addItem(
            $W->LabelledWidget(
                $Crm->translate('Description'),
                $W->TextEdit()->setValue($filter['description']),
                'description'
            )
        );

        $confirmedAction = $this->proxy()->saveCurrentFilter();
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($Crm->translate('Save'))
        );

        $page->addItem($form);

        return $page;
    }


    /**
     * Display a dialog to propose saving the current view filter under a specific name.
     *
     * @return crm_Page
     */
    public function editFilter($name)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $name, 'widgets', false);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Edit filter'));

        $form = new crm_Editor($Crm);

        $form->addItem(
            $W->LabelledWidget(
                $Crm->translate('Name'),
                $W->LineEdit()->setValue($name),
                'name'
            )
        );
        $form->addItem(
            $W->LabelledWidget(
                $Crm->translate('Description'),
                $W->TextEdit()->setValue($filter['description']),
                'description'
            )
        );

        $confirmedAction = $this->proxy()->saveCurrentFilter();
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($Crm->translate('Save'))
        );

        $page->addItem($form);

        return $page;
    }


    public function manageFilters()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
        $page = $Ui->Page();

        $page->setTitle($Crm->translate('Manage filters'));

        $filtersBox = $W->VBoxItems();

        $page->addItem($filtersBox);

        $filterNames = $this->getFilterNames();
        foreach ($filterNames as $filterName) {
            $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $filterName, 'widgets', false);

            $filterBox = $W->FlowItems()
                ->setVerticalAlign('top')
                ->setSizePolicy('widget-list-element');
            $filterBox->addItem(
                $W->VBoxItems(
                    $W->Label($filterName)
                        ->addClass('widget-strong'),
                    $W->Label($filter['description'])
                )->setSizePolicy('widget-90pc')
            );
            $filterBox->addItem(
                $W->FlowItems(
                    $W->Link('', $this->proxy()->editFilter($filterName))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $W->Link('', $this->proxy()->confirmDeleteFilter($filterName))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                )->setSizePolicy('widget-10pc')
                ->addClass(Func_Icons::ICON_LEFT_16)
            );
            $filtersBox->addItem($filterBox);
        }

        $page->setReloadAction($this->proxy()->manageFilters());

        return $page;
    }

    /**
     * Display a dialog to propose deleting sections of a view.
     *
     * @param string $view
     * @return crm_Page
     */
    public function confirmDeleteSections($view)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Deletion'));

        $form = new crm_Editor($Crm);

        $form->addItem($W->Title($view, 5));

        $form->addItem($W->Title($Crm->translate('Confirm delete?'), 6));

        $form->addItem($W->Html(bab_toHtml($Crm->translate('It will not be possible to undo this deletion.'))));

        $confirmedAction = $this->proxy()->deleteSections($view);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($Crm->translate('Delete'))
        );
        $form->addButton($W->SubmitButton()->setLabel($Crm->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }


    /**
     * Display a dialog to propose deleting a view filter.
     *
     * @param string $name
     * @return crm_Page
     */
    public function confirmDeleteFilter($name)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $name, 'widgets', false);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Delete filter'));

        $form = new crm_Editor($Crm);

        $form->addItem(
            $W->Html(bab_toHtml($filter['description']))
        );

        $form->setHiddenValue('name', $name);

        $confirmedAction = $this->proxy()->deleteFilter();
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($Crm->translate('Delete'))
        );

        $page->addItem($form);

        return $page;
    }


    /**
     *
     * @param string $name
     */
    public function deleteFilter($name = null)
    {
        $this->requireDeleteMethod();

        $W = bab_Widgets();
        $W->deleteUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $name, 'widgets', false);

        return true;
    }



    /**
     * Returns a page with the record information.
     *
     * @param int	$id
     * @return crm_Page
     */
    public function display($id, $view = '')
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();

        $recordSet = $this->getRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isReadable()) {
            throw new crm_AccessException($Crm->translate('You do not have access to this page.'));
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($id), $record->getRecordTitle());


        $W = bab_Widgets();

        $recordClassname = $this->getRecordClassName();
        $fullFrameClassname =  $recordClassname . 'FullFrame';
        $fullFrame = $Ui->$fullFrameClassname($record, $view);


        $maxHistory = 8;
        $historyFrame = $W->VBoxItems(
            $Crm->Controller()->Search(false)->history($record->getRef(), $maxHistory, 0, true)
        );
        $fullFrame->addItem(
            $W->Section(
                $Crm->translate('History'),
                $historyFrame,
                2
            )->setFoldable(true)
            ->addClass('compact')
        );

        $page->addItem($fullFrame);


        // Actions
        $page->addContextItem($this->getDisplayActionFrame($page, $record));


        $page->addClass('depends-' . $this->getRecordClassName());

        return $page;
    }



    protected function getDisplayActionFrame(Widget_Page $page, crm_Record $record)
    {
        $actionsFrame = $page->ActionsFrame();
        return $actionsFrame;
    }



    /**
     * Returns a page containing an editor for the record.
     *
     * @param string|null id    A record id or null for a new record editor.
     * @return Widget_Page
     */
    public function edit($id = null, $view = '')
    {
        $Crm = $this->Crm();

        $recordSet = $this->getEditRecordSet();

        $page = $Crm->Ui()->Page();

        $page->setTitle($Crm->translate($recordSet->getDescription()));

        $editor = $this->recordEditor();

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new crm_AccessException($Crm->translate('You do not have access to this page.'));
            }
            $editor->setRecord($record);
        } else {
            if (!$recordSet->isCreatable()) {
                throw new crm_AccessException($Crm->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
            $editor->setRecord($record);
        }

        $page->addItem($editor);

        return $page;
    }



    public function completedTasksSection($id, $itemId = null)
    {
        $Crm = $this->Crm();
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $Crm->Ui()->includeTask();

        return crm_performedUserTasksForCrmObject($record);
    }




    public function pendingTasksSection($id, $itemId = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $Crm->Ui()->includeTask();

        $box = $W->VBoxItems(
            $W->Link(
                $Crm->translate('Add'),
                $Crm->Controller()->Task()->edit(null, $record->getRef())
            )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->addClass(Func_Icons::ACTIONS_LIST_ADD, 'icon', 'widget-actionbutton', 'section-button')
            ->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC),
            $Crm->Ui()->userTasksForCrmObject($record)
        );
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->setReloadAction($this->proxy()->pendingTasksSection($record->id, $box->getId()));

        return $box;
    }


    public function ok()
    {
        return true;
    }

    /**
     * @param int       $id
     * @param string    $itemId
     * @return Widget_VBoxLayout
     */
    public function notesSection($id, $offset = 0, $limit = 3, $itemId = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $toolbar = null;
        $notesBox = null;
        $moreBox = null;

        if ($offset == 0) {
            $toolbar = $W->Link(
                $Crm->translate('Add'),
                $Crm->Controller()->Note()->add($record->getRef())
            )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->addClass(Func_Icons::ACTIONS_LIST_ADD, 'icon', 'widget-actionbutton', 'section-button')
            ->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
        }


        $notesBox = $Ui->NotesForCrmObject($record, null, $offset, $limit);

        if (isset($notesBox)) {
            $moreBox = $W->Frame(
                null,
                $W->VBoxItems(
                    $W->Link($Crm->translate('More...'))
                    ->setAjaxAction($this->proxy()->ok())
                )
            );
            $moreBox->setReloadAction($this->proxy()->notesSection($record->id, $offset + $limit, $limit));
        }

        $box = $W->VBoxItems(
            $toolbar,
            $notesBox,
            $moreBox
        );
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->setReloadAction($this->proxy()->notesSection($record->id, $offset, $limit, $box->getId()));

        return $box;
    }

    /**
     *
     * @param  $data
     */
    public function create($data)
    {

    }


    /**
     *
     * @param  $data
     */
    public function update($data)
    {

    }



    /**
     *
     */
    public function read($id)
    {

    }


    /**
     * Get the message to display on delete next page
     * @return string | true
     */
    protected function getDeletedMessage(crm_Record $record)
    {
        $Crm = $this->Crm();
        $recordSet = $this->getRecordSet();

        $recordTitle = $record->getRecordTitle();

        $message = sprintf($Crm->translate('%s has been deleted'), $Crm->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"');
        return $message;
    }

    /**
     * Get the message to display on create next page
     * @return string | true
     */
    protected function getCreatedMessage()
    {
        return true;
    }

    /**
     * Get the message to display on modification next page
     * @param crm_Record $record The record before the modification
     * @return string | true
     */
    protected function getModifedMessage(crm_Record $record)
    {
        return true;
    }


    /**
     * Method to check reord before saving
     * @param crm_Record $record
     */
    protected function preSave(crm_Record $record, $data)
    {

    }


    /**
     * Method for post save actions on record
     * @param crm_Record $record
     */
    protected function postSave(crm_Record $record, $data)
    {

    }


    /**
     * Save a record
     *
     * @requireSaveMethod
     *
     * @param array $data
     */
    public function save($data = null)
    {
        $this->requireSaveMethod();
        $Crm = $this->Crm();
        $recordSet = $this->getSaveRecordSet();
        $pk = $recordSet->getPrimaryKey();
        $message = null;

        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);

            if (!$record->isUpdatable()) {
                throw new crm_AccessException('Access denied');
            }

            $message = $this->getModifedMessage($record);
        } else {

            $record = $recordSet->newRecord();

            if (!$recordSet->isCreatable()) {
                throw new crm_AccessException('Access denied');
            }

            $message = $this->getCreatedMessage();
        }

        if (!isset($record)) {
            throw new crm_SaveException($Crm->translate('The record does not exists'));
        }


        if (!isset($data)) {
            throw new crm_SaveException($Crm->translate('Nothing to save'));
        }

        $record->setFormInputValues($data);

        $this->preSave($record, $data);
        if ($record->save()) {
            $this->postSave($record, $data);
            if (is_string($message)) {
                $this->addMessage($message);
            }
            return true;
        }

        $this->addReloadSelector('.depends-' . $this->getRecordClassName());

        return false;
    }


    /**
     * Deletes the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function delete($id = null)
    {
        $this->requireDeleteMethod();

        $recordSet = $this->getDeleteRecordSet();
        $records = $recordSet->select($recordSet->id->is($id), true);
        foreach ($records as $record) {
            if (!$record->isDeletable()) {
                throw new crm_AccessException('Sorry, You are not allowed to perform this operation');
            }
            $deletedMessage = $this->getDeletedMessage($record);
            if ($record->delete() && is_string($deletedMessage)) {
                $this->addMessage($deletedMessage);
            }

            $this->addReloadSelector('.depends-' . $record->getClassName());
        }

        return true;
    }



    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordSet = $this->getRecordSet();
        $record = null;
        $records = $recordSet->select($recordSet->id->is($id), true);
        foreach ($records as $record) {
            ;
        }

        if (!isset($record) || $record->deleted == crm_TraceableRecord::DELETED_STATUS_DELETED) {
            throw new crm_Exception($Crm->translate('Trying to delete a record with a wrong (unknown) id.'));
        }

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Deletion'));

        $form = new crm_Editor($Crm);

        $recordTitle = $record->getRecordTitle();

        $subTitle = $Crm->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $form->addItem($W->Title($Crm->translate('Confirm delete?'), 6));

        $form->addItem($W->Html(bab_toHtml($Crm->translate('It will not be possible to undo this deletion.'))));

        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($Crm->translate('Delete'))
         );
        $form->addButton($W->SubmitButton()->setLabel($Crm->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }







    /**
     * Deletes the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function remove($id)
    {
        $this->requireDeleteMethod();
        $recordSet = $this->getDeleteRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isRemovable()) {
            throw new crm_AccessException('Sorry, You are not allowed to perform this operation');
        }

        $Crm = $this->Crm();
        $deletedMessage = $Crm->translate('The element has been moved to the trash');

        if ($recordSet->delete($recordSet->id->is($record->id), crm_TraceableRecord::DELETED_STATUS_IN_TRASH)) {
            if (is_string($deletedMessage)) {
                $this->addMessage($deletedMessage);
            }
        }

        $this->addReloadSelector('.depends-' . $record->getClassName());

        return true;
    }


    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmRemove($id)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Move to trash'));

        $form = new crm_Editor($Crm);

        $recordTitle = $record->getRecordTitle();

        $subTitle = $Crm->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $form->addItem($W->Title($Crm->translate('Confirm delete?'), 6));

//         $form->addItem($W->Html(bab_toHtml($Crm->translate('It will not be possible to undo this deletion.'))));

        $confirmedAction = $this->proxy()->remove($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($Crm->translate('Move to trash'))
            );
        $form->addButton($W->SubmitButton()->setLabel($Crm->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }




    /**
     * Restores the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function restore($id)
    {
        $this->requireSaveMethod();
        $recordSet = $this->getRecordSet();

        $records = $recordSet->select($recordSet->id->is($id), true);

        $record = null;
        foreach ($records as $record) {
        }
        if (! isset($record)) {
            throw new crm_AccessException('Sorry, You are not allowed to perform this operation');
        }
        if (!$record->isUpdatable()) {
            throw new crm_AccessException('Sorry, You are not allowed to perform this operation');
        }

        $Crm = $this->Crm();
        $deletedMessage = $Crm->translate('The element has been restored from the trash');

        $record->deleted = crm_TraceableRecord::DELETED_STATUS_EXISTING;
        $record->save();
        $this->addMessage($deletedMessage);

        $this->addReloadSelector('.depends-' . $record->getClassName());

        return true;
    }

    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmRestore($id)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordSet = $this->getRecordSet();
        $records = $recordSet->select($recordSet->id->is($id), true);

        $record = null;
        foreach ($records as $record) {
        }
        if (! isset($record)) {
            throw new crm_AccessException('Sorry, You are not allowed to perform this operation');
        }

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Restore from trash'));

        $form = new crm_Editor($Crm);

        $recordTitle = $record->getRecordTitle();

        $subTitle = $Crm->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $confirmedAction = $this->proxy()->restore($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($Crm->translate('Restore from trash'))
        );
        $form->addButton($W->SubmitButton()->setLabel($Crm->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }

    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function filteredTrash($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $view = $this->modelView($filter, $type, null, $itemId);
        $view->setAjaxAction();

        if (isset($filter)) {
            $view->setFilterValues($filter);
        }
        $filter = $view->getFilterValues();

        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $view->displaySubTotalRow(true);
        }


        $recordSet = $this->getRecordSet();
        $view->setRecordSet($recordSet);
        $view->addDefaultColumns($recordSet);

        $conditions = $view->getFilterCriteria($filter);
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $conditions = $conditions->_AND_(
            $recordSet->deleted->is(crm_TraceableRecord::DELETED_STATUS_IN_TRASH)
        );

        $records = $recordSet->select($conditions, true);

        $view->setDataSource($records);

        $view->allowColumnSelection();

        $filterPanel = $view->advancedFilterPanel($filter);

//         $toolbar = $this->toolbar($view);

        $box = $W->VBoxItems(
//             $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->filteredTrash(null, null, $view->getId()));

        return $box;
    }



    public function displayTrash()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();

//         $page->setTitle($Crm->translate('Trash'));


        $trashView = $this->filteredTrash();

        $page->addItem($trashView);

        $itemMenus = $this->getListItemMenus();
        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }
        $page->setCurrentItemMenu('displayTrash');

        return $page;
    }


    /**
     * TODO in RecordController
     * Displays the list filtered by the specified tag.
     *
     * @param string	$tag		The tag name to filter the list by.
     *
     * @return Widget_Page
     */
    public function displayListForTag($tag = null)
    {
        $filter = array('tag' => $tag);
        return $this->displayList($filter);
    }
}
