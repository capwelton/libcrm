<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 *
 */
class crm_CtrlMailBox extends crm_Controller
{


	/**
	 * Displays the list of configured mailboxes.
	 *
	 * @throws crm_AccessException
	 * @return crm_Page
	 */
	public function displayList()
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Mailboxes'));


		if (!$Access->listMailBox())
		{
			throw new crm_AccessException($Crm->translate('Access denied to mailboxes list'));
		}

		$Ui = $Crm->Ui();
		$page = $Ui->Page();

		$set = $Crm->MailBoxSet();

		$table = $Ui->MailBoxTableView();

		$table->addDefaultColumns($set);
		$table->setDataSource($set->select());
		$table->addClass(Func_Icons::ICON_LEFT_16);


		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a mailbox'), Func_Icons::ACTIONS_MAIL_SEND, $this->proxy()->edit());

		$page->addToolbar($toolbar);

		$page->addItem($table);

		return $page;
	}





	public function display()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$Ui = $Crm->Ui();
		$page = $Ui->Page();

		$set = $Crm->MailBoxSet();


		$mailbox = $set->get($set->id->is(1));

		$all = $mailbox->getImap()->searchMails('ALL');
		$mId = array_pop($all);


		$email = $mailbox->getImap()->getMail($mId);

		/* @var $email LibImap_Mail */

		$page->addItem($email->getMetadata());
		$page->addItem($W->Html($email->getHtmlContent()));

		return $page;
	}





	/**
	 * Import responses for all mailbox or for one mailbox
	 *
	 */
	public function importResponses($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		if (!$Access->editMailBox())
		{
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$set = $Crm->MailBoxSet();

		if (null == $mailbox)
		{
			$count = $set->importResponses();
			return sprintf($Crm->translate('All mailboxes processed, %d responses imported'), $count);
		} else {

			$mailbox = $set->get($mailbox);
			/*@var $mailbox crm_MailBox */

			try {
				$count = $mailbox->importResponses();
			} catch (LibImap_Exception $e) {
				return $e->getMessage();
			}

			return sprintf($Crm->translate('%d responses imported'), $count);
		}
	}




	/**
	 * Edit a mailbox configuration.
	 *
	 * @param string $mailbox
	 * @throws crm_AccessException
	 * @return crm_Page
	 */
	public function edit($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($mailbox), $Crm->translate('Edit mailbox'));


		if (!$Access->editMailBox())
		{
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		$page->addClass('crm-page-editor');

		if ($mailbox)
		{
			$set = $Crm->MailBoxSet();
			$record = $set->get($mailbox);

			$page->addContextItem(
				$Ui->Actions()->addItem(
					$W->Link($W->Icon($Crm->translate('Delete mailbox'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($record->id))
				)
			);

			$page->addContextItem(
				$W->DelayedItem($Crm->Controller()->MailBox()->mailboxStatus($mailbox)),
				$W->Label($Crm->translate('Status'))
			);

		} else {
			$record = null;
		}

		$page->addItem($Ui->MailBoxEditor($record));


		return $page;
	}



	public function delete($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		if (!$Access->editMailBox())
		{
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$set = $Crm->MailBoxSet();
		$set->delete($set->id->is($mailbox));

		crm_redirect($this->proxy()->displayList());
	}



	public function mailboxStatus($mailbox)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$W = bab_Widgets();

		$set = $Crm->MailBoxSet();
		$record = $set->get($mailbox);

		if (!$record) {
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$statusBox = $W->VBoxLayout('crm-mailbox-status');

		if (isset($_SESSION['LibCrm']['MailboxStatus'])) {
			$statusBox->addItem(
				$W->Html($_SESSION['LibCrm']['MailboxStatus'])
			);
		}

		return $statusBox;
	}



	/**
	 * Check if the specified mailbox parameter are ok.
	 *
	 * @param array $mailbox
	 *
	 * @throws crm_AccessException
	 * @return crm_Page
	 */
	public function check($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		if (!$Access->editMailBox()) {
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$imap = @bab_functionality::get('Imap', false);

		if (!$imap) {
			throw new crm_Exception($Crm->translate('Imap functionality not available.'));
		}


		$path = '{'.$mailbox['host'].':'.$mailbox['port'].$mailbox['type'].'}INBOX';

		$errorMessage = null;
		try {

			$imap->setMailBox($path, $mailbox['username'], $mailbox['password']);
			$imap->connect();

		} catch (LibImap_Exception $e) {

			$errorMessage = $e->getMessage();

		}

		if (isset($errorMessage)) {
			$_SESSION['LibCrm']['MailboxStatus'] = $errorMessage;
			die;
		}

		$_SESSION['LibCrm']['MailboxStatus'] = $Crm->translate('Connection to the specified mailbox working');
		die;
	}




	/**
	 * Check if the specified mailbox parameter are ok.
	 *
	 * @param array $mailbox
	 *
	 * @throws crm_AccessException
	 * @return crm_Page
	 */
	public function displayCheck($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		if (!$Access->editMailBox()) {
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$imap = @bab_functionality::get('Imap', false);

		if (!$imap) {
			throw new crm_Exception($Crm->translate('Imap functionality not available.'));
		}


		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		$page->addClass('crm-page-editor');

		$path = '{'.$mailbox['host'].':'.$mailbox['port'].$mailbox['type'].'}INBOX';

		$errorMessage = null;
		try {

			$imap->setMailBox($path, $mailbox['username'], $mailbox['password']);
			$imap->connect();

		} catch (LibImap_Exception $e) {

			$errorMessage = $e->getMessage();

		}

		if (isset($errorMessage)) {
			$page->addItem(
				$W->VBoxItems(
					$W->Title($Crm->translate('Unable to connect to the specified mailbox'), 1),
					$W->Label($Crm->translate('Please check your configuration.')),
					$W->Label($errorMessage)
				)->setVerticalSpacing(2, 'em')
			);

			return $page;
		}

			$page->addItem(
				$W->VBoxItems(
					$W->Title($Crm->translate('Connection to the specified mailbox working'), 1)
				)->setVerticalSpacing(2, 'em')
			);

			return $page;
	}





	/**
	 * save mailbox by manager
	 * @param array $mailbox
	 */
	public function save($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		if (!$Access->editMailBox())
		{
			throw new crm_AccessException($Crm->translate('Access denied to mailbox'));
		}

		$set = $Crm->MailBoxSet();

		if (!empty($mailbox['id']))
		{
			$record = $set->get($mailbox['id']);

			if (!$record)
			{
				throw new crm_SaveException('record not found');
			}
		} else {
			$record = $set->newRecord();
		}

		$record->setValues($mailbox);

		if (empty($record->port))
		{
			$record->port = $record->getDefaultPort();
		}

		$record->save();

		return true;
	}




	/**
	 * edit my mailbox
	 */
	public function editmymailbox()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->editmymailbox(), $Crm->translate('Edit my mailbox'));


		if (!$Access->editMyMailBox()) {
			throw new crm_AccessException($Crm->translate("You can't update your own mailbox"));
		}

		if (!$Access->createMyMailBox()) {
			throw new crm_AccessException($Crm->translate("You can't create your own mailbox"));
		}

		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		$page->addClass('crm-page-editor');

		$page->addItem(
			$W->Title($Crm->translate('Edit my mail box'), 1)
		);

		$editor = $Ui->MailBoxEditor(null, bab_getUserId());
		$editor->saveButton->setSuccessAction(crm_BreadCrumbs::getPosition(-1));
		$editor->cancelButton->setSuccessAction(crm_BreadCrumbs::getPosition(-1));

		$page->addItem($editor);

		return $page;
	}





	/**
	 * Save my own mailbox
	 */
	public function savemymailbox($mailbox = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		$set = $Crm->MailBoxSet();
		$record = $set->get($set->user->is(bab_getUserId()));

		if (isset($record) && !$Access->editMyMailBox())
		{
			throw new crm_AccessException($Crm->translate("You can't update your own mailbox"));
		}

		if (null == $record)
		{
			if (!$Access->createMyMailBox())
			{
				throw new crm_AccessException($Crm->translate("You can't create your own mailbox"));
			}

			$record = $set->newRecord();
			$record->user = bab_getUserId();
		}

		if (isset($mailbox['user']))
		{
			unset($mailbox['user']);
		}

		$record->setValues($mailbox);
		$record->save();

		return true;
	}




	public function listMailBoxes()
	{
	    $Crm = $this->Crm();

	    $set = $Crm->MailBoxSet();

	    $mailbox = $set->get($set->user->is(bab_getUserId()));

	    $mailboxes = $mailbox->getImap()->listMailBoxes();

	    var_dump($mailboxes);

	    die;

	}



	public function setCurrentFolder($folder)
	{
        $_SESSION['LibCrm']['mailboxCurrentFolder'] = $folder;
        return true;
	}



	/**
	 *
	 * @param number $nbItems
	 * @param string $mode
	 * @param string $title
	 * @return Widget_VBoxLayout
	 */
	public function recentEmails($maxItems = 10, $mode = null, $title = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Access = $Crm->Access();

		$Ui->includeMailBox();

		if (!isset($mode)) {
			$mode = crm_HistoryElement::MODE_SHORT;
		}

		$frame = $W->VBoxLayout();
		$frame->addClass(Func_Icons::ICON_LEFT_16);

		$set = $Crm->MailBoxSet();

		$mailbox = $set->get($set->user->is(bab_getUserId()));



		if (!isset($mailbox)) {
			$frame->addItem(
				$W->Label($Crm->translate('Your mail box is not configured.'))
			);
			if ($Access->createMyMailBox()) {
				$frame->addItem(
					$W->Link(
						$Crm->translate('Edit my mail box'),
						$Crm->Controller()->MailBox()->editmymailbox()
					)->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
				);
			}
			return $frame;
		}


		$currentMailboxFolder = isset($_SESSION['LibCrm']['mailboxCurrentFolder']) ? $_SESSION['LibCrm']['mailboxCurrentFolder'] : '{}INBOX';


        $mailboxFolders = $mailbox->getImap()->listMailBoxes();

        $foldersFrame = $W->VBoxItems();

        foreach ($mailboxFolders as $mailboxFolder) {

            list(,$folderName) = explode('}', $mailboxFolder);
            $folderLabel = mb_convert_encoding($folderName, bab_charset::getIso(), 'UTF7-IMAP');
            $folderLink = $W->Link($folderLabel)
                ->setAjaxAction($Crm->Controller()->MailBox()->setCurrentFolder($mailboxFolder), $foldersFrame)
                ->setSizePolicy('widget-list-element');
            if ($currentMailboxFolder === $mailboxFolder) {
                $folderLink->addClass('widget-strong');
            }

            $foldersFrame->addItem($folderLink);
        }









        list(,$folderName) = explode('}', $currentMailboxFolder);

        $mailboxFolder = $mailbox->getImap($folderName);

        $folderLabel = mb_convert_encoding($folderName, bab_charset::getIso(), 'UTF7-IMAP');

        $mbox = $mailboxFolder->getMBox();



 		$info = $mailboxFolder->getStatus();
 		$frame->addItem(
 		    $W->Title($folderLabel, 3)
 		);


 		$frame->addItem(
 		    $W->FlowItems(
                $W->Label(sprintf('%d messages', $info->messages)),
     		    $W->Label(sprintf('%d unseen', $info->unseen)),
     		    $W->Label(sprintf('%d recent', $info->recent))
 		    )->setSpacing(1, 'em')
 		);

// 		var_dump($info);

		//$ts = strToTime('-1 days');
		//$date = date('d M Y');

        //$emailIds = $imap->searchMails(sprintf('SINCE "%s"', $date));

        $emailUids = $mailboxFolder->searchMails('ALL');

		if (!empty($title)) {
			$frame->addItem($W->Title($title, 1));
		}


		$list = $W->VBoxItems();


		$nbItems = 0;
		while ($emailUid = array_pop($emailUids)) {
		    $nbItems++;
		    if ($nbItems > $maxItems) {
		        break;
		    }
		    $emailNumber = imap_msgno($mbox, $emailUid);
		    $header = imap_headerinfo($mbox, $emailNumber);

		    $subject = '';
		    $subjectParts = imap_mime_header_decode($header->subject);
		    foreach ($subjectParts as $subjectPart) {
                $subject .= $subjectPart->text;
		    }

		    $fromaddress = '';
		    $fromaddressParts = imap_mime_header_decode($header->fromaddress);
		    foreach ($fromaddressParts as $fromaddressPart) {
		        $fromaddress .= $fromaddressPart->text;
		    }

		    $emailListItem = $Ui->MailBoxListItem(null, $mode);
		    $emailListItem->setFrom($fromaddress);
		    $emailListItem->setSubject($subject);
		    $emailListItem->setMailbox($folderName);
		    $emailListItem->setUid($emailUid);

		    $list->addItem($emailListItem);
		}

		$frame->addItem($list);

		$frame = $W->HBoxItems(
		    $foldersFrame->addClass('widget-small'),
		    $frame
		);

		return $frame;
	}




	/**
	 * Copy the specified imap email in the clipboard.
	 *
	 * @param int	$mId		The mail id for Func_Imap.
	 * @return boolean
	 */
	public function copyToClipBoard($mId)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();

		$mailBoxSet = $Crm->MailBoxSet();

		$mailbox = $mailBoxSet->get($mailBoxSet->user->is(bab_getUserId()));

		/* @var $imapMail LibImap_Mail */
		$imapMail = $mailbox->getImap()->getMail($mId);

		$emailSet = $Crm->EmailSet();

		/* @var $email crm_Email */
		$email = $emailSet->importFromImapMail($imapMail);

		$Crm->Controller()->Clipboard(false)->add($email->getRef());

		return true;
	}




	/**
	 *
	 */
	public function displayEmail($mId, $mailboxname = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();

		$mailBoxSet = $Crm->MailBoxSet();

		$mailbox = $mailBoxSet->get($mailBoxSet->user->is(bab_getUserId()));

		/* @var $imapMail LibImap_Mail */
		$imapMail = $mailbox->getImap($mailboxname)->getMail($mId);


		$emailSet = $Crm->EmailSet();
		$email = $emailSet->newRecord();

		$email->setValuesFromImap($imapMail);

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');

		$page->addItem($W->Title($Crm->translate('Email'), 1));

		$page->addItem($Ui->HistoryElement($email, 'full'));

		$page->addContextItem($W->Label(''));

		return $page;
	}
}





