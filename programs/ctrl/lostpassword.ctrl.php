<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed for the lsot password function.
 * send a token by email and allow to update manually the password
 */
class crm_CtrlLostPassword extends crm_Controller
{


	
	
	/**
	 * Display a form with email field
	 * Accessible for logged out users
	 */
	public function edit()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
		if (!isset($Crm->PasswordToken))
		{
			throw new crm_AccessException('this application does not allow password recovery');
		}
		
		return $Ui->Page()->addItem($Ui->ForgotPasswordEditor());
	}
	
	
	/**
	 * Send a token by email
	 * Accessible for logged out users
	 */
	public function sendToken($forgotpassword = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = crm_widgets();
		
		if (empty($forgotpassword['email']))
		{
			throw new crm_SaveException($Crm->translate('Missing email address'));
		}
		
		$set = $Crm->ContactSet();
		$res = $set->select($set->email->like($forgotpassword['email'])->_AND_($set->user->is(0)->_NOT()));
		
		if (0 === $res->count())
		{
			throw new crm_SaveException($Crm->translate('The request failed, no account was found with this email'));
		}
		
		if (1 !== $res->count())
		{
			throw new crm_SaveException($Crm->translate('The request failed, there are more than one account associated to this email'));
		}
		
		foreach($res as $contact) {}
		
		// send message

		$notif = $Crm->Notify()->lostPassword_sendToken($contact);
		if (!$notif->send())
		{
			throw new crm_SaveException($Crm->translate('Failed to process the request'));
		}
		
		// display confirmation
		
		return true;
	}
	
	
	/**
	 * display confirmation
	 */
	public function success()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = crm_widgets();
		
		return $Ui->Page()
		->addItem(
				$W->Frame()->addClass('crm-dialog')
				->addItem(
						$W->Label($Crm->translate('An email was sent to you, please check your mail account'))
				)
		);
	}
	
	
	
	/**
	 * Display password modification form for a token
	 * Accessible for logged out users
	 * 
	 * @return Widget_Page
	 */
	public function editPasswordToken($uuid)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();

		$set = $Crm->PasswordTokenSet();
		$set->contact();
		$passwordToken = $set->get($set->uuid->is($uuid));
		
		if (null === $passwordToken)
		{
			// no password token, the customer may have allready an account
			// return $Ui->Page()->addError($Crm->translate('Access denied, wrong token'));
			crm_redirect('?');
		}
		
		if (!$passwordToken->isValid())
		{
			return $Ui->Page()->addError(sprintf($Crm->translate('This page is not valid anymore, setting a new password was only possible for %d days'), crm_PasswordToken::VALIDITY));
		}
		
		if (bab_isUserLogged())
		{
			return $Ui->Page()->addError($Crm->translate('You are allready logged in, this page for an annonymous user only'));
		}
		
		$page = $Ui->Page();
		if ($Crm->loginByEmail)
		{
			$page->setTitle(sprintf($Crm->translate('Set a password for my email address : %s'), $passwordToken->contact->email));
		} else {
			$page->setTitle(sprintf($Crm->translate('Set a password for my login ID : %s'), bab_getUserNickname($passwordToken->contact->user)));
		}
		
		$page->addItem($Ui->PasswordTokenEditor($passwordToken->uuid));
		
		return $page;
	}
	
	
	/**
	 * Update a user password for temporary token
	 * Accessible for logged out users
	 * 
	 * @param	string	$uuid
	 * @param	string	$password
	 * 
	 * 
	 * @return Widget_Page
	 */
	public function savePasswordToken($passwordtoken = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
		
		
		$set = $Crm->PasswordTokenSet();
		$set->contact();
		$passwordTokenRecord = $set->get($set->uuid->is($passwordtoken['uuid']));
		/*@var $passwordTokenRecord crm_PasswordToken */
		
		if (null === $passwordTokenRecord)
		{
			return $Ui->Page()->addError($Crm->translate('Access denied, wrong token'));
		}
		
		

		if (!$passwordTokenRecord->isValid())
		{
			return $Ui->Page()->addError(sprintf($Crm->translate('This page is not valid anymore, setting a new password was only possible for %d days'), crm_PasswordToken::VALIDITY));
		}
		
		
		$passwordtoken['password1'] = trim($passwordtoken['password1']);
		$passwordtoken['password2'] = trim($passwordtoken['password2']);
		
		
		if (empty($passwordtoken['password1']))
		{
			return $Ui->Page()->addError($Crm->translate('The password is mandatory'));
		}
		
		if ($passwordtoken['password1'] !== $passwordtoken['password2'])
		{
			return $Ui->Page()->addError($Crm->translate('Wrong password verification'));
		}
		
		
		$error = '';
		
		if ($passwordTokenRecord->contact->user)
		{
			// the user account allready exists, update password
			
			$infos = array(
					'password' => $passwordtoken['password1']
			);
			
			bab_updateUserById($passwordTokenRecord->contact->user, $infos, $error);
		} else {
			
			if ($Crm->loginByEmail && $passwordTokenRecord->contact->email)
			{
				// l'email doit etre unique dans la base
				
				if (0 !== bab_getUserIdByEmail($passwordTokenRecord->contact->email))
				{
					$error = $Crm->translate('Account creation failed, an account with the same email allready exists');
				}
				
				
				
				// create the account with the email
				try {
					if ($id_user = $passwordTokenRecord->contact->createUser($passwordTokenRecord->contact->email, $passwordtoken['password1']))
					{
						$passwordTokenRecord->contact->user = $id_user;
						$passwordTokenRecord->contact->save(); // save link beetween contact and user
					}
				} catch (crm_ContactUserException $e) {
					$error = $e->getMessage();
				}
			} else {
				$error = $Crm->translate('This account does not exists');
			}
		}
		
		if ($error) {
			return $Ui->Page()->addError($error);
		}
		
		$set->delete($set->id->is($passwordTokenRecord->id));
		
		$nickname = bab_getUserNickname($passwordTokenRecord->contact->user);
		
		$W = bab_Widgets();
		$auth = bab_functionality::get('PortalAuthentication');
		/*@var $auth Func_PortalAuthentication */
		$id_user = $auth->authenticateUser($nickname, $passwordtoken['password1']);
		
		if ($auth->userCanLogin($id_user))
		{
			$auth->setUserSession($id_user);

			crm_redirect($this->proxy()->passwordModified());
		}
	}
	
	
	
	public function passwordModified()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		
		
		if (!bab_isUserLogged())
		{
			return $Ui->Page()->addError($Crm->translate('Access denied'));
		}
		
		$page = $Ui->Page();
		
		
		if ($Crm->loginByEmail)
		{
			$message = sprintf($Crm->translate('Your password has been modified, you are now connected with email address : %s'), $GLOBALS['BAB_SESS_EMAIL']);
		} else {
			$message = sprintf($Crm->translate('Your password has been modified, you are now connected with login ID : %s'), $GLOBALS['BAB_SESS_NICKNAME']);	
		}
		

		$page->addItem(
			$W->VBoxItems(
				$W->Title($message, 4),
				$W->Link($Crm->translate('Get back to homepage'), bab_getSelf())->addClass('crm-dialog-button')
			)->setVerticalSpacing(2,'em')->addClass('crm-dialog')
		);
		
		
		return $page;
	}


}
