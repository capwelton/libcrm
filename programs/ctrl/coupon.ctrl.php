<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on coupons.
 */
class crm_CtrlCoupon extends crm_Controller implements crm_ShopAdminCtrl
{

	/**
	 * Display the list of coupon for administration purpose
	 * @return Widget_Action
	 */
	public function displayList($coupon = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($coupon), $Crm->translate('Coupons'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		$page->setTitle($Crm->translate('Coupon'));
		
		
		$filter = isset($coupon['filter']) ? $coupon['filter'] : array();
		
		
		$set = $this->Crm()->CouponSet();
				
		$table = $Crm->Ui()->CouponTableView();
		$table->addDefaultColumns($set);
		
		$criteria = $table->getFilterCriteria($filter);		
		
		$couponSelection = $set->select($criteria);
		
		$table->setDataSource($couponSelection);
		
		$table->addClass('icon-left-16 icon-left icon-16x16');
		
		$filterPanel = $table->filterPanel($filter, 'coupon');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a coupon'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * 
	 * @param int $article
	 * 
	 * @return Widget_Action
	 */
	public function edit($coupon = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (null === $coupon && !$access->createCoupon()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($coupon), $Crm->translate('Edit coupon'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit coupon'));
		
		if (null !== $coupon && !is_array($coupon)) {
			$set = $Crm->CouponSet();
			
			if (isset($Crm->ArticlePackaging)) {
			   $set->articlepackaging();
			   $set->articlepackaging->article();
			}
			
			$coupon = $set->get($coupon);
			
			if (!$access->updateCoupon($coupon)) {
				throw new crm_AccessException($Crm->translate('Access denied'));
			}
			$form = $Ui->CouponEditor($coupon);
		} else {
			$form = $Ui->CouponEditor();
			if (is_array($coupon)) {
				$form->setHiddenValue('coupon[id]', $coupon['id']);
				$form->setValues($coupon, array('coupon'));
			}
		}
		
		$page->addContextItem($W->VBoxItems());
		
		$page->addItem($form);
		
		return $page;
	}
	
	
	
	/**
	 * Save coupon
	 * @param array $coupon
	 * 
	 * @return Widget_Action
	 */
	public function save($coupon = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (empty($coupon['id']) && !$access->createCoupon()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $this->Crm()->CouponSet();
		if(!isset($coupon['id'])){
			$coupon['id'] = '';
		}
		$codeRecord = $set->get($set->code->is($coupon['code'])->_AND_($set->id->isnot($coupon['id'])));
		if($codeRecord)
		{
			throw new crm_SaveException(crm_translate('This code is already in use. Remove the other coupon if you whant to use it.'));
		}
		
		if(isset($coupon['opt']) && $coupon['opt'] == 'product') {
			$apSet = $this->Crm()->ArticlePackagingSet();
			$articlepackaging = $apSet->get($apSet->id->is($coupon['articlepackaging']['id']));
			if(!$articlepackaging){
				throw new crm_SaveException(crm_translate('This article does not exist.'));
			}
			$coupon['articlepackaging'] = $articlepackaging->id;
		} else {
			$coupon['articlepackaging'] = 0;
		}
		
		if (empty($coupon['id'])) {		
			$couponRecord = $set->newRecord();
		} else {
			$couponRecord = $set->get($coupon['id']);
		}
		
		if (isset($coupon['opt']) && $coupon['opt'] == 'shipping') {
		    $coupon['shipping'] = 1;
		}
		
		$couponRecord->setFormInputValues($coupon);
		$couponRecord->save();
		
		$couponRecord;

		crm_redirect($this->Crm()->Controller()->Coupon()->Display($couponRecord->id));
		
		die;
	}
	
	/**
	 * Display one coupon for administration purpose
	 * @return Widget_Action
	 */
	public function display($coupon = null)
	{
		$Crm = $this->Crm();
		$set = $Crm->CouponSet();
		
		if (isset($Crm->ArticlePackaging)) {
    		$set->articlepackaging();
    		$set->articlepackaging->article();
    		$set->articlepackaging->packaging();
		}
		
		$coupon = $set->request($coupon);

		
		$access = $Crm->Access();
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($coupon->id), $coupon->code);
		
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->setTitle($coupon->Code);
		
		$couponDisplay = $Ui->CouponDisplay($coupon);
		
		
		$fullFrame = $couponDisplay->getFullFrame();
		$page->addItem($fullFrame);
		
		$actionsFrame = $page->ActionsFrame();
		$page->addContextItem($actionsFrame);
		
		$actions = $this->getActionFrame($coupon);
		foreach($actions as $action){
			$actionsFrame->addItem($action);
		}
		
		return $page;
	}
	
	public function getActionFrame($coupon)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$returnObjets = array();
		
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Coupons list'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Coupon()->displayList());

		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Edit coupon'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Coupon()->edit($coupon->id));
			
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Delete coupon'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Coupon()->delete($coupon->id))->setConfirmationMessage($Crm->translate('Do you really want to delete the coupon?'));
		
		return $returnObjets;
	}




	public function delete($coupon)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		$couponSset = $this->Crm()->CouponSet();
		$couponRecord = $couponSset->get($coupon);
		
		if (!$couponRecord)
		{
			throw new crm_Exception($Crm->translate('This coupon does not exists'));
		}
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$couponSset->delete($couponSset->id->is($coupon));

		crm_redirect($this->proxy()->displayList());
	}


	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::getPosition(-2));
	}
}
