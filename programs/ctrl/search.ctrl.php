<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * This controller manages actions that can be performed on articles.
 */
class crm_CtrlSearch extends crm_Controller
{

    protected $maxElementsBySection = 3;

    /**
     * Extract ann array of keywords from the search string.
     *
     * @param string $searchString
     *
     * @return array
     */
    protected function extractKeywords($searchString)
    {
        if (is_array($searchString)) {
            return $searchString;
        }
        if (!isset($searchString)) {
            $searchString = '';
        }

        $keywords = array();
        foreach (explode(' ', $searchString) as $k) {
            if (mb_strlen($k) > 1) {
                $keywords[] = $k;
            }
        }

        return $keywords;
    }



    protected function searchForm()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $searchForm = new crm_Editor($Crm);
        $searchForm->setReadOnly();
        $searchForm->setName('search');

        $searchForm->addItem(
                $searchForm->labelledField(
                        $Crm->translate('Keywords'),
                        $W->LineEdit()->setSize(80),
                        'searchString'
                )
        );

        $searchForm->setSaveAction($Crm->Controller()->Search()->display(), $Crm->translate('Search'));

        $advancedSection = $W->Section(
                $Crm->translate('Advanced search'),
                $W->VBoxItems(),
                6
        )->setFoldable(true, true);

        $searchForm->addItem(
                $items = $W->HBoxItems(
                        $searchForm->labelledField(
                                $Crm->translate('Contacts'),
                                $W->CheckBox(),
                                'includeContacts'
                        )
                )->addClass('crm-buttonset')->setHorizontalSpacing(1,'em')

        );


        if (isset($Crm->Organization))
        {
            $items->addItem(
                    $searchForm->labelledField(
                            $Crm->translate('Organizations'),
                            $W->CheckBox(),
                            'includeOrganizations'
                    )
            );
        }

        if (isset($Crm->Deal))
        {
            $items->addItem(
                    $searchForm->labelledField(
                            $Crm->translate('Deals'),
                            $W->CheckBox(),
                            'includeDeals'
                    )
            );
        }

        if (isset($Crm->Note))
        {
            $items->addItem(
                    $searchForm->labelledField(
                            $Crm->translate('Notes'),
                            $W->CheckBox(),
                            'includeNotes'
                    )
            );
        }

        if (isset($Crm->Task))
        {
            $items->addItem(
                    $searchForm->labelledField(
                            $Crm->translate('Tasks'),
                            $W->CheckBox(),
                            'includeTasks'
                    )
            );
        }

        if (isset($Crm->Email))
        {
            $items->addItem(
                    $searchForm->labelledField(
                            $Crm->translate('Emails'),
                            $W->CheckBox(),
                            'includeEmails'
                    )
            );
        }

        $searchForm->setHiddenValue('tg', bab_rp('tg'));

        return $searchForm;
    }




    /**
     *
     * @param
     * @return Widget_Action
     */
    public function display($search = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor crm-page_search-results');

        $page->addItem($W->Title($Crm->translate('Search the CRM'), 1));


        if (!isset($search) || count($search) <= 1) {
            $search = array(
                    'includeNotes' => 0,
                    'includeTasks' => 0,
                    'includeEmails' => 0,
                    'includeOrganizations' => 1,
                    'includeContacts' => 1,
                    'includeDeals' => 1,
                    'searchString' => isset($search['searchString']) ?$search['searchString'] : ''
            );
        }
        if (isset($search['maxElementsBySection']) && is_numeric($search['maxElementsBySection'])) {
            $this->maxElementsBySection = (int)$search['maxElementsBySection'];
        }
        $searchForm = $this->searchForm();
        $searchForm->setValues($search, array('search'));
        $searchLayout = $W->VBoxItems(
                $searchForm
        )
        ->setVerticalSpacing(1, 'em');

        $page->addItem($searchLayout);


        $searchString = $search['searchString'];
        $keywords = $this->extractKeywords($searchString);

        if (count($keywords) > 0) {

            if (isset($search['includeContacts']) && $search['includeContacts']) {
                $contactsSection = $this->displayContacts($searchString);
                $searchLayout->addItem($contactsSection);
            }

            if (isset($Crm->Organization) && isset($search['includeOrganizations']) && $search['includeOrganizations']) {
                $organizationsSection = $this->displayOrganizations($searchString);
                $searchLayout->addItem($organizationsSection);
            }

            if (isset($Crm->Deal) && isset($search['includeDeals']) && $search['includeDeals']) {
                $dealsSection = $this->displayDeals($searchString);
                $searchLayout->addItem($dealsSection);
            }


            if (isset($Crm->Note) && isset($search['includeNotes']) && $search['includeNotes']) {
                $notesSection = $this->displayNotes($searchString);
                $searchLayout->addItem($notesSection);
            }

            if (isset($Crm->Task) && isset($search['includeTasks']) && $search['includeTasks']) {
                $tasksSection = $this->displayTasks($searchString);
                $searchLayout->addItem($tasksSection);
            }

            if (isset($Crm->Email) && isset($search['includeEmails']) && $search['includeEmails']) {
                $emailsSection = $this->displayEmails($searchString);
                $searchLayout->addItem($emailsSection);
            }

        }

        $page->addContextItem($W->Label(''));

        return $page;
    }




    public function displayContacts($searchString = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $section = $W->Section(
            $Crm->translate('Contacts'),
            $W->VBoxLayout(),
            3
        );

        $Crm->Ui()->includeContact();

        $contactSet = $Crm->ContactSet();
        $contactSet->address();

        $criteria = $contactSet->lastname->like($searchString);

        $contacts = $contactSet->select($criteria);
        $nbContacts = $contacts->count();

        $nb = 0;
        $exactIds = array();
        foreach ($contacts as $contact) {
// 			if ($nb >= $this->maxElementsBySection) {
// 				break;
// 			}
            $nb++;
            $section->addItem(
                $Crm->Ui()->ContactCardFrame(
                    $contact, crm_ContactCardFrame::SHOW_ALL - crm_ContactCardFrame::SHOW_ADDRESS
                )->addClass('crm-search-result crm-exact-match')
            );
            $exactIds[$contact->id] = $contact->id;
        }


        $criteria = $contactSet->id->notIn($exactIds)->_AND_(
            $contactSet->lastname->matchOne($searchString)
                ->_OR_($contactSet->firstname->matchOne($searchString))
                ->_OR_($contactSet->address->postalCode->matchOne($searchString))
                ->_OR_($contactSet->address->city->matchOne($searchString))
                ->_OR_($contactSet->address->street->matchOne($searchString))
                ->_OR_($contactSet->phone->matchOne($searchString))
                ->_OR_($contactSet->mobile->matchOne($searchString))
                ->_OR_($contactSet->fax->matchOne($searchString))
        );

        $contacts = $contactSet->select($criteria);

        $nbContacts += $contacts->count();

        foreach ($contacts as $contact) {
            if ($nb >= $this->maxElementsBySection) {
                break;
            }
            $nb++;
            $section->addItem(
                $Crm->Ui()->ContactCardFrame(
                    $contact, crm_ContactCardFrame::SHOW_MINIMUM
                )->addClass('crm-search-result')
            );
        }

        if ($nb < $nbContacts)  {
            $section->addItem(
                $W->Link(
                    sprintf($Crm->translate('%d more...'), $nbContacts - $nb),
                    $Crm->Controller()->Search()->display(array('includeContacts' => 1, 'maxElementsBySection' => 100, 'searchString' => $searchString))
                )->addClass('widget-actionbutton widget-100pc widget-align-center')
            );
        }

        if ($nbContacts > 0) {
            $section->setFoldable(true);
        }

        $section
        ->addContextMenu('inline')
        ->addItem($W->Label(sprintf($Crm->translate('%d results'), $nbContacts))
                ->addClass('crm-counter-label'));

        return $section;
    }







    public function displayOrganizations($searchString = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $section = $W->Section(
                $Crm->translate('Organizations'),
                $W->VBoxLayout(),
                3
        );


        $organizationSet = $Crm->OrganizationSet();
        $organizationSet->address();

        $criteria = $organizationSet->name->like($searchString);

        $organizations = $organizationSet->select($criteria);

        $nbOrganizations = $organizations->count();
        $nb = 0;
        $exactIds = array();
        foreach ($organizations as $organization) {
            if ($nb++ >= $this->maxElementsBySection) {
                break;
            }
            $section->addItem(
                    $Crm->Ui()->OrganizationCardFrame($organization)
                    ->addClass('crm-search-result crm-exact-match')
            );
            $exactIds[$organization->id] = $organization->id;
        }


        $criteria = $organizationSet->id->notIn($exactIds)->_AND_(
                $organizationSet->name->matchOne($searchString)
                ->_OR_($organizationSet->description->matchOne($searchString))
                ->_OR_($organizationSet->address->postalCode->matchOne($searchString))
                ->_OR_($organizationSet->address->city->matchOne($searchString))
                ->_OR_($organizationSet->address->street->matchOne($searchString))
                ->_OR_($organizationSet->phone->matchOne($searchString))
                ->_OR_($organizationSet->fax->matchOne($searchString))
        );


        $organizations = $organizationSet->select($criteria);

        $nbOrganizations += $organizations->count();
        foreach ($organizations as $organization) {
            if ($nb++ >= $this->maxElementsBySection) {
                break;
            }
            $section->addItem(
                    $Crm->Ui()->OrganizationCardFrame($organization)
                    ->addClass('crm-search-result')
            );
        }

        if ($nb < $nbOrganizations)  {
            $section->addItem(
                $W->Link(
                    sprintf($Crm->translate('%d more...'), $nbOrganizations - $nb + 1),
                    $Crm->Controller()->Search()->display(array('includeOrganizations' => 1, 'maxElementsBySection' => 100, 'searchString' => $searchString))
                )->addClass('widget-actionbutton widget-100pc widget-align-center')
            );
        }

        if ($nbOrganizations > 0) {
            $section->setFoldable(true);
        }


        $section->addContextMenu('inline')->addItem($W->Label(sprintf($Crm->translate('%d results'), $nbOrganizations))->addClass('crm-counter-label'));

        return $section;
    }







    public function displayDeals($searchString = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $dealSet = $Crm->DealSet();
        $dealSet->address();

        $dealSet->addFields(
                $dealSet->year->concat('-')->concat($dealSet->number->leftPad(3, '0'))
                ->setName('fullNumber'),
                $dealSet->year->concat('-')->concat($dealSet->number)
                ->setName('fullNumberNoPad')
        );

        $criteria = $dealSet->name->matchOne($searchString)
        ->_OR_($dealSet->description->matchOne($searchString))
        ->_OR_($dealSet->address->postalCode->matchOne($searchString))
        ->_OR_($dealSet->address->city->matchOne($searchString))
        ->_OR_($dealSet->address->street->matchOne($searchString))
        ->_OR_($dealSet->fullNumber->is($searchString))
        ->_OR_($dealSet->fullNumberNoPad->is($searchString))
        ;

        $deals = $dealSet->select($criteria);

        $section = $W->Section(
                $Crm->translate('Deals'),
                $W->VBoxLayout(),
                3
        );

        $nbDeals = $deals->count();
        if ($nbDeals > 0) {
            $section->setFoldable(true);
            $nb = 0;
            foreach ($deals as $deal) {
                if ($nb++ >= $this->maxElementsBySection) {
                    break;
                }
                $section->addItem(
                        $Crm->Ui()->DealCardFrame($deal)
                        ->addClass('crm-search-result')
                );
            }

            if ($nb < $nbDeals)  {
                $section->addItem(
                    $W->Link(
                        sprintf($Crm->translate('%d more...'), $nbDeals - $nb + 1),
                        $Crm->Controller()->Search()->display(array('includeDeals' => 1, 'maxElementsBySection' => 100, 'searchString' => $searchString))
                    )->addClass('widget-actionbutton widget-100pc widget-align-center')
                );
            }
        }

        $section->addContextMenu('inline')->addItem($W->Label(sprintf($Crm->translate('%d results'), $nbDeals))->addClass('crm-counter-label'));

        return $section;
    }




    public function displayNotes($searchString = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $noteSet = $Crm->NoteSet();

        $criteria = $noteSet->summary->matchOne($searchString);

        $notes = $noteSet->select($criteria);

        $section = $W->Section(
                $Crm->translate('Notes'),
                $W->VBoxLayout(),
                3
        )->addClass('compact');

        $nbNotes = $notes->count();
        if ($nbNotes > 0) {
            $nb = 0;
            $section->setFoldable(true);
            foreach ($notes as $note) {
                if ($nb++ >= $this->maxElementsBySection) {
                    break;
                }
                $section->addItem(
                        $Crm->Ui()->HistoryElement($note)
                        ->addClass('crm-search-result')
                );
            }

            if ($nb < $nbNotes)  {
                $section->addItem(
                    $W->Link(
                        sprintf($Crm->translate('%d more...'), $nbNotes - $nb + 1),
                        $Crm->Controller()->Search()->display(array('includeNotes' => 1, 'maxElementsBySection' => 100, 'searchString' => $searchString))
                    )->addClass('widget-actionbutton widget-100pc widget-align-center')
                );
            }
        }

        $section->addContextMenu('inline')->addItem($W->Label(sprintf($Crm->translate('%d results'), $nbNotes))->addClass('crm-counter-label'));

        return $section;
    }




    public function displayEmails($searchString = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $emailSet = $Crm->EmailSet();

        $criteria = $emailSet->subject->matchOne($searchString)
        ->_OR_($emailSet->body->matchOne($searchString))
        ->_OR_($emailSet->recipients->matchOne($searchString))
        ->_OR_($emailSet->ccRecipients->matchOne($searchString))
        ->_OR_($emailSet->bccRecipients->matchOne($searchString));

        $emails = $emailSet->select($criteria);

        $section = $W->Section(
                $Crm->translate('Emails'),
                $W->VBoxLayout(),
                3
        )->addClass('compact');

        $nbEmails = $emails->count();
        if ($nbEmails > 0) {
            $section->setFoldable(true);
            $nb = 0;
            foreach ($emails as $email) {
                if ($nb++ >= $this->maxElementsBySection) {
                    break;
                }
                $section->addItem(
                        $Crm->Ui()->HistoryElement($email)
                        ->addClass('crm-search-result')
                );
            }

            if ($nb < $nbEmails)  {
                $section->addItem(
                    $W->Link(
                        sprintf($Crm->translate('%d more...'), $nbEmails - $nb + 1),
                        $Crm->Controller()->Search()->display(array('includeEmails' => 1, 'maxElementsBySection' => 100, 'searchString' => $searchString))
                    )->addClass('widget-actionbutton widget-100pc widget-align-center')
                );
            }
        }

        $section->addContextMenu('inline')->addItem($W->Label(sprintf($Crm->translate('%d results'), $nbEmails))->addClass('crm-counter-label'));

        return $section;
    }




    public function displayTasks($searchString = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $taskSet = $Crm->TaskSet();
        $criteria = $taskSet->description->matchOne($searchString);


        $tasks = $taskSet->select($criteria);

        $section = $W->Section(
                $Crm->translate('Tasks'),
                $W->VBoxLayout(),
                3
        )->addClass('compact');

        $nbTasks = 	$tasks->count();
        if ($nbTasks > 0) {
            $section->setFoldable(true);
            $nb = 0;
            foreach ($tasks as $task) {
                if ($nb++ >= $this->maxElementsBySection) {
                    break;
                }
                $section->addItem(
                        $Crm->Ui()->HistoryElement($task)
                        ->addClass('crm-search-result')
                );
            }

            if ($nb < $nbTasks)  {
                $section->addItem(
                    $W->Link(
                        sprintf($Crm->translate('%d more...'), $nbTasks - $nb + 1),
                        $Crm->Controller()->Search()->display(array('includeTasks' => 1, 'maxElementsBySection' => 100, 'searchString' => $searchString))
                    )->addClass('widget-actionbutton widget-100pc widget-align-center')
                );
            }
        }
        $section->addContextMenu('inline')->addItem($W->Label(sprintf($Crm->translate('%d results'), $nbTasks))->addClass('crm-counter-label'));

        return $section;
    }


    /**
     *
     */
    public function getOpenSearchDescription()
    {
        $addon = bab_getAddonInfosInstance($this->Crm()->addonName);

        $xml = '<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">
        <ShortName>' . $addon->getDescription() . '</ShortName>
        <Description>' . $this->Crm()->getDescription() . '</Description>
        <InputEncoding>' . bab_charset::getIso() . '</InputEncoding>
        <Image width="16" height="16" type="image/vnd.microsoft.icon">images/Puces/favicon.ico</Image>
        <Url type="text/html"
        method="get"
        template="' . $GLOBALS['babUrl'] . '?tg=' . $this->Crm()->controllerTg . '&amp;idx=search.display&amp;search[searchString]={searchTerms}">
        </Url>
        <moz:SearchForm>' . $GLOBALS['babUrl'] . '?tg=' . $this->Crm()->controllerTg . '&amp;idx=search.display</moz:SearchForm>
        </OpenSearchDescription>';

        die($xml);
    }





    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestArticle($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestArticle();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }


    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestClassification($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestClassification();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }





    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestPostalCode($search = null)
    {
        $W = bab_Widgets();

        $suggest = $W->SuggestPostalCode();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }




    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestPlaceName($search = null)
    {
        $W = bab_Widgets();

        $suggest = $W->SuggestPlaceName();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }




    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestPosition($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestPosition();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }




    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestAddressType($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestAddressType();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }


    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestAddress($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestAddress();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }


    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestEmail($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $suggestTag = $Ui->SuggestEmail();
        $suggestTag->setMetadata('suggestparam', 'search');
        $suggestTag->suggest();

        die;
    }



    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestEntry($category, $search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestEntry($category);
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }



    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestStatus($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestStatus();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }

    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestMember($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $suggest = $Ui->SuggestMember();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }





    /**
     * Returns a list of elements showing the recent activity on a specified crm_record.
     *
     * @see crm_Ui::History()
     *
     * @param string $ref       A reference to a crm_record.
     * @param int $max          The maximum number of items to display.
     * @param int $skip         The number of items to skip from the most recent history.
     * @param bool $showToolbar If a toolbar should be displayed (with add note button).
     *
     * @return Widget_Displayable_Interface
     */
    public function history($ref, $max = null, $skip = 0, $showToolbar = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        $record = $Crm->getRecordByRef($ref);

        if (!$record) {
            return null;
        }

        $history = $Ui->History($record);

        if (!($history instanceof crm_History)) {
            // This hack is because there was some addons with the History() method overloaded
            // with a displayable interface return value
            // the code has been moved to Ui->HistoryFrame() but this verification will protect forgotten cases
            $Ui->includeHistory();
            $history = new crm_History($record);
        }

        if (isset($max)) {
            $history->setMaxDisplayedElements($max);
        }
        if (isset($skip)) {
            $history->setSkipDisplayedElements($skip);
        }

        $toolbar = null;
        if (isset($Crm->Note) && $showToolbar) {
            $toolbar = new crm_Toolbar();
            $toolbar->addItem(
                $W->Link($Crm->translate('Add a note'), $Crm->Controller()->Note()->edit(null, $ref))
                    ->addClass('widget-actionbutton')
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }

        $box = $W->VBoxItems(
            $toolbar,
            $history
        );

        $olderHistory = $W->DelayedItem($Crm->Controller()->Search()->history($ref, $max, $skip + $max))
            ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE);
        $olderHistory->setSizePolicy('widget-list-element');

        $history->setObject($record);
        $history->addItem($olderHistory);

        $remainingElementsToDisplay = $history->getRemainingElementsToDisplay();

        if ($remainingElementsToDisplay > 0) {
            $olderHistory->setPlaceHolderItem(
                $W->VBoxItems(
                    $W->Link(sprintf($Crm->translate('Display older history, %d more item(s)'), $remainingElementsToDisplay))
                        ->setAjaxAction($Crm->Controller()->Search()->cancel(), $olderHistory)
                        ->addClass('icon ' . Func_Icons::ACTIONS_ARROW_DOWN)
                )
                ->addClass(Func_Icons::ICON_LEFT_16, 'widget-actionbutton')
            );
        }

        $box->setReloadAction($Crm->Controller()->Search()->history($ref, $max, $skip, $showToolbar));

        return $box;
    }



    /**
     * Validates that the value is a correct intracommunity vat number.
     * Uses the official European Commission  webservice.
     * This action is designed for use with a setAjaxValidateAction().
     *
     * @example bab_Widgets()->LineEdit()->setAjaxValidateAction($Crm->Controller()->Search()->checkIntracommunityVat());
     * @see Widget_InputWidget::setAjaxValidateAction
     * @param string $value
     * @return array
     */
    public function checkIntracommunityVat($value = null)
    {
        $Crm = $this->Crm();

        $apiserver = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';
        if (!class_exists('SoapClient')) {
            return array(
                'status' => 'ERR',
                'info' => '',
                'dump' => ''
            );
        }
        $client = new SoapClient($apiserver);
        $code = str_replace(' ', '', $value);
        $countryCode = substr($code, 0, 2);
        $vatNumber = substr($code, 2);

        try {
            $res = $client->checkVat(array('countryCode' => $countryCode, 'vatNumber' => $vatNumber));
        } catch (SoapFault $exception) {
            die ($Crm->translate('The intracommunity VAT number is not valid.'));
        }

        if ($res->valid) {
            $info = $Crm->translate('The intracommunity VAT number is valid.') . "\n"
                . $Crm->translate('Name') . ': ' . $res->name . "\n"
                . $Crm->translate('Address') . ': ' . $res->address . "\n"
                . $Crm->translate('Country code') . ': ' . $res->countryCode . "\n"
                . $Crm->translate('VAT number') . ': ' . $res->vatNumber;
            return array(
                'status' => 'OK',
                'info' => $info,
                'dump' => print_r($res, true)
            );
        }

        return array(
            'status' => 'ERROR',
            'info' => $Crm->translate('The intracommunity VAT number is not valid.')
        );
    }



    /**
     * Does nothing and returns to the previous page.
     *
     * @param array	$message
     * @return Widget_Action
     */
    public function cancel()
    {
        crm_redirect(crm_BreadCrumbs::last());
    }
}
