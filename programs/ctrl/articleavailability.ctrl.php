<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * 
 */
class crm_CtrlArticleAvailability extends crm_Controller implements crm_ShopAdminCtrl
{
	
	/**
	 * @return Widget_Action
	 */
	public function displayList($articleavailability = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($articleavailability), $Crm->translate('Article availability'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('Article availability configuration'));
		
		
		$filter = isset($articleavailability['filter']) ? $articleavailability['filter'] : array();
		
		$set = $Crm->ArticleAvailabilitySet();

		$table = $Crm->Ui()->ArticleAvailabilityTableView();
		
		$table->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		$table->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
		$table->addColumn(widget_TableModelViewColumn($set->can_be_purchased, $Crm->translate('Can be purchased')));
		$table->addColumn(widget_TableModelViewColumn($set->color, $Crm->translate('Color'))->setVisible(false));
		
		
		$criteria = $table->getFilterCriteria($filter);
		$table->setDataSource($set->select($criteria));
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'articleavailability');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add an article availability'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function display()
	{
		// Not used
	}
	
	/**
	 * @return Widget_Action
	 */
	public function edit($articleavailability = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($articleavailability), $Crm->translate('Edit article availability'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit article availability'));
		
		if (null !== $articleavailability)
		{
			$set = $Crm->ArticleAvailabilitySet();
			$articleavailability = $set->get($articleavailability);
		
		}
		
		
		
		$form = $Ui->ArticleAvailabilityEditor($articleavailability);
		
		$page->addItem($form);
		
		if ($articleavailability instanceof crm_ArticleAvailability)
		{
			$actionsFrame = $page->ActionsFrame();
			$page->addContextItem($actionsFrame);
			
			$actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($articleavailability->id)));
		}
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function save($articleavailability = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->ArticleAvailabilitySet();
		
		if (empty($articleavailability['id']))
		{
			$record = $set->newRecord();
		} else {
			$record = $set->get($articleavailability['id']);
			if (null == $record)
			{
				throw new crm_AccessException($Crm->translate('This article availability does not exists'));
			}
		}
		


		$record->setValues($articleavailability);
	
		$record->save();
		
		return true;
	}
	
	
	
	public function delete($articleavailability = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		if (!$articleavailability)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->ArticleAvailabilitySet();
		$set->delete($set->id->is($articleavailability));
		
		crm_redirect($this->proxy()->displayList());
	}
}
