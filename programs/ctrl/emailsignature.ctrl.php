<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


$Crm = crm_Crm();
$Crm->includeRecordController();


/**
 * This controller manages actions that can be performed on email signatures.
 */
class crm_CtrlEmailSignature extends crm_RecordController
{
    /**
     * @param widget_TableModelView $tableView
     * @return crm_Toolbar
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $toolbar = new crm_Toolbar();
        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Add signature'), $this->proxy()->edit()
            )->addClass('icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        return $toolbar;
    }


    /**
     * Method for post save actions on record
     * @param crm_Record $record
     */
    protected function postSave(crm_Record $record, $data)
    {
		if(!$record->userid) {
			$record->userid = bab_getUserId();
			$record->save();
		}
    }
}
