<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on discounts.
 */
class crm_CtrlDiscount extends crm_Controller implements crm_ShopAdminCtrl
{

	/**
	 * Display the list of discount for administration purpose
	 * @return Widget_Action
	 */
	public function displayList($discount = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($discount), $Crm->translate('Discounts'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		$page->setTitle($Crm->translate('Discount'));
		
		
		$filter = isset($discount['filter']) ? $discount['filter'] : array();
		
		
		$set = $this->Crm()->DiscountSet();
				
		$allConditions = new ORM_TrueCriterion();

		$table = $Crm->Ui()->DiscountTableView();
		$table->addDefaultColumns($set);
		
		$criteria = $table->getFilterCriteria($filter);		
		
		$discountSelection = $set->select($criteria);
		
		$table->setDataSource($discountSelection);
		
		$table->addClass('icon-left-16 icon-left icon-16x16');
		
		$filterPanel = $table->filterPanel($filter, 'discount');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a discount'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * 
	 * @param int $discount
	 * 
	 * @return Widget_Action
	 */
	public function edit($discount = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (null === $discount && !$access->createDiscount()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($discount), $Crm->translate('Edit discount'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit discount'));
		
		if (null !== $discount && !is_array($discount)) {
			$set = $Crm->DiscountSet();
			$discount = $set->get($discount);
			
			if (!$access->updateDiscount($discount)) {
				throw new crm_AccessException($Crm->translate('Access denied'));
			}
			$form = $Ui->DiscountEditor($discount);
		} else {
			$form = $Ui->DiscountEditor();
			if (is_array($discount)) {
				$form->setHiddenValue('discount[id]', $discount['id']);
				$form->setValues($discount, array('discount'));
			}
		}
		
		$page->addContextItem($W->VBoxItems());
		
		$page->addItem($form);
		
		return $page;
	}
	
	
	
	/**
	 * Save discount
	 * @param array $discount
	 * 
	 * @return Widget_Action
	 */
	public function save($discount = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (empty($discount['id']) && !$access->createDiscount()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $this->Crm()->DiscountSet();
		if(!isset($discount['id'])){
			$discount['id'] = '';
		}
		
		if (empty($discount['id'])) {		
			$discountRecord = $set->newRecord();
		} else {
			$discountRecord = $set->get($discount['id']);
		}
		
		$discount['shipping'] = 0;
		if ($discount['opt'] == 'shipping') {
		    $discount['shipping'] = 1;
		}
		
		$discountRecord->setFormInputValues($discount);
		$discountRecord->save();
		
		$discountRecord;

		crm_redirect($this->Crm()->Controller()->Discount()->Display($discountRecord->id));
		
		die;
	}
	
	/**
	 * Display one discount for administration purpose
	 * @return Widget_Action
	 */
	public function display($discount = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$set = $Crm->DiscountSet();
		
		$discount = $set->get($discount);
		
		if (null == $discount)
		{
			throw new crm_AccessException($Crm->translate('Discount not found'));
		}
		
		$access = $Crm->Access();
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($discount->id), bab_abbr($discount->description, BAB_ABBR_FULL_WORDS, 80));
		
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		
		$discountDisplay = $Ui->DiscountDisplay($discount);
		
		
		$fullFrame = $discountDisplay->getFullFrame();
		$page->addItem($fullFrame);
		
		$actionsFrame = $page->ActionsFrame();
		$page->addContextItem($actionsFrame);
		
		$actions = $this->getActionFrame($discount);
		foreach($actions as $action){
			$actionsFrame->addItem($action);
		}
		
		return $page;
	}
	
	public function getActionFrame($discount)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$returnObjets = array();
		
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Edit discount'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Discount()->edit($discount->id));
			
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Delete discount'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Discount()->delete($discount->id))->setConfirmationMessage($Crm->translate('Do you really want to delete the discount?'));
		
		return $returnObjets;
	}




	public function delete($discount)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		$discountSset = $this->Crm()->DiscountSet();
		$discountRecord = $discountSset->get($discount);
		
		if (!$discountRecord)
		{
			throw new crm_Exception($Crm->translate('This discount does not exists'));
		}
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$discountSset->delete($discountSset->id->is($discount));

		crm_redirect($this->proxy()->displayList());
	}


	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::getPosition(-2));
	}
}
