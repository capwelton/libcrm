<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on catalog
 */
class crm_CtrlCatalog extends crm_Controller implements crm_ShopAdminCtrl
{
	/**
	 * Get action object used in catalog controller
	 * @return crm_Actions
	 */
	protected function getActions()
	{
		return new crm_Actions;
	}

	/**
	 *
	 * @return Widget_Link
	 */
	protected function listLink()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		return $W->Link(
			$W->Icon($Crm->translate('List categories'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS ),
			$this->proxy()->displayList()
		);
	}

	/**
	 *
	 * @return Widget_Link
	 */
	protected function editLink($catalogRecord)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		return $W->Link(
			$W->Icon($Crm->translate('Edit category'), Func_Icons::ACTIONS_DOCUMENT_EDIT ),
			$this->proxy()->edit($catalogRecord->id)
		);
	}

	/**
	 *
	 * @return Widget_Link
	 */
	protected function deleteLink($catalogRecord)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		return $W->Link(
			$W->Icon($Crm->translate('Delete category'), Func_Icons::ACTIONS_EDIT_DELETE ),
			$this->proxy()->delete($catalogRecord->id)
		)->setConfirmationMessage($Crm->translate('Do you really want to delete the category?'));
	}




	protected function catalogDisplay($catalogRecord)
	{
		return new crm_CatalogDisplay($this->Crm(), $catalogRecord);
	}
	
	


	/**
	 * Display catalogs list
	 * @return Widget_Action
	 */
	public function displayList($catalogs = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Categories'));
		
		if (!$access->listCatalog())
		{
		    $message = $Crm->translate('Access denied');
			throw new crm_AccessException($message);
		}
		
		$Crm->Ui()->includeCatalog();
		$set = $Crm->CatalogSet();
		$set->parentcatalog();
		

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		$filter = isset($catalogs['filter']) ? $catalogs['filter'] : array();
		
		$table = $Crm->Ui()->CatalogTableView();
		$table->addDefaultColumns($set);
		$table->setSortField('sortkey:up');
		
		$criteria = $table->getFilterCriteria($filter);
		
		$table->setDataSource($set->Select($criteria));
		
		
		$filterPanel = $table->filterPanel($filter, 'catalogs');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a category'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		$toolbar->addButton($Crm->translate('Sort first-level categories'), Func_Icons::ACTIONS_ARROW_UP_DOUBLE, $this->proxy()->firstlevel());
		
		$page->addToolbar($toolbar);

		return $page;
	}


	/**
	 * Display a catalog
	 * @return Widget_Action
	 */
	public function display($catalog = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($catalog), $Crm->translate('Category'));
		
		if (!$access->readCatalog($catalog))
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		
		$Crm->Ui()->includeCatalog();
		$Crm->Ui()->includeCatalogItem();

		$catalogSet = $Crm->CatalogSet();

		$catalogRecord = $catalogSet->get($catalog);

		$page = $Crm->Ui()->Page();




		// display catalog frame

		$fullFrame = $this->catalogDisplay($catalogRecord)->getFullFrame();

		$page->addItem($fullFrame);

		
		$actions = $this->getActions();
		$actions->addItem($this->listLink());
		$actions->addItem($this->editLink($catalogRecord));
		$actions->addItem($this->deleteLink($catalogRecord));
		
		if ($access->viewCatalog($catalogRecord))
		{
			$actions->addItem($W->Link($W->Icon($Crm->translate('View online'), Func_Icons::ACTIONS_GO_HOME), $Crm->Controller()->CatalogItem()->catalog($catalogRecord->id)));
		}

		$page->addContextItem($actions);

		return $page;
	}



	/**
	 * Edit a catalog
	 * @return Widget_Action
	 */
	public function edit($catalog = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($catalog), null === $catalog ? $Crm->translate('Create category') : $Crm->translate('Edit category'));
		
		if (!$access->updateCatalog($catalog))
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$catalogSet = $Crm->CatalogSet();

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');

		$catalogRecord = null;

		if (isset($catalog)) {
			$catalogRecord = $catalogSet->get($catalog);
		}

		$form = $Crm->Ui()->CatalogEditor($catalogRecord);
		
		


		$page->addItem($form);


		$actions = $this->getActions();
		$actions->addItem($this->listLink());

		$page->addContextItem($actions);


		return $page;
	}


	/**
	 * Save a catalog
	 *
	 * @param array $catalog
	 * @return Widget_Action
	 */
	public function save($catalog = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();

		$set = $Crm->CatalogSet();


		if (is_numeric($catalog['id'])) {
			
			if (!$access->updateCatalog($catalog['id']))
			{
				throw new crm_AccessException($Crm->translate('Access denied'));
			}
			
			
			$record = $set->get($catalog['id']);
		} else {

			if (empty($catalog['name'])) {
				throw new crm_SaveException($Crm->translate('The name is mandatory'));
			}

			$record = $set->newRecord();
		}


		$record->setValues($catalog);
		$record->save();
		
		/* @var $record crm_Catalog */
		
		$W = bab_Widgets();
		$files = $W->ImagePicker()->getTemporaryFiles('photo');
		if (isset($files))
		{
			foreach($files as $file)
			{
				/* @var $file Widget_FilePickerItem */
				
				$source = $file->getFilePath();
				
				$target = $record->getPhotoFolder();
				$target->push($file->getFileName());
				
				rename($source->toString(), $target->toString());
			}
		}
		
		/**
		 * catalogs are recorded into sitemap for online shop
		 * @see crm_Event::onBeforeSiteMapCreated
		 */
		bab_siteMap::clearAll();

		return true;
	}
	
	
	
	
	public function firstlevel()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Ui->includeCatalog();
		$access = $Crm->Access();
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->firstlevel(), $Crm->translate('First level of categories'));
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$catalogSet = $Crm->CatalogSet();
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');
		
		
		$res = $catalogSet->select($catalogSet->parentcatalog->is(0))->orderAsc($catalogSet->sortkey);
		$form = crm_SortCatalogForm($Crm, $res, $this->proxy()->saveFirstlevelSort(), $this->proxy()->displayList());
		$form->addClass('crm-editor');
		$page->addItem($form);
		
		$actions = $this->getActions();
		$actions->addItem($this->listLink());
		
		$page->addContextItem($actions);
		
		return $page;
	}
	
	
	public function saveFirstlevelSort($sortkeys = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->CatalogSet();
		
		$res = $set->select($set->parentcatalog->is(0));
		$res->orderAsc($set->sortkey);
		$childnodes = array();
		
		foreach($res as $childnode)
		{
			/* @var $childnode crm_Catalog */
			$childnodes[$childnode->id] = $childnode;
		}
		
		$i = 0;
		$i++;
		
		foreach($sortkeys as $id => $dummy)
		{
			$i = $childnodes[$id]->setSortKey($i);
			$i++;
		}
		
		bab_sitemap::clear();
		
		return true;
	}
	
	
	
	public function saveSort($catalog = null, $sortkeys = null)
	{
		
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->updateCatalog($catalog))
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->CatalogSet();
		$catalogRecord = $set->get($catalog);
		
		/* @var $catalogRecord crm_Catalog */
		
		
		$res = $catalogRecord->getCatalogs();
		$childnodes = array();
		
		foreach($res as $childnode)
		{
			/* @var $childnode crm_Catalog */
			$childnodes[$childnode->id] = $childnode;
		}
		
		$i = (int) $catalogRecord->sortkey;
		$i++;
		
		foreach($sortkeys as $id => $dummy)
		{
			$i = $childnodes[$id]->setSortKey($i);
			$i++;
		}
		
		bab_sitemap::clear();
		
		return true;
	}





	/**
	 *
	 * @param int	$catalog
	 * @return unknown_type
	 */
	public function delete($catalog = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->deleteCatalog($catalog))
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->CatalogSet();
		$set->delete($set->id->is($catalog));
		
		// categories are in sitemaps
		
		bab_sitemap::clearAll();

		crm_redirect($this->proxy()->displayList());
	}





	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::getPosition(-2));
	}

}