<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on articles.
 */
class crm_CtrlComment extends crm_Controller implements crm_ShopAdminCtrl
{


	/**
	 *
	 * @param $comment
	 * @return Widget_Action
	 */
	public function edit($comment = null, $for = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Crm->includeStatusSet();
		
		// crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit(), $Crm->translate('Edit comment'));


		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');

		if (isset($comment)) {
			$Crm->includeArticleSet();
			$commentSet = $Crm->CommentSet()->joinArticleForDisplay();
			
			$comment = $commentSet->get($comment);
			if (!isset($comment)) {
				throw new crm_Exception($Crm->translate('Trying to access a comment with a wrong (unknown) id.'));
			}
			$page->addItem($W->Title($Crm->translate('Edit comment'), 1));
			
			$targetRecord = $comment->article;
			
		} else {
			$page->addItem($W->Title($Crm->translate('Create a new comment'), 1));
			
			if (isset($for)) {
				$targetRecord = $Crm->getRecordByRef($comment['for']);
			} else {
				throw new crm_Exception('missing target record');
			}
		}
		
		
		

		$editor = $this->Crm()->Ui()->CommentEditor(true, $targetRecord, $comment);

		$page->addItem($editor);

		if (isset($comment)) {
			$page->addContextItem(crm_commentLinkedObjects($comment));
		}

		return $page;
	}



	/**
	 *
	 * @param int $comment 		The comment id
	 * @return Widget_Action
	 */
	public function display($comment)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$commentSet = $Crm->CommentSet();
		$comment = $commentSet->get($comment);
		if (!isset($comment)) {
			throw new crm_Exception($Crm->translate('Trying to access a comment with a wrong (unknown) id.'));
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($comment->id), $Crm->translate('Comment'));

		$page = $this->Crm()->Ui()->Page();
		$page->addClass('crm-page-editor');

		if (null !== $comment->rating) {
			$page->addItem($W->RatingEdit()->setValue($comment->rating)->setDisplayMode());
		}

		$commentFrame = $Crm->Ui()
			->HistoryElement($comment, crm_HistoryElement::MODE_FULL)
			->addComments();

		$page->addItem($commentFrame);




		$actionsFrame = $page->ActionsFrame();

		$page->addContextItem($actionsFrame);


		if ($Crm->Access()->updateComment($comment)) {
			$actionsFrame->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Edit this comment'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
					$Crm->Controller()->Comment()->edit($comment->id)
				)
			);
		}

		if ($Crm->Access()->deleteComment($comment)) {
			$actionsFrame->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Delete this comment'), Func_Icons::ACTIONS_EDIT_DELETE),
					$Crm->Controller()->Comment()->delete($comment->id)
				)->setConfirmationMessage($Crm->translate('Are you sure you want to delete this comment? It will be definitely lost!'))
			);
		}



		$page->addContextItem(crm_commentLinkedObjects($comment));

		return $page;
	}




	/**
	 * Saves the comment
	 *
	 * @param array	$comment
	 * @return Widget_Action
	 */
	public function save($comment = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$set = $Crm->CommentSet();

		if (isset($comment['id'])) {
			$record = $set->get($comment['id']);
			if (!isset($record)) {
				throw new crm_SaveException($Crm->translate('Trying to access a comment with a wrong (unknown) id.'));
			}
		} else {
			$record = $set->newRecord();
		}
		
		
		if (isset($comment['confirmed'])) unset($comment['confirmed']);
		if (isset($comment['confimedBy'])) unset($comment['confimedBy']);
		if (isset($comment['confirmedOn'])) unset($comment['confirmedOn']);
		
		
		/*@var $record crm_Comment */
		
		$target = null;
		if (isset($comment['for'])) {
			// for contains a string like Article:123
			$target = $Crm->getRecordByRef($comment['for']);
			
			if (!$Access->createCommentOn($target))
			{
				throw new crm_AccessException(sprintf($Crm->translate('You are not allowed to add a comment to %s'), $comment['for']));
			}
			
			if (!$Access->setPrivateCommentOn($target))
			{
				$comment['private'] = 0;
			} else {
				if ($comment['private'])
				{
					// autoconfirm private comments
					$comment['confirmed'] = 1;
				}
			}
			
		} elseif (!$record->id) {
			
			throw new crm_SaveException('reference in mandatory for comment creation');
		}
		
		
		
		if ($record->id && !$Access->updateComment($record))
		{
			throw new crm_AccessException($Crm->translate('You are not allowed to update this comment'));
		}
		
		
		if (empty($comment['summary']))
		{
			throw new crm_SaveException($Crm->translate('The comment is mandatory'));
		}
		

		if (bab_Charset::getIso() !== bab_Charset::UTF_8) {
			$comment['summary'] = iconv(bab_Charset::getIso(), bab_Charset::UTF_8, $comment['summary']);
			$comment['summary'] = html_entity_decode($comment['summary'], ENT_QUOTES, bab_Charset::UTF_8);
			$comment['summary'] = iconv(bab_Charset::UTF_8, bab_Charset::getIso() .'//TRANSLIT', $comment['summary']);
		}
		
		
		if ($record->id && isset($comment['private']))
		{
			// on ne peut pas rendre public 1 commentaire prive qui n'a pas passe l'etape approbation
			unset($comment['private']);
		}

		$record->setValues($comment);
		
		if (!$record->id) {
			
			switch(true)
			{
				case $target instanceof crm_Article:
					$record->article = $target->id;
					break;
					
				default:
					throw new crm_SaveException('Failed to save comment because of unexpected object to link to');
					break;
			}
			
			
			
		}

		$record->save();


		

		$record->updateTargetAverage();
		
		return true;
	}
	
	
	
	/**
	 * Display list of waiting comments to confirm
	 */
	public function displayList()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$set = $Crm->CommentSet();
		
		
		if (!$Access->confirmComments())
		{
			throw new crm_AccessException($Crm->translate('You are not allowed to confirm comments'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Waiting comments'));
		
		
		$page = $this->Crm()->Ui()->Page();
		$page->addClass('crm-page-editor');
		
		$page->addItem($Crm->Ui()->WaitingCommentsEditor());
		
		return $page;
	}
	

	public function loadWaitingComment($comment)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		
		if (!$Access->confirmComments())
		{
			throw new crm_AccessException($Crm->translate('You are not allowed to confirm comments'));
		}
		
		
		$set = $Crm->CommentSet()->joinArticleForDisplay();
		
		
		$comment = $set->get($comment);
		
		if (null == $comment)
		{
			throw new crm_AccessException($Crm->translate('The comment does not exists'));
		}
		
		// simulate an allredy approved comment
		
		$comment->confirmed = 1;
		
		$frame = $W->Frame();
		if ($comment->article)
		{
			$frame->addItem($Crm->Ui()->ArticleCardFrame($comment->article));
		}
		$frame->addItem($Crm->Ui()->HistoryElement($comment, crm_HistoryElement::MODE_FULL));
		$frame->addItem($buttons = $W->HBoxLayout());
		
		$buttons->addItem($W->Link($Crm->translate('Edit'), $Crm->Controller()->Comment()->edit($comment->id))->addClass('crm-dialog-button'));
		$buttons->addItem($W->Link($Crm->translate('Delete'), $Crm->Controller()->Comment()->delete($comment->id))->addClass('crm-dialog-button')->setConfirmationMessage($Crm->translate('Are you sure you whant to delete this comment?')));

		return $frame;
	}


	/**
	 * Confirm a list of comments
	 * 
	 * @param array $comments
	 * 
	 * @throws crm_SaveException
	 * @throws crm_AccessException
	 * 
	 * @return Widget_Action
	 */
	public function confirm($comments = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$set = $Crm->CommentSet();
		
		
		if (!$Access->confirmComments())
		{
			throw new crm_AccessException($Crm->translate('You are not allowed to confirm comments'));
		}
		
		if (!isset($comments))
		{
			throw new crm_SaveException($Crm->translate('No comments to confirm'));
		}
		
		
		foreach($comments as $comment => $confirmed)
		{
			$record = $set->get($comment);
			
			if (isset($record) && $confirmed)
			{
				$record->confirmed = 1;
				$record->confirmedBy = $Access->currentUser();
				$record->confirmedOn = date('Y-m-d H:i:s');
				$record->save();
				$record->updateTargetAverage();
			}
		}
		
		
		
		return true;
	}





	/**
	 * Deletes the specified comment
	 *
	 * @param int	$comment
	 * @return Widget_Action
	 */
	public function delete($comment)
	{

		$set = $this->Crm()->CommentSet();

		$set->delete($set->id->is($comment));

		crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The comment has been deleted.'));
	}



}
