<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */








/**
 * This controller manages actions that can be performed on the home page.
 */
class crm_CtrlHome extends crm_Controller
{
	/**
	 *
	 * @return string
	 */
	protected function getConfigurationCookieName()
	{
		$Crm = $this->Crm();
		return $Crm->classPrefix . 'homepage_deals_configuration_iduser'.$GLOBALS['BAB_SESS_USERID'];
	}


	/**
	 * Save the homepage deals configuration : configuration is saved to a cookie for each user
	 *
	 * @param array		$status
	 */
	function saveHomepageDealsConfiguration($status = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$configurationCookieName = $this->getConfigurationCookieName();

		if (isset($status)) {
			$statusList = '';
			foreach ($status as $key => $value) {
				if ($value === 'on') {
					if ($statusList === '') {
						$statusList .= $key;
					} else {
						$statusList .= ','.$key;
					}
				}
			}
			setcookie($configurationCookieName, $statusList, time()+60*60*24*365); /* One year to expiration */
		} else {
			/* Set to default values */
			setcookie($configurationCookieName, '0', time()+60*60*24*365); /* One year to expiration */
		}

		crm_redirect($Crm->Controller()->Home()->displaySummary());
	}


	/**
	 * Display the configuration form of deals for homepage : configuration is saved to a cookie for each user
	 */
	public function dealsConfiguration()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$Crm->includeStatusSet();

		$Crm->Ui()->includeBase();

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-editor');

		/* Display the list of deals status */
		$page->addItem($W->Title($Crm->translate('Homepage configuration'), 1));
		$page->addItem($W->Title($Crm->translate('Choose statutes for opportunities to be shown:'), 2));

		/* Select status currently selected by the current user : by cookie */
		$statusCurrentlySelected = $this->getSelectedStatuses();

		/* Create the statutes selector */
		$statusSelector = new crm_Editor($Crm); /* crm_Editor : Form */
		$statusSelector->setHiddenValue('tg', bab_rp('tg'));
		$statusSelector->setName('status');

		$statusSet = $Crm->StatusSet();
		$root = $statusSet->get(1);
		$treeview = $W->SimpleTreeView();

		/* Select all status */
		$nodes = $statusSet->select();

		foreach ($nodes as $node) {
			$element =& $treeview->createElement($node->id, '', $node->name, '', '');
			$element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

			$selected = array_key_exists($node->id, $statusCurrentlySelected);

			$element->addCheckBox('status[' . $node->id . ']', $selected);
			$parentId = ($node->id_parent == 1) ? null : $node->id_parent;
			$treeview->appendElement($element, $parentId);
		}

		$statusSelector->addItem($treeview);

		/* Button */
		$statusSelector->setCancelAction($this->proxy()->cancel());
		$statusSelector->setSaveAction($Crm->Controller()->Home()->saveHomepageDealsConfiguration(), $Crm->translate('Save the configuration'));

		$page->addItem($statusSelector);
		$page->addContextItem($W->Frame());

		$page->displayHtml();
	}


	/**
	 * @param crm_Deal $deal
	 * @return Widget_Frame
	 */
	protected function recentDeal(crm_Deal $deal)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$Crm->includeOrganizationSet();
		$Crm->Ui()->includeOrganization();
		$Crm->Ui()->includeDeal();

		$lead = $deal->lead();
		$photo = crm_dealPhoto($deal, 30, 30, false, true);

		if ($lead) {
			$orgLink = $W->Link($W->Label($deal->lead()->name), $Crm->Controller()->Organization()->display($deal->lead()->id));
		} else {
			$orgLink = $W->Label('');
		}

		$recentDealFrame = $W->Frame()->setLayout(
			$W->HBoxItems(
				$W->VBoxItems(
					$W->Title($W->Link($deal->name, $Crm->Controller()->Deal()->display($deal->id)), 6),
					$orgLink->addClass('crm-small')
				)->setSizePolicy(Widget_SizePolicy::MAXIMUM),
				$W->VBoxItems(
					$photo,
					$W->Label($deal->getParentSet()->amount->output($deal->amount) . '' . $Crm->translate('_euro_'))
						->addClass('crm-small crm-nowrap')
				)->setHorizontalAlign('right')
			)

			->setVerticalAlign('top')
			->setHorizontalSpacing(1, 'em')
		)
//		->addClass('crm-history-element');
		->setSizePolicy('widget-list-element');

		return $recentDealFrame;
	}


	/**
	 * Returns an array of statuses selected by the current user to be displayed on her hompage.
	 *
	 * @return array
	 */
	protected function getSelectedStatuses()
	{
		$Crm = $this->Crm();
		$configurationCookieName = $this->getConfigurationCookieName();

		$currentlySelectedStatuses = array();

		if (isset($_COOKIE[$configurationCookieName])) {
			// Configuration does exist: we select all the statuses stored in the cookie.
			$configurationExist = true;
			$config = $_COOKIE[$configurationCookieName];
			$statusIds = explode(',', $config);
			foreach ($statusIds as $statusId) {
				if (is_numeric($statusId)) {
					$currentlySelectedStatuses[$statusId] = $statusId;
				}
			}
		} else {
			// Configuration does not exist: we select all the statuses.
			$statusSet = $Crm->StatusSet();
			$statuses = $statusSet->select();
			foreach ($statuses as $status) {
				$currentlySelectedStatuses[$status->id] = $status->id;
			}
		}

		return $currentlySelectedStatuses;
	}


	/**
	 * @param int $maxItems
	 *
	 * @return widget_VBoxLayout
	 */
	public function recentDeals($maxItems = null, $selectedStatus = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$Crm->Ui()->includeOrganization();
		$Crm->includeStatusSet();

		$vspacing = 1;
		$vspacingUnit = 'px';
		$frame = $W->VBoxLayout()->setVerticalSpacing(4, 'px');

		/*
		 * Display deals filtred by status selected in configuration
		 * We display deals from the level 1 of status and after the others levels
		 * If a status of level 1 is selected and childs status are not selected : we display deals who have the status and their childs status
		 * If a status of level 2 is selected : we display deals who have only this status
		 *
		 * Status titles :
		 * If a status of level 1 is selected and childs status are not selected : we display only the status to the level 1
		 * If a status of level 2 is selected : we display status to the level 1 and level 2
		 */

		/* Select deals filtred by status (status are saved to cookie) */
		if (!isset($selectedStatus)) {
			$statusCurrentlySelected = $this->getSelectedStatuses();
		} else {
			$statusCurrentlySelected = $selectedStatus;
		}


		/* List of status of level 1 */
		$statusSet = $Crm->StatusSet();
		$statusRoot = $statusSet->get(1); /* status root node */
		$statutesLevel1 = $statusRoot->getChildren();

		/* Deals display */
		foreach ($statutesLevel1 as $statusLevel1) {
			$elements = $W->VBoxItems()->setVerticalSpacing($vspacing, $vspacingUnit);
			$nbStatusElements = 0;
			$amountStatusElements = 0;

			/* List of descendants status */
			$descendantsLevel1 = $statusLevel1->getDescendants();

			/* 3 cases :
			 * 1 : status selected (in configuration) contains ID of the status of level 1
			 * 2 : status selected (in configuration) don't contains ID of the status of level 1 but other sub-levels
			 * 3 : status selected (in configuration) don't contains ID of the status
			 */
			$level1Selected = false;
			if (array_key_exists($statusLevel1->id, $statusCurrentlySelected)) {
				$level1Selected = true;
			}
			$subLevelsOfLevel1Selected = false;
			foreach ($descendantsLevel1 as $s) {
				if (array_key_exists($s->id, $statusCurrentlySelected)) {
					$subLevelsOfLevel1Selected = true;
				}
			}
			if ($level1Selected || $subLevelsOfLevel1Selected) {
				/* We display deals of this status */
				if ($subLevelsOfLevel1Selected) {
					/* Display deals of the status of level 1 */
					if ($level1Selected) {
						$elements3 = $W->VBoxItems()->setVerticalSpacing($vspacing, $vspacingUnit);
						$nbStatusElements3 = 0;
						$amountStatusElements3 = 0;

						$dealSet = $Crm->DealSet();
						$filters = $dealSet->status->is($statusLevel1->id);
						$deals = $dealSet->select($filters)->orderDesc($dealSet->modifiedOn);
						foreach ($deals as $deal) {
							$elements3->addItem($this->recentDeal($deal));

							$nbStatusElements3++;
							$amountStatusElements3 += $deal->amount;
							$nbStatusElements++;
							$amountStatusElements += $deal->amount;
						}

						/* Title of status of sub-levels of level 1 */
						if ($nbStatusElements3 > 0) {
							$statusElementsSection3 = $W->Section(
								$statusLevel1->name,
								$elements3,
								5
							)->setPersistent(true)->setFoldable(true)->addClass('compact');
							$menu2 = $statusElementsSection3->addContextMenu();
							$menu2->addItem(
								$W->Label($nbStatusElements3 . ' opp. / ' . $dealSet->amount->output($amountStatusElements3, 0) . ' ' . $Crm->translate('_euro_'))->addClass('crm-counter-label')
							);
							$elements->addItem($statusElementsSection3);
						}
					}

					/* Display deals of the status of sub-levels of level 1 */
					foreach ($descendantsLevel1 as $s) {
						if (array_key_exists($s->id, $statusCurrentlySelected)) {
							//$html .= '<h4>'.$s->name.'</h4>';
							//$elements->addItem($W->Html($s->name.'<br />'));
							$elements2 = $W->VBoxItems()->setVerticalSpacing($vspacing, $vspacingUnit);
							$nbStatusElements2 = 0;
							$amountStatusElements2 = 0;

							$dealSet = $Crm->DealSet();
							$filters = $dealSet->status->is($s->id);
							$deals = $dealSet->select($filters)->orderDesc($dealSet->modifiedOn);
							foreach ($deals as $deal) {
								$elements2->addItem($this->recentDeal($deal));

								$nbStatusElements2++;
								$amountStatusElements2 += $deal->amount;
								$nbStatusElements++;
								$amountStatusElements += $deal->amount;
							}

							/* Title of status of sub-levels of level 1 */
							if ($nbStatusElements2 > 0) {
								$statusElementsSection2 = $W->Section(
									$s->name,
									$elements2,
									5
								)->setPersistent(true)->setFoldable(true)->addClass('compact');
								$menu2 = $statusElementsSection2->addContextMenu();
								$menu2->addItem(
									$W->Link(
										$W->Label($nbStatusElements2 . ' opp. / ' . $dealSet->amount->output($amountStatusElements2, 0) . ' ' . $Crm->translate('_euro_'))->addClass('crm-counter-label'),
										$Crm->Controller()->Deal()->displayList()->url() . '&deals[filter][status]='.$s->id
									)
								);

								$elements->addItem($statusElementsSection2);
							}
						}
					}
				} else {
					/* Only level 1 is selected : we display all deals of this status of level 1 and others sub-levels */
					$IDStatusToDisplay = array();
					$IDStatusToDisplay[] = $statusLevel1->id;
					foreach ($descendantsLevel1 as $s) {
						if (array_key_exists($s->id, $statusCurrentlySelected)) {
							$IDStatusToDisplay[] = $s->id;
						}
					}

					$dealSet = $Crm->DealSet();
					$filters = $Crm->Access()->canPerformActionOnSet($dealSet, 'deal:read');
					$filters = $filters->_AND_($dealSet->status->in($IDStatusToDisplay));

					$deals = $dealSet->select($filters)->orderDesc($dealSet->modifiedOn);
					foreach ($deals as $deal) {
						$elements->addItem($this->recentDeal($deal));

						$nbStatusElements++;
						$amountStatusElements += $deal->amount;
					}
				}

				/* Title of status of level 1 */
				if ($nbStatusElements > 0) {
					$statusElementsSection = $W->Section(
						$statusLevel1->name,
						$elements,
						4
					)->setPersistent(true)->setFoldable(true)->addClass('compact');
					$menu = $statusElementsSection->addContextMenu();
					$menu->addItem(
						$W->Link(
							$W->Label($nbStatusElements . ' opp. / ' . $dealSet->amount->output($amountStatusElements, 0) . ' ' . $Crm->translate('_euro_'))->addClass('crm-counter-label'),
							$Crm->Controller()->Deal()->displayList()->url() . '&deals[filter][status]='.$statusLevel1->id
						)
					);
					$frame->addItem($statusElementsSection);
				}
			}
		}

		return $frame;
	}





	/**
	 *
	 * @see crm_HistoryElement
	 *
	 * @param string $maxItems		Maximum number of elements to display.
	 * @param string $mode			Elements display mode (crm_HistoryElement::MODE_xxx)
	 *
	 * @return Widget_VBoxLayout
	 */
	public function globalHistoryContent($maxItems = null, $mode = null)
	{
		$Crm = $this->Crm();

		$Access = $Crm->Access();

		$W = bab_Widgets();

		$historyContentFrame = $W->VBoxLayout();
		$historyContentFrame->setVerticalSpacing(4, 'px');

		$elements = array();

		if (isset($Crm->Note))
		{
			$noteSet = $Crm->NoteSet();
			$notes = $noteSet->select($noteSet->private->isNot(true)->_OR_($noteSet->createdBy->is($Access->currentUser())));
			$notes->orderDesc($notes->getSet()->modifiedOn);

			$nbElements = 0;
			foreach ($notes as $note) {

				/* @var $note crm_Note */
				if (!$Access->readNote($note)) {
					continue;
				}

				if (isset($maxItems) && $nbElements++ >= $maxItems) {
					break;
				}
				$elements[] = array('date' => $note->modifiedOn, 'data' => $note);
			}
		}

		if (isset($Crm->Email))
		{
			$emails = $Crm->EmailSet()->select();
			$emails->orderDesc($emails->getSet()->modifiedOn);

			$nbElements = 0;
			foreach ($emails as $email) {

				/* @var $email crm_Email */
				if (!$Access->readEmail($email)) {
					continue;
				}

				if (isset($maxItems) && $nbElements++ >= $maxItems) {
					break;
				}
				$elements[] = array('date' => $email->modifiedOn, 'data' => $email);
			}
		}


		if (isset($Crm->Task))
		{
			$tasks =  $Crm->TaskSet()->select();
			$tasks->setCriteria($tasks->getCriteria()->_AND_($tasks->getSet()->isCompleted()));
			$tasks->orderDesc($tasks->getSet()->completedOn);

			$nbElements = 0;
			foreach ($tasks as $task) {

				/* @var $task crm_Task */
				if (!$Access->readTask($task)) {
					continue;
				}

				if (isset($maxItems) && $nbElements++ >= $maxItems) {
					break;
				}
				$elements[] = array('date' => $task->completedOn, 'data' => $task);
			}
		}


		if (isset($Crm->Deal))
		{
			$dealSet = $Crm->DealSet();
			$deals = $dealSet->select(
				$Access->canPerformActionOnSet($dealSet, 'deal:read')
			);
			$deals->orderDesc($deals->getSet()->createdOn);

			$nbElements = 0;
			foreach ($deals as $deal) {
				if (isset($maxItems) && $nbElements++ >= $maxItems) {
					break;
				}
				$elements[] = array('date' => $deal->createdOn, 'data' => $deal);
			}
		}


		if (isset($Crm->Order))
		{
		    $oSet = $Crm->OrderSet();
		    $oSet->currency();
			$orders = $oSet->select();
			$orders->orderDesc($orders->getSet()->createdOn);

			$nbElements = 0;
			foreach ($orders as $order) {
				if (isset($maxItems) && $nbElements++ >= $maxItems) {
					break;
				}
				$elements[] = array('date' => $order->createdOn, 'data' => $order);
			}
		}


		if (isset($Crm->Comment))
		{
			$comments = $Crm->CommentSet()->select();
			$comments->orderDesc($comments->getSet()->createdOn);

			$nbElements = 0;
			foreach ($comments as $comment) {
				if (isset($maxItems) && $nbElements++ >= $maxItems) {
					break;
				}
				$elements[] = array('date' => $comment->createdOn, 'data' => $comment);
			}
		}

		$Crm->Ui()->includeBase();

		if (!isset($mode)) {
			$mode = crm_HistoryElement::MODE_SHORT;
		}

		bab_Sort::asort($elements, 'date');
		$elements = array_reverse($elements, true);

		$nbElements = 0;
		foreach ($elements as $element) {

			if (isset($maxItems) && $nbElements++ >= $maxItems) {
				break;
			}
			$element = $element['data'];

			$historyContentFrame->addItem($Crm->Ui()->historyElement($element, $mode));
		}

		return $historyContentFrame;
	}



	/**
	 * @param int $maxItems
	 *
	 * @return widget_VBoxLayout
	 */
	public function globalHistory($maxItems = null)
	{
		$Crm = $this->Crm();

		$Access = $Crm->Access();

		$W = bab_Widgets();

		$Crm->Ui()->includeNote();
		$Crm->Ui()->includeTask();
		$Crm->Ui()->includeEmail();

		// Recent general history
		$historyFrame = $W->Section(
			$Crm->translate('Recent activity'),
			$this->globalHistoryContent($maxItems),
			2
		)
		->addClass('crm-global-history');

		return $historyFrame;
	}





	public function userTasks()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$Crm->Ui()->includeTask();
		$Crm->Ui()->includeDeal();
		$taskFrame = new crm_UserTasks($Crm);
		$taskFrame->setUser($GLOBALS['BAB_SESS_USERID']);

		return $taskFrame;
	}




	/**
	 * @return Widget_Action
	 */
	public function displaySummary($maxHistory = null, $maxRecentDeals = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$Crm->Ui()->includeTask();
		$Crm->Ui()->includeDeal();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displaySummary($maxHistory, $maxRecentDeals), $Crm->translate('Home'));

		if (!isset($maxHistory)) {
			$maxHistory = 6;
		}
		if (!isset($maxRecentDeals)) {
			$maxRecentDeals = 6;
		}

		$page = $Crm->Ui()->Page();

		$mainFrame = $W->Frame();

//		$dealsFrame = $this->recentDeals($maxRecentDeals);

		$configurationDealsIcon = $W->Link(
    		$W->Icon('Configuration', Func_Icons::CATEGORIES_PREFERENCES_DESKTOP),
    		$Crm->Controller()->Home()->dealsConfiguration()
		);
		$dealsSection = $W->Section(
    		$Crm->translate('Deals'),
    		$W->VBoxItems(
        		$W->DelayedItem($this->proxy()->recentDeals($maxRecentDeals))
    		),
    		2
		)->setFoldable(false)
		->addClass('crm-summary-current-deals');
		$dealsSectionMenu = $dealsSection->addContextMenu('popup');
		$dealsSectionMenu->addItem($configurationDealsIcon);


		$historyFrame = $this->globalHistory($maxHistory);


		$mainFrame->addItem(
			$W->HBoxItems(
				$dealsSection,
				$historyFrame->setSizePolicy(Widget_SizePolicy::MAXIMUM)
			)->setId('main_panel_box')
		);

		$page->addItem($mainFrame);



		$actionsFrame = $page->ActionsFrame();
		$page->addContextItem($actionsFrame);


		$taskFrame = new crm_UserTasks($Crm);
		$taskFrame->setUser($GLOBALS['BAB_SESS_USERID']);
		$page->addContextItem($taskFrame);

		$actionsFrame->addItem(
			$W->VBoxItems(
				$W->Link($W->Icon($Crm->translate('Add a task'), Func_Icons::ACTIONS_EVENT_NEW), $Crm->Controller()->Task()->edit())
					->addClass('widget-instant-button'),
				$Crm->Ui()->TaskEditor()
					->setTitle($Crm->translate('Add a task'))
					->addClass('widget-instant-form')
			)->addClass('widget-instant-container')
		);


		return $page;
	}



	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}

}


