<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * 
 */
class crm_CtrlSearchCatalog extends crm_Controller implements crm_ShopAdminCtrl
{
	

	
	/**
	 * @return Widget_Action
	 */
	public function displayList($searchcatalog = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Virtual folders'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('Virtual folders'));
		
		$filter = isset($searchcatalog['filter']) ? $searchcatalog['filter'] : array();
		
	
		$set = $Crm->SearchCatalogSet();
		$set->parentcatalog();

		$table = $Crm->Ui()->SearchCatalogTableView();
		$table->addDefaultColumns($set);
		
		$criteria = $table->getFilterCriteria($filter);
		$table->setDataSource($set->select($criteria));
		
		
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'searchcatalog');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add an virtual folder'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function display()
	{
		// Not used
	}
	
	/**
	 * @return Widget_Action
	 */
	public function edit($searchcatalog = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($searchcatalog), $Crm->translate('Edit virtual folder'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit virtual folder'));
		
		if (null !== $searchcatalog)
		{
			$set = $Crm->SearchCatalogSet();
			$searchcatalog = $set->get($searchcatalog);
		
		}
		
		
		
		$form = $Ui->SearchCatalogEditor($searchcatalog);
		
		$page->addItem($form);
		
		if ($searchcatalog instanceof crm_SearchCatalog)
		{
			$actionsFrame = $page->ActionsFrame();
			$page->addContextItem($actionsFrame);
			
			$actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($searchcatalog->id)));
		}
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function save($searchcatalog = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->SearchCatalogSet();
		
		if (empty($searchcatalog['id']))
		{
			$record = $set->newRecord();
		} else {
			$record = $set->get($searchcatalog['id']);
			if (null == $record)
			{
				throw new crm_AccessException($Crm->translate('This virtual folder does not exists'));
			}
		}
		
		/*@var $record crm_SearchCatalog */
		
		$record->setValues($searchcatalog);
		
		$record->save();
		
		
		$W = bab_Widgets();
		$files = $W->ImagePicker()->getTemporaryFiles('photo');
		if (isset($files))
		{
			foreach($files as $file)
			{
				/* @var $file Widget_FilePickerItem */
		
				$source = $file->getFilePath();
		
				$target = $record->getPhotoFolder();
				$target->push($file->getFileName());
		
				rename($source->toString(), $target->toString());
			}
		}
		
		/**
		 * catalogs are recorded into sitemap for online shop
		 * @see crm_Event::onBeforeSiteMapCreated
		 */
		bab_siteMap::clearAll();
		
		return true;
	}
	
	
	
	public function delete($searchcatalog = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		if (!$searchcatalog)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->SearchCatalogSet();
		$set->delete($set->id->is($searchcatalog));
		
		crm_redirect($this->proxy()->displayList());
	}
}
