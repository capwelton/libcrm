<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Shopping cart administration in back office
 *
 */
class crm_CtrlShoppingCartAdm extends crm_Controller implements crm_ShopAdminCtrl
{


	/**
	 * Display the list of ongoing shopping cart for administration purpose
	 * @param array $carts
	 * @throws crm_AccessException
	 * @return Widget_Action
	 */
	public function displayList($carts = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $this->Crm()->Access();
		$Crm->includeShoppingCartSet();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($carts), $Crm->translate('Shopping carts list'));

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');


		$page->setTitle($Crm->translate('Shopping carts'));

		$page->addItem($this->listByStatus($carts, crm_ShoppingCart::getOngoingStatus()));

		return $page;
	}


	/**
	 * Display list of confirmed shopping carts (job to do for managers)
	 * @param array $carts
	 * @throws crm_AccessException
	 * @return Widget_Action
	 */
	public function displayWaitingList($carts = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $this->Crm()->Access();
		$Crm->includeShoppingCartSet();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($carts), $Crm->translate('Waiting orders'));

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');


		$page->setTitle($Crm->translate('Waiting shopping carts'));


		$page->addItem($this->listByStatus($carts, crm_ShoppingCart::getManagersStatus()));


		return $page;
	}


	/**
	 * Display list of shipped shopping carts
	 * @param array $carts
	 * @throws crm_AccessException
	 * @return Widget_Action
	 */
	public function displayHistoryList($carts = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $this->Crm()->Access();
		$Crm->includeShoppingCartSet();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($carts), $Crm->translate('Shipped orders'));

		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');


		$page->setTitle($Crm->translate('History of shipped orders'));


		$page->addItem($this->listByStatus($carts, array(crm_ShoppingCart::SHIPPED)));


		return $page;
	}




	protected function listByStatus($carts, $status)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();

		$filter = isset($carts['filter']) ? $carts['filter'] : array('sort' => 'createdOn:down');

		$this->Crm()->includeContactSet();
		$this->Crm()->includeOrderSet();
		$scSet = $this->Crm()->ShoppingCartSet();
		$scSet->join('contact');
		$scSet->join('paymentorder');
		$scSet->join('accountingdocument');

		$scSet->removeExpired();
		$scSet->removeEmpty();
		
		$sciSet = $this->Crm()->ShoppingCartItemSet();

		$table = $Crm->Ui()->ShoppingCartAdmTableView();

		$table->addColumn(widget_TableModelViewColumn($scSet->id, $Crm->translate('N.')));
		$table->addColumn(widget_TableModelViewColumn('customer', $Crm->translate('Customer')));
		$table->addColumn(widget_TableModelViewColumn('contact/email', $Crm->translate('Email'))->setSearchable(false)->setVisible(false));
		$table->addColumn(widget_TableModelViewColumn($scSet->status, $Crm->translate('Status'))->setSearchable(false));
		$table->addColumn(widget_TableModelViewColumn($scSet->createdOn, $Crm->translate('Date')));
		if (0 < count(array_intersect(crm_ShoppingCart::getOngoingStatus(), $status)))
		{
			$table->addColumn(widget_TableModelViewColumn($scSet->expireOn, $Crm->translate('Expiration date'))->setSearchable(false)->setVisible(false));
		}
		$table->addColumn(widget_TableModelViewColumn('_items_', $Crm->translate('Number of products'))->setSearchable(false)->setVisible(false));


		if (0 < count(array_intersect(crm_ShoppingCart::getStatusWithPayment(), $status)))
		{
			$table->addColumn(widget_TableModelViewColumn($scSet->paymentToken, $Crm->translate('Payment order reference'))->setVisible(false));
			$table->addColumn(widget_TableModelViewColumn($scSet->paymentorder->name, $Crm->translate('Invoice'))->setSearchable(false)->setVisible(false));
		}
		
		$table->addColumn(widget_TableModelViewColumn($scSet->accountingdocument->name, $Crm->translate('Accounting document'))->setSearchable(false)->setVisible(false));

		$cartSelection = $scSet->select($table->getFilterCriteria($filter)
				->_AND_($scSet->status->in($status))
				->_AND_($scSet->name->is(''))
		);
		$table->setDataSource($cartSelection);

		$filterPanel = $table->filterPanel($filter, 'carts');

		$table->addClass(Func_Icons::ICON_LEFT_16);

		return $filterPanel;

	}





	/**
	 * Display shopping cart admin view
	 * - change status from confirmed to archived (confirm payment reception)
	 * - delete shopping cart (confirm informations transfer to order)
	 *
	 *
	 * @param	int	$cart
	 *
	 * @return Widget_Action
	 */
	public function display($cart = null)
	{
		$Crm = $this->Crm();
		$this->Crm()->includeContactSet();
		$this->Crm()->includeOrderSet();

		$set = $this->Crm()->ShoppingCartSet();
		$set->join('contact');
		$set->join('paymentorder');
		$set->join('accountingdocument');

		$cartRecord = $set->get($cart);
		/*@var $cartRecord crm_ShoppingCart */
		$access = $this->Crm()->Access();
		$W = bab_Widgets();
		$Ui = $this->Crm()->Ui();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		if (!$cartRecord)
		{
			throw new crm_AccessException($Crm->translate('This shopping cart does not exists'));
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($cart), $cartRecord->getTitle());

		$page = $Ui->Page();


		$page->setTitle($cartRecord->getTitle());

		$mainFrame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		$mainFrame->addClass('crm-detailed-info-frame');
		$page->addItem($mainFrame);
		
		$mainFrame->addItem($W->Title(sprintf($Crm->translate('Following order created from shopping cart #%d'), $cartRecord->id), 2));
		
		
		

		
		

		$mainFrame->addItem(
			$W->FlowItems(
				$Ui->ContactCardFrame($cartRecord->contact),
				$orderFrame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.8,'em')),
				$Ui->ShoppingCartTraceFrame($cartRecord)
			)->setSpacing(0,'em', 9,'em')->setVerticalAlign('top')
		);

		$orderFrame->addItem($W->Title($set->status->output($cartRecord->status), 5));

		$orderFrame->addItem(
			$W->HBoxItems(
				$W->Label($Crm->translate('Creation date'))->colon(),
				$W->Label(bab_longDate(bab_mktime($cartRecord->createdOn)))
			)->setHorizontalSpacing(.5,'em')
		);
		
		


		if ($cartRecord->paymentorder->id) {

			$oSet = $cartRecord->paymentorder->getParentSet();

			$orderFrame->addItem(
				$W->FlowItems(
					$W->Label($oSet->type->output($cartRecord->paymentorder->type))->colon(),
					$W->Link($cartRecord->paymentorder->name, $this->Crm()->Controller()->Order()->display($cartRecord->paymentorder->id))
				)->setHorizontalSpacing(.5,'em')
			);
			
		} else if ($cartRecord->accountingdocument->id) {
			
			$oSet = $cartRecord->accountingdocument->getParentSet();
			
			$orderFrame->addItem(
					$W->FlowItems(
							$W->Label($Crm->translate('Accounting document'))->colon(),
							$W->Link($cartRecord->accountingdocument->name, $this->Crm()->Controller()->Order()->display($cartRecord->accountingdocument->id))
					)->setHorizontalSpacing(.5,'em')
			);
			
		} else {
			$orderFrame->addItem(
				$W->Label($Crm->translate('The order is not yet registered'))
			);
		}

		if (isset($Crm->PaymentError) && $errors = $Ui->PaymentErrorTableView($cartRecord))
		{
			$orderFrame->addItem($W->Section($Crm->translate('Failed payment attempts'), $errors, 5)->setFoldable(true, true));
		}
		

		$table = $Ui->ShoppingCartItemAdmTableView($cartRecord);


		$table->addClass(Func_Icons::ICON_LEFT_16);
		$mainFrame->addItem($table);

		if ($cartRecord->gift_comment)
		{
			$mainFrame->addItem(
				$W->Section($Crm->translate('Message for recipient'), $W->RichText($cartRecord->gift_comment), 5)
			);
		}


		$buttons = $W->Frame(null, $W->HBoxLayout()->setSpacing(2,'em'));
		$buttons->addClass('icon-48x48')->addClass('icon-left')->addClass('icon-left-48')->addClass('crm-shoppingcart-buttons');
		$mainFrame->addItem($buttons);


		if (crm_ShoppingCart::ARCHIVED === (int) $cartRecord->status) {
			$buttons->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Prepare the delivery'), Func_Icons::ACTIONS_DIALOG_OK),
					$this->proxy()->setDeliveryStatus($cartRecord->id)
				)->addClass('crm-dialog-button')->setConfirmationMessage($Crm->translate("A notification will be sent to the customer to inform him that his order is in preparation, do you want to continue?"))
			);
			
			$buttons->addItem(
					$W->Link(
					$W->Icon($Crm->translate('Close order'), Func_Icons::ACTIONS_DIALOG_CANCEL),
					$this->proxy()->delete($cartRecord->id)
				)->addClass('crm-dialog-button')->setConfirmationMessage($Crm->translate("The order will be deleted, only the associated invoice will last, do you want to continue?"))	
			);
		}

		if (crm_ShoppingCart::DELIVERY === (int) $cartRecord->status) {

			$editor = $Ui->ShoppingCartShippedCommentEditor($cartRecord);
			$buttons->addItem($editor);
		}

		/*
		 * Ce bouton permet la suppression du panier (shoppingCart), la commande est conservee (order)
		 * le bouton a ete desactive car les clients n'en comprennent pas l'utilisation, il faut soit supprimer l'ensemble order+shopping cart soit ne rien faire
		 * 
		if (crm_ShoppingCart::SHIPPED === (int) $cartRecord->status) {

			if (null === $cartRecord->getOrder())
			{
				switch($cartRecord->getOrder()->type)
				{
					case crm_Order::ORDER:
						$message = $Crm->translate("The order will be deleted, only the associated order form will last, do you want to continue?");
					break;
					case crm_Order::INVOICE:
						$message = $Crm->translate("The order will be deleted, only the associated invoice will last, do you want to continue?");
					break;
					default:
						$message = $Crm->translate("The order will be deleted, only the associated accounting document will last, do you want to continue?");
					break;
				}
			
			} else {
				$message = $Crm->translate("The shopping cart will be deleted, do you want to continue?");
			}
			
			$button = $W->Link(
					$W->Icon($Crm->translate('Close order'), Func_Icons::ACTIONS_DIALOG_OK),
					$this->proxy()->delete($cartRecord->id)
				)->addClass('crm-dialog-button')->setConfirmationMessage($Crm->translate("The order will be deleted, only the associated invoice will last, do you want to continue?"));

			$buttons->addItem($button);
		}
		*/


		// add note

		$mainFrame->addItem(
				$W->VboxItems(
						$W->Link($Crm->translate('Add a note'), $Crm->Controller()->Note()->edit(null, $cartRecord->getRef()))
						->addClass('widget-instant-button widget-actionbutton'),
						$Crm->Ui()->AddNoteEditor($cartRecord, 'ShoppingCart')
						->setTitle($Crm->translate('Add a note'))
						->addClass('widget-instant-form')
				)->addClass('widget-instant-container')
		);


		// Recent history
		$historyFrame = $Crm->Ui()->History($cartRecord);
		$mainFrame->addItem(
				$W->Section(
						$Crm->translate('History'),
						$historyFrame,
						2
				)->setFoldable(true)
				->addClass('compact')
		);




		return $page;
	}





	/**
	 * @param	Array 	$shoppingcart
	 */
	public function save($shoppingcart = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$access = $this->Crm()->Access();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		$set = $this->Crm()->ShoppingCartSet();

		if (!empty($shoppingcart['id'])) {
			$cartRecord = $set->get($shoppingcart['id']);
		} else {
			$cartRecord = $set->newRecord();
		}

		$cartRecord->setValues($shoppingcart);
		$cartRecord->status = crm_ShoppingCart::SHIPPED;

		$cartRecord->save();

		$set = $this->Crm()->ShoppingCartSet();
		$this->Crm()->includeOrderSet();
		$this->Crm()->includeContactSet();
		$set->join('paymentorder');
		$set->join('accountingdocument');
		$set->join('contact');

		$cart = $set->get($cartRecord->id);

		require_once dirname(__FILE__).'/mailnotif.class.php';
		$notify = new crm_NotifShipped($cart);
		$notify->send();

		crm_redirect(crm_BreadCrumbs::last());
	}




	/**
	 * Set delivery status to shopping cart
	 */
	public function setDeliveryStatus($cart = null)
	{
		$Crm = $this->Crm();
		$access = $this->Crm()->Access();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}


		if (empty($cart))
		{
			throw new crm_AccessException($Crm->translate('Missing shopping cart ID'));
		}


		$cSet = $Crm->ShoppingCartSet();
		$cartRecord = $cSet->get($cart);
		$cartRecord->status = crm_ShoppingCart::DELIVERY;
		$cartRecord->save();

		// notify

		$cSet = $Crm->ShoppingCartSet();
		$Crm->includeContactSet();
		$Crm->includeOrderSet();
		$cSet->join('contact');
		$cSet->join('paymentorder');
		$cSet->join('accountingdocument');
		$cartRecord = $cSet->get($cart);

		$notify = $Crm->Notify()->shoppingCartAdm_setDeliveryStatus($cartRecord);
		$notify->send();

		return true;
	}

	/**
	 * Set shipped status to shopping cart
	 * @param	array	$shoppingcart
	 */
	public function setShippedStatus($shoppingcart = null)
	{
		$Crm = $this->Crm();
		$access = $this->Crm()->Access();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		if (empty($shoppingcart['id']))
		{
			throw new crm_AccessException($Crm->translate('Missing shopping cart ID'));
		}


		$cSet = $Crm->ShoppingCartSet();
		$cartRecord = $cSet->get($shoppingcart['id']);

		if (null === $cartRecord)
		{
			throw new crm_AccessException($Crm->translate('This shopping cart does not exists'));
		}

		$cartRecord->status = crm_ShoppingCart::SHIPPED;
		$cartRecord->shipped_comment = $shoppingcart['shipped_comment'];
		$cartRecord->save();
		
		$cSet = $Crm->ShoppingCartSet();
		$cSet->paymentorder();
		$cSet->accountingdocument();
		$cartRecord = $cSet->get($shoppingcart['id']);
		
		// copy shippment informations to the associated order for history
		
		$Crm->includeContactSet();
		$Crm->includeOrderSet();
		
		$cSet = $Crm->ShoppingCartSet();
		$cSet->contact();
		$cSet->paymentorder();
		$cSet->accountingdocument();
		// get with joined contact and orders for order modification and notification
		$cartRecord = $cSet->get($shoppingcart['id']);
		
		$order = $cartRecord->getOrder();
		$order->delivery = $cartRecord->delivery; // update delivery method if modified before shippment
		$order->shipped_date = date('Y-m-d H:i:s');
		$order->shipped_comment = $shoppingcart['shipped_comment'];
		$order->save();
		
		
		if ($shoppingcart['noemail'])
		{
			return true;
		}
		

		// notify
		$notify = $Crm->Notify()->shoppingCartAdm_setShippedStatus($cartRecord);
		$notify->send();

		return true;
	}



	/**
	 * Close shopping cart for admin
	 *
	 * @param	int	$cart
	 */
	public function delete($cart = null)
	{
		$Crm = $this->Crm();
		$access = $this->Crm()->Access();

		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		$cSet = $this->Crm()->ShoppingCartSet();
		$cSet->delete($cSet->id->is($cart));

		crm_redirect($this->proxy()->displayList());
	}



	/**
	 * Does nothing and returns to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}
}
