<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on user contact.
 */
class crm_CtrlShopAdmin extends crm_Controller implements crm_ShopAdminCtrl
{


	/**
	 * Display user contact information
	 *
	 *
	 * @param int $contact
	 * @return Widget_Action
	 */
	public function admHome()
	{
	    bab_requireCredential();
	    
		$Crm = $this->Crm();
		$access = $Crm->Access();
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->admHome(), $Crm->translate('Online shop administration'));


		$Ui = $Crm->Ui();
		$page = $Ui->Page();

		$fullFrame = $Ui->ShopAdminHomeFrame();

		$page->additem($fullFrame);

		return $page;
	}
	
	
	/**
	 * @deprecated because there is a controller named Home()
	 * The $proxy argument is here just to prevent the warning about icompatibility with the parent method Home()
	 */
	public function home($proxy = true)
	{
	    return $this->admHome();
	}




	public function panel()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$access = $Crm->Access();
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->panel(), $Crm->translate('Panel'));


		$Ui = $Crm->Ui();
		$page = $Ui->Page();

		$addon = bab_getAddonInfosInstance('LibCrm');
		$page->additem($W->Html(bab_printOvmlTemplate($addon->getRelativePath().'panel.ovml', array('PanelId' => get_class($Crm)))));

		return $page;
	}




	/*
	 *
	 */
	public function configuration()
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		if (!$access->viewShopAdmin()) {
			return;
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->admHome(), $Crm->translate('Online shop configuration'));


		$Ui = $Crm->Ui();
		$page = $Ui->Page();

		$page->addClass('crm-page-editor');

		$form = $Ui->ShopConfigurationEditor();

		$page->addItem($form);

		return $page;
	}

	/**
	 *
	 */
	public function saveConfiguration($configuration = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		if (!$access->viewShopAdmin()) {
			throw new crm_SaveException($Crm->translate('Access denied'));
		}


		if (empty($configuration['seller_organization_name']))
		{
			throw new crm_SaveException($Crm->translate('The organization name is mandatory'));
		}


		// save configuration in registry

		$addonname = $Crm->getAddonName();

		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");

		if (isset($configuration['payment_system']))
		{
			$registry->setKeyValue('payment_system'				, $configuration['payment_system']);
		}
		
		if (isset($configuration['seller_organization_name']))
		{
			$registry->setKeyValue('seller_organization_name'	, $configuration['seller_organization_name']);
		}
		
		if (isset($configuration['customer_pdf_type']))
		{
		    $registry->setKeyValue('customer_pdf_type'	, $configuration['customer_pdf_type']);
		}
		
		if (isset($configuration['seller_organization_details']))
		{
			$registry->setKeyValue('seller_organization_details', $configuration['seller_organization_details']);
		}
		
		if (isset($configuration['seller_organization_footer']))
		{
			$registry->setKeyValue('seller_organization_footer'	, $configuration['seller_organization_footer']);
		}
		
		if (isset($configuration['terms']))
		{
			$registry->setKeyValue('terms'						, $configuration['terms']);
		}
		
		if (isset($configuration['excltaxpriority']))
		{
			$registry->setKeyValue('excltaxpriority'			, (bool) $configuration['excltaxpriority']);
		}
			
		if (isset($configuration['excltaxcart']))
		{
			$registry->setKeyValue('excltaxcart'				, (bool) $configuration['excltaxcart']);
		}
		
		if (isset($configuration['nextInvoiceNumber']))
		{
		    $registry->setKeyValue('nextInvoiceNumber'		, (int) $configuration['nextInvoiceNumber']);
		}
		
		if (isset($configuration['shipping_cost_limit']))
		{
			$registry->setKeyValue('shipping_cost_limit'		, (float) $configuration['shipping_cost_limit']);
		}
		
		if (isset($configuration['items_per_page']))
		{
			$registry->setKeyValue('items_per_page'				, (int) $configuration['items_per_page']);
		}
		
		if (isset($configuration['giftcard_disabled']))
		{
			$registry->setKeyValue('giftcard_disabled'			, (bool) $configuration['giftcard_disabled']);
		}
		
		if (isset($configuration['giftcard_description']))
		{
			$registry->setKeyValue('giftcard_description'		, $configuration['giftcard_description']);
		}
		
		if (isset($configuration['giftcard_validity']))
		{
			$registry->setKeyValue('giftcard_validity'			, (int) $configuration['giftcard_validity']);
		}
		
		if (isset($configuration['display_empty_categories']))
		{
			$registry->setKeyValue('display_empty_categories'	, (bool) $configuration['display_empty_categories']);
		}

		return true;
	}

	/**
	 * set groups for shop administration
	 * The table must be created in the addon install program
	 * in addonini.php :
	 * 		configuration_page	= "main&idx=shopadmin:setAdminGroups"
	 *
	 * @return Widget_Action
	 */
	public function setAdminGroups()
	{
		if (!bab_isUserAdministrator()) {
			return;
		}

		$Crm = $this->Crm();

		include_once $GLOBALS['babInstallPath']."admin/acl.php";

		maclGroups();

		$macl = new macl(bab_rp('tg'), bab_rp('idx'), 1, 'returnvar');
		$macl->addtable( $Crm->classPrefix.'access_groups',$Crm->translate("Who is online shop staff?"));
		$macl->filter(0,0,1,1,1);
		$macl->babecho();
	}




	/**
	 * Delete all orders and shopping carts

	public function emptyOrders()
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();

		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}

		if (!bab_isUserAdministrator()) {
			throw new crm_AccessException($Crm->translate('Access denied to non administrators'));
		}




		$oSet = $Crm->OrderSet();
		$sSet = $Crm->ShoppingCartSet();

		$res = $oSet->Select();
		foreach($res as $order)
		{
			$order->delete();
		}


		$res = $sSet->Select();
		foreach($res as $shoppingCart)
		{
			$shoppingCart->delete();
		}
	}

	*/







	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		// 0 == the cancel action
		// -1 == the edit form
		// -2 == the previous page

		$target = crm_BreadCrumbs::getPosition(-2);
		crm_redirect($target);
	}
}
