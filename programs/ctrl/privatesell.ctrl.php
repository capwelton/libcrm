<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on private sell.
 */
class crm_CtrlPrivateSell extends crm_Controller implements crm_ShopAdminCtrl
{

	/**
	 * Display the list of private sell for administration purpose
	 * @return Widget_Action
	 */
	public function displayList($privatesell = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($privatesell), $Crm->translate('Private sell'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		$page->setTitle($Crm->translate('Private sell'));
		
		
		$filter = isset($privatesell['filter']) ? $privatesell['filter'] : array();
		
		
		$set = $this->Crm()->PrivateSellSet();
				
		$allConditions = new ORM_TrueCriterion();

		$table = $Crm->Ui()->PrivateSellTableView();
		$table->addDefaultColumns($set);
		
		$criteria = $table->getFilterCriteria($filter);		
		
		$privateSellSelection = $set->select($criteria);
		
		$table->setDataSource($privateSellSelection);
		
		$table->addClass('icon-left-16 icon-left icon-16x16');
		
		$filterPanel = $table->filterPanel($filter, 'privatesell');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a private sell'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * 
	 * @param int $article
	 * 
	 * @return Widget_Action
	 */
	public function edit($privatesell = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (null === $privatesell && !$access->createArticle()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($privatesell), $Crm->translate('Edit private sell'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit private sell'));
		
		if (null !== $privatesell)
		{
			$set = $Crm->PrivateSellSet();
			$privatesell = $set->get($privatesell);
			
			/*if (!$access->updateArticle($privatesell))
			{
				throw new crm_AccessException($Crm->translate('Access denied'));
			}*/
		}
		
		
	
		$form = $Ui->PrivateSellEditor($privatesell);
		
		$page->addItem($form);
		
		$page->addContextItem($form->MainPhoto());
		
		return $page;
	}
	
	
	
	/**
	 * Save private sell
	 * @param array $privatesell
	 * 
	 * @return Widget_Action
	 */
	public function save($privatesell = null)
	{
		$privateSellRecord = $this->saveRecord($privatesell);
		
		$this->saveAttachments($privateSellRecord);
		bab_sitemap::clearAll();
		crm_redirect($this->Crm()->Controller()->PrivateSell()->Display($privateSellRecord->id));
		
		die;
	}
	
	/**
	 * 
	 * @param array $privatesell
	 * @throws crm_SaveException
	 * @throws crm_AccessException
	 * 
	 * @return crm_PrivateSell
	 */
	protected function saveRecord(Array $privatesell)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (empty($privatesell['id']) && !$access->createArticle()) {
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $this->Crm()->PrivateSellSet();
		
		if (empty($privatesell['id'])) {		
			$privateSellRecord = $set->newRecord();
		} else {
			$privateSellRecord = $set->get($privatesell['id']);
		}
		
		$privateSellRecord->setValues($privatesell);
		$privateSellRecord->save();
		
		return $privateSellRecord;
	}
	
	
	/**
	 * 
	 * @param crm_PrivateSell $privateSellRecord
	 */
	protected function saveAttachments(crm_PrivateSell $privateSellRecord)
	{
		$Crm = $this->Crm();
		// attach main photo to private sells for private sell creation
		
		$W = bab_Widgets();
		
		$res = $W->ImagePicker()->getTemporaryFiles('mainphoto');
		if (isset($res))
		{
			foreach($res as $filePickerItem)
			{
				/*@var $filePickerItem Widget_FilePickerItem */
				$privateSellRecord->importPhoto($filePickerItem->getFilePath());
			}
		}
	}

	/**
	 * Display one article for administration purpose
	 * @return Widget_Action
	 */
	public function display($privatesell = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$set = $Crm->PrivateSellSet();
		
		$privatesell = $set->get($privatesell);
		
		if (null == $privatesell)
		{
			throw new crm_AccessException($Crm->translate('Private sell not found'));
		}
		
		$access = $Crm->Access();
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($privatesell->id), $privatesell->name);
		
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->setTitle($privatesell->name);
		
		$articleDisplay = $Ui->PrivateSellDisplay($privatesell);
		
		
		$fullFrame = $articleDisplay->getFullFrame();
		$page->addItem($fullFrame);
		
		$actionsFrame = $page->ActionsFrame();
		$page->addContextItem($actionsFrame);
		
		$actions = $this->getActionFrame($privatesell);
		foreach($actions as $action){
			$actionsFrame->addItem($action);
		}
		
		return $page;
	}
	
	public function getActionFrame($privatesell)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$returnObjets = array();
		
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Edit private sell'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->PrivateSell()->edit($privatesell->id));
		
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Link article') , Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->PrivateSell()->linkArticle($privatesell->id));
			
		$returnObjets[] = $W->Link($W->Icon($Crm->translate('Delete private sell'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->PrivateSell()->delete($privatesell->id))->setConfirmationMessage($Crm->translate('Do you really want to delete the private sell?'));
		
		return $returnObjets;
	}




	public function delete($privatesell)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		$PSset = $this->Crm()->PrivateSellSet();
		$PrivateSellRecord = $PSset->get($privatesell);
		
		if (!$PrivateSellRecord)
		{
			throw new crm_Exception($Crm->translate('This private sell does not exists'));
		}
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$PSset->delete($PSset->id->is($privatesell));
		
		$ArticlePSset = $this->Crm()->ArticlePrivateSellSet();
		$ArticlePSset->delete($ArticlePSset->privatesell->is($privatesell));

		crm_redirect($this->proxy()->displayList());
	}


	/**
	 * Does nothing and returns to the previous page.
	 *
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::getPosition(-2));
	}
	
	
	
	public function linkArticle($privatesell, $articles = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$title = crm_translate('Link article to private sell');
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->linkArticle($privatesell, $articles), $title);
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		$page->setTitle($title);
		
		
		$filter = isset($articles['filter']) ? $articles['filter'] : array();
		
		
		$set = $this->Crm()->ArticleSet();
		
		if (isset($Crm->ArticleType))
		{
			$Crm->includeArticleTypeSet();
			$set->articletype();
		}
		
		if (isset($set->packaging))
		{
			$Crm->includePackagingSet();
			$set->packaging();
		}
		
		if (isset($Crm->ArticleAvailability))
		{
			$Crm->includeArticleAvailabilitySet();
			$set->articleavailability();
		}
		
		$allConditions = new ORM_TrueCriterion();
		
		$table = $Crm->Ui()->ArticleLinkTableView();
		
		
		$table->addDefaultColumns($set);
		$table->addColumn(widget_TableModelViewColumn('__privatesell__', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		
		$criteria = $table->getFilterCriteria($filter, $set);
		
		$articleSelection = $set->select($criteria);
		
		$table->setDataSource($articleSelection);
		
		$table->addClass('icon-left-16 icon-left icon-16x16');
		
		$filterPanel = $table->filterPanel($filter, 'articles');
		$page->addItem($filterPanel);
		
		$page->addContextItem(
			$W->Section(
				$title,
				$W->DelayedItem($Crm->Controller()->PrivateSell()->getLinkedArticle($privatesell))
					->setPlaceHolderItem($Crm->Controller()->PrivateSell(false)->getLinkedArticle($privatesell))
					->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE)->setSizePolicy(Widget_SizePolicy::MINIMUM)
			)->addClass(Func_Icons::ICON_LEFT_16)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);
		return $page;
		
	}
	
	/*AJAX*/
	public function getToggleLink($privatesell, $article, $id)
	{
		bab_functionality::get('Icons');
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$set = $Crm->ArticlePrivateSellSet();
		$access = $Crm->Access();
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->ArticlePrivateSellSet();
		$articleLink = $set->get(
			$set->privatesell->is($privatesell)
			->_AND_($set->article->is($article))
		);
		
		if($articleLink){
			$title = crm_translate('Remove this article from this private sell.');
			$icon = Func_Icons::ACTIONS_LIST_REMOVE;
		}else{
			$icon = Func_Icons::ACTIONS_LIST_ADD;
			$title = crm_translate('Add this article to this private sell.');
		}
		$link = $W->Link(
			$W->Icon(
				'',
				$icon
			),
			$Crm->Controller()->PrivateSell()->toggleLink($privatesell, $article),
			$id
		)->setTitle($title);
		 
		 return $link->setAjaxAction(
		 	$Crm->Controller()->PrivateSell()->toggleLink($privatesell, $article),
		 	array('linked-list-'.$article, 'linked-article-'.$article, 'linked-article')
		 );
	}
	
	public function toggleLink($privatesell, $article)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->ArticlePrivateSellSet();
		$articleLink = $set->get(
			$set->privatesell->is($privatesell)
			->_AND_($set->article->is($article))
		);
		if($articleLink){
			$articleLink->delete(
			$set->privatesell->is($privatesell)
			->_AND_($set->article->is($article))
			);
			echo '1';
		}else{
			$record = $set->newRecord();
			$record->privatesell = $privatesell;
			$record->article = $article;
			$record->save();
			echo '2';
		}
		
		die;
	}
	
	/*AJAX*/
	public function getLinkedArticle($privatesell){
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$listLinked = $W->VBoxLayout('linked-article');
		
		$Set = $this->Crm()->ArticlePrivateSellSet();
		$Set->article();
		$articles = $Set->select($Set->privatesell->is($privatesell));
		$articles->orderAsc($Set->article->name);
		
		foreach($articles as $article){
			$listLinked->addItem(
				$W->HBoxItems(
					$W->Label($article->article->reference . ' ' . $article->article->name)->setSizePolicy(Widget_SizePolicy::MAXIMUM),
					$W->DelayedItem($Crm->Controller()->PrivateSell()->getToggleLink($privatesell, $article->article->id, 'linked-article-'.$article->article->id))
						->setPlaceHolderItem($Crm->Controller()->PrivateSell(false)->getToggleLink($privatesell, $article->article->id, 'linked-article-'.$article->article->id))
						->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE)->setSizePolicy(Widget_SizePolicy::MINIMUM)
				)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
			);
		}
		
		return $listLinked;
	}

}
