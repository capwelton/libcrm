<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * This controller manages actions that can be performed on orders.
 */
class crm_CtrlOrderItem extends crm_Controller implements crm_ShopAdminCtrl
{
	
protected function orderItemTableModelView($filter)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
		$table = $Ui->OrderItemTableView();
		
		$orderItemSet = $Crm->OrderItemSet();
		$orderItemSet->parentorder();
		
		$table->addDefaultColumns($orderItemSet);
		$orderItemSelection = $orderItemSet->select($table->getFilterCriteria($filter));
		
		
		$table->setDataSource($orderItemSelection);
		
		return $table;
	}
	
	
	/**
	 * Display the list of orders items : exportable sold products
	 * @return Widget_Action
	 */
	public function displayList($orders = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), $Crm->translate('Sold products'));
		
		
		if (!$Access->listOrderItem())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}

		$page = $Ui->Page();
		$page->addClass('crm-page-list');

		$filter = isset($orders['filter']) ? $orders['filter'] : array();

		$table = $this->orderItemTableModelView($filter);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Export in CSV'), Func_Icons::ACTIONS_ARROW_DOWN, $this->proxy()->exportList($orders));
		$page->addToolbar($toolbar);
		
		$page->addItem($table->filterPanel($filter, 'orders'));



		return $page;
	}
	
	
	
	
	/**
	 * @param array	 $contacts		List parameters
	 * @param bool   $inline
	 * @param string $filename
	 *
	 * @return Widget_Action
	 */
	public function exportList($orders = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Access = $Crm->Access();
	
		if (!$Access->listOrderItem())
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
	
		$filter = isset($orders['filter']) ? $orders['filter'] : array();
	
		$tableView = $this->orderItemTableModelView($filter);
		$tableView->downloadCsv('export.csv');
	}
}