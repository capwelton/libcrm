; <?php/*

[general]
name							="LibCrm"
version							="1.0.50"
encoding						="UTF-8"
description						="Customer relation management library"
description.fr					="Librairie partagée de gestion de relation clients"
long_description.fr             ="README.md"
delete							=1
db_prefix						="crm_"
ov_version						="8.3.0"
php_version						="5.1.0"
mysql_version					="4.1.2"
addon_access_control			=0
author							="Laurent Choulette(laurent.choulette@cantico.fr)"
mysql_character_set_database	="latin1,utf8"
tags                            ="library,crm"

[addons]
LibOrm							="0.9.15"
widgets							="1.0.115"
LibFileManagement				="0.2.23"
LibCkEditor						="2.8.3"

;*/?>
