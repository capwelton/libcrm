<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */






function LibCrm_upgrade($sVersionBase, $sVersionIni)
{
	require_once dirname(__FILE__) . '/crm.php';
	Func_Crm::register();

	$Crm = bab_functionality::get('Crm');

	bab_functionality::get('LibOrm')->initMysql();
	$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
//	require_once dirname(__FILE__) . '/link.class.php';

	$sql = $mysqlbackend->setToSql($Crm->LinkSet());

//	require_once dirname(__FILE__) . '/base.class.php';
	$sql .= $mysqlbackend->setToSql($Crm->LogSet());

	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	$synchronize = new bab_synchronizeSql();

	$synchronize->fromSqlString($sql);
	
	@bab_functionality::includefile('PortletBackend');
	$functionalities = new bab_functionalities();
	
	$addonInfo = bab_getAddonInfosInstance('LibCrm');
	$addonPhpPath = $addonInfo->getPhpPath();
	
	if (class_exists('Func_PortletBackend')) {
		$functionalities->registerClass('Func_PortletBackend_Crm', $addonPhpPath . 'portletbackend.class.php');
	}
	
	$functionalities->registerClass('Func_Ovml_Container_CrmShoppingCart', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmShoppingCartItems', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmCatalog', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmArticle', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmArticleAttachments', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmIfShopManager', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmIfContactLogged', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Container_CrmIfShopPage', $addonPhpPath . 'ovml.class.php');
	$functionalities->registerClass('Func_Ovml_Function_CrmBreadCrumbs', $addonPhpPath . 'ovml.class.php');

	return true;
}


function LibCrm_onDeleteAddon()
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();
	$functionalities->unregister('Crm');
	$functionalities->unregister('PortletBackend/Crm');
	$functionalities->unregister('Ovml/Container/CrmShoppingCart');
	$functionalities->unregister('Ovml/Container/CrmShoppingCartItems');
	$functionalities->unregister('Ovml/Container/CrmCatalog');
	$functionalities->unregister('Ovml/Container/CrmArticle');
	$functionalities->unregister('Ovml/Container/CrmArticleAttachments');
	$functionalities->unregister('Ovml/Container/CrmIfShopManager');
	$functionalities->unregister('Ovml/Container/CrmIfContactLogged');
	$functionalities->unregister('Ovml/Container/CrmIfShopPage');
	$functionalities->unregister('Ovml/Function/CrmBreadCrumbs');
	return true;
}