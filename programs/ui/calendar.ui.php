<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 *
 * @param string	$recipients		A string of comma-separated email addresses.
 *
 * @param bool $iCreation		The form is displayed for creating a new event (true) or editing one (false).
 * @return Widget_Form
 */
function crm_eventEditor(Func_Crm $Crm, $isCreation)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

	$W = bab_Widgets();

	$Crm->Ui()->includeTask();

	$form = $W->Form();
	$frame = new crm_BaseForm('event_editor');
	$frame->setName('event')
			->addClass('workspace-dialog');

	$titleFormItem = crm_FormField($Crm->translate('Title:'),
											$W->LineEdit()
												->setName('title')
												->setSize(80)
												->setMandatory(true, $Crm->translate("The event title must not be empty")));
	$categorieSelect = $W->Select()
						->setName('category')
						->addOption('','')
						->setId('category_selector');
	$categorieFormItem = crm_FormField($Crm->translate('Category:'),
											$categorieSelect);
	$locationFormItem = crm_FormField($Crm->translate('Location:'),
											$W->LineEdit()
												->setName('location')
												->setSize(80));
	$startdateFormItem = crm_FormField($Crm->translate('Start date:'),
											$W->DatePicker()
												->setName('startdate')
												->setMandatory(true, $Crm->translate("The event start date must not be empty")));
	$starttimeFormItem = crm_FormField($Crm->translate('Start time:'),
											$W->TimePicker()
												->setName('starttime')
												->setMandatory(true, $Crm->translate("The event start time must not be empty")));
	$enddateFormItem = crm_FormField($Crm->translate('End date:'),
											$W->DatePicker()
												->setName('enddate'));
	$endtimeFormItem = crm_FormField($Crm->translate('End time:'),
											$W->TimePicker()
												->setName('endtime')
												->setMandatory(true, $Crm->translate("The event end time must not be empty")));
	$descriptionFormItem = crm_FormField($Crm->translate('Description:'),
											$W->SimpleHtmlEdit()
												->setName('description')
												->setLines(5)
												->setColumns(80));

	$frame->addItem(
		$topBox = $W->HBoxItems(
			$titleFormItem,
			$categorieFormItem
		)->setHorizontalSpacing(1, 'em')
	);

	$categories = bab_calGetCategories();
	foreach($categories as $categorie){
		$categorieSelect->addOption($categorie['id'],$categorie['name']);
	}
	$frame->addItem(
		$W->HBoxItems(
			$startdateFormItem,
			$starttimeFormItem,
			$endtimeFormItem,
			$enddateFormItem
		)->setHorizontalSpacing(1, 'em')
	);
	$frame->addItem($locationFormItem);
	$frame->addItem($descriptionFormItem);

	$frame->addButton(
		$W->SubmitButton('send')
			->validate(true)
			->setLabel($Crm->translate("Save event"))
			->setAction($Crm->Controller()->Calendar()->saveEvent())
	);

	$frame->addButton(
		$W->SubmitButton('cancel')
			->setLabel($Crm->translate("Cancel"))
			->setAction($Crm->Controller()->Calendar()->cancel())
	);

	if (!$isCreation) {
		  $frame->addButton(
		  		$W->SubmitButton('delete')
					->setLabel($Crm->translate("Delete"))
					->setAction($Crm->Controller()->Calendar()->deleteEvent())
		);
	}

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	$form->setHiddenValue('tg', bab_rp('tg'));
	return $form;
}




function crm_calendarView(Func_Crm $Crm, $type = 'week', $showDate = null)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

	$W = bab_Widgets();

	$I = bab_Functionality::get('Icons');

	$calendar = $W->FullCalendar('calendar');

	$calendar->setFirstDayOfWeek(1)
		->setSizePolicy(Widget_SizePolicy::MAXIMUM);

	$calendar->onViewDisplayed($Crm->Controller()->Calendar()->setDate());

	if (isset($showDate)) {
		$showDate = BAB_DateTime::fromIsoDateTime($showDate . ' 00:00:00');
		$calendar->setDate($showDate);
	}

	switch ($type) {
		case crm_CtrlCalendar::TYPE_DAY:
			$calendar->setView(Widget_FullCalendar::VIEW_DAY);
			break;
		case crm_CtrlCalendar::TYPE_WEEK:
			$calendar->setView(Widget_FullCalendar::VIEW_WEEK);
			break;
		case crm_CtrlCalendar::TYPE_WORKWEEK:
			$calendar->setView(Widget_FullCalendar::VIEW_WORKWEEK);
			break;
		case crm_CtrlCalendar::TYPE_MONTH:
		default:
			$calendar->setView(Widget_FullCalendar::VIEW_MONTH);
			break;
	}

	$calendar->fetchPeriods($Crm->Controller()->Calendar()->events());

	$calendar->onDoubleClick($Crm->Controller()->Calendar()->addEvent());

	$calendar->onPeriodMoved($Crm->Controller()->Calendar()->moveEvent());

	$calendar->onPeriodDoubleClick($Crm->Controller()->Calendar()->editEvent());


	$splitview = $W->HBoxItems(
		$W->VBoxItems(
			$calendar
		)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
	)->addClass('expand');

	$explorer = $W->Frame('calendar_explorer');
	$explorer->setLayout($W->VBoxLayout())
		->addItem($splitview);

	return $explorer;
}



function crm_calendarToolbar(Func_Crm $Crm)
{
	$W = bab_Widgets();

	$toolbar = new crm_Toolbar('main-toolbar');

	$monthViewIcon = $W->Icon($Crm->translate('Month view'), Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH);
	$monthViewButton = $W->Link($monthViewIcon, $Crm->Controller()->Calendar()->display(crm_CtrlCalendar::TYPE_MONTH));

	$dayViewIcon = $W->Icon($Crm->translate('Day view'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY);
	$dayViewButton = $W->Link($dayViewIcon, $Crm->Controller()->Calendar()->display(crm_CtrlCalendar::TYPE_DAY));

	$weekViewIcon = $W->Icon($Crm->translate('Week view'), Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK);
	$weekViewButton = $W->Link($weekViewIcon, $Crm->Controller()->Calendar()->display(crm_CtrlCalendar::TYPE_WEEK));

	$workWeekViewIcon = $W->Icon($Crm->translate('Work week view'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK);
	$workWeekViewButton = $W->Link($workWeekViewIcon, $Crm->Controller()->Calendar()->display(crm_CtrlCalendar::TYPE_WORKWEEK));

	$newEventIcon = $W->Icon($Crm->translate('New event'), Func_Icons::ACTIONS_EVENT_NEW);
	$newEventButton = $W->Link($newEventIcon, $Crm->Controller()->Calendar()->addEvent());

	$toolbar->addItem($newEventButton);
	$toolbar->addItem($monthViewButton);
	$toolbar->addItem($weekViewButton);
	$toolbar->addItem($workWeekViewButton);
	$toolbar->addItem($dayViewButton);

	return $toolbar;
}









/**
 * @return int		The current user's personal calendar id
 */
function crm_getPersonalCalendarId()
{
	$personalCalendar = bab_getIcalendars()->getPersonalCalendar();

	if (!$personalCalendar) {
		return null;
	}
	return $personalCalendar->getUrlIdentifier();
}





/**
 *
 * Enter description here ...
 * @param bab_DateTime $start
 * @param bab_DateTime $end
 * @throws Exception
 */
function crm_getCalendarEvents(bab_DateTime $start, bab_DateTime $end)
{
	include_once $GLOBALS['babInstallPath'].'utilit/cal.userperiods.class.php';


	$userPeriods = new bab_UserPeriods($start, $end);

	$factory = bab_getInstance('bab_PeriodCriteriaFactory');
	/* @var $factory bab_PeriodCriteriaFactory */

	$criteria = $factory->Collection(
		array(
			'bab_NonWorkingDaysCollection',
			'bab_VacationPeriodCollection',
			'bab_CalendarEventCollection',
			'bab_InboxEventCollection'
		)
	);

	$calids[] = crm_getPersonalCalendarId();


	$calendars = array();
	foreach($calids as $idcal) {

		$calendar = bab_getICalendars()->getEventCalendar($idcal);
		if (!$calendar) {
			throw new Exception('Calendar not found for identifier : '.$idcal);
		}
		$calendars[] = $calendar;
	}

	$criteria = $criteria->_AND_($factory->Calendar($calendars));

	$userPeriods->createPeriods($criteria);
	$userPeriods->orderBoundaries();

//	while ($p = $userPeriods->getNextPeriod()) {
//		$events[] = $p;
//	}

	return $userPeriods;
}


/**
 *
 */
function crm_getNonWorkingDays($start, $end)
{
	require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';
	require_once $GLOBALS['babInstallPath'].'utilit/mcalincl.php';

	$calendars = array();

	if ($_SESSION['crm_personal_calendar_visible'] == true) {
		$calendars[] = crm_getPersonalCalendarId();
	}

	$nwh = new bab_userWorkingHours(
		BAB_dateTime::fromIsoDateTime($start),
		BAB_dateTime::fromIsoDateTime($end)
	);

	$events = array();
	foreach ($calendars as $calendar) {
		$nwh->createPeriods(BAB_PERIOD_NWDAY);
		$nwh->orderBoundaries();
		$events += $nwh->getEventsBetween(bab_mktime($start), bab_mktime($end), BAB_PERIOD_NWDAY);
	}

	return $events;
}

/**
 * @param int	$eventId
 * @return array
 */
function crm_getEvent($eventId)
{

}


/**
 * Checks whether the current user can delete the specified calendar event.
 *
 * @param int $eventId
 * @return bool
 */
function crm_canDeleteEvent($eventId)
{
	return true;
}






/**
 *
 * @param	array		$args
 *
 *	$args['startdate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$args['enddate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$args['owner'] 		: id of the owner
 *	$args['rrule'] 		: // BAB_CAL_RECUR_DAILY, ...
 *	$args['until'] 		: array('month', 'day', 'year')
 *	$args['rdays'] 		: repeat days array(0,1,2,3,4,5,6)
 *	$args['ndays'] 		: nb days
 *	$args['nweeks'] 	: nb weeks
 *	$args['nmonths'] 	: nb weeks
 *	$args['category'] 	: id of the category
 *	$args['private'] 	: if the event is private
 *	$args['lock'] 		: to lock the event
 *	$args['free'] 		: free event
 *	$args['alert'] 		: array('day', 'hour', 'minute', 'email'=>'Y')
 *	$args['selected_calendars']
 *
 * @param	string		&$msgerror
 * @param	string		[$action_function]
 */


/**
 * @property string	$subject		The post subject
 * @property string	$message		The post content
 */
class crm_Event
{
	private $loaded;
	private $id = null;
	private $creatorId = null;
	private $title = null;
	private $description = null;
	private $location = null;
	private $startDatetime = null;
	private $endDatetime = null;
	private $calendarId = null;
	private $backgroundColor = null;
	private $category = null;
	private $allDay = false;
	private $editable = true;

	/**
	 */
	public function __construct($event = null)
	{
		if (is_array($event)) {
			$this->id = $event['id'];
			$this->creatorId = $event['creatorId'];
			$this->title = $event['title'];
			$this->description = $event['description'];
			$this->location = $event['location'];
			$this->startDatetime = $event['startDatetime'];
			$this->endDatetime = $event['endDatetime'];
			$this->calendarId = $event['calendarId'];
			$this->backgroundColor = $event['backgroundColor'];
			$this->category = $event['category'];
			$this->loaded = true;
		} else {
			$this->id = $event;
			$this->loaded = false;
		}
	}

	/**
	 *
	 * @param array $event
	 * @return crm_Event
	 */
	public static function fromBabEvent($event)
	{
		$categories = crm_getCalendarCategories();

		$evt = new crm_Event($event['id_event']);
		$evt->title = $event['title'];
		$evt->description = $event['description'];
		$evt->location = $event['location'];
		$evt->startDatetime = $event['begindate'];
		$evt->endDatetime = $event['enddate'];

		list(,$startTime) = explode(' ', $evt->startDatetime);
		list(,$endTime) = explode(' ', $evt->endDatetime);

//		$evt->allDay = ($startTime === '00:00:00' && ($endTime === '00:00:00' || $endTime === '23:59:59'));

		if (!empty($event['backgroundcolor'])) {
			$evt->color = '#' . $event['backgroundcolor'];
		} else if (isset($categories[$event['id_category']])) {
			$evt->color = '#' . $categories[$event['id_category']]['color'];
		} else {
			$evt->color = '#ddbbee';
		}
		$evt->category = $event['category'];
		$evt->editable = !empty($event['id_event']);

		return $evt;
	}


	/**
	 *
	 * @param bab_CalendarPeriod $event
	 * @return crm_Event
	 */
	public static function fromCalendarPeriod(bab_CalendarPeriod $event)
	{
		$categories = crm_getCalendarCategories();
		include_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
		$calendarObj = $event->getCollection()->getCalendar();

		if (isset($calendarObj)) {

			$calendarId = $calendarObj->getUrlIdentifier();
			$eventUid = $event->getProperty('UID');
			$evt = new crm_Event($calendarId . ':' . $eventUid);
			$evt->title = $event->getProperty('SUMMARY');
			$evt->description = $event->getProperty('DESCRIPTION');
			$evt->location = $event->getProperty('LOCATION');
			$startDatetime = $event->getProperty('DTSTART');
			$evt->startDatetime = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
			$endDatetime = $event->getProperty('DTEND');
			$evt->endDatetime = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);

			$evt->category = $event->getProperty('CATEGORIES');

			$color = $event->getProperty('X-CTO-COLOR');
			if (!empty($color)) {
				$evt->color = '#' . $color;
			} else if (isset($categories[$evt->category])) {
				$evt->color = '#' . $categories[$evt->category]['color'];
			}

			$evt->editable = !empty($eventUid);

		} else {

			$evt = new crm_Event();
			$evt->id = null;
			$evt->calendarId = null;
			$evt->creatorId = null;
			$evt->title = $event->getProperty('SUMMARY');
			$evt->description = $event->getProperty('DESCRIPTION');
			$evt->location = $event->getProperty('LOCATION');
			$startDatetime = $event->getProperty('DTSTART');
			$evt->startDatetime = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
			$endDatetime = $event->getProperty('DTEND');
			$evt->endDatetime = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);

			$evt->category = $event->getProperty('CATEGORIES');

			$evt->color = '#FFFFFF';

			$evt->loaded = true;
			$evt->editable = false;
		}

		return $evt;
	}



	/**
	 *
	 */
	public function save()
	{
		require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
		require_once $GLOBALS['babInstallPath'] . 'utilit/calapi.php';

		$start = BAB_DateTime::fromIsoDateTime($this->startDatetime);
		$end = BAB_DateTime::fromIsoDateTime($this->endDatetime);

		if (!isset($this->id)) {

			$eventArgs = array(
				'title' => $this->title,
				'description' => $this->description,
				'location' => $this->location,
				'startdate' => array(
									'year' => $start->getYear(),
									'month' => $start->getMonth(),
									'day' => $start->getDayOfMonth(),
									'hours' => $start->getHour(),
									'minutes' => $start->getMinute()),
				'enddate' => array(
									'year' => $end->getYear(),
									'month' => $end->getMonth(),
									'day' => $end->getDayOfMonth(),
									'hours' => $end->getHour(),
									'minutes' => $end->getMinute()),
				'owner' => $this->creatorId,
				'color' => $this->backgroundColor, //'DCC7F2'
				'category' => $this->category
			);


			$calendarIds = array($this->calendarId);
			$errorMessages = null;
			return bab_newEvent($calendarIds, $eventArgs, $errorMessages);
		} else {
			global $babDB;
			$sql = 'UPDATE '.BAB_CAL_EVENTS_TBL.'
					SET
						title			='.$babDB->quote($this->title).',
						description		='.$babDB->quote($this->description).',
						location		='.$babDB->quote($this->location).',
						start_date		='.$babDB->quote($start->getIsoDateTime()).',
						end_date		='.$babDB->quote($end->getIsoDateTime()).',
						date_modification=now(),
						id_modifiedby	='.$babDB->quote($GLOBALS['BAB_SESS_USERID']).',
						id_cat			='.$babDB->quote($this->category).'
					WHERE id=' . $babDB->quote($this->id);
			;
			$babDB->db_query($sql);
//			bab_updateSelectedCalendars();
		}
	}


	public function getId()
	{
		return $this->id;
	}

	public function getTitle()
	{
		$this->load();
		return $this->title;
	}


	public function getDescription()
	{
		$this->load();
		return $this->description;
	}


	public function getLocation()
	{
		$this->load();
		return $this->location;
	}


	public function getStartDatetime()
	{
		$this->load();
		return $this->startDatetime;
	}


	public function getEndDatetime()
	{
		$this->load();
		return $this->endDatetime;
	}


	public function getCreator()
	{
		$this->load();
		return $this->creatorId;
	}


	public function getCategory()
	{
		$this->load();
		return $this->category;
	}


	public function __get($propertyName)
	{
		$this->load();

		$method = 'get' . $propertyName;
		if (method_exists($this, $method)) {
			return $this->$method();
		}
	}


	public function __set($propertyName, $value)
	{
		$this->$propertyName = $value;
	}


	/**
	 * Checks whether the current user can delete this forum post.
	 *
	 * @return bool
	 */
	public function canDelete()
	{
		return true;
	}


	/**
	 * Deletes this calendar event.
	 */
	public function delete()
	{
		if (!$this->canDelete()) {
			return false;
		}
		bab_deleteEventById($this->id);
	}


	public function load()
	{
		if ($this->loaded) {
			return;
		}

		global $babDB;

		$res = $babDB->db_query('SELECT * FROM '.BAB_CAL_EVENTS_TBL.' WHERE id=' . $babDB->quote($this->id));
		if ($res && $event = $babDB->db_fetch_assoc($res)) {
			$this->creatorId = $event['creatorId'];
			$this->title = $event['title'];
			$this->description = $event['description'];
			$this->location = $event['location'];
			$this->startDatetime = $event['start_date'];
			$this->endDatetime = $event['end_date'];
			$this->category = $event['id_cat'];
//			$this->calendarId = $event['calendarId'];
			$this->backgroundColor = $event['color'];
			$this->loaded = true;
		}
		return true;
	}


	/**
	 * Returns a json encoded version of the event.
	 *
	 * @see http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
	 * @return string
	 */
	public function toJson()
	{
		$event = array(
			'id' => $this->id,
			'title' => $this->title,
			'allDay' => $this->allDay,
			'start' => str_replace(' ', 'T', $this->startDatetime),
			'end' => str_replace(' ', 'T', $this->endDatetime),
			'description' => $this->description,
			'color' => $this->color,
			'location' => $this->location,
			'category' => $this->category,
			'editable' => $this->editable
		);
		return bab_json_encode($event);
	}
}
