<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of private sell from back office
 *
 */
class crm_PrivateSellTableView extends crm_TableModelView
{
	
	public function addDefaultColumns(crm_PrivateSellSet $set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn('image', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		$this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Title')));
		$this->addColumn(widget_TableModelViewColumn($set->description, $Crm->translate('Description')));
		$this->addColumn(widget_TableModelViewColumn($set->startedOn, $Crm->translate('Start date')));
		$this->addColumn(widget_TableModelViewColumn($set->expireOn, $Crm->translate('Expiration date')));
		
	}
	
	
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_PrivateSell $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();
		
		/*@var $Ui crm_Ui */
		
		$displayAction = $Crm->Controller()->PrivateSell()->display($record->id);
		
		
		if ('image' === $fieldPath)
		{
			return $W->Link(
				$Ui->PrivateSellMainPhoto($record, 22, 22),
				$displayAction
			);
		}
		if ('name' === $fieldPath)
		{
			return $W->Link(
				$record->name,
				$displayAction
			);
		}
		return parent::computeCellContent($record, $fieldPath);
	}
	
	
	
	protected function handleRow(ORM_Record $record, $row)
	{
		if ($record->disabled)
		{
			$this->addRowClass($row, 'disabled');
		}
	
		return parent::handleRow($record, $row);
	}

}








/**
 *
 * @param crm_PrivateSell 	$privatesell
 * @param int			$width
 * @param int			$height
 * @param bool			$border
 * @return Widget_Icon
 */
function crm_privateSellMainPhoto(crm_PrivateSell $privatesell, $width, $height)
{
	$W = bab_Widgets();
	
	$path = $privatesell->getPhotoPath();
	
	if (null === $path)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
		$addon = bab_getAddonInfosInstance('LibCrm');
		$path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
	}

	$image = $W->ImageThumbnail($path)->setThumbnailSize($width, $height);
	
	$image->setTitle($privatesell->name);
	$image->addClass('crm-privatesell-image crm-element-image small');

	return $image;
}
















/**
 * @return Widget_Form
 */
class crm_PrivateSellEditor extends crm_MetaEditor
{
	/**
	 * 
	 * @var crm_PrivateSell
	 */
	protected $privatesell;
	
	public function __construct(Func_Crm $crm, crm_PrivateSell $privatesell = null, $id = null, Widget_Layout $layout = null)
	{
		$this->privatesell = $privatesell;
		
	
		parent::__construct($crm, $id, $layout);
		$this->setName('privatesell');
		$this->colon();
	
		$this->addFields();
		$this->addButtons();
	
		$this->setHiddenValue('tg', bab_rp('tg'));
	
		if (isset($privatesell)) {

			$this->setValues($this->getPrivateSellValues(), array('privatesell'));
			$this->setHiddenValue('privatesell[id]', $privatesell->id);
		}
	}
	
	
	/**
	 * @return Array
	 */
	protected function getPrivateSellValues()
	{
		return $this->privatesell->getValues();
	}
	
	
	
	protected function addFields()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();
		
		$this->addItem($this->Title());
		$this->addItem($this->description());
		$this->addItem($this->periodeDate());
		
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$id_privatesell = null !== $this->privatesell ? $this->privatesell->id : null;

		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->PrivateSell()->save())
				->setFailedAction($Crm->Controller()->PrivateSell()->edit($id_privatesell))
		);
		
		
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction($Crm->Controller()->PrivateSell()->cancel())
		);
	}

	/**
	 *
	 * @return Widget_Item
	 */
	protected function Title()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
			$Crm->translate('Title'),
			$W->LineEdit()->setSize(70)->setMandatory(true, $Crm->translate('The title is mandatory')),
			'name'
		);
	}

	/**
	 *
	 * @return Widget_Item
	 */
	protected function periodeDate()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
			$Crm->translate('Availability periode'),
			$W->PeriodPicker()
				->setNames('startedOn', 'expireOn')
				->setLabels($Crm->translate('From'), $Crm->translate('to'))
				->setMandatory(true, $Crm->translate('The periode is mandatory'))
		);
	}
	
	
	/**
	 *
	 * @return Widget_Item
	 */
	protected function description()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Description'),
				$W->TextEdit()->setColumns(80),
				__FUNCTION__
		);
	}
	
	

	/**
	 * main product photo
	 * @return Widget_ImagePicker
	 */
	public function MainPhoto()
	{
		/* @var $W Func_Widgets */
		$W = $this->widgets;

		$addon = bab_getAddonInfosInstance('LibCrm');

		$imagePicker = $W->ImagePicker()
			->setName('mainphoto')
			->oneFileMode()
			->setDefaultImage(new bab_Path($addon->getStylePath() . 'images/article-default.png'))
			->setTitle($this->Crm()->translate('Set the main photo'));
		
		/*@var $imagePicker Widget_ImagePicker */
		
		if (isset($this->privatesell))
		{
			$imagePicker->setFolder($this->privatesell->getPhotoUploadPath());
		}

		return $imagePicker;
	}
}




/**
 * Display private sell
 *
 */
class crm_PrivateSellDisplay extends crm_Object {


	/**
	 * @var crm_Article
	 */
	protected $privatesell = null;

	/**
	 *
	 * @param crm_Article $privatesell
	 *
	 */
	public function __construct(Func_Crm $Crm, crm_PrivateSell $privatesell)
	{
		parent::__construct($Crm);
		$this->privatesell = $privatesell;
	}
	


	/**
	 * Get privatesell full frame visible by privatesell manager
	 *
	 * @return Widget_Item
	 */
	public function getFullFrame()
	{
		$W = bab_Widgets();
		
		$frame = $W->Frame();
		
		$frame->addItem($W->Title($this->privatesell->name,2));
		$HBoxLayout = $W->HBoxItems($this->Photo(300, 300, 500), $this->getTopFrame())->setHorizontalSpacing(1,'em');
		$frame->addItem($HBoxLayout);
		
		$frame->addClass('crm-detailed-info-frame');
		
		return $frame;
	}


	/**
	 * Main photo
	 *
	 * @param int			$width
	 * @param int			$height
	 * @param int			$zoom
	 * @return Widget_Image
	 */
	public function Photo($width, $height, $zoom = null)
	{
		$W = bab_Widgets();
		$path = $this->getMainPhotoPath();
		
		if (null === $path)
		{
			$addon = bab_getAddonInfosInstance('LibCrm');
			require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
			$path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
		}
		
		if (null === $zoom)
		{
			return $W->ImageThumbnail($path)->setThumbnailSize($width, $height);
		}
		
		return $W->ImageZoomerThumbnail($path)->setThumbnailSize($width, $height)->setZoomSize($zoom, $zoom);

	}


	/**
	 * Get original main photo path
	 * @return bab_Path
	 */
	protected function getMainPhotoPath()
	{
		return $this->privatesell->getPhotoPath();
	}
	
	/**
	 * Article frame without the main photo, contain information displayed on the right of the main photo
	 * @return Widget_Frame
	 */
	protected function getTopFrame()
	{
		$W = bab_Widgets();
		
		$top = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'));
		
		$top->addItem($this->periode());
		$top->addItem($this->description());
		
		return $top;
	}
	
	
	
	protected function periode()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$set = $this->privatesell->getParentSet();
		
		return $W->FlowItems(
			$W->Label($Crm->translate('Available'))->colon(),
			$W->Label($Crm->translate('From')),
			$W->Label($set->startedOn->output($this->privatesell->startedOn)),
			$W->Label($Crm->translate('to')),
			$W->Label($set->expireOn->output($this->privatesell->expireOn))
		)->setSpacing(0,'em',.5,'em');
	}


	/**
	 * Display privatesell action
	 * @return Widget_Action
	 */
	protected function getDisplayAction() {

		return $this->Crm()->Controller()->Article()->display($this->privatesell->id);
	}

	/**
	 * Get a link to the detail view of privatesell
	 * @return unknown_type
	 */
	protected function getDisplayLink()
	{
		$W = bab_Widgets();
		return $W->Link($W->Icon($this->Crm()->translate('View product sheet'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $this->getDisplayAction());
	}






	/**
	 * Get similar privatesells as widget
	 * @return Widget_Item
	 */
	public function getSimilar()
	{
		$W = bab_Widgets();
		$I = $this->privatesell->similarArticles();

		if (null === $I) {
			return null;
		}

		if (0 === $I->count()) {
			return null;
		}

		$frame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em')->addClass('similar-privatesells');

		$n = 0;
		foreach($I as $privatesell)
		{
			$link = $this->getSimilarLink($privatesell);
			$frame->addItem($link);

			$n++;

			if ($n > 10) {
				break;
			}
		}

		return $frame;
	}


	/**
	 * Get item with a link to display a similar privatesell
	 * @param crm_Article $privatesell
	 * @return Widget_Item
	 */
	protected function getSimilarLink(crm_Article $privatesell)
	{
		$W = bab_Widgets();
		return $W->Link($privatesell->name, $this->Crm()->Controller()->Article()->display($privatesell->id));
	}

	/**
	 * 
	 * @param int $width
	 * @param int $height
	 */
	public function PhotoUrl($width, $height)
	{
		$path = $this->getMainPhotoPath();
		
		if (null === $path)
		{
			$addon = bab_getAddonInfosInstance('LibCrm');
			require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
			$path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
		}
		
		$T = @bab_functionality::get('Thumbnailer');
		/*@var $T Func_Thumbnailer */
		
		if (false == $T)
		{
			return '';
		}
		
		$T->setSourceFile($path);
		
		return $T->getThumbnail($width, $height);
	}
	
	
	protected function description()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		if (empty($this->privatesell->description))
		{
			return $W->Label('');
		}
	
		return $W->Section(
				$Crm->translate('Description'),
				$W->RichText($this->privatesell->description)
		)->setFoldable();
	}
	
	
	/**
	 * Get privatesell card frame visible by privatesell manager
	 * 
	 * @return Widget_Item
	 */
	public function getCardFrame()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$frame = $W->Frame()->addClass('crm-privatesell-cardframe');
		
		$frame->addItem($W->Link($W->Title($this->privatesell->name,5), $Crm->Controller()->Article()->display($this->privatesell->id)));
		$frame->addItem($W->Title($this->privatesell->subtitle,6));
		$hbox = $W->HBoxItems($this->Photo(64, 64))->setHorizontalSpacing(2,'em');
		
		$vbox = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
		$hbox->addItem($vbox);
		
		
		if ($privateselltype = $this->privateselltype())
		{
			$vbox->addItem($privateselltype);
		}
		
		if ($packaging = $this->packaging())
		{
			$vbox->addItem($packaging);
		}
		
		if ($privatesellavailability = $this->privatesellavailability())
		{
			$vbox->addItem($privatesellavailability);
		}
		
		$frame->addItem($hbox);
		
		return $frame;
	}
	
	private function endDate()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$set = $this->privatesell->getParentSet();
		
		
		if ($this->privatesell->startedOn > date('Y-m-d'))
		{
			$endDate = $W->Label(sprintf($Crm->translate('Sales will begin the %s'), $set->startedOn->output($this->privatesell->startedOn)))->addClass('crm-shopping-hightlight');
		
		} else if ($this->privatesell->expireOn < date('Y-m-d'))
		{
			$endDate = $W->Label(sprintf($Crm->translate('Sales closed since the %s'), $set->expireOn->output($this->privatesell->expireOn)))->addClass('crm-shopping-hightlight');
		
		} else {
			$endDate = $W->Label($Crm->translate('Will end') . $set->expireOn->output($this->privatesell->expireOn))->addClass('crm-shopping-hightlight');
		
		}
		
		return $endDate;
	}
	
	
	/**
	 * Online shop list result Frame
	 * @return Widget_Frame
	 */
	public function getShopListFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$displayAction = $Crm->Controller()->CatalogItem()->privateSellDisplay($this->privatesell->id);
		
		$vbox = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
		
		$set = $this->privatesell->getParentSet();
		
		$photo = $W->Link($this->Photo(92, 120), $displayAction)->addClass('photo-link');
		
		
		$name = $W->Link($W->Title($this->privatesell->name, 3), $displayAction);
		$description = $W->RichText($this->privatesell->description)->setRenderingOptions(BAB_HTML_ENTITIES);
		$link = $W->Link($this->Crm()->translate('Access the sell'), $displayAction);
		
		
		$vbox->addItem($name);
		$vbox->addItem($description);
		$vbox->addItem($this->endDate());
		$vbox->addItem($link);
		
		$layout = $W->HBoxItems(
			$photo,
			$vbox->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		)->setVerticalAlign('top')->setHorizontalSpacing(2,'em');
		
		return $W->Frame(null, $layout)->addClass('crm-catalogitem-searchresult')->addClass(Func_Icons::ICON_LEFT_16);
	}
	
	
	/**
	 * Header of a private sell
	 */
	public function getShopHeaderFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$displayAction = $Crm->Controller()->CatalogItem()->privateSellDisplay($this->privatesell->id);
		
		$vbox = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
		
		$set = $this->privatesell->getParentSet();
		
		$photo = $W->Link($this->Photo(92, 120), $displayAction)->addClass('photo-link');
		$name = $W->Link($W->Title($this->privatesell->name, 3), $displayAction);
		$description = $W->RichText($this->privatesell->description)->setRenderingOptions(BAB_HTML_ENTITIES);
		
		$vbox->addItem($W->Title($Crm->translate('Private sell'), 2));
		$vbox->addItem($name);
		$vbox->addItem($description);
		$vbox->addItem($this->endDate());
		
		
		$layout = $W->HBoxItems(
				$photo,
				$vbox->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		)->setVerticalAlign('top')->setHorizontalSpacing(2,'em');
		
		return $W->Frame(null, $layout)->addClass('crm-privatesell-header');
	}
	
	

}