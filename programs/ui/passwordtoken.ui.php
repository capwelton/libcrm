<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



class crm_ForgotPasswordEditor extends crm_Editor 
{
	public function prependFields()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		$this->setName('forgotpassword');
		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->colon();
	
		$this->addItem($W->Title($Crm->translate('To obtain a new password, please enter your e-mail address and a link will be emailed to you.'), 5));
		$this->addItem($this->email());
		
		$lostPassword = $this->Crm()->Controller()->LostPassword();
	
		$this->addItem(
			$W->SubmitButton()
				->setLabel($Crm->translate('Send'))
				->setAction($lostPassword->sendToken())
				->setSuccessAction($lostPassword->success())
				->setFailedAction($lostPassword->edit())
		);
	}
	
	
	
	protected function email()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		return $this->labelledField(
				$Crm->translate('Email address'),
				$W->EmailLineEdit()->setSize(45)->setMandatory(true, $Crm->translate('The email field is mandatory')),
				__FUNCTION__
		);
	}
	
	
	
}



class crm_PasswordTokenEditor extends crm_Editor
{

	public function __construct(Func_Crm $Crm, $uuid)
	{
		parent::__construct($Crm);
	
		$this->setHiddenValue('passwordtoken[uuid]', $uuid);
	}
	
	
	public function prependFields()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$this->setName('passwordtoken');
		$this->setHiddenValue('tg', bab_rp('tg'));
	
	
		$this->colon();
	
		$this->addItem($this->password('password1', $Crm->translate('Password')));
		$this->addItem($this->password('password2', $Crm->translate('Password verification')));
		/*
		 * // TODO
		$this->addItem($W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->setAction($this->Crm()->Controller()->LostPassword()->savePasswordToken())
		);
		*/
		$this->setSaveAction($this->Crm()->Controller()->LostPassword()->savePasswordToken(), $Crm->translate('Save'));
	}
	
	
	
	/**
	 *
	 * @return Widget_Item
	 */
	protected function password($name, $label)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		return $this->labelledField(
				$label,
				$W->LineEdit()->obfuscate(true)->setMandatory(true, $Crm->translate('The two password fields are mandatory')),
				$name
		);
	}


}

