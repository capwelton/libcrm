<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * Returns a icon view containing icons representing the content of the specified folder.
 *
 * @return Widget_Frame
 */
class crm_FileFullFrame extends crm_UiObject
{
    /**
     *
     * @var crm_FileAttachment
     */
    protected $file;

    protected $folderView;

    private $uploadAction = null;

    protected $uploadAjaxAction = null;
    protected $uploadReloadItem = null;


    public $attributes = 0xFF;

    const	SHOW_UP_BUTTON =                 0x1;
    const	SHOW_NEW_FOLDER_BUTTON =	     0x2;
    const	SHOW_ADD_FILE_BUTTON =		     0x4;
    const	SHOW_DELETE_ALL_FILES_BUTTON =   0x8;

    protected $previewThumbnailWidth = 200;
    protected $previewThumbnailHeight = 200;

    protected $iconWidth = 128;

    protected $iconThumbnailWidth = 84;
    protected $iconThumbnailHeight = 42;

    protected $iconPosition = 'top';

    protected $hSpacing = 4;
    protected $hSpacingUnit = 'px';
    protected $vSpacing = 4;
    protected $vSpacingUnit = 'px';

    /**
     *
     * @param Func_Crm 				$Crm
     * @param crm_FileAttachment 	$file
     *
     */
    public function __construct(Func_Crm $Crm, crm_FileAttachment $file)
    {
        parent::__construct($Crm);

        $this->file = $file;
        $W = bab_Widgets();

        $this->setInheritedItem($W->Frame(null, $W->VBoxLayout()));


        $this->addClass('crm-file-full-frame');

        $this->setUploadAction($Crm->Controller()->File()->display($this->file->getRecord()->getRef(), $this->file->getRelativePath()->toString()));
    }



    /**
     * @return crm_FileFullFrame
     */
    public function setIconThumbnailSize($width, $height = null)
    {
        if (!isset($height)) {
            $height = $width;
        }


        $this->iconThumbnailWidth = $width;
        $this->iconThumbnailHeight = $height;

        return $this;
    }



    /**
     * @return crm_FileFullFrame
     */
    public function setPreviewThumbnailSize($width, $height = null)
    {
        if (!isset($height)) {
            $height = $width;
        }


        $this->previewThumbnailWidth = $width;
        $this->previewThumbnailHeight = $height;

        return $this;
    }



    /**
     * @return crm_FileFullFrame
     */
    public function setIconPosition($position)
    {
        $this->iconPosition = $position;

        return $this;
    }



    /**
     * @return crm_FileFullFrame
     */
    public function setIconWidth($width)
    {
        $this->iconWidth = $width;
        return $this;
    }

    /**
     * @return crm_FileFullFrame
     */
    public function setHorizontalSpacing($spacing, $unit = 'px')
    {
        $this->hSpacing = $spacing;
        $this->hSpacingUnit = $unit;
        if (isset($this->folderView)) {
            $this->folderView->setHorizontalSpacing($this->hSpacing, $this->hSpacingUnit);
        }
        return $this;
    }



    /**
     * @return crm_FileFullFrame
     */
    public function setVerticalSpacing($spacing, $unit = 'px')
    {
        $this->vSpacing = $spacing;
        $this->vSpacingUnit = $unit;
        if (isset($this->folderView)) {
            $this->folderView->setVerticalSpacing($this->vSpacing, $this->vSpacingUnit);
        }
        return $this;
    }


    /**
     * @return Widget_Frame
     */
    protected function listView()
    {
        if (isset($this->folderView)) {
            return $this->folderView;
        }
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $record = $this->file->getRecord();
        $folderFiles = $W->FilePicker()->getFolderFiles($this->file->getPath());
        $basePath = $record->uploadPath()->toString();

        $folders = array();
        $files = array();

        foreach ($folderFiles as $file) {

            /* @var $file Widget_FilePickerItem */
            $pathname = $file->getFilePath()->toString();

            if (substr($pathname, 0, strlen($basePath)) !== $basePath) {
                continue;
            }

            if ($file->isDir()) {
                $folders[strtolower($file->toString())] = $file;
            } else {
                $files[strtolower($file->toString())] = $file;
            }
        }

        if (count($folders) + count($files) === 0) {
            $this->folderView = $W->Frame()->addItem($W->Label($Crm->translate('The folder is empty'))->addClass('crm-emptyframe-placeholder'));
            return $this->folderView;
        }


        ksort($folders);
        ksort($files);

        $listView = $W->Frame()->setLayout(
            $W->FlowLayout()
                ->setHorizontalSpacing($this->hSpacing, $this->hSpacingUnit)
                ->setVerticalSpacing($this->vSpacing, $this->vSpacingUnit)
        )->addClass('crm-folder-view');

        foreach ($folders as $file) {

            $pathname = $file->getFilePath()->toString();

            $relativePathname = substr($pathname, strlen($basePath) + 1);

            $fileIcon = $this->getFileIcon($file);

            $fileIcon->setMetadata('pathname', $relativePathname);
            $fileIcon->setMetadata('moveUrl', $Crm->Controller()->File()->move($record->getRef(), '__1__', $relativePathname)->url());

            $contextMenu = $this->contextMenu($file, $relativePathname);
            $contextMenu->attachTo($fileIcon);

            $link = $W->Link($fileIcon, $Crm->Controller()->File()->open($record->getRef(), $relativePathname));

            $listView->addItem($link);
            $listView->addItem($contextMenu);
        }

        foreach ($files as $file) {

            $pathname = $file->getFilePath()->toString();

            $relativePathname = substr($pathname, strlen($basePath) + 1);

            $fileIcon = $this->getFileIcon($file);

            $fileIcon->setMetadata('pathname', $relativePathname);

            $contextMenu = $this->contextMenu($file, $relativePathname);
            $contextMenu->attachTo($fileIcon);

            $link = $W->Link($fileIcon, $Crm->Controller()->File()->open($record->getRef(), $relativePathname));

            $listView->addItem($link);
            $listView->addItem($contextMenu);
        }


        $this->folderView = $listView;

        return $listView;
    }


    public function getFilePickerButton()
    {
        $this->FilePicker();
    }

    /**
     * @return Widget_Item
     */
    public function toolbar()
    {
        $W = bab_Widgets();
        $record = $this->file->getRecord();
        $Crm = $this->Crm();
        $toolbar = new crm_Toolbar();


        if ($relativePath = $this->file->getRelativePath()) {
            $relativePath = $relativePath->toString();
        } else {
            $relativePath = null;
        }

        $for = $record->getRef();


        if (($this->attributes & self::SHOW_UP_BUTTON) && ($parent = $this->file->getParent()) && ($Crm->Access()->openFile($parent))) {

            if ($parentRelativePath = $parent->getRelativePath()) {
                $parentRelativePath = $parentRelativePath->toString();
            } else {
                $parentRelativePath = null;
            }

            $toolbar->addItem(
                $W->Link($W->Icon($Crm->translate('Parent directory'), Func_Icons::ACTIONS_GO_UP), $Crm->Controller()->File()->display($for, $parentRelativePath))
            );

        }

        if ($this->attributes & self::SHOW_NEW_FOLDER_BUTTON) {
            $newFolderForm = $W->Form()
            ->setLayout(
            $W->FlowItems(
            $W->LineEdit()->setName('folderName'),
            $W->SubmitButton()
                ->setLabel( $Crm->translate('Create'))
                ->setAction($Crm->Controller()->File()->createFolder())
            )->setHorizontalSpacing(1, 'em')
        );

            $newFolderForm->setHiddenValue('for', $record->getRef());
            if (isset($relativePath)) {
                $newFolderForm->setHiddenValue('path', $relativePath);
            }
            $newFolderForm->setHiddenValue('tg', bab_rp('tg'));
            $toolbar->addInstantForm(
                $newFolderForm,
                $Crm->translate('New folder'),
                Func_Icons::ACTIONS_FOLDER_NEW,
                $Crm->Controller()->File()->createFolder($for, $relativePath)
            );
        }


        if ($this->attributes & self::SHOW_ADD_FILE_BUTTON) {
            $toolbar->addItem($this->FilePicker());
        }

        if ($this->attributes & self::SHOW_DELETE_ALL_FILES_BUTTON) {
            $toolbar->addItem(
                $W->Link(
                    $Crm->translate('Delete all files...'),
                    $Crm->Controller()->File()->cleanFolder($for, $relativePath)
                )->setConfirmationMessage($Crm->translate("Are you sure you want to delete all the files in this folder?"))
                ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE, 'widget-actionbutton')
            );
        }


        return $toolbar;
    }


    /**
     * @return Widget_FilePicker
     */
    public function FilePicker()
    {
        $W = bab_Widgets();

        $filePicker = $W->FilePicker()
            ->oneFileMode(false)
            ->hideFiles()
            ->addClass('widget-actionbutton')
            ->setName('file')
            ->setFolder($this->file->getPath())
            ->setAssociatedDropTarget($this->listView());

        if (null !== $act = $this->getUploadAction())
        {
            $filePicker->onUploadAction($act);
        }

        if (isset($this->uploadAjaxAction)) {
            $filePicker->setAjaxAction($this->uploadAjaxAction, $this->uploadReloadItem);
        }


        return $filePicker;
    }



    public function setUploadAction($action)
    {
        $this->uploadAction = $action;
        return $this;
    }


    /**
     * @return Widget_Action
     */
    public function getUploadAction()
    {
        return $this->uploadAction;
    }


    public function setUploadAjaxAction($action, $reloadItem = null)
    {
        $this->uploadAjaxAction = $action;
        $this->uploadReloadItem = $reloadItem;
        return $this;
    }


    /**
     * default upload action
     * @return Widget_Action
     */
    public function getUploadAjaxAction()
    {
        return $this->uploadAjaxAction;
    }


    /**
     * A bar containing a breadrumb to the current folder location.
     *
     * @return Widget_Item
     */
    public function pathbar()
    {
        $W = bab_Widgets();
        $pathbar = $W->FlowLayout()
        ->setHorizontalSpacing(1, 'em');
        $pathbar->addClass('crm-locationbar');

        $path = $this->file->getRelativePath();
        $pathElements = array();
        if ($path) {
            while ($folder = $path->pop()) {
                array_unshift($pathElements, $folder);
            }
            foreach ($pathElements as $folder) {
                $pathbar->addItem($W->Link($W->Title($folder, 3), ''));
            }
        }
        return $pathbar;
    }



    /**
     *
     * @param	Widget_FilePickerItem	$file
     *
     *
     * @return Widget_Icon
     */
    protected function getFileIcon(Widget_FilePickerItem $file)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $T = @bab_functionality::get('Thumbnailer');
        $F = bab_functionality::get('FileInfos');

        $path = $file->getFilePath();
        $pathname = $path->toString();

        if ($path->isDir()) {

            $nbElements = $path->count();
            if ($nbElements === 0) {
                $nbElementsText = $Crm->translate('Empty');
            } else if ($nbElements === 1) {
                $nbElementsText = sprintf($Crm->translate('%d element'), $nbElements);
            } else {
                $nbElementsText = sprintf($Crm->translate('%d elements'), $nbElements);
            }
            $fileIcon = $W->Icon($file->toString(). "\n" . $nbElementsText, 'places-folder');
            $fileIcon->addClass('crm-droppable');
            $fileIcon->setMetadata('filetype', 'folder');

        } else {

            $fileSize = filesize($pathname);
            if ($fileSize > 1024*1024) {
                $fileSizeText = round($fileSize / (1024*1024), 1) . ' ' . $Crm->translate('MB');
            } elseif ($fileSize > 1024) {
                $fileSizeText = round($fileSize / 1024) . ' ' . $Crm->translate('KB');
            } else {
                $fileSizeText = $fileSize . ' ' . $Crm->translate('Bytes');
            }


            $fileIcon = $W->FileIcon($file->toString() . "\n" . $fileSizeText, $file->getFilePath());
            $fileIcon->setThumbnailSize($this->iconThumbnailWidth, $this->iconThumbnailHeight);
            $fileIcon->setMetadata('filetype', 'file');
        }

        $fileIcon->addClass('crm-draggable');
        $fileIcon->addClass('crm-file-icon');

        $fileIcon->setCanvasOptions(Widget_Item::Options()->width($this->iconWidth, 'px'));

        return $fileIcon;
    }


    /**
     *
     * @param	Widget_FilePickerItem	$file
     * @param	string					$relativePathname		relative path from basePath
     *
     * @return Widget_Menu
     */
    protected function contextMenu(Widget_FilePickerItem $file, $relativePathname)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $record = $this->file->getRecord();

        $contextMenu = $W->Menu()
        ->setLayout($W->FlowLayout())
        ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);

        if (strtolower(substr($relativePathname, -4)) === '.zip') {
            $uncompressLink = $W->Link(
                $Crm->translate('Uncompress here'),
                $Crm->Controller()->File()->uncompress($record->getRef(), $relativePathname)
            )->addClass('icon', Func_Icons::MIMETYPES_PACKAGE_X_GENERIC);
        }

        if ($this->canRenameFile($relativePathname) && function_exists('bab_Path_encodeQuotedPrintable')) {
            $renameLink = $W->Link(
                $Crm->translate('Rename...'),
                $Crm->Controller()->File()->edit($record->getRef(), $relativePathname)
            )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES);
        }

        if ($file->isDir()) {
            if ($this->canDeleteFile($relativePathname)) {
                $deleteLink = $W->Link(
                    $Crm->translate('Delete folder...'),
                    $Crm->Controller()->File()->delete($record->getRef(), $relativePathname)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);
                $deleteLink->setConfirmationMessage(sprintf($Crm->translate('Are you sure you want to delete the folder \'%s\'?'), $file->toString()));
            } else {
                $deleteLink = null;
            }

            $downloadLink = null;

        } else {

            if ($this->canDeleteFile($relativePathname)) {
                $deleteLink = $W->Link(
                    $Crm->translate('Delete file...'),
                    $Crm->Controller()->File()->delete($record->getRef(), $relativePathname)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);
                $deleteLink->setConfirmationMessage(sprintf($Crm->translate('Are you sure you want to delete the file \'%s\'?'), $file->toString()));
            } else {
                $deleteLink = null;
            }

            $downloadLink = $W->Link(
                $Crm->translate('Download file'),
                $Crm->Controller()->File()->open($record->getRef(), $relativePathname, 0)
            )->addClass('icon', Func_Icons::ACTIONS_ARROW_DOWN);
        }

        if (isset($renameLink)) {
            $contextMenu->addItem($renameLink);
        }
        if (isset($deleteLink)) {
            $contextMenu->addItem($deleteLink);
        }
        if (isset($downloadLink)) {
            $contextMenu->addItem($downloadLink);
        }
        if (isset($uncompressLink)) {
            $contextMenu->addItem($uncompressLink);
        }

        $preview = $this->preview($file);
        $meta = $this->getMetadata($file);

        if ($meta || $preview) {
            $frame = $W->Frame(null, $W->HBoxItems()->setHorizontalSpacing(1, 'em'));
            if ($preview) {
                $frame->addItem(
                    $W->Frame()->addItem(
                        $preview
                    )->setCanvasOptions(Widget_Item::Options()
                    ->horizontalAlign('center')
                    ->width($this->previewThumbnailWidth, 'px')
                    ->height($this->previewThumbnailHeight, 'px'))
                );
            }
            if ($meta) {
                $frame->addItem($meta);
            }
            $frame->addClass('crm-file-details');
            $contextMenu->addItem($frame);
        }

        return $contextMenu;
    }




    /**
     *
     * @param Widget_FilePickerItem $file
     * @return Widget_Image
     */
    protected function preview(Widget_FilePickerItem $file)
    {
        $W = bab_Widgets();
        $T = @bab_functionality::get('Thumbnailer');
        if (!$T) {
            return null;
        }

        $T->setSourceFile($file->getFilePath());
        $imageUrl = $T->getDelayedThumbnail($this->previewThumbnailWidth, $this->previewThumbnailHeight);

        if (!$imageUrl) {
            return null;
        }

        return $W->Image($imageUrl);
    }



    /**
     *
     * @param Widget_FilePickerItem $file
     * @return Widget_Item
     */
    protected function getMetadata(Widget_FilePickerItem $file)
    {
        $W = bab_Widgets();
        $infos = @bab_functionality::get('FileInfos');

        if (false === $infos) {
            return null;
        }

        $widgets = array();

        /* @var $infos Func_FileInfos */
        $m = $infos->getMetadata($file->getFilePath()->toString());

//		var_dump($m);

        foreach($m->getAllNs() as $ns) {

            $meta = $m->$ns;

//			var_dump('===>' . $ns);
//			var_dump($meta);
            foreach($meta->getAllMeta() as $name) {
// 			    var_dump($name, $meta->$name);
                if ('' !== $meta->$name && null !== $meta->$name) {
                    $widgets[$name] = $W->Pair($W->Label($meta->getTitle($name))->colon(), $W->Label($meta->$name));
                }
            }
        }

        if (0 === count($widgets))
        {
            return null;
        }


        $layout = $W->VBoxLayout();
        foreach($widgets as $item)
        {
            $layout->additem($item);
        }

        return $layout;
    }








    protected function addFiles($frame, bab_Path $p, bab_Path $basePath)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();


    }


    /**
     *
     */
    protected function canDeleteFile($relativePathname)
    {
        $file = $this->Crm()->FileAttachment($this->file->getRecord(), $relativePathname);

        $Access = $this->Crm()->Access();
        return $Access->deleteFile($file);
    }



    protected function canRenameFile($relativePathname)
    {
        $file = $this->Crm()->FileAttachment($this->file->getRecord(), $relativePathname);

        $Access = $this->Crm()->Access();
        return $Access->renameFile($file);
    }



    public function display(Widget_Canvas $canvas)
    {
        //		$this->addItem($this->toolbar());
        //		$this->addItem($this->pathbar());


        $size = ($this->iconThumbnailWidth > $this->iconThumbnailHeight ? $this->iconThumbnailWidth : $this->iconThumbnailHeight);

        if ($this->iconPosition == 'top') {
            $size = $this->iconThumbnailHeight;
            if ($size <= 16) {
                $this->addClass('icon-top-16 icon-top icon-16x16');
            } else if ($size <= 24) {
                $this->addClass('icon-top-24 icon-top icon-24x24');
            } else if ($size <= 32) {
                $this->addClass('icon-top-32 icon-top icon-32x32');
            } else if ($size <= 48) {
                $this->addClass('icon-top-48 icon-top icon-48x48');
            } else {
                $this->addClass('icon-top-64 icon-top icon-64x64');
            }
        } else {
            $size = $this->iconThumbnailWidth;
            if ($size <= 16) {
                $this->addClass('icon-left-16 icon-left icon-16x16');
            } else if ($size <= 24) {
                $this->addClass('icon-left-24 icon-left icon-24x24');
            } else if ($size <= 32) {
                $this->addClass('icon-left-32 icon-left icon-32x32');
            } else if ($size <= 48) {
                $this->addClass('icon-left-48 icon-left icon-48x48');
            } else {
                $this->addClass('icon-left-64 icon-left icon-64x64');
            }
        }

        $this->addItem($this->listView());

        return parent::display($canvas);
    }
}



/**
 * @param string	$for
 * @param string	$path
 * @throws Exception
 *
 * @return crm_FileFullFrame
 */
function crm_folderView(Func_Crm $Crm, $for = null, $path = null)
{
    $W = bab_Widgets();
    $Access = $Crm->Access();

    $record = $Crm->getRecordByRef($for);

    if (!$record)
    {
        throw new Exception($Crm->translate('Access denied to folder'));
    }

    $file = $Crm->FileAttachment($record, $path);


    if (!$file->getPath()->isDir()) {
        if ($Access->addFile($file)) {
            // create root folder if necessary
            $file->getPath()->createDir();
        } else {
            throw new Exception($Crm->translate('Access denied to folder and folder does not exists'));
        }
    }

    $file->getPath()->createDir();


    if (!$Access->openFile($file)) {
         throw new Exception($Crm->translate('Access denied to folder (' . $file->getPath()->toString() . '), folder is not readable'));
    }


    $folderView = $Crm->Ui()->FileFullFrame($file);

    return $folderView;
}


