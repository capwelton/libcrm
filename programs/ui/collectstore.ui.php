<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * list of collect stores from back office
 *
 */
class crm_CollectStoreTableView extends crm_TableModelView
{

    public function addDefaultColumns(crm_CollectStoreSet $set)
    {
        $Crm = $this->Crm();

        $this->addColumn(widget_TableModelViewColumn('_edit', $Crm->translate('Edit'))->addClass('widget-column-thin'));
        $this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
        $this->addColumn(widget_TableModelViewColumn($set->address->street, $Crm->translate('Street')));
        $this->addColumn(widget_TableModelViewColumn($set->address->postalCode, $Crm->translate('Postal code')));
        $this->addColumn(widget_TableModelViewColumn($set->address->city, $Crm->translate('City')));
        $this->addColumn(widget_TableModelViewColumn($set->address->country, $Crm->translate('Country')));
        $this->addColumn(widget_TableModelViewColumn($set->openinghours, $Crm->translate('Opening hours'))->setSearchable(false)->setVisible(false));
    }



    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(crm_CollectStore $record, $fieldPath)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        if ('_edit' === $fieldPath) {
            return $W->Link(
                $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $Crm->Controller()->CollectStore()->edit($record->id)
            );
        }

        if ('openinghours' === $fieldPath) {
            return $W->Label(bab_abbr($record->openinghours, BAB_ABBR_FULL_WORDS, 50));
        }

        return parent::computeCellContent($record, $fieldPath);
    }

}




class crm_CollectStoreEditor extends crm_Editor
{
    /**
     * 
     * @var crm_CollectStore
     */
    protected $collectStore;
    
    
    public function __construct(Func_Crm $Crm, crm_CollectStore $collectStore = null)
    {
        parent::__construct($Crm);
        $this->setName('collectstore');
        $this->colon();
        $this->addClass(Func_Icons::ICON_LEFT_16);
        
        $this->collectStore = $collectStore;
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', bab_rp('tg'));
        
        if (isset($collectStore)) {
            $values = $collectStore->getValues();
            $this->setValues($values, array('collectstore'));
            $this->setHiddenValue('collectstore[id]', $collectStore->id);
        }
    }
    
    
    


    protected function addFields()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();
    
        $this->addItem($this->name());
        $this->addItem($this->address());
        $this->addItem($this->openinghours());
    
    }
    
    
    protected function name()
    {
        return $this->widgets->LabelledWidget(
            crm_translate('Name'),
            $this->widgets->LineEdit()
                ->setMandatory(true, crm_translate('The name is mandatory'))
                ->addClass('widget-100pc'),
            __FUNCTION__
        );
    }
    
    protected function address()
    {
        return $this->Crm()->Ui()->MandatoryAddressEditor()
            ->showInstructions(false)
            ->setName(__FUNCTION__);
    }
    
    protected function openinghours()
    {
        return $this->widgets->LabelledWidget(
                crm_translate('Opening hours'),
                $this->widgets->TextEdit()->addClass('widget-100pc'),
                __FUNCTION__
        );
    }
    
    
    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
    
        $id_collectStore = null !== $this->collectStore ? $this->collectStore->id : null;
        $ctrl = $Crm->Controller()->CollectStore();
    
        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Save'))
            ->validate(true)
            ->setAction($ctrl->save())
            ->setFailedAction($ctrl->edit($id_collectStore))
            ->setSuccessAction($ctrl->displayList())
        );
    

    
        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Cancel'))
            ->setAction($ctrl->displayList())
        );
        
        if (isset($this->collectStore)) {
            $this->addButton(
                $W->Link(crm_translate('Delete'), $ctrl->delete($this->collectStore->id))->addClass(Func_Icons::ACTIONS_EDIT_DELETE)->addClass('icon')   
            );
        }
    }
}