<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


bab_functionality::get('Widgets')->includePhpClass('widget_TableView');




class crm_CampaignWidget {

	private $Crm = null;




	/**
	 * get a table model view widget populated with all campaigns
	 * @param	Widget_Action	$edit_action		Action used on edit button
	 * @param	Widget_Action 	$populate_action	Action used on edit poulation button
	 *
	 * @return 	crm_CampaignTableView
	 */
	public function getList(Widget_Action $edit_action, Widget_Action $populate_action)
	{
		$set = $this->Crm()->CampaignSet();
		$campaignIterator = $set->select();


		$table = new crm_CampaignTableView($this->Crm());
		$table->edit_action = $edit_action;
		$table->populate_action = $populate_action;
		$table->setDataSource($campaignIterator);


		return $table;
	}



	/**
	 * edit/create a campaign form
	 *
	 * @param	Widget_Action	$edit_action		Action used on save form button
	 * @param	Widget_Action 	$delete_action		Action used on delete button
	 * @param	int				$campaign			campaign ID
	 *
	 * @return Widget_Form
	 */
	public function getForm(Widget_Action $save_action, Widget_Action $delete_action, $campaign = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();


		$form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));


		$cSet = $this->Crm()->CampaignSet();

		$form->addItem(crm_LabelledOrmWidget($cSet->name));
		$form->addItem(crm_LabelledOrmWidget($cSet->description));
		$form->addItem(crm_LabelledOrmWidget($cSet->type));

		$form->addItem(
			$W->VBoxItems(
				$W->Label($Crm->translate('Validity period:')),
				$W->PeriodPicker()->setNames('period_begin', 'period_end')
			)
		);

		$buttons = $W->HBoxLayout();
		$buttons->setHorizontalSpacing(1, 'em');
		$form->addItem($buttons);

		$buttons->addItem($W->SubmitButton()->setLabel($Crm->translate('Save'))->setAction($save_action));

		if (null !== $campaign) {

			$set = $this->Crm()->CampaignSet();
			$record = $set->get($campaign);
			$form->setValues($record->getValues());

			$form->setHiddenValue('campaign', $campaign);

			$buttons->addItem($W->SubmitButton()->setLabel($Crm->translate('Delete'))->setAction($delete_action));
		}

		return $form;
	}





	/**
	 *
	 * @param Func_Crm $Crm
	 * @return Func_Crm
	 */
	public function Crm($Crm = null)
	{
		if (null !== $Crm) {
			$this->Crm = $Crm;
		}

		if (null !== $this->Crm) {
			return $this->Crm;
		}

		return crm_Crm();
	}
}

















class crm_IconsTableModelView extends crm_TableModelView
{

	public function display(Widget_Canvas $canvas) {

		$this->addClass('icon-left-16 icon-16x16 icon-left');

		return parent::display($canvas);
	}

}





class crm_CampaignTableView extends crm_IconsTableModelView
{

	public $edit_action = null;
	public $populate_action = null;

	public function display(Widget_Canvas $canvas) {

		$this->setVisibleColumns(
			array(
				'name' => true,
				'description' => true,
				'type' => true
			)
		);

		return parent::display($canvas);
	}




	protected function initHeaderRow(ORM_RecordSet $set)
	{
		$W = bab_functionality::get('Widgets');
		$return = parent::initHeaderRow($set);
		$this->addItem($W->Label($this->Crm()->translate('Recipients')), 0, 3);
		return $return;
	}




	protected function handleRow(ORM_Record $record, $row)
	{
		$W = bab_functionality::get('Widgets');

		$return = parent::handleRow($record, $row);
		$this->populate_action->setParameter('campaign', $record->id);

		$C = $record->getFromType();

		$this->addItem($C->getPopulateLinkWidget($this->populate_action, $record), $row);

		return $return;

	}




	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @param int			$row
	 * @param int			$col
	 * @return bool
	 */
	protected function handleCell(ORM_Record $record, $fieldPath, $row, $col)
	{
		$W = bab_functionality::get('Widgets');

		$value = self::getRecordFieldValue($record, $fieldPath);
		if ($fieldPath == 'name') {

			$this->edit_action->setParameter('campaign', $record->id);

			$cellContent = $W->Link($W->Label($value), $this->edit_action);

		} else {
			$cellContent = $W->Label($value);
		}

		$this->addItem($cellContent, $row, $col);
		return true;
	}

}


















/**
 * Generic list of recipient
 *
 */
class crm_RecipientTableView extends crm_IconsTableModelView
{

	public $edit_action = null;

	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Link
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{

		list($field) = explode('/', $fieldPath);

		$W = bab_functionality::get('Widgets');

		if (isset($record->$field)) {
			return $W->Label((string) self::getRecordFieldValue($record, $fieldPath));
		} else {

			$this->edit_action->setParameter('recipient', $record->id);
			return $W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->edit_action);
		}
	}




}