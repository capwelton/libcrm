<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * @return Widget_Form
 */
class crm_DiscountBaseEditor extends crm_MetaEditor
{

    protected $suggestArticlePackaging = null;

    /**
     *
     * @var crm_DiscountBase
     */
    protected $discount;

    public function __construct(Func_Crm $crm, crm_DiscountBase $discount = null, $id = null, Widget_Layout $layout = null)
    {
        $this->discount = $discount;


        parent::__construct($crm, $id, $layout);
        $this->colon();

        $this->addFields();
        $this->addButtons();

        $this->setHiddenValue('tg', bab_rp('tg'));

    }




    protected function addFields()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();


        $this->addItem($this->description());

        $opt = $W->RadioSet()
        ->addOption('total', crm_translate('A discount on total'))
        ->addOption('shipping', crm_translate('A discount on shipping'))
        ->setHorizontalView()
        ->setValue('total');

        $this->addItem($this->labelledField(
            $Crm->translate('Advantage type offered'),
            $opt,
            'opt'
            ));

        $this->addItem($W->FlowItems($this->value(), $this->percentage()));
        $this->addItem($this->threshold_type());
        $this->addItem($this->threshold());
        $this->addItem($this->periodeDate());

    }






    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $id_discount = null !== $this->discount ? $this->discount->id : null;

        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Save'))
            ->validate(true)
            ->setAction($Crm->Controller()->Discount()->save())
            ->setFailedAction($Crm->Controller()->Discount()->edit($id_discount))
            );



        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Cancel'))
            ->setAction($Crm->Controller()->Discount()->cancel())
            );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function description()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Description'),
            $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, $Crm->translate('The description is mandatory')),
            __FUNCTION__
            );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function value()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Discount value'),
            $W->LineEdit()->setSize(65),
            'value'
            );
    }



    protected function percentage()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
         
        return $W->Select()
        ->setOptions(array(
            0 => crm_translate($Crm->Ui()->Euro()),
            1 => crm_translate('%')
        ))
        ->setName(__FUNCTION__);
    }


    
    protected function threshold_type()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        $set = $Crm->DiscountSet();
         
        return $this->labelledField(
            $Crm->translate('Threshold type'),
            $W->RadioSet()->setHorizontalView()->setOptions($set->getThresholdTypes())->setValue('amount'),
            __FUNCTION__
        );
    }



    protected function threshold()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
         
        return $this->labelledField(
            $Crm->translate('Threshold'),
            $W->LineEdit(),
            __FUNCTION__,
            $Crm->translate('The discount will be usable if the shopping cart total exceed the threshold')
       );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function periodeDate()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Availability period'),
            $W->PeriodPicker()
            ->setNames('start_date', 'end_date')
            ->setLabels($Crm->translate('From'), $Crm->translate('to'))
            ->setMandatory(true, $Crm->translate('The period is mandatory'))
            );
    }


}




/**
 * Display discount
 *
 */
class crm_DiscountBaseDisplay extends crm_Object {


    /**
     * @var crm_DiscountBase
     */
    protected $discount = null;

    /**
     *
     * @param crm_Discount $discount
     *
     */
    public function __construct(Func_Crm $Crm, crm_Discount $discount)
    {
        parent::__construct($Crm);
        $this->discount = $discount;
    }



    /**
     * Get discount full frame visible by discount manager
     *
     * @return Widget_Item
     */
    public function getFullFrame()
    {
        $W = bab_Widgets();

        $frame = $W->Frame();

        $frame->addItem($this->getTopFrame());

        $frame->addClass('crm-detailed-info-frame');

        return $frame;
    }
    /**
     * Article frame without the main photo, contain information displayed on the right of the main photo
     * @return Widget_Frame
     */
    protected function getTopFrame()
    {
        $W = bab_Widgets();

        $top = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'));

        
        $top->addItem($this->threshold());
        $top->addItem($this->period());
        $top->addItem($this->description());

        $top->addItem($W->Title(crm_translate('The discount'), 4));
        $top->addItem($this->discount());



        return $top;
    }





    protected function threshold()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
         
        if ($this->discount->threshold > 0) {
             
            $set = $this->discount->getParentSet();
            $threshold = $set->threshold->output($this->discount->threshold);
             
            return $W->Label(sprintf(crm_translate('For shopping cart with total greater than %s %s'), $threshold, $this->discount->getThresholdUnit()));
        }
    }

    protected function period()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $set = $this->discount->getParentSet();

        return $W->FlowItems(
            $W->Label($Crm->translate('Available'))->colon(),
            $W->Label($Crm->translate('From')),
            $W->Label($set->start_date->output($this->discount->start_date)),
            $W->Label($Crm->translate('to')),
            $W->Label($set->end_date->output($this->discount->end_date))
            )->setSpacing(0,'em',.5,'em');
    }


    protected function description()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (empty($this->discount->description))
        {
            return $W->Label('');
        }

        return $W->Section(
            $Crm->translate('Description'),
            $W->RichText($this->discount->description)
            );
    }



    protected function discount()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        

        $discount = $this->discount;
        return $W->Label($discount->getDiscount());
    }



}