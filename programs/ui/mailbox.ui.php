<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of mail boxes from back office
 *
 */
class crm_MailBoxTableView extends crm_TableModelView
{

	public function addDefaultColumns(crm_MailBoxSet $set)
	{
		$Crm = $this->Crm();

		$this->addColumn(widget_TableModelViewColumn('_edit', $Crm->translate('Edit')));
		$this->addColumn(widget_TableModelViewColumn($set->user, $Crm->translate('Owner')));
		$this->addColumn(widget_TableModelViewColumn($set->host, $Crm->translate('Host name')));
		$this->addColumn(widget_TableModelViewColumn($set->type, $Crm->translate('Type')));
		$this->addColumn(widget_TableModelViewColumn($set->username, $Crm->translate('User name')));
		$this->addColumn(widget_TableModelViewColumn('_importResponses', $Crm->translate('Copy responses')));
	}



	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_MailBox $record, $fieldPath)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		if ('_edit' === $fieldPath)
		{
			return $W->Link(
				$W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
				$Crm->Controller()->MailBox()->edit($record->id)
			);
		}

		if ('user' === $fieldPath)
		{
			return $W->Label(bab_getUserName($record->user));
		}

		if ('_importResponses' === $fieldPath)
		{
			return $W->Link(
					$W->Icon('', Func_Icons::ACTIONS_MAIL_SEND),
					$Crm->Controller()->MailBox()->importResponses($record->id)
			);
		}

		return parent::computeCellContent($record, $fieldPath);
	}

}




/**
 * @return Widget_Form
 */
class crm_MailBoxEditor extends crm_MetaEditor
{
	/**
	 *
	 * @var crm_MailBox
	 */
	protected $mailbox;

	protected $userId = null;

	public $saveButton = null;

	public $checkButton = null;

	public $cancelButton = null;

	public function __construct(Func_Crm $crm, crm_MailBox $mailbox = null, $userId = null)
	{

		parent::__construct($crm);
		$this->setName('mailbox');
		$this->colon();

		$this->mailbox = $mailbox;
		$this->userId = $userId;

		$this->addFields();
		$this->addButtons();

		$this->setHiddenValue('tg', bab_rp('tg'));

		if (isset($mailbox)) {
			$values = $mailbox->getValues();
			$this->setValues($values, array('mailbox'));
			$this->setHiddenValue('mailbox[id]', $mailbox->id);
		}

		if (isset($userId)) {
			$this->setHiddenValue('mailbox[user]', $userId);
		}
	}



	protected function addFields()
	{
		$W = $this->widgets;
		$Crm = $this->Crm();

		$this->addItem($this->user());
		$this->addItem($W->FlowItems($this->host(), $this->port()));
		$this->addItem($this->type());

		$this->addItem($this->username());
		$this->addItem($this->password());

	}


	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$id_mailbox = null !== $this->mailbox ? $this->mailbox->id : null;

		$this->addButton(
			$this->saveButton = $W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->MailBox()->save())
				->setFailedAction($Crm->Controller()->MailBox()->edit($id_mailbox))
				->setSuccessAction($Crm->Controller()->MailBox()->displayList())
			);


		$this->addButton(
			$this->checkButton = $W->SubmitButton()
				->setLabel($Crm->translate('Check configuration'))
				->validate(true)
				->setAjaxAction($Crm->Controller()->MailBox()->check(), 'crm-mailbox-status')
				->setAction($Crm->Controller()->MailBox()->displayCheck())
//				->setFailedAction($Crm->Controller()->MailBox()->edit($id_mailbox))
//				->setSuccessAction($Crm->Controller()->MailBox()->edit($id_mailbox))
		);

		$this->addButton(
			$this->cancelButton = $W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction($Crm->Controller()->MailBox()->displayList())
		);
	}

	/**
	 *
	 * @return Widget_Item
	 */
	protected function host()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
			$Crm->translate('Host name'),
			$W->LineEdit()->setSize(70)->setMandatory(true, $Crm->translate('The host name is mandatory')),
			__FUNCTION__
		);
	}





	/**
	 *
	 * @return Widget_Item
	 */
	protected function user()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$userPicker = $W->UserPicker();

		if (isset($this->userId)) {
			$userPicker->setValue($this->userId);
			$userPicker->setDisplayMode();
		}

		return $this->labelledField(
				$Crm->translate('User'),
				$userPicker,
				__FUNCTION__
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function port()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Port'),
				$W->LineEdit()->setSize(3),
				__FUNCTION__
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function type()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Type'),
				$W->Select()->setOptions($Crm->MailBoxSet()->getTypes()),
				__FUNCTION__
		);
	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function username()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('User name'),
				$W->LineEdit()->setAutoComplete(false)->setSize(50)->setMandatory(true, $Crm->translate('The user name is mandatory')),
				__FUNCTION__
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function password()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Password'),
				$W->LineEdit()->setAutoComplete(false)->obfuscate()->setSize(50)->setMandatory(true, $Crm->translate('The password is mandatory')),
				__FUNCTION__
		);
	}

}




class crm_MyMailBoxEditor extends crm_MailBoxEditor
{
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		$id_mailbox = null !== $this->mailbox ? $this->mailbox->id : null;

		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->MailBox()->save())
				->setFailedAction($Crm->Controller()->MailBox()->editmymailbox())
				->setSuccessAction($Crm->Controller()->MyContact()->display())
		);



		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction($Crm->Controller()->MailBox()->displayList())
		);
	}
}




class crm_MailBoxListItem extends crm_HistoryElement
{
    protected $mId = null;

    protected $from = null;

    protected $fromContact = null;

    protected $subject = null;

    protected $uid = null;

    protected $mailbox = null;


    public function setFrom($address)
    {
        $this->from = $address;
        $Crm = $this->Crm();
        $contactSet = $Crm->ContactSet();
        $this->fromContact = $contactSet->get($contactSet->email->is($address));
        return $this;
    }


    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    public function setMailbox($mailbox)
    {
        $this->mailbox = $mailbox;
        return $this;
    }

    public function setEmail(LibImap_Mail $email)
    {
        $this->mId = $email->mId;
        $this->setElement($email);

        $this->setFrom($email->getFromAddress());
    }



	/**
	 * Add a contextual menu with several actions applicable
	 * to the email.
	 *
	 * @return crm_MailBoxListItem
	 */
	public function addActions()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$this->addAction(
			$W->Link(
				$Crm->translate('Display'),
				$Crm->Controller()->MailBox()->displayEmail($this->uid, $this->mailbox)
			)->addClass('icon', Func_Icons::ACTIONS_VIEW_LIST_DETAILS)
		);


		return $this;
	}



	/**
	 * (non-PHPdoc)
	 * @see crm_HistoryElement::getAttachmentWidget()
	 */
	protected function getAttachmentWidget(Widget_FilePickerItem $file)
	{
		return null;
	}


	public function addDefaultLinkedElements()
	{
		return $this;
	}


	public function display(Widget_Canvas $canvas)
	{
	    $Crm = $this->Crm();
	    $W = bab_Widgets();

	    if ($this->fromContact) {
	        $this->addIcon(
	            $W->Link(
	                $Crm->Ui()->ContactPhoto($this->fromContact, 32, 32, 0),
	                $Crm->Controller()->Contact()->display($this->fromContact->id)
	            )
	        );
	    } else {
	        $this->addIcon(
	            $Crm->Ui()->ContactPhoto($this->fromContact, 32, 32, 0)
	        );
	    }

	    $subtitleLayout = $W->FlowLayout();
	    $subtitleLayout->setHorizontalSpacing(1, 'ex')
    	    ->addClass(Func_Icons::ICON_LEFT_24)
    	    ->addClass('crm-small');


	    $subTitle = sprintf($Crm->translate('Email sent by %s'),  $this->from);



	    $this->addDefaultLinkedElements();


	    $this->addTitle(
	        $W->Title(
	            $W->Link(
	                $this->subject,
	                $Crm->Controller()->MailBox()->displayEmail($this->uid, $this->mailbox)
	            ),
	            4
	        )
	    );

// 	    $dateSentIso = date('Y-m-d H:i:s', $email->getTimestamp());

	    $dateSent = 'TODO';
// 	    $dateSent = $this->getMode() == self::MODE_SHORT ?
// 	    BAB_DateTimeUtil::relativePastDate($dateSentIso, true)
// 	    : bab_longDate($email->getTimestamp(), true);
	    $emailIcon = $W->Icon($subTitle . "\n" . $dateSent, Func_Icons::OBJECTS_EMAIL);
	    $subtitleLayout->addItem($emailIcon);


	    $this->addTitle($subtitleLayout);


	    switch ($this->getMode()) {

	        case self::MODE_COMPACT :
	            break;

// 	        case self::MODE_SHORT :
// 	            //				$this->addTitle($W->Label(BAB_DateTimeUtil::relativePastDate($email->createdOn, true))->addClass('crm-small'));
// 	            $this->addContent($W->Html(bab_toHtml(bab_abbr($email->getTextPlain(), BAB_ABBR_FULL_WORDS, 200))));
// 	            break;

// 	        case self::MODE_FULL :
// 	        default:
// 	            //				$this->addTitle($W->Label(bab_longDate(bab_mktime($email->createdOn), true))->addClass('crm-small'));
// 	            $recipientsBox = $W->GridLayout()->setSpacing(1, 'em');
// 	            $this->addContent($recipientsBox);
// 	            $recipientsBox->addItem($W->Label($Crm->translate('Recipients'))->colon(), 0, 0);
// 	            $recipientsTxt = implode(', ', $email->getTo());
// 	            $recipientsBox->addItem($W->Label($recipientsTxt), 0, 1);

// 	            $ccRecipients = $email->getCc();
// 	            if (!empty($ccRecipients)) {
// 	                $recipientsBox->addItem($W->Label($Crm->translate('CC'))->colon(), 1, 0);
// 	                $recipientsBox->addItem($W->Label(implode(', ', $ccRecipients)), 1, 1);
// 	            }

// 	            $this->addContent($W->Html(preg_replace('/<style.*>.*<\/style>/is','',$email->getHtmlContent())));
// 	            if ($attachments = $email->getAttachments()) {
// 	                $this->setAttachments($attachments);
// 	            }
// 	            break;
	    }

        return parent::display($canvas);
	}
}



