<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


bab_Widgets()->includePhpClass('widget_TableView');




/**
 * A crm_TeamSection is a section containing details about a team and its members.
 */
class crm_TeamSection extends crm_UiObject // extends Widget_Frame
{
	/* @var $section Widget_Section */
	protected $section;

	public function __construct(Func_Crm $crm, crm_Team $team = null, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($crm);

		$W = bab_Widgets();

		$this->section = $W->Section('', $W->VBoxLayout()->setVerticalSpacing(2, 'px'), 5);
		$this->section->setFoldable(true);
		$this->section->addClass('crm-team-section');
		$this->setInheritedItem($this->section);

		if (isset($team)) {
			$this->setTeam($team);
		}
	}


	/**
	 * @param crm_Team $team
	 */
	public function setTeam(crm_Team $team)
	{
		$Crm = $this->Crm();
		$Crm->Ui()->includeContact();
		$Crm->Ui()->includeOrganization();
		$W = bab_Widgets();

		$this->section->setHeaderText($team->name);

		$members = $team->getMembers();

		foreach ($members as $linkId => $memberLink) {

			$member = $memberLink['member'];

			$memberPosition = null;
			$memberClass = null;

			$emailUrl = null;
			if ($member instanceof crm_Contact) {
				/* @var $member crm_Contact */
				$memberHeader = $member->getFullName();
				$memberSubHeader = $member->getMainPosition();
				$memberCard = $Crm->Ui()->ContactCardFrame($member, crm_contactCardFrame::SHOW_ALL & ~(crm_contactCardFrame::SHOW_NAME | crm_contactCardFrame::SHOW_POSITION | crm_contactCardFrame::SHOW_ADDRESS));
				$displayUrl = $Crm->Controller()->Contact()->display($member->id);
				$displayUrlText = $Crm->translate('View contact page');
				$removeUrl = $Crm->Controller()->Team()->removeMember($team->id, $linkId);
				if ($email = $member->getMainEmail()) {
					$emailUrl = $Crm->Controller()->Email()->edit($email, null, null, $member->getRef() . ',' . $team->deal()->getRef());
				}
			} else {
				/* @var $member crm_Organization */
				$memberHeader = $member->getName();
				$memberSubHeader = $member->activity;
				$memberCard = $Crm->Ui()->OrganizationCardFrame($member);
				$displayUrl = $Crm->Controller()->Organization()->display($member->id);
				$displayUrlText = $Crm->translate('View organization page');
				$removeUrl = $Crm->Controller()->Team()->removeMember($team->id, $linkId);
			}

			$memberSection = $W->Section(
				$memberHeader,
				$W->VboxItems($memberCard)->setVerticalSpacing(0.5, 'em'),
				6
			)->addClass('crm-team-member')
			->setFoldable(true, true);


			$memberSectionMenu = $memberSection->addContextMenu('popup');
			$memberSectionMenu->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Remove from team...'), Func_Icons::ACTIONS_LIST_REMOVE),
					$removeUrl
				)->setConfirmationMessage(sprintf($Crm->translate('Remove %s from %s?'), $memberHeader, $team->name))
			);
			$memberSectionMenu->addItem(
				$W->Link(
					$W->Icon($displayUrlText, Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
					$displayUrl
				)
			);
			if ($emailUrl) {
				$memberSectionMenu->addItem(
					$W->Link(
						$W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
						$emailUrl
					)
				);
			}
			$memberSection
				->setSubHeaderText($memberSubHeader);

			$this->section->addItem($memberSection);
		}

	}

}



/**
 * @param crm_Deal $deal
 *
 * @return Widget_Item
 */
function crm_teamsPreview(crm_Deal $deal)
{
	$Crm = $deal->Crm();
	$W = bab_Widgets();

	$teamSet = $Crm->TeamSet();
	$teams = $teamSet->select($teamSet->deal->is($deal->id));

	$teamsBox = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

	$teamSections = array();

	foreach ($teams as $team) {

		$teamSection = new crm_TeamSection($Crm, $team);
//		$teamSection = $W->Section('test', $W->VBoxItems());

		$teamSections[] = $teamSection;

		$teamMenu = $teamSection->addContextMenu('inline');
		$teamMenu
			->addItem(
				$W->Link(
					$W->Icon('', Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
					$Crm->Controller()->Team()->display($team->id)
				)->setTitle($Crm->translate('View team page'))
			);

		$teamPopupMenu = $teamSection->addContextMenu('popup');

		$teamPopupMenu
			->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Send email to all members...'), Func_Icons::ACTIONS_MAIL_SEND),
					$Crm->Controller()->Email()->edit(implode(',', $team->getMembersEmails()), '', '', $team->getRef() . ',' . $deal->getRef())
				)
			);

		$teamPopupMenu
			->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Rename team...'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
					$Crm->Controller()->Team()->rename($team->id)
				)
			)
			->addItem(
				$W->Link(
					$W->Icon($Crm->translate('Delete...'), Func_Icons::ACTIONS_EDIT_DELETE),
					$Crm->Controller()->Team()->delete($team->id)
				)
				->setConfirmationMessage($Crm->translate('Delete this team?'))
				->setTitle($Crm->translate('Delete this team'))
			);

	}
	foreach ($teamSections as $teamSection) {
		$teamsBox->addItem($teamSection);
	}

	return $teamsBox;
}


class crm_TeamCardFrame extends crm_CardFrame
{
	public function __construct(Func_Crm $crm, crm_Team $team, $id = null)
	{
		$W = bab_Widgets();
		$cardLayout = $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
		parent::__construct($crm, $id, $cardLayout);

		$Crm = $this->Crm();


		$members  = $team->getMembers();

		$cardLayout->addItem(
			$W->VBoxItems(
				$W->Link(
					$W->Label($team->name)->addClass('crm-strong'),
					$Crm->Controller()->Team()->display($team->id)
				),
				$W->Label($Crm->translate('Members:') . ' ' . count($members))
			)
		);
	}
}



class crm_TeamContactAndOrganizationTableView extends widget_TableModelView
{
	private $deal;

	private $set;

	private $teams = array();

	public function setDataSource(array $data, $set)
	{
		$this->iterator = new ArrayIterator($data);
		$this->set = $set;
		return $this;
	}


	/**
	 *
	 * @return array
	 */
	public function getVisibleColumns()
	{
		if (empty($this->columnsDescriptions)) {
			$this->initColumns($this->set);
		}
		return $this->columns;
	}



	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @param int			$row
	 * @param int			$col
	 * @return bool			True if a cell was added
	 */
	protected function handleCell(array $record, $fieldPath, $row, $col)
	{
		$cellContent = $this->computeCellContent($record, $fieldPath);
		$this->addItem($cellContent, $row, $col);
		return true;
	}


	/**
	 * @param array	$record
	 * @param int			$row
	 * @return bool			True if a row was added
	 */
	protected function handleRow($record, $row)
	{
		$col = 0;
		foreach ($this->columns as $fieldPath => $column) {
			if ($this->handleCell($record, $fieldPath, $row, $col)) {
				$col++;
			}
		}
		return true;
	}


	/**
	 * Fills the table view with data from the data source.
	 */
	protected function init()
	{
		$iterator = $this->iterator;

		$W = bab_Functionality::get('Widgets', false);

		$set = $this->set;

		if (isset($this->sortField)) {
			$i = 0;
			foreach ($this->columns as $colPath => $col) {
				if ($this->sortField === $colPath) {
					break;
				}
				$i++;
			}
			$sortField = self::getRecordSetField($set, $this->sortField);
			if ($this->sortAscending) {
				$this->addColumnClass($i, 'widget-table-column-sorted-asc');
//				$this->iterator->orderAsc($sortField);
			} else {
				$this->addColumnClass($i, 'widget-table-column-sorted-desc');
//				$this->iterator->orderDesc($sortField);
			}
		}
		$this->initColumns($set);
		$this->initHeaderRow($set);


		$this->addSection('body', null, 'widget-table-body');
		$this->setCurrentSection('body');


		$currentPage = $this->getCurrentPage();

		if ($iterator->count() > 0) {
			$pageLength = $this->getPageLength();

			$startRow = isset($pageLength) ? $currentPage * $pageLength : 0;
			$iterator->seek($startRow);

			$row = 0;

			$currentGroupValue = null;

			while ($iterator->valid() && (!isset($pageLength) || $row < $pageLength)) {
			    $record = $iterator->current();
			    $iterator->next();
				if ($this->handleRow($record, $row)) {
					$row++;
				}
			}
		}
	}

	/**
	 * Sets the seal associated to the list of contact.
	 *
	 * @param int $deal
	 * @return crm_TeamContactTableView
	 */
	public function setDeal($deal)
	{
		$Crm = $deal->Crm();

		$this->deal = $deal;

		$teamSet = $Crm->TeamSet();

		$dealTeams = $teamSet->select($teamSet->deal->is($deal->id));
		foreach ($dealTeams as $team) {
			$this->teams[$team->id] = $team;
		}

		return $this;
	}

	protected function computeCellContent($record, $fieldPath)
	{
		$Crm = $this->deal->Crm();
		$W = bab_Widgets();

		if ($fieldPath === '__actions__') {
			$menu = $W->Menu()
						->setLayout($W->FlowLayout())
						->addClass('icon-left-16 icon-16x16 icon-left');
			$link = $W->Link(
				$W->Icon($Crm->translate('Add to new team'), Func_Icons::ACTIONS_USER_GROUP_NEW),
				$Crm->Controller()->Team()->addMemberToNewTeam($this->deal->id, $record['id']));
			$menu->addItem($link);

			foreach ($this->teams as $team) {
				$link = $W->Link(
					$W->Icon(sprintf($Crm->translate('Add to %s'), $team->name), Func_Icons::ACTIONS_ARROW_RIGHT),
					$Crm->Controller()->Team()->addMember($team->id, $record['id'])
				);
				$menu->addItem($link);
			}

			$cellContent = $menu;
		} else if ($fieldPath === 'name') {
			$cellContent = $W->Link($W->Icon($record['name']), $record['displayUrl']);
		} else {
			$cellContent = $W->Label(isset($record[$fieldPath]) ? $record[$fieldPath] : '-');
		}

		return $cellContent;

	}
}

/**
 * Creates a generic form fragment for the specified set.
 *
 * @param crm_RecordSet $set
 * @return Widget_Item
 */
function crm_teamBuildingTableModelViewFilterForm(Func_Crm $Crm, Array $classifications = array())
{
	$O = bab_functionality::get('LibOrm');
	$O->init();
	$W = bab_Widgets();

//	require_once dirname(__FILE__) . '/classification.ui.php';

//	$layout = $W->FlowLayout();
//	$layout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');


	$classificationsSelector = $Crm->Ui()->ClassificationsSelector();

	foreach ($classifications as $classificationId => $classificationName) {
		$classificationsSelector->addClassification($classificationId);
	}

	$box = $W->VBoxItems(
		crm_LabelledWidget($Crm->translate('Codifications'), $classificationsSelector->setName('classifications')),
		$W->FlowItems(
			crm_LabelledWidget($Crm->translate('Function or activity'), $Crm->Ui()->SuggestPosition()->setSize(30)->setName('position')),
			crm_LabelledWidget($Crm->translate('Name or reason'), $W->LineEdit()->setSize(30)->setName('name')),
			crm_LabelledWidget($Crm->translate('Postal code'), $W->SuggestPostalCode()->setSize(5)->setName('address/postalCode'))
		)->setSpacing(0.5, 'em'),
		$W->FlowItems(
			$W->RadioSet()
				->setHorizontalView(true)
				->setName('type')
				->addOption('organizations', $Crm->translate('Organizations'))
				->addOption('contacts', $Crm->translate('Contacts'))
		)
	)->setVerticalSpacing(1, 'em');


	return $box;
}


/**
 * Creates a specific filter panel containing the table and an auto-generated form.
 *
 * @param widget_TableModelView $table
 * @param array					$filterValues
 * @param string				$name
 * @return Widget_Filter
 */
function crm_teamBuildingTableModelViewFilterPanel(Func_Crm $Crm, widget_TableModelView $table, $filter = null, $name = null)
{
	$W = bab_Widgets();

	$filterPanel = $W->Filter();
	$filterPanel->setLayout($W->VBoxLayout());
	if (isset($name)) {
		$filterPanel->setName($name);
	}

	$table->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : 12);

	$table->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : 0);

	$table->sortParameterName = $name . '[filter][sort]';

	if (isset($filter['sort'])) {
		$table->setSortField($filter['sort']);
	} else {
		$columns = $table->getVisibleColumns();
		list($sortField) = each($columns);
		$table->setSortField($sortField);
	}


	$classifications = isset($filter['classifications']) ? $filter['classifications'] : array();
	unset($filter['classifications']);


	$layout = crm_teamBuildingTableModelViewFilterForm($Crm, $classifications);

	$form = $W->Form();
	$form->setReadOnly(true);
	$form->setName('filter');
	$form->setLayout($layout);

	$form->addItem($W->SubmitButton()->setLabel($Crm->translate('Filter')));

	if (isset($filter) && is_array($filter)) {
		$form->setValues($filter, array($name, 'filter'));
	}
	$form->setSelfPageHiddenFields();

	$filterPanel->setFilter($form);
	$filterPanel->setFiltered($table);


	return $filterPanel;
}


/**
 * @return crm_Editor
 */
class crm_TeamEditor extends crm_Editor
{

	protected function prependFields()
	{
		$W = $this->widgets;

		$Crm = $this->Crm();

		$nameFormItem =
			$this->labelledField(
				$Crm->translate('Name'),
				$W->LineEdit()->setSize(52)->setMaxSize(255)
					->addClass('widget-fullwidth')
					->setMandatory(true, $Crm->translate('The team name must not be empty.')),
				'name'
			);

		$this->addItem($nameFormItem);

	}
}



/**
 * @return crm_TeamEditor
 */
function crm_teamEditor(Func_Crm $Crm, crm_Team $team = null)
{
	$Crm->Ui()->includeTask();

	$editor = new crm_TeamEditor($Crm);

	$editor->setName('team');

	$editor->colon();


	$teamSet = $Crm->TaskSet();

	$editor->setCancelAction($Crm->Controller()->Team()->cancel());
	$editor->setSaveAction($Crm->Controller()->Team()->save());

	if (isset($team)) {
		$editor->setValues($team->getValues(), array('team'));
		$editor->setHiddenValue('team[id]', $team->id);
	}
	$editor->setHiddenValue('tg', bab_rp('tg'));

	return $editor;
}



/**
 *
 * @param crm_Team 	$team
 * @param int		$width
 * @param int		$height
 * @param bool		$border
 * @return string
 */
function crm_teamPhotoUrl(crm_Team $team, $width, $height, $border = true)
{
	/* @var $T Func_Thumbnailer */
	$T = @bab_functionality::get('Thumbnailer');

	if (!$T) {
		return null;
	} else {

//		$photoPath = $team->getPhotoPath();
//
//		if (isset($photoPath)) {
//	 		$T->setSourceFile($photoPath->toString());
//
//		} else {
			$addon = bab_getAddonInfosInstance('LibCrm');
	 		$T->setSourceFile($addon->getStylePath() . 'images/organization-default.png');
//		}

		if ($border) {
			$padWidth = min(array(2, round(min(array($width, $height)) / 24)));
			$T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
		}

		$imageUrl = $T->getThumbnail($width, $height);
	}

	return $imageUrl;
}

/**
 *
 * @param crm_Team 	$team
 * @param int		$width
 * @param int		$height
 * @param bool		$border
 * @return Widget_Image
 */
function crm_teamPhoto(crm_Team $team, $width, $height, $border = true)
{
	$W = bab_Widgets();
	$image = $W->Image();

	$url = crm_teamPhotoUrl($team, $width, $height, $border);

	if (null !== $url) {
		$image->setUrl($url);
	}
	$image->setTitle($team->name);

	return $image;
}
