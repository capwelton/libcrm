<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2017 by CANTICO ({@link http://www.cantico.fr})
 */






class crm_CriteriaEditor extends crm_Editor
{
    /**
     * @var ORM_Criteria
     */
    protected $criteria = null;

    protected $recordSet = null;


    /**
     * @param Func_Crm $Crm
     * @param ORM_Criteria $criteria
     * @param unknown $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_Crm $Crm, ORM_Criteria $criteria = null, $id = null, Widget_Layout $layout = null)
    {
        $this->criteria = $criteria;

        parent::__construct($Crm, $id, $layout);
        $this->setName('criteria');
        $this->colon();

        $this->addFields();

        $this->setHiddenValue('tg', $Crm->controllerTg);
    }


    /**
     * Returns the recordSet associated to the criteria
     *
     * @since 1.0.88
     * @return ORM_RecordSet
     */
    public function getRecordSet()
    {
        if (isset($this->recordSet)) {
            return $this->recordSet;
        }

        $parentField = $this->criteria->getField();

        if (!isset($parentField)) {
            return null;
        }

        do {
            $field = $parentField;
            $parentField = $field->getParentSet();
        } while (isset($parentField));

        return $field;
    }


    /**
     *
     * @param ORM_RecordSet $recordSet
     * @return Widget_FlowLayout
     */
    protected function recordSetCriterionEdit(ORM_RecordSet $recordSet)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $recordSetFields = $recordSet->getFields();


        $recordSetFieldSelect = $W->Select();
        $recordSetFieldSelect->setName('field');

        $defaultConditionSelect = $W->Select();
        $defaultConditionSelect->addOption('', '');
        $defaultConditionSelect->addOption('is', $Crm->translate('_CONDITION_IS_'));
        $defaultConditionSelect->addOption('isNot', $Crm->translate('_CONDITION_IS_NOT_'));
        $defaultConditionSelect->addOption('contains', $Crm->translate('_CONDITION_CONTAINS_'));
        $defaultConditionSelect->setName(array('condition', 'default'));

        $boolConditionSelect = $W->Select();
        $boolConditionSelect->addOption('', '');
        $boolConditionSelect->addOption('is', $Crm->translate('_CONDITION_IS_'));
        $boolConditionSelect->addOption('isNot', $Crm->translate('_CONDITION_IS_NOT_'));
        $boolConditionSelect->setName(array('condition', 'bool'));

        $valueEdit = $W->LineEdit();
        $valueEdit->setName('value');


        $defaultFieldPathes = array();
        $booleanFieldPathes = array();

        foreach ($recordSetFields as $recordSetField) {

            $fieldName = $recordSetField->getName();
            $fieldDescription = $recordSetField->getDescription();
            if (substr($fieldName, 0, 1) !== '_') {
                $fieldDescription = $Crm->translate($fieldDescription);
            }
            if (empty($fieldDescription)) {
                $fieldDescription = $fieldName;
            }

            $recordSetFieldSelect->addOption(
                $recordSetField->getPath(),
                $fieldDescription
            );

            if ($recordSetField instanceof ORM_BoolInterface) {
                $booleanFieldPathes[] = $recordSetField->getPath();
            } else {
                $defaultFieldPathes[] = $recordSetField->getPath();
            }
        }

        $recordSetFieldSelect->setAssociatedDisplayable($boolConditionSelect, $booleanFieldPathes);
        $recordSetFieldSelect->setAssociatedDisplayable($defaultConditionSelect, $defaultFieldPathes);

        return $W->FlowItems(
            $recordSetFieldSelect,
            $W->FlowItems($defaultConditionSelect, $boolConditionSelect),
            $valueEdit
        )->setHorizontalSpacing(1, 'em');
    }


    /**
     *
     */
    protected function addFields()
    {
        $recordSet = $this->getRecordSet();

        $item = $this->recordSetCriterionEdit($recordSet);

        $this->addItem($item);
    }
}
