<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/../campaign.ui.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

/**
 * Generic list of recipient
 * 
 */ 
class crm_MailingRecipientTableView extends crm_RecipientTableView 
{
	
	
	
	public function display(Widget_Canvas $canvas) {

		$this->setVisibleColumns(
			array(
				
				'contact/organization/name'	=> $this->Crm()->translate('Organization'),
				'contact/firstname' 		=> true,
				'contact/lastname' 			=> true,
				'sentdate' 					=> true,
				'edit'						=> $this->Crm()->translate('Edit')
			)
		);

		return parent::display($canvas);
	}
	
	
	
	
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_functionality::get('Widgets');
		
		if ('sentdate' === $fieldPath) {
			return $W->Label(BAB_DateTimeUtil::relativePastDate($record->sentdate));
		}
		
		return parent::computeCellContent($record, $fieldPath);
	}
	
}
