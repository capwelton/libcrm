<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


bab_functionality::includeOriginal('Icons');


/**
 * list of articles from back office
 *
 */
class crm_ArticleTableView extends crm_TableModelView
{
    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        $this->addColumn(widget_TableModelViewColumn('edit', $Crm->translate('Edit'))->setSortable(false)->setExportable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn('image', '')->setSortable(false)->setExportable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));

        $this->addColumn(widget_TableModelViewColumn('_categories', $Crm->translate('Categories'))->setSearchable(false)->setInList(false));
        $this->addColumn(widget_TableModelViewColumn($set->reference, $Crm->translate('Reference')));
        $this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
        $this->addColumn(widget_TableModelViewColumn($set->subtitle, $Crm->translate('Subtitle'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($set->shortdesc, $Crm->translate('Short description'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn('_description_text', $Crm->translate('Description'))->setSearchable(false)->setInList(false));
        $this->addColumn(widget_TableModelViewColumn($set->description, $Crm->translate('Description HTML'))->setSearchable(false)->setInList(false));

        $this->addColumn(widget_TableModelViewColumn($set->modifiedOn, $Crm->translate('Last update'))->setSearchable(false)->setVisible(false));

        if (isset($set->articletype))
        {
            $this->addColumn(widget_TableModelViewColumn($set->articletype, $Crm->translate('Type'))->setSearchable(true)->setVisible(true));
        }

        if (isset($set->articleavailability))
        {
            $this->addColumn(widget_TableModelViewColumn($set->articleavailability, $Crm->translate('Availability'))->setSearchable(false)->setVisible(false));
        }

        $this->addColumn(widget_TableModelViewColumn($set->disabled, $Crm->translate('Disabled'))->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn('_disable_enable', $Crm->translate('Disable/Enable'))->setVisible(true)->setExportable(false));

        if (isset($Crm->Comment))
        {
            $this->addColumn(widget_TableModelViewColumn($set->rating_average, $Crm->translate('Ratings'))->setSearchable(false)->setVisible(false));
        }

        if (isset($Crm->ShoppingCart))
        {
            $this->addColumn(widget_TableModelViewColumn($set->sales_count, $Crm->translate('Sales'))->setSearchable(false)->setVisible(false));
        }

        if ($Access->usePackaging())
        {
            $this->addColumn(widget_TableModelViewColumn($set->mainpackaging->packaging->name, $Crm->translate('Main packaging'))->setVisible(false)->setSearchable(false));
        }

        $this->addColumn(widget_TableModelViewColumn($set->mainpackaging->unit_cost, $Crm->translate('Unit cost tax excl'))->setSearchable(false));
        if (isset($set->vat))
        {
            $this->addColumn(widget_TableModelViewColumn($set->vat, $Crm->translate('Vat rate'))->setSearchable(false)->setVisible(false));
        }
        $this->addColumn(widget_TableModelViewColumn('_unit_cost_tax_incl', $Crm->translate('Unit cost tax incl'))->setExportable(true)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn('_unit_cost_undiscounted', $Crm->translate('Undiscounted unit cost tax incl'))->setExportable(true)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->mainpackaging->reduction, $Crm->translate('Reduction'))->setVisible(false)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->mainpackaging->reduction_end, $Crm->translate('Reduction end'))->setVisible(false)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->mainpackaging->weight, $Crm->translate('Weight'))->setSearchable(false));
		
		$this->addCustomFields($set);

        $this->addLinksCollumns();
    }

    /**
     * Add collumns with links to front-office
     */
    protected function addLinksCollumns()
    {
        $Crm = $this->Crm();

        $this->addColumn(widget_TableModelViewColumn('_product_link', $Crm->translate('Link to product'))->setSortable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn('_image_link', $Crm->translate('Link to image'))->setSortable(false)->setVisible(false));
    }


    /**
     * Protect a catalog string for separators > and ,
     * return string
     */
    protected function encodeCatalog($text)
    {
        $text = str_replace('>', '\>', $text);
        $text = str_replace(',', '\,', $text);
        return $text;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellTextContent()
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $record->Crm();

        if ('_categories' === $fieldPath)
        {
            $text_catalogs = array();
            foreach($record->getCatalogs() as $catalogItem)
            {

                /* @var $catalogItem crm_CatalogItem */
                $text_catalog = array();
                foreach($catalogItem->catalog->getAncestors() as $c) {
                    $text_catalog[] = $this->encodeCatalog($c->name);
                }
                $text_catalogs[] = implode(' > ', $text_catalog);
            }

            return implode(', ', $text_catalogs);
        }

        if ('vat' === $fieldPath)
        {
            return $Crm->numberFormat($record->vat->value);
        }

        if ('mainpackaging/unit_cost' === $fieldPath)
        {
            return $Crm->numberFormat($record->mainpackaging->getUnitCostDF());
        }

        if ('_unit_cost_tax_incl' === $fieldPath)
        {
            $unit_cost = (float) $record->mainpackaging->getUnitCostDF();
            $vat = ($record->mainpackaging->getUnitCostDF() * $record->vat->value / 100);
            return $Crm->numberFormat($unit_cost + $vat);
        }

        if ('_unit_cost_undiscounted'  === $fieldPath)
        {
            $unit_cost = (float) $record->mainpackaging->getRawUnitCostDF();
            $vat = ($record->mainpackaging->getRawUnitCostDF() * $record->vat->value / 100);
            return $Crm->numberFormat($unit_cost + $vat);
        }


        if ('_product_link' === $fieldPath && $catalogItem = $record->getCatalogItem())
        {
            return $catalogItem->getRewritenUrl();
        }


        if ('_image_link' === $fieldPath)
        {
            return $this->getPhotoUrl($record);
        }

        if ('_description_text' === $fieldPath)
        {
            return strip_tags(bab_unhtmlentities($record->description));
        }


        return parent::computeCellTextContent($record, $fieldPath);
    }


    protected function getPhotoUrl(crm_Article $record)
    {
        return $record->getPhotoUrl();
    }




    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $Crm = $record->Crm();
        $Ui = $Crm->Ui();

        /*@var $Ui crm_Ui */

        $displayAction = $Crm->Controller()->Article()->display($record->id);

        if ('name' === $fieldPath)
        {
            if ($record->subtitle)
            {
                return $W->VBoxItems(
                        $W->Link($record->name, $displayAction)->addClass('crm-strong'),
                        $W->Label($record->subtitle)->addClass('crm-small')
                );
            }
            else {
                return $W->Link($record->name, $displayAction)->addClass('crm-strong');
            }
        }


        if ('edit' === $fieldPath)
        {
            return $W->Link(
                    $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $Crm->Controller()->Article()->edit($record->id)
            );
        }

        if ('image' === $fieldPath)
        {
            return $W->Link(
                $Ui->ArticleMainPhoto($record, 22, 22),
                $displayAction
            );
        }

        if ('reference' === $fieldPath)
        {
            return $W->Link($record->reference, $displayAction);
        }

        if ('weight' === $fieldPath)
        {
            return $W->Label($record->getWeight());
        }

        if ('rating_average' === $fieldPath)
        {
            if (0 == $record->rating_count)
            {
                return $W->Label('');
            }

            if (1 === (int) $record->rating_count)
            {
                $nb = $Crm->translate('1 rating');
            } else {
                $nb = sprintf($Crm->translate('%d ratings'), $record->rating_count);
            }

            return $W->Label($Crm->numberFormat($record->rating_average,1).'/5 ('.$nb.')');
        }


        if ('articleavailability' === $fieldPath)
        {
            $label = $W->Label(bab_nbsp())->addClass('crm-circle');
            $label->setCanvasOptions($label->Options()->backgroundColor('#'.$record->articleavailability->color));
            return $W->HBoxItems($label, parent::computeCellContent($record, $fieldPath))->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle');
        }

        if ('disabled' === $fieldPath)
        {
            if ($record->disabled)
            {
                return $W->Label($Crm->translate('Yes'));
            } else {
                return null;
            }
        }

        if ('_disable_enable' === $fieldPath)
        {
            return $W->DelayedItem($Crm->Controller()->Article()->disable_enable_button($record->id))
                    ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE)
                    ->setPlaceHolderItem($Crm->Controller()->Article(false)->disable_enable_button($record->id));
        }



        if ('mainpackaging/unit_cost' === $fieldPath)
        {
            return $W->Label($Crm->numberFormat($record->mainpackaging->getUnitCostDF()).bab_nbsp().$Ui->Euro());
        }

        if ('_unit_cost_tax_incl' === $fieldPath)
        {
            $unit_cost = (float) $record->mainpackaging->getUnitCostDF();
            $vat = ($record->mainpackaging->getUnitCostDF() * $record->vat->value / 100);
            return $W->Label($Crm->numberFormat($unit_cost + $vat).bab_nbsp().$Ui->Euro());
        }


        if ('_unit_cost_undiscounted'  === $fieldPath)
        {
            $unit_cost = (float) $record->mainpackaging->getRawUnitCostDF();
            $vat = ($record->mainpackaging->getRawUnitCostDF() * $record->vat->value / 100);
            return $W->Label($Crm->numberFormat($unit_cost + $vat).bab_nbsp().$Ui->Euro());
        }


        if ('mainpackaging/reduction' === $fieldPath)
        {
            if ('0.00' === $record->mainpackaging->reduction)
            {
                return null;
            }

            return $W->Label($Crm->numberFormat($record->mainpackaging->reduction).'%');
        }


        if ('mainpackaging/reduction_end' === $fieldPath)
        {
            if ('0000-00-00' === $record->mainpackaging->reduction_end)
            {
                return null;
            }

            return $W->Label(bab_shortDate(bab_mktime($record->mainpackaging->reduction_end), false));
        }



        if ('mainpackaging/weight' === $fieldPath)
        {
            return $W->Label(crm_Article::weightFormat($record->mainpackaging->weight));
        }


        if ('_product_link' === $fieldPath && $catalogItem = $record->getCatalogItem())
        {
            $url = $catalogItem->getRewritenUrl();
            return $W->Link(rawurldecode(basename($url)), $url);
        }


        if ('_image_link' === $fieldPath)
        {
            $url = $this->getPhotoUrl($record);
            return $W->Link(rawurldecode(basename($url)), $url);
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    protected function getCatalogFilter()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        
        // add a category filter
        
        $label = $W->Label($Crm->translate('Category'));
        $input = $W->Select()->setOptions($Crm->CatalogSet()->getAllOptions())->setName('catalog');
        $label->setAssociatedWidget($input);
        
        return $this->handleFilterLabel($label, $input);
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleFilterFields()
     */
    protected function handleFilterFields(Widget_Item $form)
    {
        parent::handleFilterFields($form);
        $form->addItem($this->getCatalogFilter());
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        if ($record->disabled)
        {
            $this->addRowClass($row, 'disabled');
        }

        return parent::handleRow($record, $row);
    }

}








/**
 *
 * @param crm_Article 	$article
 * @param int			$width
 * @param int			$height
 * @param bool			$border
 * @return Widget_Icon
 */
function crm_articleMainPhoto(crm_Article $article, $width, $height)
{
    $W = bab_Widgets();

    $path = $article->getPhotoPath();

    if (null === $path)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $addon = bab_getAddonInfosInstance('LibCrm');
        $path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
    }

    $image = $W->ImageThumbnail($path)->setThumbnailSize($width, $height);

    $image->setTitle($article->getName());
    $image->addClass('crm-article-image crm-element-image small');

    return $image;
}







class crm_ArticlePackagingTableView extends crm_TableModelView
{

    private $vat;


    /**
     *
     * @var crm_Article
     */
    protected $article;



    /**
     * @param	crm_Article	$article
     *
     */
    public function setArticle(crm_Article $article = null) {

        $this->article = $article;

        $this->addClass('icon-16x16 icon-left icon-left-16');

        if ($article) {
            $this->Crm()->includeVatSet();
            $I = $article->selectPackagings();
            if ($vat = $article->vat())
            {
                $this->vat = $vat->value / 100;
            } else {
                $this->vat = 0;
            }
        } else {
            $this->vat = 0;
            $oSet = $this->Crm()->ArticlePackagingSet();
            $oSet->packaging();
            $I = $oSet->select(new ORM_FalseCriterion());
        }


        $this->addDefaultColumns($I->getSet());
        $this->setDataSource($I);


        return $this;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if ($Access->usePackaging())
        {
            $this->addColumn(widget_TableModelViewColumn($set->packaging->name, $Crm->translate('Packaging')));
        }
        $this->addColumn(widget_TableModelViewColumn($set->weight, $Crm->translate('Weight')));
        $this->addColumn(widget_TableModelViewColumn($set->unit_cost, $Crm->translate('Unit cost tax excl')));
        $this->addColumn(widget_TableModelViewColumn('_tax_incl', $Crm->translate('Unit cost tax incl'))->addClass('widget-align-right'));
        $this->addColumn(widget_TableModelViewColumn($set->reduction, $Crm->translate('Reduction')));
        $this->addColumn(widget_TableModelViewColumn('unit_cost_ri', $Crm->translate('UC with taxes and reduction'))->addClass('widget-align-right'));
        $this->addColumn(widget_TableModelViewColumn($set->reduction_end, $Crm->translate('Reduction end')));
    }



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        if ($fieldPath === 'unit_cost') {
            return $W->Label($Crm->numberFormat($record->getRawUnitCostDF()).bab_nbsp().$Ui->Euro());
        }

        if ('_tax_incl' === $fieldPath) {
            return $W->Label($Crm->numberFormat(round( ($record->getRawUnitCostDF() * (1 + $this->vat)), 2)).bab_nbsp().$Ui->Euro());
        }

        if ($fieldPath === 'weight') {
            return $W->Label(crm_Article::weightFormat($record->weight));
        }

        if ($fieldPath === 'reduction') {
            return $W->Label($Crm->numberFormat($record->reduction).' %');
        }

        if ($fieldPath === 'unit_cost_ri') {
            return $W->Label($Crm->numberFormat(round( $record->getUnitCostTI(), 2)).bab_nbsp().$Ui->Euro());
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





/**
 *
 *
 */
class crm_ArticlePackagingEditTableView extends crm_ArticlePackagingTableView
{

    /**
     *
     * @var Widget_RadioSet
     */
    protected $radioSet;


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if ($Access->usePackaging())
        {
            $this->addColumn(widget_TableModelViewColumn($set->packaging->name, $Crm->translate('Packaging')));
        }
        $this->addColumn(widget_TableModelViewColumn($set->weight, $Crm->translate('Weight')));
        $this->addColumn(widget_TableModelViewColumn($set->unit_cost, $Crm->translate('Unit cost tax excl')));
        $this->addColumn(widget_TableModelViewColumn('unit_cost_ti', $Crm->translate('Unit cost tax incl')));
        $this->addColumn(widget_TableModelViewColumn($set->reduction, $Crm->translate('Reduction')));
        $this->addColumn(widget_TableModelViewColumn('unit_cost_ri', $Crm->translate('UC with taxes and reduction')));
        $this->addColumn(widget_TableModelViewColumn($set->reduction_end, $Crm->translate('Reduction end')));
    }


    /**
     * @param	crm_Article	$article
     *
     */
    public function setArticle(crm_Article $article = null) {

        parent::setArticle($article);

        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        $set = $this->getDataSource()->getSet();
        $this->addDefaultColumns($set);

        if ($this->getDataSource()->count() > 1) {
            $this->addColumn(widget_TableModelViewColumn('main', $Crm->translate('Main')));
        }

        if ($Access->usePackaging())
        {
            $this->addColumn(widget_TableModelViewColumn('delete', ''));
        }

        $this->setName('articlePackaging');
        $this->addClass('crm-article-packaging-edit');

        $this->radioSet = $W->RadioSet()->setRadioNamePath(array('article','articlePackaging','main'));

        if ($article && ($articlePackaging = $article->getMainArticlePackaging())) {
            $this->radioSet->setValue($articlePackaging->id);
        }

        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleCell()
     */
    protected function handleCell(ORM_Record $record, $fieldPath, $row, $col, $name = null, $rowSpan = null, $colSpan = null)
    {
        return parent::handleCell($record, $fieldPath, $row, $col, $record->id, $rowSpan, $colSpan);
    }


    /**
     * {@inheritDoc}
     * @see crm_ArticlePackagingTableView::computeCellContent()
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        if ($fieldPath === 'packaging/name') {
            return $W->Select()
                ->setOptions($Crm->PackagingSet()->getOptions())
                ->setName('packaging')
                ->setValue($record->packaging->id);
        }

        if ($fieldPath === 'unit_cost') {
            return $W->Items(
                $W->LineEdit()->setSize(3)->setName('unit_cost')->setValue($Crm->numberFormat($record->getRawUnitCostDF())),
                $W->Label($Ui->Euro())
            );
        }


        if ($fieldPath === 'unit_cost_ti') {
            return $W->Items(
                    $W->LineEdit()->setSize(3)->setName('unit_cost_ti')->setValue($Crm->numberFormat($record->getRawUnitCostTI()))->addClass('crm-articlepackaging-notsaved'),
                    $W->Label($Ui->Euro())
            );
        }

        if ($fieldPath === 'weight') {
            return $W->Items(
                $W->LineEdit()->setSize(3)->setName('weight')->setValue($Crm->numberFormat($record->weight, 4)),
                $W->Label($this->Crm()->translate('Kg'))
            );
        }

        if ($fieldPath === 'reduction') {
            return $W->Items(
                $W->LineEdit()->setSize(3)->setName('reduction')->setValue($Crm->numberFormat($record->reduction)),
                $W->Label('%')
            );
        }



        if ($fieldPath === 'unit_cost_ri') {

            return $W->Items(
                    $W->LineEdit()->setSize(3)->setName('unit_cost_ri')->setValue($Crm->numberFormat($record->getUnitCostTI()))->addClass('crm-articlepackaging-notsaved'),
                    $W->Label($Ui->Euro())
            );
        }


        if ($fieldPath === 'reduction_end') {
            return $W->DatePicker()->setName('reduction_end')->setValue($record->reduction_end);
        }


        if ($fieldPath === 'main') {
            return $W->Radio(null, $this->radioSet)->setCheckedValue($record->id);
        }

        if ($fieldPath === 'delete') {
            return $W->Link($W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Article()->removeArticlePackaging($record->id))
            ->setConfirmationMessage($this->Crm()->translate('Are you sure you want to remove the packaging in this article?'));
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initFooterRow()
     */
    protected function initFooterRow($row)
    {
        parent::initFooterRow($row);


        $Crm = $this->Crm();
        $Access = $Crm->Access();


        if (!$Access->usePackaging() && $row >= 1)
        {
            // do nothing if we use only one predefined packaging
            return;
        }


        static $newId = 0;
        $newId--;

        $newId = (string) $newId;

        $W = bab_Widgets();

        $col = 0;
        foreach ($this->columns as $columnId => $column) {


            switch($columnId)
            {
                case 'packaging/name':
                    $packaging = $W->Select()
                        ->setOptions($Crm->PackagingSet()->getOptions())
                        ->setName('packaging');
                    $this->addItem($W->NamedContainer()->setName($newId)->addItem($packaging), 0, $col);
                    break;

                case 'unit_cost':
                    $this->addItem($W->NamedContainer()->setName($newId)
                        ->addItem($W->LineEdit()->setSize(3)->setName('unit_cost'))
                        ->addItem($W->Label($Crm->Ui()->Euro()))
                    , 0, $col);
                    break;

                case 'unit_cost_ti':
                    $this->addItem($W->NamedContainer()->setName($newId)
                    ->addItem($W->LineEdit()->setSize(3)->setName('unit_cost_ti'))
                    ->addItem($W->Label($Crm->Ui()->Euro()))
                    , 0, $col);
                    break;

                case 'unit_cost_ri':
                    $this->addItem($W->NamedContainer()->setName($newId)
                    ->addItem($W->LineEdit()->setSize(3)->setName('unit_cost_ri'))
                    ->addItem($W->Label($Crm->Ui()->Euro()))
                    , 0, $col);
                    break;

                case 'weight':
                    $this->addItem($W->NamedContainer()->setName($newId)
                        ->addItem($W->LineEdit()->setSize(3)->setName('weight'))
                        ->addItem($W->Label($Crm->translate('Kg')))
                    , 0, $col);
                    break;

                case 'reduction':
                    $this->addItem($W->NamedContainer()->setName($newId)
                        ->addItem($W->LineEdit()->setSize(3)->setName('reduction'))
                        ->addItem($W->Label('%'))
                    , 0, $col);
                    break;

                case 'reduction_end':
                    $this->addItem($W->NamedContainer()->setName($newId)
                        ->addItem($W->DatePicker()->setName('reduction_end'))
                    , 0, $col);
                    break;

                default:
                    $this->addItem($W->Label(''), 0, $col);
                    break;
            }

            $col++;
        }
    }
}






/**
 * Article creation, edit article type, reference and name and continue to the main editor page
 *
 */
class crm_ArticleCreateEditor extends crm_ArticleEditor
{
    /**
     * {@inheritDoc}
     * @see crm_ArticleEditor::init()
     */
    protected function init()
    {
        if (isset($this->article)) {

            $set = $this->article->getParentSet();

            // create from article
            $this->setValues(array('name' => $set->name->formOutput($this->article->name)), array('article'));
            $this->setHiddenValue('article[from]', $this->article->id);
        }
    }



    protected function addFields()
    {
        $W = $this->widgets;

        $this->addItem($W->HBoxItems($this->Reference(), $this->articletype())->setHorizontalSpacing(3,'em'));
        $this->addItem($this->Name());
    }


    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        if (isset($this->article)) {
            $failedAction = $Crm->Controller()->Article()->copy($this->article->id);
        } else {
            $failedAction = $Crm->Controller()->Article()->edit();
        }

        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Continue'))
            ->validate(true)
            ->setAction($Crm->Controller()->Article()->save())
            ->setFailedAction($failedAction)
            ->setSuccessAction($Crm->Controller()->Article()->displayList())
        );

        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Cancel'))
            ->setAction($Crm->Controller()->Article()->displayList())
        );
    }
}








/**
 * Article editor, the field can depend on article type
 * @return Widget_Form
 */
class crm_ArticleEditor extends crm_MetaEditor
{
    /**
     *
     * @var crm_Article
     */
    protected $article;

    public function __construct(Func_Crm $crm, crm_Article $article = null, $id = null, Widget_Layout $layout = null)
    {
        $this->article = $article;


        parent::__construct($crm, $id, $layout);
        $this->setName('article');
        $this->colon();

        $this->addFields();
        $this->addButtons();

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->init();

    }


    protected function init()
    {
        if (isset($this->article)) {

            $this->setValues($this->getArticleValues(), array('article'));
            $this->setHiddenValue('article[id]', $this->article->id);
        }
    }


    /**
     * @return Array
     */
    protected function getArticleValues()
    {

        $values = $this->article->getValues();

        // checked if the product is new
        if ($this->article->newproduct > date('Y-m-d'))
        {
            $values['newproduct_checkbox'] = 1;
        }

        return $values;
    }



    protected function addFields()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();


        $this->addItem($W->HBoxItems($this->Reference(), $this->articletype()->setDisplayMode())->setHorizontalSpacing(3,'em'));
        $this->addItem($this->Name());
        $this->addItem($this->subtitle());
        $this->addItem($this->shortdesc());
        $this->addItem($this->description());
        $this->addItem($this->properties());


        $availability = null;
        if (isset($Crm->ArticleAvailability))
        {
            $availability = $this->articleavailability();
        }

        $checkbox = $W->VBoxItems($this->disabled(), $this->homepage(), $this->newproduct())->setVerticalSpacing(1,'em');


        $this->addItem($W->HBoxItems($checkbox, $availability, $this->attachements())->setHorizontalSpacing(4,'em'));


        if (null !== $articlePackagingSection = $this->ArticlePackagingSection())
        {
            $this->addItem($articlePackagingSection);
        }



        if (null !== $customfield = $this->CustomFieldSection())
        {
            $this->addItem($customfield);
        }

        if (null !== $metaSection = $this->MetaSection())
        {

            $this->addItem($metaSection);
        }

    }


    protected function attachements()
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $W = $this->widgets;

        if (!$Access->useArticleAttachments()) {
            return null;
        }

        return $W->LabelledWidget(
            $Crm->translate('Downloadable attachments for product page'),
            $W->FilePicker()->setFolder($this->article->uploadPath()->push('attachments'))
        );
    }



    protected function MetaSection()
    {
        return parent::MetaSection()->addItem($this->rewritename());
    }



    /**
     *
     * @return Widget_Item
     */
    protected function rewritename()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $f = $this->labelledField(
                $Crm->translate('Rewrite name'),
                $W->LineEdit()->setSize(60),
                __FUNCTION__
        );

        return $f;
    }


    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $id_article = null !== $this->article ? $this->article->id : null;

        $this->addButton(
                $W->SubmitButton()
                ->setLabel($Crm->translate('Save'))
                ->validate(true)
                ->setAction($Crm->Controller()->Article()->save())
                ->setSuccessAction($Crm->Controller()->Article()->display($id_article))
            //	->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
                ->setFailedAction($Crm->Controller()->Article()->edit($id_article))
        );



        $this->addButton(
                $W->SubmitButton()
                ->setLabel($Crm->translate('Cancel'))
                ->setAction($Crm->Controller()->Article()->cancel())
        );
    }




    /**
     *
     * @return Widget_Section
     */
    protected function ArticlePackagingSection()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!isset($Crm->ArticlePackaging))
        {
            return null;
        }

        if ($Access->usePackaging())
        {
            $title = $Crm->translate('Packagings and prices');
        } else {
            $title = $Crm->translate('Prices and discount');
        }

        $artpacks = $W->Section($title,
            $W->VBoxItems(
                $this->ArticlePackagings(),
                $this->vat()
            )->setVerticalSpacing(1, 'em')
        )->setFoldable(true);


        return $artpacks;
    }


    /**
     *
     */
    protected function ArticlePackagings()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $table = $Ui->ArticlePackagingEditTableView($this->article);
        /*
        $table = new crm_ArticlePackagingEditTableView($this->Crm());
        $table->setArticle($this->article);
        */
        $addonname = $Crm->getAddonName();

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory("/$addonname/configuration/");

        $excltaxpriority = $registry->getValue('excltaxpriority', false);

        $table->setMetadata('excltaxpriority', $excltaxpriority);

        return $table;
    }







    protected function reduction_end()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $date = $W->Frame()->addItem(
                $this->labelledField(
                        $Crm->translate('Reduction until the'),
                        $W->DatePicker(),
                        __FUNCTION__
                )
        );


        return $date;
    }




    /**
     *
     * @return Widget_Item
     */
    protected function unit_cost()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $f = $this->labelledField(
                $Crm->translate('Unit cost'),
                $W->LineEdit()->setSize(10),
                __FUNCTION__,
                null,
                sprintf($Crm->translate('%s tax excl'), $Crm->Ui()->Euro())
        );

        return $f;
    }


    /**
     *
     * @return Widget_Item
     */
    protected function vat()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $f = $this->labelledField(
                $Crm->translate('VAT'),
                $W->Select()->setOptions($Crm->VatSet()->getOptions())->setMandatory(true),
                __FUNCTION__,
                null,
                '%'
        );

        if (!($this->article instanceof crm_Article))
        {
            // init with default value

            $addonname = $Crm->getAddonName();
            $registry = bab_getRegistryInstance();
            $registry->changeDirectory("/$addonname/configuration/");
            $f->setValue($registry->getValue('vat'));
        }

        return $f;
    }


    /**
     *
     * @return Widget_Item
     */
    protected function reduction()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $f = $this->labelledField(
                $Crm->translate('Reduction'),
                $W->LineEdit()->setSize(10),
                __FUNCTION__,
                null,
                '%'
        );

        return $f;
    }


    /**
     *
     * @return Widget_Item
     */
    protected function Reference()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Reference'),
            $W->LineEdit()->setSize(10)->setMandatory(true, $Crm->translate('The reference is mandatory')),
            'reference'
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function Name()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Name'),
            $W->LineEdit()->setSize(70)->setMandatory(true, $Crm->translate('The name is mandatory')),
            'name'
        );
    }



    public function subtitle()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Subtitle'),
                $W->LineEdit()->setSize(70),
                __FUNCTION__
        );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function shortdesc()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        if (isset($this->article))
        {
            $aSet = $this->article->getParentSet();
        } else {
            $aSet = $Crm->ArticleSet();
        }

        return $this->labelledField(
                $Crm->translate('Short description'),
                $W->TextEdit()->setLines(2)->setColumns(80)->setMaxSize($aSet->shortdesc->getCustomPropertyValue('maxlength')),
                __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function description()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Description'),
                $W->SimpleHtmlEdit()->setColumns(82)->setMetadata('showCreateLinkControl', true),
                __FUNCTION__
        );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function properties()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Properties'),
                $W->TextEdit()->setColumns(80),
                __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function articletype()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $options = $Crm->ArticleTypeSet()->getOptions();

        if (1 === count($options)) {
            return null;
        }

        $f = $this->labelledField(
                $Crm->translate('Type'),
                $select = $W->Select()->setOptions($options),
                __FUNCTION__
        );



        if (!($this->article instanceof crm_Article) || ($this instanceof crm_ArticleCreateEditor))
        {
            // init with the last created article

            $set = $Crm->ArticleSet();
            $res = $set->select();
            $res->orderDesc($set->createdOn);
            foreach($res as $lastarticle)
            {
                $f->setValue($lastarticle->articletype);
                break;
            }
        } else {
            $select->setDisplayMode();
        }

        return $f;
    }


    /**
     *
     * @return Widget_Item
     */
    protected function packaging()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Packaging'),
                $W->Select()->setOptions($Crm->PackagingSet()->getOptions()),
                __FUNCTION__
        );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function articleavailability()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $set = $Crm->ArticleAvailabilitySet();

        return $this->labelledField(
                $Crm->translate('Availability'),
                $W->Select()->setOptions($set->getOptions()),
                __FUNCTION__
        );
    }






    /**
     *
     * @return Widget_Item
     */
    protected function weight()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $weight = $this->labelledField(
                $Crm->translate('Weight'),
                $W->LineEdit()->setSize(10),
                __FUNCTION__,
                $Crm->translate('Weight of product and packing'),
                'Kg'
        );

        return $weight;
    }


    /**
     *
     * @return Widget_Item
     */
    protected function disabled()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Disabled'),
                $W->Checkbox(),
                __FUNCTION__
        );
    }

    /**
     *
     * @return Widget_Item
     */
    protected function homepage()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Display on homepage'),
                $W->Checkbox(),
                __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function newproduct()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $date = $W->Frame()->addItem(
            $this->labelledField(
                $Crm->translate('until the'),
                $W->DatePicker(),
                __FUNCTION__
            )
        );

        $checkbox =  $this->labelledField(
                $Crm->translate('New product'),
                $W->Checkbox()->setAssociatedDisplayable($date, array('1')),
                'newproduct_checkbox'
        );



        return $W->FlowItems($checkbox, $date)->setHorizontalSpacing(.5, 'em')->setVerticalAlign('top');
    }



    /**
     * main product photo
     * @return Widget_ImagePicker
     */
    public function MainPhoto()
    {
        /* @var $W Func_Widgets */
        $W = $this->widgets;

        $addon = bab_getAddonInfosInstance('LibCrm');

        $imagePicker = $W->ImagePicker()
            ->setDimensions(256, 256)
            ->setName('mainphoto')
            ->oneFileMode()
            ->setDefaultImage(new bab_Path($addon->getStylePath() . 'images/article-default.png'))
            ->setTitle($this->Crm()->translate('Set the main photo'));

        /*@var $imagePicker Widget_ImagePicker */

        if (isset($this->article) && $this->article->id)
        {
            $imagePicker->setFolder($this->article->getPhotoUploadPath());
        }

        return $imagePicker;
    }

    /**
     * additional Photo gallery
     * @return Widget_ImagePicker
     */
    public function OthersPhotos()
    {
        /* @var $W Func_Widgets */
        $W = $this->widgets;

        $imagePicker = $W->ImagePicker()
        ->setDimensions(64,64)
        ->setFileNameWidth(64)
        ->setName('othersphoto')
        ->setTitle($this->Crm()->translate('Add a photo'));

        /*@var $imagePicker Widget_ImagePicker */

        if (isset($this->article))
        {
            $imagePicker->setFolder($this->article->getOthersPhotosUploadPath());
        }

        return $imagePicker;
    }






    protected function CustomFieldSection()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $layout = $W->VBoxItems()->setVerticalSpacing(1,'em');

        $set = $Crm->CustomFieldSet();
        $res = $set->select($set->object->is('Article'));

        if (0 === $res->count())
        {
            return null;
        }

        foreach($res as $customfield)
        {
            /*@var $customfield crm_CustomField */
            $layout->addItem($customfield->getWidget());
        }

        return $W->Section(
                $Crm->translate('Custom fields'),
                $layout
        )->setFoldable(true);
    }

}











class crm_ArticleImportPhotosEditor extends crm_Editor
{

    public function __construct(Func_Crm $Crm, $id = null, Widget_Layout $layout = null)
    {

        parent::__construct($Crm, $id, $layout);
        $this->setName('photos');
        $this->colon();

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();
        $this->addButtons();
    }


    protected function addFields()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        $this->addItem($W->Label($Crm->translate('You can upload multiple articles images with format reference.jpg or reference.png for the main image and reference.xxx.jpg or reference.yyy.png for the article image gallery')));
        $this->addItem($W->Label($Crm->translate('If the file xxx.jpg exists in the article image gallery, it will be updated, otherwise the image will be added')));
        $this->addItem($W->Label($Crm->translate("To use a '.' in reference or in a gallery image filename, it should be replaced with '..' in the filename")));

        $this->addItem($this->files());

    }


    protected function files()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
                $Crm->translate('Files'),
                $W->Uploader()->setMultiple()->setAcceptedMimeTypes(array('image/png', 'image/jpeg')),
                __FUNCTION__
        );
    }




    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Upload'))
            ->validate(true)
            ->setAction($Crm->Controller()->Article()->importPhotosEnd())
            ->setSuccessAction($Crm->Controller()->Article()->displayList())
            ->setFailedAction($Crm->Controller()->Article()->importPhotos())
        );



        $this->addButton(
            $W->SubmitButton()
            ->setLabel($Crm->translate('Cancel'))
            ->setAction($Crm->Controller()->Article()->cancel())
        );

    }


}



class crm_ArticleClassifyEditor extends crm_Editor
{
    private $classification = array();

    public function __construct(Func_Crm $Crm, crm_Article $article = null, $id = null, Widget_Layout $layout = null)
    {
        $this->article = $article;


        parent::__construct($Crm, $id, $layout);
        $this->setName('articleClassify');
        $this->colon();


        $this->setHiddenValue('tg', bab_rp('tg'));

        $catalogItemSet = $Crm->CatalogItemSet();
        $res = $catalogItemSet->select($catalogItemSet->article->is($article->id));


        foreach ($res as $catalogItem) {
            $this->classification[$catalogItem->catalog] = $catalogItem->catalog;
        }

        $this->addFields();
        $this->addButtons();


        $this->setHiddenValue('articleClassify[article]', $article->id);

    }


    protected function addFields()
    {
        $this->addItem($this->treeview());
    }





    protected function addButtons()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $this->addButton(
                $W->SubmitButton()
                ->setLabel($Crm->translate('Save'))
                ->validate(true)
                ->setAction($Crm->Controller()->Article()->saveClassification())
                ->setSuccessAction($Crm->Controller()->Article()->display($this->article->id))
                ->setFailedAction($Crm->Controller()->Article()->classify($this->article->id))
        );



        $this->addButton(
                $W->SubmitButton()
                ->setLabel($Crm->translate('Cancel'))
                ->setAction($Crm->Controller()->Article()->cancel())
        );

    }


    protected function treeview()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();
        $treeview = $W->SimpleTreeView();

        $set = $Crm->CatalogSet();
        $res = $set->select();

        if (0 === $res->count())
        {
            return $W->Label($Crm->translate('There are no categories configured, please add categories before'));
        }


        foreach ($res as $catalog) {

            $element = $treeview->createElement($catalog->id, '', $catalog->name, '', '');

            if ($catalog->disabled)
            {
                $element->setIcon($GLOBALS['babSkinPath'] . 'images/Puces/action_fail.gif');
            } else {
                $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');
            }

            $selected = array_key_exists($catalog->id, $this->classification);

            $element->addCheckBox('articleClassify[catalogItem][' . $catalog->id . ']', $selected);
            $parentId = ($catalog->parentcatalog == 0) ? null : $catalog->parentcatalog;
            $treeview->appendElement($element, $parentId);
        }

        return $treeview;
    }
}




/**
 * Display article or catalog item
 *
 */
class crm_ArticleDisplay extends crm_Object {


    /**
     * @var crm_Article
     */
    protected $article = null;

    /**
     *
     * @param crm_Article $article
     *
     */
    public function __construct(Func_Crm $Crm, crm_Article $article)
    {
        parent::__construct($Crm);
        $this->article = $article;
    }


    /**
     * Display article action
     * @return Widget_Action
     */
    protected function getDisplayAction() {

        return $this->Crm()->Controller()->Article()->display($this->article->id);
    }

    /**
     * Get a link to the detail view of article
     * @return unknown_type
     */
    protected function getDisplayLink()
    {
        $W = bab_Widgets();
        return $W->Link($W->Icon($this->Crm()->translate('View product sheet'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $this->getDisplayAction());
    }






    /**
     * Get similar articles as widget
     * @return Widget_Item
     */
    public function getSimilar()
    {
        $W = bab_Widgets();
        $I = $this->article->similarArticles();

        if (null === $I) {
            return null;
        }

        if (0 === $I->count()) {
            return null;
        }

        $frame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em')->addClass('similar-articles');

        $n = 0;
        foreach($I as $article)
        {
            $link = $this->getSimilarLink($article);
            $frame->addItem($link);

            $n++;

            if ($n > 10) {
                break;
            }
        }

        return $frame;
    }


    /**
     * Get item with a link to display a similar article
     * @param crm_Article $article
     * @return Widget_Item
     */
    protected function getSimilarLink(crm_Article $article)
    {
        $W = bab_Widgets();
        return $W->Link($article->name, $this->Crm()->Controller()->Article()->display($article->id));
    }





    /**
     * Get original main photo path
     * @return bab_Path
     */
    protected function getMainPhotoPath()
    {
        return $this->article->getPhotoPath();
    }

    /**
     *
     * @param int $width
     * @param int $height
     */
    public function PhotoUrl($width, $height)
    {
        $path = $this->getMainPhotoPath();

        if (null === $path)
        {
            $addon = bab_getAddonInfosInstance('LibCrm');
            require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
            $path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
        }

        $T = @bab_functionality::get('Thumbnailer');
        /*@var $T Func_Thumbnailer */

        if (false == $T)
        {
            return '';
        }

        $T->setSourceFile($path);

        return $T->getThumbnail($width, $height);
    }


    /**
     * Main photo or article
     *
     * @param int			$width
     * @param int			$height
     * @param int			$zoom
     * @return Widget_Image
     */
    public function Photo($width, $height, $zoom = null)
    {
        $W = bab_Widgets();
        $path = $this->getMainPhotoPath();

        if (null === $path)
        {
            $addon = bab_getAddonInfosInstance('LibCrm');
            require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
            $path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
        }

        if (null === $zoom)
        {
            return $W->ImageThumbnail($path, $this->article->image_alt)->setThumbnailSize($width, $height)->addClass('crm-article-photo');
        }

        return $W->ImageZoomerThumbnail($path, $this->article->image_alt)->setThumbnailSize($width, $height)->setZoomSize($zoom, $zoom)->addClass('crm-article-photo');

    }


    /**
     * All photos for fullframe
     */
    public function AllPhotos()
    {
        $W = bab_Widgets();
        $layout = $W->VBoxlayout()->setVerticalSpacing(.3, 'em');

        $layout->addItem($this->Photo(200, 200, 500));

        if ($othersphotos = $this->OthersPhotosLayout())
        {
            $layout->addItem($othersphotos);
        }

        return $layout;
    }




    public function OthersPhotosLayout($thumbwidth = 64, $thumbheight = 64, $zoom = 600)
    {
        $W = bab_Widgets();

        $I = $this->article->getOthersPhotosIterator();

        if (!isset($I))
        {
            return null;
        }

        $photos = $W->FlowLayout()->setSpacing(.3,'em')->addClass('crm-article-otherphotos');

        foreach($I as $filePickerItem)
        {
            $image = $W->ImageZoomerThumbnail($filePickerItem->getFilePath());

            $image->setThumbnailSize($thumbwidth,$thumbheight);
            $image->setZoomSize($zoom, $zoom);

            $photos->addItem($image);
        }

        return $photos;
    }


    /**
     * @return Widget_Section
     */
    public function OthersPhotos()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $photos = $this->OthersPhotosLayout();

        if (null === $photos)
        {
            return null;
        }

        $frame = $W->Section($Crm->translate('Product image gallery'), $photos)->setFoldable();

        return $frame;
    }



    /**
     * Technical sheet of product
     */
    protected function getTechnicalProperties()
    {
        $W = bab_Widgets();

        $article = $this->article;
        $set = $article->getParentSet();

        $table = $W->TableView();

        $arr = $this->article->getTechnicalProperties();



        $row = 0;
        foreach($arr as $fieldname => $prop)
        {
            // ignore empty properties

            if (isset($prop['value']))
            {
                $value = $prop['value'];
            } else {

                $value = $set->$fieldname->output($article->$fieldname);
                if ($value instanceof crm_Record)
                {
                    $value = $value->getRecordTitle();
                }
            }

            if ('' === (string) $value)
            {
                continue;
            }

            $name = $W->Label($prop['name']);
            if (isset($prop['description']))
            {
                $name->setTitle($prop['description']);
            }

            $table->addItem($name, $row, 0);
            $table->addItem($W->RichText($value)->setRenderingOptions(BAB_HTML_ALL ^ BAB_HTML_P), $row, 1);

            $row++;
        }

        if (!$row)
        {
            return null;
        }

        return $table;
    }



    protected function reference()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        return $W->FlowItems(
            $W->Label($Crm->translate('Reference'))->colon(),
            $W->Label($this->article->reference)
        )->setSpacing(0,'em',.5,'em');
    }


    protected function weight()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $weight = $this->article->getWeight();

        if ('' === $weight)
        {
            return null;
        }

        return $W->FlowItems(
                $W->Label($Crm->translate('Weight'))->colon(),
                $W->Label($weight)
        )->setSpacing(0,'em',.5,'em');
    }


    protected function articletype()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (!isset($Crm->ArticleType) || (!$this->article->articletype instanceof crm_ArticleType))
        {
            return null;
        }

        $articletype = $this->article->articletype->name;



        return $W->FlowItems(
                $W->Label($Crm->translate('Type'))->colon(),
                $W->Label($articletype)
        )->setSpacing(0,'em',.5,'em');
    }



    protected function articleavailability()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (!isset($Crm->ArticleAvailability) || (!$this->article->articleavailability instanceof crm_ArticleAvailability))
        {
            return null;
        }

        $articleavailability = $this->article->articleavailability->name;



        return $W->FlowItems(
                $W->Label($Crm->translate('Availability'))->colon(),
                $W->Label($articleavailability)
        )->setSpacing(0,'em',.5,'em');
    }



    protected function packaging(crm_Packaging $packaging)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();


        return $W->FlowItems(
                $W->Label($Crm->translate('Packaging'))->colon(),
                $W->Label($packaging->name)
        )->setSpacing(0,'em',.5,'em');
    }




    protected function description()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (empty($this->article->description))
        {
            return null;
        }

        return $W->Section(
                $Crm->translate('Description'),
                $W->Html($this->article->description)
        )->setFoldable();
    }


    protected function properties()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (empty($this->article->properties))
        {
            return null;
        }

        return $W->Section(
                $Crm->translate('Properties'),
                $W->RichText($this->article->properties)
        )->setFoldable();
    }



    /**
     *
     */
    protected function ArticlePackagings()
    {

        $table = new crm_ArticlePackagingTableView($this->Crm());
        $table->setArticle($this->article);

        return $table;
    }


    protected function packagingPrice()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!isset($Crm->ArticlePackaging))
        {
            return null;
        }

        if ($Access->usePackaging())
        {
            $title = $Crm->translate('Packagings and prices');
        } else {
            $title = $Crm->translate('Prices and discount');
        }

        $artpacks = $W->Section($title,
                $W->VBoxItems(
                        $this->ArticlePackagings(),
                        $this->vat()
                )->setVerticalSpacing(1, 'em')
        )->setFoldable(true);

        return $artpacks;
    }




    protected function getAttachmentsLayout()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();



        if (!$Access->useArticleAttachments()) {
            return null;
        }

        $list = $this->article->attachments(new bab_Path('attachments'));

        if (!isset($list)) {
            return null;
        }


        $layout = $W->FlowLayout()->setSpacing(1, 'em')
            ->addClass(Func_Icons::ICON_TOP_64);
        foreach($list as $filePickerItem) {

            $layout->addItem(
                $W->Link(
                    $W->FileIcon($filePickerItem->toString(), $filePickerItem->getFilePath())
                        ->setBorder(0, '#000000')
                        ->setThumbnailSize(64, 64),
                    $this->getAttachmentUrl($filePickerItem->getFileName())
                )
            );
        }

        return $layout;
    }


    protected function attachments()
    {
        $layout = $this->getAttachmentsLayout();

        if (!isset($layout)) {
            return null;
        }

        $layout->addClass('crm-article-attachements');

        return $layout;
    }


    /**
     * @return Widget_Action
     */
    protected function getAttachmentUrl($filename)
    {
        $Crm = $this->Crm();
        return $Crm->Controller()->Article()->downloadAttachment($this->article->id, $filename, 'attachments');
    }



    protected function vat()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        return $W->FlowItems(
                $W->Label($Crm->translate('VAT'))->colon(),
                $W->Label($Crm->numberFormat($this->article->vat->value).' %')
        )->setSpacing(0,'em',.5,'em');
    }




    protected function catalogs()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $set = $Crm->CatalogItemSet();
        $set->join('catalog');

        $res = $set->select($set->article->is($this->article->id));

        $list = $W->VBoxLayout()->setVerticalSpacing(.3,'em');

        if (1 === $res->count())
        {
            $section = $W->Section($Crm->translate('Category'), $list);
        } else {
            $section = $W->Section($Crm->translate('Categories'), $list);
        }

        foreach($res as $catalogItem)
        {
            $list->addItem($this->category($catalogItem->catalog));
        }

        return $section->setFoldable();
    }


    /**
     * Display a category
     * @return Widget_Displayable_Interface
     */
    protected function category(crm_Catalog $catalog)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $list = array();
        do {

            if ($Access->viewCatalog($catalog))
            {
                $item = $W->Link($catalog->name, $Crm->Controller()->CatalogItem()->catalog($catalog->id));
            } else {
                $item = $W->Label($catalog->name);
            }

            array_unshift(
                $list,
                $item
            );


            array_unshift($list, $W->Label(' > '));

        } while($catalog = $catalog->parentcatalog());

        $flow = $W->FlowLayout()->setHorizontalSpacing(.5,'em');

        unset($list[0]);
        foreach($list as $element)
        {
            $flow->addItem($element);
        }

        return $flow;
    }

    /**
     * Article frame without the main photo, contain information displayed on the right of the main photo
     * @return Widget_Frame
     */
    protected function getTopFrame()
    {
        $W = bab_Widgets();

        $top = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'));

        $top->addItem($this->reference());

        if ($articletype = $this->articletype())
        {
            $top->addItem($articletype);
        }

        if ($articleavailability = $this->articleavailability())
        {
            $top->addItem($articleavailability);
        }



        if ($meta = $this->getMetaFields())
        {
            $top->addItem($meta);
        }

        return $top;
    }



    protected function getMetaFields()
    {
        if (!$this->article->page_title && !$this->article->page_description && !$this->article->page_keywords)
        {
            return null;
        }

        $W = bab_Widgets();
        $Crm = $this->Crm();

        $frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'))->addClass('crm-metafields');
        $frame->addItem($W->Title($Crm->translate('HTML page Metadata fields'), 5));

        if ($this->article->page_title)
        {
            $frame->addItem($W->FlowItems(
                $W->Label($Crm->translate('Page title'))->colon(),
                $W->Label($this->article->page_title)
            )->setSpacing(0,'em',.5,'em'));
        }


        if ($this->article->page_description)
        {
            $frame->addItem($W->FlowItems(
                    $W->Label($Crm->translate('Page description'))->colon(),
                    $W->Label($this->article->page_description)
            )->setSpacing(0,'em',.5,'em'));
        }

        if ($this->article->page_keywords)
        {
            $frame->addItem($W->FlowItems(
                    $W->Label($Crm->translate('Page keywords'))->colon(),
                    $W->Label($this->article->page_keywords)
            )->setSpacing(0,'em',.5,'em'));
        }

        return $frame;
    }


    /**
     * Contain items displayed on full frame page
     * after the HBox with photo
     */
    public function getFullFrameItems()
    {
        $W = bab_Widgets();

        $frame = $W->Frame();

        if ($catalogs = $this->catalogs())
        {
            $frame->addItem($catalogs);
        }

        if ($description = $this->description())
        {
            $frame->addItem($description);
        }

        if ($properties = $this->properties())
        {
            $frame->addItem($properties);
        }

        if ($packagingPrice = $this->packagingPrice())
        {
            $frame->addItem($packagingPrice);
        }

        if ($attachments = $this->attachments())
        {
            $frame->addItem($attachments);
        }

        return $frame;
    }


    /**
     * Get article card frame visible by article manager
     *
     * @return Widget_Item
     */
    public function getCardFrame()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $frame = $W->Frame()->addClass('crm-article-cardframe');


        $hbox = $W->HBoxItems($this->Photo(64, 64))->setHorizontalSpacing(1,'em');

        $vbox = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
        $hbox->addItem($vbox);


        $vbox->addItem($W->Link($W->Title($this->article->name,5), $Crm->Controller()->Article()->display($this->article->id)));
        $vbox->addItem($W->Title($this->article->subtitle,6));


        if ($articletype = $this->articletype())
        {
            $vbox->addItem($articletype);
        }


        if ($articleavailability = $this->articleavailability())
        {
            $vbox->addItem($articleavailability);
        }

        $frame->addItem($hbox);

        return $frame;
    }



    /**
     * Get article full frame visible by article manager
     *
     * @return Widget_Item
     */
    public function getFullFrame()
    {
        $W = bab_Widgets();

        $frame = $W->Frame();

        $frame->addItem($W->Title($this->article->name,2)->addClass('crm-article-name'));
        $frame->addItem($W->Title($this->article->subtitle,3)->addClass('crm-article-subtitle'));
        $HBoxLayout = $W->HBoxItems($this->AllPhotos(), $this->getTopFrame())->setHorizontalSpacing(1,'em');
        $frame->addItem($HBoxLayout);
        $frame->additem($this->getFullFrameItems());

        $frame->addClass('crm-detailed-info-frame');

        return $frame;
    }




}








/**
 * list of articles from back office
 *
 */
class crm_ArticleLinkTableView extends crm_ArticleTableView
{
    /**
     * {@inheritDoc}
     * @see crm_ArticleTableView::computeCellContent()
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $Crm = $record->Crm();

        if ('__action__' === $fieldPath)
        {
            return $W->DelayedItem($Crm->Controller()->Article()->getToggleLink(bab_rp('linkType' ,1), bab_rp('article'), $record->id, 'linked-list-'.$record->id))
                ->setPlaceHolderItem($Crm->Controller()->Article(false)->getToggleLink(bab_rp('linkType' ,1), bab_rp('article'), $record->id, 'linked-list-'.$record->id))
                ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE);
        }

        if ('__privatesell__' === $fieldPath)
        {
            return $W->DelayedItem($Crm->Controller()->PrivateSell()->getToggleLink(bab_rp('privatesell'), $record->id, 'linked-list-'.$record->id))
                ->setPlaceHolderItem($Crm->Controller()->PrivateSell(false)->getToggleLink(bab_rp('privatesell'), $record->id, 'linked-list-'.$record->id))
                ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE);
        }

        return parent::computeCellContent($record, $fieldPath);
    }





    /**
     * {@inheritDoc}
     * @see crm_ArticleTableView::handleFilterFields()
     */
    protected function handleFilterFields(Widget_Item $form)
    {

        parent::handleFilterFields($form);

        $Crm = $this->Crm();
        $W = bab_Widgets();

        // add a Linked product filter
        $arrayLinked = array(
            '' => crm_translate('All product'),
            '1' => crm_translate('No'),
            '2' => crm_translate('Yes')
        );
        $label = $W->Label($Crm->translate('Linked Article'));
        $input = $W->Select()->setOptions($arrayLinked)->setName('linked');
        $form->addItem($this->handleFilterLabel($label, $input));
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::getFilterCriteria()
     */
    public function getFilterCriteria($filter = null, $set)
    {
        $criteria = parent::getFilterCriteria($filter);

        $Crm = $this->Crm();

        if(isset($filter['linked']) && $filter['linked'] != ''){
            $articleLinkSet = $Crm->ArticleLinkSet();
            $articleLinks = $articleLinkSet->select(
                $articleLinkSet->article_from->is(bab_rp('article'))
                ->_AND_($articleLinkSet->type->is(bab_rp('linkType')))
            );

            $arrLinkedTo = array();
            foreach($articleLinks as $articleLink){
                $arrLinkedTo[] = $articleLink->article_to;
            }
            if($filter['linked'] == 1){
                $criteria = $criteria->_AND_($set->id->notIn($arrLinkedTo));
            }elseif($filter['linked'] == 2){
                $criteria = $criteria->_AND_($set->id->in($arrLinkedTo));
            }
        }

        return $criteria;
    }
}
