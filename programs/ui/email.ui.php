<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');


/* If you use this, don't forget to create the table EmailSignature (when include EmailSignatureSet) */
class crm_EmailEditor extends crm_Editor
{
	/* @var Widget_FilePicker */
	private $filepicker = null;


	public function __construct($crm, $id = null, Widget_Layout $layout = null, $signature = true)
	{
		parent::__construct($crm, $id, $layout);

		$this->setHiddenValue('tg', bab_rp('tg'));

		$W = $this->widgets;
		$Crm = $this->Crm();


		$this->addItem(
			$this->labelledField(
				$Crm->translate('Recipients (separated by commas)'),
				$Crm->Ui()->SuggestEmail()
					->setColumns(65)
					->addClass('widget-fullwidth', 'widget-autoresize')
					->setMandatory(true, $Crm->translate('Your must specify at least on recipient.')),
				'recipients'
			)->colon(true)
		);


		$this->addItem(
			$this->labelledField(
				$Crm->translate('Recipients CC (separated by commas)'),
				$Crm->Ui()->SuggestEmail()
					->setColumns(65)
					->addClass('widget-fullwidth')
					->setMandatory(false),
				'ccRecipients'
			)->colon(true)
		);

		$this->addItem(
			$this->labelledField(
				$Crm->translate('Recipients BCC (separated by commas)'),
				$Crm->Ui()->SuggestEmail()
					->setColumns(65)
					->addClass('widget-fullwidth')
					->setMandatory(false),
				'bccRecipients'
			)->colon(true)
		);

		$bccMyselfFormItem = $this->labelledField(
			$Crm->translate('Put myself in BCC'),
			$W->CheckBox(),
			'bccMyself'
		)->colon(false);


		$subjectFormItem = $this->labelledField(
			$Crm->translate('Subject'),
			$W->LineEdit()
				->setSize(75)
				->addClass('widget-fullwidth')
				->setMandatory(true, $Crm->translate('The message subject must not be empty.')),
			'subject'
		)->colon(true);

		$bodyFormItem = $this->labelledField(
			$Crm->translate('Message'),
			$W->BabHtmlEdit()->setEmail()
				//->addClass('widget-fullwidth', 'widget-autoresize')
				->setLines(10)
				->setColumns(75),
			'body'
		)->colon(true);

		$attachmentsFormItem = $this->labelledField(
			$Crm->translate('Attached files'),
			$this->getFilePicker()
		)->colon(true);



		$this->addItem($bccMyselfFormItem);
		$this->addItem($subjectFormItem);
		$this->addItem($bodyFormItem);


		if ($signature) {
		    if ($signatureFormItem = $this->signature())
		    {
		  		$this->addItem($signatureFormItem);
		    }
		}

		$this->addItem($attachmentsFormItem);

	}



	public function signature()
	{
	    $W = $this->widgets;
	    $Crm = $this->Crm();

	    if (!isset($Crm->EmailSignature))
	    {
	    	return null;
	    }


	    $radiomenuSignature = $W->RadioMenu()
	        ->setName('signature')
	        ->addClass('icon-top-24 icon-top icon-24x24 widget-fullwidth');

	    $radiomenuSignature->addOption(
	    	'',
	        $W->HBoxItems(
	            $W->Icon('', Func_Icons::MIMETYPES_SIGNATURE),
           	    $W->Html($Crm->translate('No signature'))
    	    )->setVerticalAlign('middle')
	    );



	    $Crm->includeEmailSignatureSet();

	    $emailSignatureSet = $Crm->EmailSignatureSet();

	    $emailSignatures = $emailSignatureSet->select(
	        $emailSignatureSet->userid->is($GLOBALS['BAB_SESS_USERID'])
	    )->orderAsc($emailSignatureSet->name);

	    foreach ($emailSignatures as $signature) {
	        $radiomenuSignature->addOption(
	            $signature->id,
	            $W->HBoxItems(
	                $W->Icon('', Func_Icons::MIMETYPES_SIGNATURE),
    	            $W->VBoxItems(
	                    $W->Title($signature->name, 6),
	                    $W->Html($signature->signature)
    	            )
	            )
	        );
	    }

	    $frameLinkSignature = $W->Frame()->addClass('icon-left-16 icon-left icon-16x16');
	    $manageSignature = $W->Link(
	        '',
	        $Crm->Controller()->EmailSignature()->displayList()
        )->setConfirmationMessage($Crm->translate('You are going to lose your text, are you sure of wanting to go on?'))
	    ->addClass('icon ' .  Func_Icons::ACTIONS_DOCUMENT_PROPERTIES) // CATEGORIES_PREFERENCES_DESKTOP)
	    ->setId('crm_emailSignatureLink');

	    $frameLinkSignature->addItem($manageSignature);


// 	    $signatureBox = $W->VBoxItems(
// 	        $W->HBoxItems(
//     	        $radiomenuSignature,
//     	        $frameLinkSignature->setSizePolicy(Widget_SizePolicy::MINIMUM)
// 	        )
// 	        ->setHorizontalSpacing(1, 'em')
// 	        ->setVerticalAlign('middle')
// 	        ->setSizePolicy(Widget_SizePolicy::MAXIMUM)
// 	    );

	    $signatureBox = $W->VBoxItems($radiomenuSignature);

	    return $signatureBox;
	}


	/**
	 * Returns the filepicker widget associated to the email editor.
	 *
	 * @return Widget_FilePicker
	 */
	public function getFilePicker()
	{
		if (null === $this->filepicker) {
			$W = $this->widgets;
			$this->filepicker = $W->FilePicker()->setName('attachments[]');
		}
		return $this->filepicker;
	}


}




class crm_HistoryEmail extends crm_HistoryElement
{
    private $email;

	public function __construct(crm_Email $email, $mode = null, $id = null)
	{

		$Crm = $email->Crm();

		parent::__construct($Crm, $mode, $id);

		$W = bab_Widgets();

		$this->email = $email;

		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

		$Crm->Ui()->includeOrganization();
		$Crm->Ui()->includeContact();
		$Crm->Ui()->includeDeal();
		$Crm->Ui()->includeTeam();

		$contactSet = $Crm->ContactSet();


		$access = $Crm->Access();

		$contactSet = $Crm->ContactSet();

		$this->setElement($email);

		$creatorName = $email->fromname;

		if ($email->createdBy) {
			$creator = $contactSet->get($contactSet->user->is($email->createdBy));
			if ($creator && $access->viewContact($creator)) {
				$creatorName = $creator->getFullName();

				$this->addIcon(
					$W->Link(
						$Crm->Ui()->ContactPhoto($creator, 32, 32, 0),
						$Crm->Controller()->Contact()->display($creator->id)
					)
				);
			} else {
				$creatorName = bab_getUserName($email->createdBy, true);
			}
		}


		$subtitleLayout = $W->FlowLayout()
			->setHorizontalSpacing(1, 'ex')
			->addClass(Func_Icons::ICON_LEFT_24)
			->addClass('crm-small');


		if ($creatorName)
		{
			$subTitle = sprintf($Crm->translate('Email sent by %s'),  $creatorName);
		} else {
			$subTitle = '';
		}


		if (isset($email->mailbox) && $email->mailbox)
		{
			if ($subTitle)
			{
				$subTitle .= ', ';
			}

			$Crm->includeMailBoxSet();
			$mailbox = $email->mailbox();
			if ($mailbox->user > 0)
			{
				$subTitle .= sprintf($Crm->translate('imported from the mailbox of %s'), bab_getUserName($mailbox->user));
			} else {
				$subTitle .= $Crm->translate('imported from a mailbox');
			}
		}


		$this->addDefaultLinkedElements();


		$this->addTitle(
			$W->Title(
				$W->Link(
					$email->subject,
					$Crm->Controller()->Email()->display($email->id)
				),
				4
			)
		);

		$dateSent = $this->getMode() == self::MODE_SHORT ?
			BAB_DateTimeUtil::relativePastDate($email->createdOn, true)
			: bab_longDate(bab_mktime($email->createdOn), true);
		$emailIcon = $W->Icon($subTitle . "\n" . $dateSent, Func_Icons::OBJECTS_EMAIL);
		$subtitleLayout->addItem($emailIcon);



        if ($status = $this->getStatus()) {
            $subtitleLayout->addItem($status);
        }


		$this->addTitle($subtitleLayout);




		switch ($this->getMode()) {

			case self::MODE_COMPACT :
				break;

			case self::MODE_SHORT :
//				$this->addTitle($W->Label(BAB_DateTimeUtil::relativePastDate($email->createdOn, true))->addClass('crm-small'));
				$this->addContent($W->Html(bab_toHtml(bab_abbr(strip_tags($email->body), BAB_ABBR_FULL_WORDS, 200))));
				break;

			case self::MODE_FULL :
			default:
//				$this->addTitle($W->Label(bab_longDate(bab_mktime($email->createdOn), true))->addClass('crm-small'));
				$recipientsBox = $W->GridLayout()->setSpacing(1, 'em');
				$this->addContent($recipientsBox);
				$recipientsBox->addItem($W->Label($Crm->translate('Recipients'))->colon(), 0, 0);
				$recipientsTxt = str_replace(',', ', ', $email->recipients);
				$recipientsBox->addItem($W->Label($recipientsTxt), 0,1);
				if (!empty($email->ccRecipients)) {
				    $ccRecipients = str_replace(',', ', ', $email->ccRecipients);
					$recipientsBox->addItem($W->Label($Crm->translate('CC'))->colon(), 1, 0);
					$recipientsBox->addItem($W->Label($ccRecipients), 1, 1);
				}
				if (!empty($email->bccRecipients)) {
				    $bccRecipients = str_replace(',', ', ', $email->bccRecipients);
					$recipientsBox->addItem($W->Label($Crm->translate('BCC'))->colon(), 2, 0);
					$recipientsBox->addItem($W->Label($bccRecipients), 2, 1);
				}

                $html = preg_replace('|<style.*>.*</style>|is', '', $email->body);
                if ($html === null) {
                    // A problem with preg_replace can occur on big strings.
                    $html = preg_replace('|<style.*>|i', '<div style="display:none">', $email->body);
                    $html = preg_replace('|</style>|i', '</div>', $html);
                }

				$this->addContent($W->Html($html));
				if ($attachments = $email->attachments()) {
					$this->setAttachments($attachments);
				}
				break;
		}

	}


	/**
	 * Get status information
	 * @return Widget_Displayable_Interface
	 */
	private function getStatus()
	{
	    $Crm = $this->Crm();
	    $W = bab_Widgets();
	    $email = $this->email;

	    if ($email->hash && $Spooler = @bab_functionality::get('Mailspooler'))
	    {
	        /*@var $Spooler Func_Mailspooler */
	        if (true !== $Spooler->getStatus($email->hash, $label)) {

	            // status found in spooler, if nothing in spooler getStatus return true

	            return $W->VBoxItems(
	                $W->Label($label)->addClass('crm-error'),
	                $W->Link(
	                    $W->Icon($Crm->translate('Retry'), Func_Icons::ACTIONS_VIEW_REFRESH),
	                    $Crm->Controller()->Email()->retry($email->id)
	                )
	            );
	        }


	        if ($email->status !== 'Sent') {

	            // cela ne devrais pas arriver, sauf si bug

	            // not in spooler but not sent
	            return $W->Label($Crm->translate('An error happened while trying to send this email.'))
    	            ->setTitle($email->status)
    	            ->addClass('crm-error');
	        }


	        // not in spooler but sent
	        return null;
	    }


	    // not linked to spooler

	    if ('' === $email->status) {
	        return $W->Label($Crm->translate('The email was not sent'))
    	        ->addClass('crm-error');
	    }

	    if ($email->status !== 'Sent') {
	        return $W->Label($Crm->translate('An error happened while trying to send this email.'))
	            ->setTitle($email->status)
	            ->addClass('crm-error');
	    }

	    return null;
	}





	/**
	 * Add a contextual menu with several actions applicable
	 * to the email.
	 *
	 * @return crm_historyTask
	 */
	public function addActions()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$this->addAction(
			$W->Link(
				$W->Icon($Crm->translate('Display details'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
				$Crm->Controller()->Email()->display($this->element->id)
			)
		);

		return $this;
	}

	/**
	 * Returns a link widget to the specified attached file
	 *
	 * @param	crm_Note				$note
	 * @param	Widget_FilePickerItem 	$file
	 *
	 * @return Widget_Link
	 */
	protected function getAttachmentWidget(Widget_FilePickerItem $file)
	{
		$W = bab_Widgets();

		return $W->Link(
			crm_fileAttachementIcon($file, 28, 28),
			$this->Crm()->Controller()->Email()->downloadAttachment($this->element->id, $file->getFileName(), 0)
		)->setOpenMode(Widget_Link::OPEN_POPUP);
	}
}






/**
 * @param crm_Email $email
 *
 * @return widget_Frame
 */
function crm_emailLinkedObjects(crm_Email $email)
{
	$W = bab_Widgets();
	$Crm = $email->Crm();

	$Crm->includeOrganizationSet();

	$objectsFrame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');

	$orgs = array();

	$contacts = $email->getLinkedContacts();
	if (count($contacts) > 0) {

		$Crm->Ui()->includeContact();

		$objectsFrame->addItem(
			$W->Title($Crm->translate('Contacts linked to this email'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);

		foreach ($contacts as $contact) {
			$contactFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
			$objectsFrame->addItem($contactFrame);
			/* @var $contact crm_Contact */
			$card = $Crm->Ui()->contactCardFrame($contact);
			$contactFrame->addItem($card);

			$displayLink = $W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Contact()->display($contact->id));

//			$mainOrganization = $contact->getMainOrganization();
//			if (isset($mainOrganization)) {
//				$orgs[] = $mainOrganization;
//			}
		}
	}

	$deals = $email->getLinkedDeals();
	if (count($deals) > 0) {

		$Crm->Ui()->includeDeal();

		$objectsFrame->addItem(
			$W->Title($Crm->translate('Deals linked to this email'), 4)
				->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);

		foreach ($deals as $deal) {

			$dealFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
			$objectsFrame->addItem($dealFrame);

			/* @var $deal crm_Deal */
			$card = $Crm->Ui()->DealCardFrame($deal);
			$dealFrame->addItem($card);

			$displayLink = $W->Link(
				$W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
				$Crm->Controller()->Deal()->display($deal->id)
			);

			$lead = $deal->lead();
			if (isset($lead)) {
				$orgs[] = $lead;
			}
		}
	}


	$orgs = $email->getLinkedOrganizations();
	if (count($orgs) > 0) {

		$Crm->Ui()->includeOrganization();
		$Crm->Ui()->includeContact();

		$objectsFrame->addItem(
			$W->Title($Crm->translate('Organizations linked to this email'), 4)
				->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);

		foreach ($orgs as $org) {
			/* @var $org crm_Organization */

			$card = $Crm->Ui()->OrganizationCardFrame($org);

			$organizationFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
			$objectsFrame->addItem($organizationFrame);

			$organizationFrame->addItem($card);

			$orgContacts = $org->selectContacts();

			$contactsAccordion = $W->Accordions();
			foreach ($orgContacts as $orgContact) {
				$contact = $orgContact->getContact();
				/* @var $contact crm_Contact */
				$contactCard = $Crm->Ui()->contactCardFrame($contact, crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_ORGANIZATION));
				$address = $contact->getMainAddress();
				$contactsAccordion->addPanel(
					$contact->getFullName() . ' - ' . $Crm->translate($contact->getMainPosition()),
					$W->VboxItems(
						$contactCard,
						$W->FlowLayout()->addClass('crm-context-actions')->addClass('icon-left-16 icon-left icon-16x16')
							->addItem(
								$W->Link(
									$W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
									$Crm->Controller()->Contact()->display($contact->id)
								)
							)
					)->setVerticalSpacing(0.5, 'em')
				);
			}
			$organizationFrame->addItem($contactsAccordion);
		}
	}


	$teams = $email->getLinkedTeams();
	if (count($teams) > 0) {

		$Crm->Ui()->includeTeam();

		$objectsFrame->addItem(
			$W->Title($Crm->translate('Teams linked to this email'), 4)
				->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);

		foreach ($teams as $team) {

			$teamFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
			$objectsFrame->addItem($teamFrame);

			/* @var $team crm_Team */
			$card = $Crm->Ui()->TeamCardFrame($team);
			$teamFrame->addItem($card);

			$displayLink = $W->Link(
				$W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
				$Crm->Controller()->Team()->display($team->id)
			);
		}
	}



	return $objectsFrame;
}

