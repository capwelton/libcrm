<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');





/**
 * Display the emblems associated to a crm_Record.
 *
 * @extends Widget_FlowLayout
 */
class crm_EmblemsDisplay extends crm_UiObject
{
	/**
	 * @param Func_Crm			$crm
	 * @param array|Iterator	$emblems		Array or Iterator of crm_Emblem
	 * @param string			$id
	 */
	public function __construct($crm, $emblems, crm_Record $associatedObject, Widget_Action $emblemAction = null, $id = null)
	{
		parent::__construct($crm);
		$W = bab_Widgets();
		// We simulate inheritance from Widget_FlowLayout.
		// Now $this can be used as a FlowLayout
		$this->setInheritedItem($W->FlowLayout($id));

		$this->setSpacing(2, 'px');

		list($addon, $associatedObjectClass) = explode('_', get_class($associatedObject));
		$associatedObjectClass = ucwords($associatedObjectClass);

		foreach ($emblems as $emblem) {
			/* @var $emblem crm_Emblem */
			if ($emblem instanceof crm_Link) {
				$emblem = $emblem->targetId;
			}

			$emblemLabel = $W->Label($emblem->label)
						->setTitle($emblem->description);
			if (isset($emblemAction)) {
				$emblemLabel = $W->Link($emblemLabel, $emblemAction->setParameter('emblem', $emblem->label));
			}

			$emblemDeleteLink = $W->Link(
				$W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
				$crm->Controller()->Emblem()->unlink($emblem->id, $associatedObjectClass . ':' . $associatedObject->id)
			);

			$emblemLayout = $W->FlowItems(
				$emblemLabel,
				$emblemDeleteLink
			)
			->setVerticalAlign('middle')
//			->setHorizontalSpacing(0.3, 'em')
			->addClass('crm-emblem')->addClass('icon-left-16 icon-16x16 icon-left-16');

			if (!$emblem->checked) {
				$emblemLayout->addClass('crm-not-checked');
			}
			$this->addItem(
				$emblemLayout
			);
		}

	}
}





/**
 * @return crm_Editor
 */
class crm_EmblemEditor extends crm_Editor
{

	protected function prependFields()
	{
		$W = $this->widgets;

		$Crm = $this->Crm();

		$emblemSet = $Crm->EmblemSet();

		$labelFormItem =
			$this->labelledField(
				$Crm->translate('Label:'),
				$W->LineEdit()->setSize(52)->setMaxSize(255)
					->addClass('widget-fullwidth')
					->setMandatory(true, $Crm->translate('The emblem label must not be empty.')),
				'summary'
			);
		$this->addItem($labelFormItem);
	}
}




class crm_EmblemTableView extends widget_TableModelView
{

	protected function computeCellContent($record, $fieldPath)
	{

		$Crm = lcrm_Crm();
		$W = bab_Widgets();

		if ($fieldPath == '__actions__') {

			$cellContent = $W->Menu()
			->setLayout($W->FlowLayout())
			->addClass(Func_Icons::ICON_LEFT_16);
// 			$cellContent->addItem(
// 					$W->Link($W->Icon($Crm->translate('Edit...'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Admin()->editEmblem($record->id))
// 			);
 			$cellContent->addItem(
				$W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Admin()->deleteEmblem($record->id))
 					->setConfirmationMessage($Crm->translate('Are you sure you want to delete this emblem?'))
 			);

		} else if ($fieldPath == 'icon') {
			$cellContent = $W->Icon('', $record->icon);

		} else if ($fieldPath == 'label') {
			$cellContent = $W->VBoxItems(
					$W->Label($record->label),
					$W->Label($record->description)->addClass('crm-sub-label')
			);

		} else if ($fieldPath == 'modifiedOn') {
			$cellContent = $W->VBoxItems(
					$W->Label(bab_shortDate(bab_mktime($record->modifiedOn), true)),
					$W->Label(bab_getUserName($record->modifiedBy, true))->addClass('crm-sub-label')
			);
		} else {
			$cellContent = parent::computeCellContent($record, $fieldPath);
		}

		return $cellContent;

	}

}
