<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





bab_functionality::get('TcPdf')->includeTCPDF();



class crm_OrderPdf extends TCPDF {



	const FONT_FAMILY	= 'vera';
	const FONT_STYLE	= '';
	const FONT_SIZE 	= 7;


	private $Crm;

	/**
	 *
	 * @var crm_Order
	 */
	protected $order;


	/**
	 *
	 * @var bool
	 */
	private $vatColumn = null;


	/**
	 *
	 * @var string
	 */
	protected $ordertitle = null;
	
	
	/**
	 *
	 * @var string
	 */
	private $ordertype = null;
	
	
	
	
	/**
	 * @var LibPdf_column
	 */
	protected $reference;
	
	/**
	 * @var LibPdf_column
	 */
	protected $name;
	
	/**
	 * Product type
	 * @var LibPdf_column
	 */
	protected $type;
	
	
	/**
	 * Target Customer in the marketplace
	 * @var LibPdf_column
	 */
	protected $marketplaceCustomer;
	
	/**
	 * total sold on the marketplace
	 * @var LibPdf_column
	 */
	protected $marketplaceTotal;
	
	/**
	 * rate to compute fees for the fees invoice
	 * @var LibPdf_column
	 */
	protected $marketplaceRate;
	
	
	/**
	 * @var LibPdf_column
	 */
	protected $quantity;
	
	/**
	 * prix unitaire HT
	 * @var LibPdf_column
	 */
	protected $unit_cost_df;
	
	/**
	 * @var LibPdf_column
	 */
	protected $reduction;
	
	/**
	 * prix unitaire HT + remise
	 * @var LibPdf_column
	 */
	protected $unit_red_df;
	
	/**
	 * TVA
	 * @var LibPdf_column
	 */
	protected $vat;
	
	/**
	 * prix unitaire TTC
	 * @var LibPdf_column
	 */
	protected $unit_cost_ti;
	
	/**
	 * Total TTC
	 * @var LibPdf_column
	 */
	protected $total_ti;
	
	


	public function __construct(Func_Crm $Crm, crm_Order $order) {

		parent::__construct();

		$this->Crm = $Crm;
		
		// We avoid font subsetting to ensure the pdf is readable on most pdf readers (iphone, ipad, gmail pdf viewer, firefox 19).
		$this->setFontSubsetting(false);

		$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE);
		$this->setLanguageArray(bab_functionality::get('TcPdf')->getLanguageArray());

		
		$this->setPrintHeader(false);
		$this->SetLeftMargin(5);
		$this->SetTopMargin(5);
		$this->SetRightMargin(5);
		$this->SetLineWidth(0.1);
		$this->setFooterMargin(8);

		$this->setHeaderFont(Array(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE));
		$this->setFooterFont(Array(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE));

		$this->order = $order;
		$this->setOrderTitle($order->name);
	}




	public function Output($name='doc.pdf', $dest='I')
	{
		$this->init();

		return parent::Output($name, $dest);
	}



	/**
	 * @return Func_Crm
	 */
	protected function Crm()
	{
		return $this->Crm;
	}


	/**
	 *
	 * @return string
	 */
	protected function colon()
	{
	    $lang = null;
	    
	    if (function_exists('bab_getLanguage')) {
	        // need 8.0.94
	        $lang = bab_getLanguage();
	    } elseif (isset($GLOBALS['babLanguage'])) {
	        $lang = $GLOBALS['babLanguage'];
	    }
	    
		if ('en' === $lang) {
			return ': ';
		}

		return ' : ';
	}
	
	
	protected function getSubject()
	{
		return bab_convertStringFromDatabase($this->ordertype	, 'UTF-8');
	}


	public function init() {


		$this->SetTitle($this->order->name);


		$this->SetSubject($this->getSubject());
		$this->SetAuthor(bab_convertStringFromDatabase(bab_getUserName(bab_getUserId())			, 'UTF-8'));
		$this->SetCreator(bab_convertStringFromDatabase($GLOBALS['babUrl']						, 'UTF-8'));

		$this->AddPage();
		
		$this->addSellerContactInfos();
		$this->setY($this->getY() - 10);
		$this->setX(70);
		
		$this->getBillingDelivery()->createMultiCells($this);
		$this->Ln();
		
		$this->addTitle();
		$this->addHeaderText();
		$this->getItemTable()->createMultiCells($this);
		$this->Ln();
		$this->addFooterText();
		$this->Ln();

	}

	/**
	 * Seller contact informations
	 * 
	 */
	protected function addSellerContactInfos()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$addonname = $Crm->getAddonName();
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		$name = bab_convertStringFromDatabase($registry->getValue('seller_organization_name'), 'UTF-8');
		$details = bab_convertStringFromDatabase($registry->getValue('seller_organization_details'), 'UTF-8');
		
		
		$addon = bab_getAddonInfosInstance($addonname);
		$path = new bab_Path($addon->getUploadPath());
		$path->push('seller');
		$path->push('logo');
		$images = $W->ImagePicker()->getFolderFiles($path);
		if ($images)
		{
			$images->rewind();
			if ($images->valid())
			{
				$image = $images->current();
				/*@var $image Widget_FilePickerItem */
				$this->Image($image->getFilePath()->tostring(), '', '', 30, '', '', '', 'N', true);
	
			}
		} else {
			$this->SetFont(self::FONT_FAMILY, 'B', 2+ self::FONT_SIZE);
			$this->MultiCell(0, 0, $name, 0, 'L');
			$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE);
		}
		
		$this->MultiCell(0, 0, $details, 0, 'L');
	}


	protected function addTitle()
	{
		$margins = $this->getMargins();
		$width = $this->getPageWidth() - $margins['left'] - $margins['right'];

		$this->SetFont(self::FONT_FAMILY, 'B', 7);
		$this->SetFillColor(230);
		$this->Cell($width, 4, bab_convertStringFromDatabase($this->titleCell(), 'UTF-8'), 0, 1, 'L', true);
		$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE);
		
	}



	public function setOrderTitle($title)
	{
		$this->ordertitle = $title;
	}
	
	public function setOrderType($type)
	{
		$this->ordertype = $type;
	}


	protected function titleCell()
	{
		$Crm = $this->Crm();
		return sprintf($Crm->translate('%s number: %s'), $this->ordertype, $this->ordertitle);
	}


	protected function addHeaderText()
	{
		if (!$this->order->header)
		{
			return;
		}


		$margins = $this->getMargins();
		$width = $this->getPageWidth() - $margins['left'] - $margins['right'];

		// back to default style
		$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE);

		$this->MultiCell($width, 4, bab_convertStringFromDatabase($this->order->header, 'UTF-8'), 0, 1, 'L');

	}

	protected function addFooterText()
	{
		if (!$this->order->footer)
		{
			return;
		}

		$margins = $this->getMargins();
		$width = $this->getPageWidth() - $margins['left'] - $margins['right'];

		// back to default style
		$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE);

		$this->MultiCell($width, 4, bab_convertStringFromDatabase($this->order->footer, 'UTF-8'), 0, 1, 'L');
	}
	
	
	protected function getBillingLabel()
	{
	    return $this->Crm()->translate('Billing');
	}


	protected function billing()
	{
		$coordinates = $this->getCoodinates('billing');

		if ('' === $coordinates)
		{
			return '';
		}

		$return = '<em color="#999">'.bab_toHtml($this->getBillingLabel()).'</em><br /><br />';
		$return .= $coordinates;
		return $return;
	}
	
	
	protected function getShippingLabel()
	{
	    return $this->Crm()->translate('Shipping');
	}
	

	protected function shipping()
	{
		$coordinates = $this->getCoodinates('shipping');

		if ('' === $coordinates)
		{
			return '';
		}

		$return = '<em color="#999">'.bab_toHtml($this->getShippingLabel()).'</em><br /><br />';
		$return .= $coordinates;
		return $return;
	}
	
	
	protected function getDeliveryLabel()
	{
	    return $this->Crm()->translate('Delivery');
	}

	protected function delivery()
	{
		$coordinates = $this->getCoodinates('delivery');

		if ('' === $coordinates)
		{
			return '';
		}

		$return = '<em color="#999">'.bab_toHtml($this->getDeliveryLabel()).'</em><br /><br />';
		$return .= $coordinates;
		
		return $return;
	}



	/**
	 *
	 * @param string $prefix
	 * @return string html
	 */
	protected function getCoodinates($prefix)
	{
		$order = $this->order;
		$organizationname 	= $prefix.'organizationname';
		$person 			= $prefix.'person';
		$address 			= $prefix.'address';

		if (!($order->$address instanceof crm_Address)) {
			throw new Exception('missing join on '.$address);
		}

		$organizationname 	= $order->$organizationname;
		$person 			= $order->$person;
		$address 			= $order->$address;

		$return = '';
		if ($organizationname) {
			$return .= "<strong>$organizationname</strong><br />";
		}

		if ($person) {
			$return .= "<strong>".bab_toHtml($person, BAB_HTML_BR)."</strong><br />";
		}

		if (!$address->isEmpty()) {
			$return .= bab_toHtml($address->street, BAB_HTML_BR).'<br />';
			$return .= $address->postalCode;
			if ($address->city) {
				$return .= ' '.$address->city;
			}

			if ($address->complement) {
				$return .= ' '.$address->cityComplement;
			}

			$return .= '<br />';

			if ($address->state) {
				$return .= $address->state.'<br />';
			}

			if ($country = $address->getCountry())
			{
				$return .= $country->getName().'<br />';
			}
		}


		if (empty($return))
		{
			return '';
		}


		return $return;
	}



	protected function getBillingDelivery()
	{
		$table = bab_functionality::get('TcPdf')
			->getTableHelper()
			->setBorder(1);
		
		/*@var $table LibPdf_table */
		
		$margin = $this->getMargins();
		$table->setTableWith($this->w - $this->getX() - $margin['right'], '');

		$billing	= $table->createColumn()->setCellWith(50, '%');
		$delivery 	= $table->createColumn()->setCellWith(50, '%');

		$r = $table->createRow();

		$r->createCell()->inColumn($billing)	->setData(bab_convertStringFromDatabase($this->billing()  ,'UTF-8'))->isHtml(true);
		$r->createCell()->inColumn($delivery)	->setData(bab_convertStringFromDatabase($this->delivery() ,'UTF-8'))->isHtml(true);

		return $table;
	}


	/**
	 *
	 * @return unknown_type
	 */
	protected function getItemTable() {


		$table = bab_functionality::get('TcPdf')
			->getTableHelper()
			->setBorder(1);

		$this->itemTableAddHeader($table);
		$this->itemTableAddBody($table);
		$this->itemTableAddBodyTotal($table);
		$this->itemTableAddTaxes($table);
		$this->itemTableAddFooter($table);

		return $table;
	}
	
	
	/**
	 * Default method for columns creation
	 */
	protected function createColumns(LibPdf_table $table)
	{
	    $this->reference	= $table->createColumn()->setCellWith(-1);
	    $this->name 		= $table->createColumn()->setCellWith(0);
	 // $this->type         = $table->createColumn()->setCellWith(2, '%'); // type can be used by extended class
	    $this->quantity		= $table->createColumn()->setAlignment('R')->setCellWith(4, '%');
	    $this->unit_cost_df	= $table->createColumn()->setAlignment('R')->setCellWith(8, '%');
	    $this->reduction	= $table->createColumn()->setAlignment('R')->setCellWith(8, '%');
	    $this->unit_red_df	= $table->createColumn()->setAlignment('R')->setCellWith(12, '%'); // prix unitaire HT + remise
	    $this->vat			= $table->createColumn()->setAlignment('R')->setCellWith(6, '%');
	    $this->unit_cost_ti	= $table->createColumn()->setAlignment('R')->setCellWith(8, '%'); // prix unitaire TTC
	    $this->total_ti		= $table->createColumn()->setAlignment('R')->setCellWith(10, '%'); // Total TTC
	    	  
	}
	
	
	
	
	protected function getReferenceHead()
	{
	    return $this->Crm()->translate('Reference');
	}
	
	protected function getMarketplaceCustomerHead()
	{
	    return $this->Crm()->translate('Customer');
	}
	
	protected function getNameHead()
	{
	    return $this->Crm()->translate('Product description');
	}
	
	protected function getTypeHead()
	{
	    return $this->Crm()->translate('Product type');
	}
	
	protected function getMarketplaceTotalHead()
	{
	    return $this->Crm()->translate('Allowance');
	}
	
	protected function getMarketplaceRateHead()
	{
	    return $this->Crm()->translate('Rate');
	}
	
	protected function getQuantityHead()
	{
	    return $this->Crm()->translate('Qty.');
	}
	
	protected function getUnitCostDfHead()
	{
	    return $this->Crm()->translate('UC excl tax');
	}
	
	protected function getReductionHead()
	{
	    return $this->Crm()->translate('Reduction %');
	}
	
	protected function getUnitRedDfHead()
	{
	    return $this->Crm()->translate('UC excl tax reduc.');
	}
	
	protected function getVatHead()
	{
	    return $this->Crm()->translate('VAT %');
	}
	
	protected function getUnitCostTiHead()
	{
	    return $this->Crm()->translate('UC incl tax');
	}
	
	protected function getTotalTiHead()
	{
	    return $this->Crm()->translate('Amount incl tax');
	}
	


	/**
	 * add header to table
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddHeader(LibPdf_table $table)
	{
		$Crm = $this->Crm();
		

        $this->createColumns($table);

		$r = $table->createRow()->setBackgroundColor(240, 240, 240);

		if (isset($this->reference)) {
		    $r->createCell()
		    ->inColumn($this->reference)
		    ->setData(bab_convertStringFromDatabase($this->getReferenceHead(), 'UTF-8'));
		}
		

		if (isset($this->marketplaceCustomer)) {
		    $r->createCell()
		    ->inColumn($this->marketplaceCustomer)
		    ->setData(bab_convertStringFromDatabase($this->getMarketplaceCustomerHead(), 'UTF-8'));
		}
		
		if (isset($this->name)) {
		    $r->createCell()
		    ->inColumn($this->name)
		    ->setData(bab_convertStringFromDatabase($this->getNameHead(), 'UTF-8'));
		}

		
		if (isset($this->type)) {
		    $r->createCell()
		    ->inColumn($this->type)
		    ->setData(bab_convertStringFromDatabase($this->getTypeHead(), 'UTF-8'));
		}
		

		if (isset($this->quantity)) {
		    $r->createCell()
		    ->inColumn($this->quantity)
		    ->setData(bab_convertStringFromDatabase($this->getQuantityHead(), 'UTF-8'));
		}
		
		if (isset($this->marketplaceTotal)) {
		    $r->createCell()
		    ->inColumn($this->marketplaceTotal)
		    ->setData(bab_convertStringFromDatabase($this->getMarketplaceTotalHead(), 'UTF-8'));
		}
		
		if (isset($this->marketplaceRate)) {
		    $r->createCell()
		    ->inColumn($this->marketplaceRate)
		    ->setData(bab_convertStringFromDatabase($this->getMarketplaceRateHead(), 'UTF-8'));
		}

		
		if (isset($this->unit_cost_df)) {
		    $r->createCell()
		    ->inColumn($this->unit_cost_df)
		    ->setData(bab_convertStringFromDatabase($this->getUnitCostDfHead(), 'UTF-8'));
		}
		
		
		if (isset($this->reduction)) {
		    $r->createCell()
		    ->inColumn($this->reduction)
		    ->setData(bab_convertStringFromDatabase($this->getReductionHead(), 'UTF-8'));
		}
		
		if (isset($this->unit_red_df)) {
		    $r->createCell()
		    ->inColumn($this->unit_red_df)
		    ->setData(bab_convertStringFromDatabase($this->getUnitRedDfHead(), 'UTF-8')); 
		}
		
		if (isset($this->vat)) {
		    $r->createCell()
		    ->inColumn($this->vat)
		    ->setData(bab_convertStringFromDatabase($this->getVatHead(), 'UTF-8'));
		}
		
		if (isset($this->unit_cost_ti)) {
		    $r->createCell()
		    ->inColumn($this->unit_cost_ti)
		    ->setData(bab_convertStringFromDatabase($this->getUnitCostTiHead(), 'UTF-8'));
		}
		
		if (isset($this->total_ti)) {
		    $r->createCell()
		    ->inColumn($this->total_ti)
		    ->setData(bab_convertStringFromDatabase($this->getTotalTiHead(), 'UTF-8'));
		}
		


	}




	/**
	 * Display VAT column if one or more order item contain a VAT
	 * @return bool
	 */
	protected function displayVatColumn(crm_Order $order)
	{
		if (null === $this->vatColumn)
		{
			$this->vatColumn = false;
			
			foreach($order->getItems() as $item)
			{
				if ('0.0000' !== $item->vat)
				{
					$this->vatColumn = true;
					break;
				}
			}
		}

		return $this->vatColumn;
	}





	/**
	 * add products body
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddBody(LibPdf_table $table)
	{
		$items = $this->order->getItems();
		
		if (0 == $items->count())
		{
			return null;
		}
		
		
		foreach ($items as $item) {

			$r = $table->createRow()->setBorder('LR');

			if (isset($this->reference)) {
			     $r->createCell()->inColumn($this->reference)->setData(bab_convertStringFromDatabase($this->referenceCell($item), 'UTF-8'))->isHtml(true);
			}
			
			if (isset($this->marketplaceCustomer)) {
			    $r->createCell()->inColumn($this->marketplaceCustomer)->setData(bab_convertStringFromDatabase($this->marketplaceCustomerCell($item), 'UTF-8'));
			}
			
			if (isset($this->name)) {
			     $r->createCell()->inColumn($this->name)->setData(bab_convertStringFromDatabase($this->itemNameCell($item), 'UTF-8'))->isHtml(true);
			}
			
			if (isset($this->type)) {
			    $r->createCell()->inColumn($this->type)->setData(bab_convertStringFromDatabase($this->itemTypeCell($item), 'UTF-8'));
			}
			
			if (isset($this->quantity)) {
			    $r->createCell()->inColumn($this->quantity)->setData(bab_convertStringFromDatabase($this->quantityCell($item), 'UTF-8'));
			}
			
			if (isset($this->marketplaceTotal)) {
			    $r->createCell()->inColumn($this->marketplaceTotal)->setData(bab_convertStringFromDatabase($this->marketplaceTotalCell($item), 'UTF-8'));
			}
			
			if (isset($this->marketplaceRate)) {
			    $r->createCell()->inColumn($this->marketplaceRate)->setData(bab_convertStringFromDatabase($this->marketplaceRateCell($item), 'UTF-8'));
			}
			
			if (isset($this->unit_cost_df)) {
			     $r->createCell()->inColumn($this->unit_cost_df)->setData(bab_convertStringFromDatabase($this->unitCostCell($item, false, false), 'UTF-8'));
			}
			
			if (isset($this->reduction)) {
			     $r->createCell()->inColumn($this->reduction)->setData(bab_convertStringFromDatabase($this->reductionCell($item)	, 'UTF-8'));
			}
			
			if (isset($this->unit_red_df)) {
			     $r->createCell()->inColumn($this->unit_red_df)	->setData(bab_convertStringFromDatabase($this->unitCostCell($item, true, false), 'UTF-8'));
			}
			
			if (isset($this->vat)) {
			     $r->createCell()->inColumn($this->vat)->setData(bab_convertStringFromDatabase($this->vatCell($item), 'UTF-8'));
			}
			
			if (isset($this->unit_cost_ti)) {
			     $r->createCell()->inColumn($this->unit_cost_ti)->setData(bab_convertStringFromDatabase($this->unitCostCell($item, true, true), 'UTF-8'));
			}
			
			if (isset($this->total_ti)) {
			     $r->createCell()->inColumn($this->total_ti)->setData(bab_convertStringFromDatabase($this->totalTiCell($item), 'UTF-8'));
			}

		}
		
		$r->setBorder('LRB');

	}



	protected function createBottomTitleCell(LibPdf_row $r)
	{
		$cell = $r->createCell();
		
		if (isset($this->reference)) {
		    $cell->inColumn($this->reference);
		}
		
		if (isset($this->marketplaceCustomer)) {
		    $cell->inColumn($this->marketplaceCustomer);
		}
		
		if (isset($this->name)) {
		    $cell->inColumn($this->name);
		}
		
		if (isset($this->type)) {
		    $cell->inColumn($this->type);
		}
		
		if (isset($this->marketplaceTotal)) {
		    $cell->inColumn($this->marketplaceTotal);
		}
			
		if (isset($this->marketplaceRate)) {
		    $cell->inColumn($this->marketplaceRate);
		}
		
		if (isset($this->quantity)) {
		    $cell->inColumn($this->quantity);
		}
		
		if (isset($this->unit_cost_df)) {
		    $cell->inColumn($this->unit_cost_df);
		}
		
		if (isset($this->reduction)) {
		    $cell->inColumn($this->reduction);
		}
		
		if (isset($this->unit_red_df)) {
		    $cell->inColumn($this->unit_red_df);
		}
		
		if (isset($this->vat)) {
		    $cell->inColumn($this->vat);
		}
		
		if (isset($this->unit_cost_ti)) {
		    $cell->inColumn($this->unit_cost_ti);
		}
		
		return $cell;
	}



	/**
	 * add duty-free total
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddBodyTotal(LibPdf_table $table)
	{
		$Crm = $this->Crm();
		$r = $table->createRow()->setBorder(0);
		$cell = $this->createBottomTitleCell($r);
		
		$cell->setData(bab_convertStringFromDatabase($Crm->translate('Total duty-free').$this->colon(), 'UTF-8'));
		

		$cell->setAlignment('R');

		$r->createCell()->inColumn($this->total_ti)	->setData(bab_convertStringFromDatabase($this->currency($Crm->numberFormat($this->order->total_df))	, 'UTF-8'));

	}



	/**
	 * add taxes
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddTaxes(LibPdf_table $table)
	{
		foreach ($this->order->getTaxes() as $item) {

			$r = $table->createRow()->setBorder(0);
			$cell = $this->createBottomTitleCell($r);
			$cell->setData(bab_convertStringFromDatabase($this->taxNameCell($item), 'UTF-8'))->setAlignment('R');


			$r->createCell()->inColumn($this->total_ti)		->setData(bab_convertStringFromDatabase($this->taxAmountCell($item)		, 'UTF-8'));

		}

	}


	/**
	 * add tax included total
	 * @param LibPdf_table $table
	 * @return unknown_type
	 */
	protected function itemTableAddFooter(LibPdf_table $table)
	{
		$Crm = $this->Crm();
		$r = $table->createRow()->setBorder(0);
		$cell = $this->createBottomTitleCell($r);
		$cell->setData(bab_convertStringFromDatabase($Crm->translate('Total taxes included').$this->colon()								, 'UTF-8'))->setFontStyle('B')->setAlignment('R');
		$r->createCell()->inColumn($this->total_ti)		->setData(bab_convertStringFromDatabase($this->currency($this->Crm()->numberFormat($this->order->total_ti))	, 'UTF-8'))->setFontStyle('B');

	}




	/**
	 * Reference
	 * @param crm_OrderItem $item
	 * @return string	HTML
	 */
	protected function referenceCell(crm_OrderItem $item)
	{
		return bab_toHtml($item->reference);
	}
	
	
	/**
	 * In this cell, the customer name, for a market place fees invoice
	 * @param crm_OrderItem $item
	 * @return string	HTML
	 */
	protected function marketplaceCustomerCell(crm_OrderItem $item)
	{
	    return '';
	}


	/**
	 * Item Name
	 * @param crm_OrderItem $item
	 * @return string	HTML
	 */
	protected function itemNameCell(crm_OrderItem $item)
	{
		if ($item->subtitle)
		{
			$html = sprintf('<strong>%s</strong><br />%s', bab_toHtml($item->name), bab_toHtml($item->subtitle));
		} else {
			$html = bab_toHtml($item->name);
		}
		
		if (!empty($item->packaging))
		{
			$html .= '<br /><em style="color:#aaa">'.bab_toHtml($item->packaging).'</em>';
		}
		
		return $html;
	}
	
	
	/**
	 * Product type cell
	 * @return string
	 */
	protected function itemTypeCell(crm_OrderItem $item)
	{
	    return '';
	}
	
	
	/**
	 * Total allowance for a marketplace fees invoice
	 * @return string
	 */
	protected function marketplaceTotalCell(crm_OrderItem $item)
	{
	    return '';
	}
	
	
	/**
	 * fees rate for a marketplace fees invoice
	 * @return string
	 */
	protected function marketplaceRateCell(crm_OrderItem $item)
	{
	    return '0 %';
	}


	/**
	 * Unit cost
	 * @param crm_OrderItem $item
	 * @param bool			$reduction
	 * @param bool			$taxincl
	 * @return string	Text
	 */
	protected function unitCostCell(crm_OrderItem $item, $reduction, $taxincl)
	{
		$unit_cost = (float) $item->unit_cost;
		
		if (!$reduction)
		{
			$p = (int) round($item->reduction *100);
			
			if (0 !== $p)
			{
				// retirer la remise du prix unitaire
				
				$r = 1 - (((float) $item->reduction) / 100);
				$unit_cost = ($unit_cost / $r);
			}
		}
		
		
		if ($taxincl)
		{
			$p = (int) round($item->vat *100);
			if (0 !== $p)
			{
			
				// ajouter la TVA au prix unitaire
				
				$vat = ((float) $item->vat / 100);
				$unit_cost += ($vat * $unit_cost);
			}
		}
		
		return $this->currency($this->Crm()->numberFormat($unit_cost));
	}


	/**
	 * Quantity
	 * @param crm_OrderItem $item
	 * @return string	Text
	 */
	protected function quantityCell(crm_OrderItem $item)
	{
		return $this->Crm()->numberFormat($item->quantity, 0);
	}

	/**
	 * Reduction %
	 * @param crm_OrderItem $item
	 * @return string	Text
	 */
	protected function reductionCell(crm_OrderItem $item)
	{
		return $this->Crm()->numberFormat($item->reduction).'%';
	}
	

	/**
	 * VAT
	 * @param crm_OrderItem $item
	 * @return string	Text
	 */
	protected function vatCell(crm_OrderItem $item)
	{
		return $this->Crm()->numberFormat($item->vat).'%';
	}
	
	/**
	 * Total tax incl
	 * @param crm_OrderItem $item
	 * @return string	Text
	 */
	protected function totalTiCell(crm_OrderItem $item)
	{
		return $this->currency($this->Crm()->numberFormat($item->getTotalTI()));
	}

	/**
	 * Total duty-free
	 * @param crm_OrderItem $item
	 * @return string	Text
	 */
	protected function totalDfCell(crm_OrderItem $item)
	{
		return $this->currency($this->Crm()->numberFormat($item->getTotalDF()));
	}




	/**
	 * Tax Name
	 * @param crm_OrderTax $item
	 * @return string	HTML
	 */
	protected function taxNameCell(crm_OrderTax $tax)
	{
		return $tax->name.$this->colon();
	}


	/**
	 * Tax amount
	 * @param crm_Ordertax 	$tax
	 * @return string		Text
	 */
	protected function taxAmountCell(crm_OrderTax $tax)
	{
		return $this->currency($this->Crm()->numberFormat($tax->amount));
	}






	/**
	 * return same string with currency symbol dependant of order configuration
	 * @param	string	$amount
	 * @return string
	 */
	protected function currency($amount)
	{
		if (isset($this->order->currency))
		{
			$currency = $this->order->currency;
			
			if ($currency instanceof crm_Currency)
			{
				return $amount.$currency->symbol;
			}
		}
		
		return $amount.$this->Crm()->Ui()->Euro();
	}


	/**
 	 * This method is used to render the page footer.
 	 * It is automatically called by AddPage() and could be overwritten in your own inherited class.
	 */
	public function Footer() {
		$cur_y = $this->GetY();
		$ormargins = $this->getOriginalMargins();
		$this->SetTextColor(0, 0, 0);
		
		$Crm = $this->Crm();
		$addon = $Crm->getAddonName();
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addon/configuration/");
		$footer_text = bab_convertStringFromDatabase($registry->getValue('seller_organization_footer'), 'UTF-8');
		
		$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, 6);
		
		$this->MultiCell(0, 0, $footer_text, 'T', 'L');
		
		$this->SetFont(self::FONT_FAMILY, self::FONT_STYLE, self::FONT_SIZE);

		$pagenumtxt = $this->l['w_page']." ".$this->PageNo().' / '.$this->getAliasNbPages();
		$this->SetY($cur_y);
		//Print page number
		$this->SetX($ormargins['left']);
		$this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
		
	}



}


