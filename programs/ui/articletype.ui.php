<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of articletype of articles from back office
 *
 */
class crm_ArticleTypeTableView extends crm_TableModelView
{
	
	public function addDefaultColumns($set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		$this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
	
		return $this;
	}
	
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();

		/*@var $Ui crm_Ui */

		$editAction = $Crm->Controller()->ArticleType()->edit($record->id);

		switch ($fieldPath) {

			case '_edit_':
				return $W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $editAction);
				break;
		}
		
		

		return parent::computeCellContent($record, $fieldPath);
	}







}







/**
 * 
 */
class crm_ArticleTypeEditor extends crm_Editor
{
	/**
	 * 
	 * @var crm_ArticleType
	 */
	protected $articletype = null;
	
	
	public function __construct(Func_Crm $Crm, crm_ArticleType $articletype = null, $id = null, Widget_Layout $layout = null)
	{
		$this->articletype = $articletype;
		
		parent::__construct($Crm, $id, $layout);
		$this->setName('articletype');
		$this->colon();
		
		$this->addFields();
		$this->addButtons();
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		if (isset($articletype)) {
			$this->setHiddenValue('articletype[id]', $articletype->id);
			$this->setValues($articletype->getValues(), array('articletype'));
		}
	}
	
	
	protected function addFields()
	{
		$this->addItem($this->name());
		$this->addItem($this->description());
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$id = null !== $this->articletype ? $this->articletype->id : null;
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->ArticleType()->save())
				->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
				->setFailedAction($Crm->Controller()->ArticleType()->edit($id))
		);
		
		
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction(crm_BreadCrumbs::getPosition(-1))
		);
	}
	
	
	protected function name()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Name'),
				$W->LineEdit()->setSize(70)->setMaxSize(100)->setMandatory(true, $Crm->translate('The name is mandatory')),
				__FUNCTION__
		);
	}
	
	
	protected function description()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Description'),
				$W->TextEdit(),
				__FUNCTION__
		);
	}
	
}



