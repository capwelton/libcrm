<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of email signature of articles from back office
 *
 */
class crm_EmailSignatureTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();

		$editAction = $Crm->Controller()->EmailSignature()->edit($record->id);
		$deleteAction = $Crm->Controller()->EmailSignature()->confirmDelete($record->id);

		switch ($fieldPath) {
			case 'signature':
				return $W->Html($record->signature);
				break;
			case '_edit_':
			case '_actions_':
				return $W->HBoxItems(
					$W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $editAction)->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
					$W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $deleteAction)->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
				)->setHorizontalSpacing(.5, 'em');
				break;

			case 'mandatory':
			case 'visible_in_shop':
				if (self::getRecordFieldValue($record, $fieldPath))
				{
					return $W->Label($Crm->translate('Yes'));
				} else {
					return $W->Label($Crm->translate('No'));
				}
				break;
		}

		return parent::computeCellContent($record, $fieldPath);
	}



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $Crm = $this->Crm();

        $this->addColumn(
            crm_TableModelViewColumn($recordSet->name)
        );
        $this->addColumn(
            crm_TableModelViewColumn($recordSet->signature)
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center')
        );
		
		$this->addClass(Func_Icons::ICON_LEFT_16);
    }
}







/**
 *
 */
class crm_EmailSignatureEditor extends crm_Editor
{
	/**
	 *
	 * @var crm_EmailSignature
	 */
	protected $emailSignature = null;


	public function __construct(Func_Crm $Crm, crm_EmailSignature $emailSignature = null, $id = null, Widget_Layout $layout = null)
	{
		$this->emailSignature = $emailSignature;

		parent::__construct($Crm, $id, $layout);
		$this->setName('emailSignature');
		$this->colon();

		$this->addFields();
//		$this->addButtons();

		$this->setHiddenValue('tg', bab_rp('tg'));;
		if (isset($emailSignature)) {
			$this->setHiddenValue('emailSignature[id]', $emailSignature->id);
			$values = $emailSignature->getValues();

			$this->setValues($values, array('emailSignature'));
		}
	}
	
	
	public function setRecord($record)
	{
		$this->setHiddenValue('data[id]', $record->id);
		parent::setRecord($record);
	}


	protected function addFields()
	{
		$Crm = $this->Crm();

		$this->addItem($this->name());
		$this->addItem($this->signature());
		$this->addItem($this->warning());
	}


// 	protected function addButtons()
// 	{
// 		$Crm = $this->Crm();
// 		$W = $this->widgets;

// 		$id = null !== $this->emailSignature ? $this->emailSignature->id : null;

// 		$this->addButton(
// 				$W->SubmitButton()
// 				->setLabel($Crm->translate('Save'))
// 				->validate(true)
// 				->setAction($Crm->Controller()->EmailSignature()->save())
// 				->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
// 				->setFailedAction($Crm->Controller()->EmailSignature()->edit($id))
// 		);



// 		$this->addButton(
// 				$W->SubmitButton()
// 				->setLabel($Crm->translate('Cancel'))
// 				->setAction(crm_BreadCrumbs::getPosition(-1))
// 		);
// 	}


	protected function warning()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $W->Label($Crm->translate('Warning, if you want the signature to display correctly in the mail we advise you to not use copy/paste from external sources.'));
	}


	protected function name()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Name'),
				$W->LineEdit()->setSize(70)->setMaxSize(100)->setMandatory(true, $Crm->translate('The name is mandatory')),
				__FUNCTION__
		);
	}


	protected function signature()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;

		return $this->labelledField(
				$Crm->translate('Description'),
				$W->BabHtmlEdit()->setEmail(true),
				__FUNCTION__
		);
	}
}
