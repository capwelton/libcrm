<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of search catalog from back office
 *
 */
class crm_SearchCatalogTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();

		/*@var $Ui crm_Ui */

		$editAction = $Crm->Controller()->SearchCatalog()->edit($record->id);

		switch ($fieldPath) {
			
			case '_edit_':
				return $W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), $editAction);
				break;
		}
		
		

		return parent::computeCellContent($record, $fieldPath);
	}



	public function addDefaultColumns(crm_SearchCatalogSet $set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn('_edit_', $Crm->translate('Edit'))->addClass('widget-column-thin')->addClass('widget-column-center'));
		$this->addColumn(widget_TableModelViewColumn($set->parentcatalog->name, $Crm->translate('Parent catalog'))->setSearchable(true));
		$this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name'))->setSearchable(true)->setVisible(true));
		$this->addColumn(widget_TableModelViewColumn($set->description, $Crm->translate('Description')));
		$this->addColumn(widget_TableModelViewColumn($set->catalogitem_displaymode, $Crm->translate('Products display'))->setVisible(false)->setSearchable(false));
	}



}







/**
 * 
 */
class crm_SearchCatalogEditor extends crm_MetaEditor
{
	/**
	 * 
	 * @var crm_SearchCatalog
	 */
	protected $searchcatalog = null;
	
	
	public function __construct(Func_Crm $Crm, crm_SearchCatalog $searchcatalog = null, $id = null, Widget_Layout $layout = null)
	{
		$this->searchcatalog = $searchcatalog;
		
		parent::__construct($Crm, $id, $layout);
		$this->setName('searchcatalog');
		
		$this->addFields();
		$this->addButtons();
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		if (isset($searchcatalog)) {
			$this->setHiddenValue('searchcatalog[id]', $searchcatalog->id);
			$this->setValues($searchcatalog->getValues(), array('searchcatalog'));
		}
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();

		$this->addItem($W->Hidden()->setName('id'));

		$this->addItem(
			$W->HBoxItems(
				$W->VBoxItems(
					$this->Name(),
					$this->Description(),
					$this->search_url(),
					$this->parentcatalog(),
					$this->catalogitem_displaymode() 
				)->setSpacing(1, 'em'),
				$this->Photo()
			)->setHorizontalSpacing(5, 'em')
		);
		
		
		$this->addItem(
			$this->MetaSection()		
		);
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$id = null !== $this->searchcatalog ? $this->searchcatalog->id : null;
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->SearchCatalog()->save())
				->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
				->setFailedAction($Crm->Controller()->SearchCatalog()->edit($id))
		);
		
		
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction(crm_BreadCrumbs::getPosition(-1))
		);
	}
	
	
	
	
	protected function catalogitem_displaymode()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		return $this->labelledField(
				$Crm->translate('Products display'),
				$W->RadioSet()
					->setHorizontalView()
					->setOptions(crm_CatalogSet::getDisplayModes()),
				__FUNCTION__
		);
	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function Name()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $this->labelledField(
			$Crm->translate('Name'),
			$W->LineEdit()->setSize(30)->setMandatory(),
			'name'
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function Description()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $this->labelledField(
			$Crm->translate('Description'),
			$W->TextEdit(),
			'description'
		);
	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function Photo()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$filePicker = $W->FilePicker()
				->setTitle($Crm->translate('Set photo'))
				->oneFileMode();
		
		if (isset($this->searchcatalog))
		{
			$filePicker->setFolder($this->searchcatalog->getPhotoFolder());
		}

		return $this->labelledField(
			$Crm->translate('Category image'),
			$filePicker,
			'photo'
		);

	}
	
	
	
	protected function search_url()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		return $this->labelledField(
				$Crm->translate('Search URL'),
				$W->LineEdit()->setSize(40)->setMandatory(),
				__FUNCTION__
		);
	}
	
	
	
	protected function parentcatalog()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$set = $Crm->CatalogSet();
		
		return $this->labelledField(
				$Crm->translate('Parent category'),
				$W->Select()->setOptions($set->getAllOptions()),
				__FUNCTION__
		);
	}
}



