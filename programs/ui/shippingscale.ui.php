<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of shipping scale from back office
 *
 */
class crm_ShippingScaleTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();

		/*@var $Ui crm_Ui */

		$editAction = $Crm->Controller()->ShippingScale()->edit($record->id);

		switch ($fieldPath) {
			
			case 'shipping_amount':
				return $W->Label($Crm->numberFormat($record->shipping_amount,2).' '.$Ui->Euro());
				break;
			
			case 'weight':
				return $W->Label($record->getWeight());
				break;
			
			case '_edit_':
				return $W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $editAction);
				break;
				

			case 'country/name_fr':
			case 'country/name_en':
				
				if (empty($record->country->id))
				{
					return $W->Label($Crm->translate('Others countries'));
				}
							
				$flag = $W->CountryFlag($record->country->code);
				if ($flag)
				{
					return $W->HBoxItems($flag, $W->Label($record->country->getName()))->setHorizontalSpacing(.3,'em');
				}
				break;
				
				
			case 'department':
				if ('' === $record->department)
				{
					return $W->Label($Crm->translate('Others departments'));
				}
				break;
			
		}
		
		

		return parent::computeCellContent($record, $fieldPath);
	}







}







/**
 * 
 */
class crm_ShippingScaleEditor extends crm_Editor
{
	/**
	 * 
	 * @var crm_ShippingScale
	 */
	protected $shippingscale = null;
	
	
	public function __construct(Func_Crm $Crm, crm_ShippingScale $shippingscale = null, $id = null, Widget_Layout $layout = null)
	{
		$this->shippingscale = $shippingscale;
		
		parent::__construct($Crm, $id, $layout);
		$this->setName('shippingscale');
		
		$this->addFields();
		$this->addButtons();
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		if (isset($shippingscale)) {
			$this->setHiddenValue('shippingscale[id]', $shippingscale->id);
			$this->setValues($shippingscale->getValues(), array('shippingscale'));
		}
	}
	
	
	protected function addFields()
	{
	    $this->addItem($this->comment());
		$this->addItem($this->shipping_amount());
		$this->addItem($this->weight());
		$this->addItem($this->department());
		$this->addItem($this->country());
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$id = null !== $this->shippingscale ? $this->shippingscale->id : null;
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->ShippingScale()->save())
				->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
				->setFailedAction($Crm->Controller()->ShippingScale()->edit($id))
		);
		
		
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction(crm_BreadCrumbs::getPosition(-1))
		);
	}
	
	
	protected function comment()
	{
	    $Crm = $this->Crm();
	    $W = $this->widgets;
	    
	    return $this->labelledField(
	        $Crm->translate('Comment'),
	        $W->LineEdit()->addClass('widget-100pc'),
	        __FUNCTION__
	    );
	}
	
	
	protected function shipping_amount()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Shipping amount'),
				$W->LineEdit()->setSize(5)->setMaxSize(16)->setMandatory(true, $Crm->translate('The shipping amount is mandatory')),
				__FUNCTION__,
				null,
				$Crm->Ui()->Euro().' '.$Crm->translate('incl tax')
		);
	}
	
	protected function weight()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Shopping cart total weight'),
				$W->LineEdit()->setSize(5)->setMaxSize(16)->setMandatory(true, $Crm->translate('The weight is mandatory')),
				__FUNCTION__,
				$Crm->translate('The shipping amount will be used for shopping carts up to this weight (included)'),
				'Kg'
		);
	}
	
	
	
	protected function department()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Department'),
				$W->LineEdit()->setSize(3)->setMaxSize(2),
				__FUNCTION__,
				$Crm->translate('First two characters of delivery address postal code, if the department is empty, the shipping cost will be appliquable for all departments')
		);
	}
	
	
	protected function country()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$CSet = $Crm->CountrySet();
		$options = $CSet->getOptions();
		$options[0] = $Crm->translate('Others countries');
		
		return $this->labelledField(
				$Crm->translate('Country of delivery address'),
				$W->Select()->setOptions($options),
				__FUNCTION__
		);
	}
}



