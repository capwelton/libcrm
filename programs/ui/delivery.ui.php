<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


class crm_ShoppingCartDeliverySelect extends crm_UiObject
{
    protected $cartRecord;
    
    /**
     * @param Func_Crm $Crm
     * @param string   $id
     */
    public function __construct(Func_Crm $Crm, $id = null)
    {
        parent::__construct($Crm);
    
        $W = bab_Widgets();
    
        $set = $Crm->ShoppingCartSet();
        $this->cartRecord = $set->getMyCart();
        
        $this->setInheritedItem(
            $W->Frame(null, $this->deliveryInformation())
        );
    }
    
    
    
    /**
     * @return Widget_Layout
     */
    public function deliveryInformation()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
    
        $Ui->includeAddress();
    
        
    
        $cartRecord = $this->cartRecord;
    
        $deliveryLayout = $W->HBoxLayout()->setHorizontalSpacing(6, 'em');
        $deliveryaddress = $cartRecord->getDeliveryAddress();
    
        if ($cartRecord->isShippable())
        {
            $shippingamount = null;
            if (null !== $deliveryaddress)
            {
                $shippingamount = $cartRecord->computeShippingCost(
                        $deliveryaddress->postalCode,
                        $deliveryaddress->getCountry()->id
                );
            }
            
            $addressesLayout = $W->VBoxItems(
                $W->Items(
                    $W->Title($Crm->translate('Delivery address'), 2),
                    $W->Label($Crm->translate('Choose the address where you want your package shipped'))
                    ),
                $this->contactAddresses()
                )->setVerticalSpacing(2, 'em');
            
            
            if ($stores = $this->collectStores()) {
                $addressesLayout->addItem($W->Items(
                    $W->Title($Crm->translate('Collect stores'), 2),
                    $W->Label($Crm->translate('Choose the address where you want to get your package'))
                ));
                
                $addressesLayout->addItem($stores);
            }
            
            
            
    
            $deliveryLayout->addItem($addressesLayout);
            
    
        } else {
            $shippingamount = 0.0;
    
            $deliveryLayout->addItem(
                    $W->Label($Crm->translate('No package to deliver for this order'))
            );
        }
    
    
    
    
    
    
        $deliveryLayout->addItem(
                $W->Items(
                        $W->Title($Crm->translate('Shipping cost'), 2),
                        $W->Label($Crm->numberFormat($shippingamount).' '.$Ui->Euro().' '.$Crm->translate('incl tax'))
                )
        );
    
    
    
        return $deliveryLayout;
    }
    
    
    
    
    

    /**
     * @return Widget_Layout
     */
    protected function contactAddresses()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
        $Ui->includeAddress();
    
        $shoppingCartSet = $Crm->ShoppingCartSet();
    
        $cartRecord = $this->cartRecord;
    
        $contact = $cartRecord->getContact();
        if (!$contact) {
            throw new crm_AccessException($Crm->translate('You are not logged in as a regular customer'));
        }
    
    
        $deliveryAddress = $contact->getDeliveryAddress();
    
        $allContactAddressesBox = $W->VBoxItems();
        $allContactAddressesBox->setVerticalSpacing(1, 'em');
    
        $typedAddresses = $contact->getAddresses();
    
        $invoiceAddress = $contact->address();
        if (!$invoiceAddress->isEmpty()) {
            $typedAddresses =  array('billingaddress' => array($invoiceAddress)) + $typedAddresses;
        }
    
        $collectStore = 0;
        if (isset($Crm->CollectStore)) {
            $collectStore = (int) $cartRecord->getFkPk('collectstore');
        }
    
        $displayedAddresses = array();
    
        foreach ($typedAddresses as $addresses) {
            	
            foreach ($addresses as $address) {
    
                /*@var $address crm_Address */
    
                if (isset($displayedAddresses[$address->id])) {
                    continue;
                }
                $displayedAddresses[$address->id] = $address->id;
    
                if (empty($address->recipient))
                {
                    $addressLabel = $contact->getParentSet()->title->output($contact->title) . ' ' . $contact->getFullName();
                } else {
                    $addressLabel = null;
                }
                
                $selected = (!$collectStore && $deliveryAddress->id == $address->id);
    
                $radioaddress = $this->deliveryAddressFrame($address, $addressLabel, $selected, $invoiceAddress->id == $address->id);
                $allContactAddressesBox->addItem($radioaddress);
            }
        }
    
    
        $allContactAddressesBox->addItem($buttonsLayout = $W->FlowLayout()->setSpacing(1, 'em'));
    
    
        // add address button
    
        $newDeliveryAddressForm = $W->Form();
        $newDeliveryAddressForm->setName('address');
        $newDeliveryAddressForm->setLayout(
            $W->VBoxLayout()->setVerticalSpacing(2, 'em')
        );
    
        $newDeliveryAddressForm->addItem($Ui->MandatoryAddressEditor(true, false, true));
    
        $addAddressButton = $W->VboxItems(
            $W->Link($Crm->translate('Add an address'))
            ->setSizePolicy(Func_Icons::ICON_LEFT_16)
            ->addClass(Func_Icons::ACTIONS_LIST_ADD . ' icon widget-icon crm-shop-deliverybutton widget-instant-button'),
    
            $newDeliveryAddressForm
            ->addClass('widget-instant-form')
    
        )->addClass('widget-instant-container');

        $newDeliveryAddressForm->addItem(
            $W->SubmitButton()
            ->setAjaxAction($Crm->Controller()->MyContact()->saveDeliveryAddress(), $addAddressButton)
            ->setLabel($Crm->translate('Add this address'))
        );



        $buttonsLayout->addItem($addAddressButton);

        // set gift message button


        $giftCommentForm = $W->Form();
        $giftCommentForm->setLayout(
            $W->VBoxLayout()->setVerticalSpacing(1, 'em')
        );


        $giftCommentForm->addItem($W->TextEdit()->setColumns(80)->setLines(5)->setName('gift_comment')->setValue($cartRecord->gift_comment));


        $giftButton = $W->VboxItems(
            $W->Link(empty($cartRecord->gift_comment) ? $Crm->translate('Set message for recipient') : $Crm->translate('Edit message for recipient'))
            ->setSizePolicy(Func_Icons::ICON_LEFT_16)
            ->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT . ' icon widget-icon crm-shop-deliverybutton widget-instant-button'),

            $giftCommentForm
            ->addClass('widget-instant-form')

        )->addClass('widget-instant-container');

        $giftCommentForm->addItem(
            $W->SubmitButton()
            ->setAjaxAction($Crm->Controller()->ShoppingCart()->saveGiftComment(), $giftButton)
            ->setLabel($Crm->translate('Save message'))
        );

        $giftCommentForm->addItem($W->Label($Crm->translate('Note: prices will not be indicated in the package')));



        $buttonsLayout->addItem($giftButton);


        return $allContactAddressesBox;
    }
    
    
    
    
    


    protected function collectStores()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
        
        $layout = $W->VBoxItems();
         
        $cartRecord = $this->cartRecord;
         
        $set = $Crm->CollectStoreSet();
        $set->address();
         
        $res = $set->select();
        $res->orderAsc($set->name);
        
        if ($res->count() === 0) {
            return null;
        }
         
        foreach ($res as $collectStore) {
            
            $selected = ($cartRecord->collectstore == $collectStore->id);
            
            $layout->addItem($this->addressFrame(
                $collectStore->name,
                $Ui->AddressFrame($collectStore->address),
                $selected,
                $this->getCollectStoreButton($collectStore)
            ));
        }
        
        
        return $layout;
    }
    
    
    
    protected function getContactAddressButton(crm_Address $address)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        
        $selectLink = $W->Link(
            $Crm->translate('Choose this address'),
            $Crm->Controller()->MyContact()->selectDeliveryAddress($address->id)
            );
        $selectLink->setAjaxAction($Crm->Controller()->MyContact()->selectDeliveryAddress($address->id), $selectLink);
        $selectLink->addClass('crm-dialog-button');
        
        return $selectLink;
    }
    
    
    protected function getCollectStoreButton(crm_CollectStore $collectStore)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
    
        $selectLink = $W->Link(
            $Crm->translate('Choose this address'),
            $Crm->Controller()->ShoppingCart()->selectCollectStore($collectStore->id)
            );
        $selectLink->setAjaxAction($Crm->Controller()->ShoppingCart()->selectCollectStore($collectStore->id), $selectLink);
        $selectLink->addClass('crm-dialog-button');
    
        return $selectLink;
    }
    
    
    
    
    protected function getAddressEditButton($address, $invoiceaddress)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $Ui = $Crm->Ui();
        
        $editAddressForm = $W->Form();
        $editAddressForm->setName('address');
        $editAddressForm->setLayout(
            $W->VBoxLayout()->setVerticalSpacing(2, 'em')
            );
        
        $editAddressForm->addItem(
            $Ui->MandatoryAddressEditor(true, false, !$invoiceaddress)
            );
        $editAddressForm->setValues($address->getValues(), array('address'));
        $editAddressForm->setHiddenValue('address[id]', $address->id);
        
        $editAddressButton = $W->VboxItems(
            $W->Link($Crm->translate('Edit this address'))
            ->setSizePolicy(Func_Icons::ICON_LEFT_16)
            ->addClass('widget-instant-button crm-shop-edit-address'),
            $editAddressForm
            ->addClass('widget-instant-form')
        
        )->addClass('widget-instant-container');
    
        $editAddressForm->setHiddenValue('tg', bab_rp('tg'));
    
        $editAddressForm->addItem(
            $W->SubmitButton()
            ->setAction($Crm->Controller()->MyContact()->saveDeliveryAddress())
            ->setLabel($Crm->translate('Save address'))
        );
        
        return $editAddressButton;
    }
    
    
    
    /**
     * 
     */
    protected function addressFrame($label, $addressWidget, $selected, $selectLink, $editButton = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
        
        $iconClass = $selected ? Func_Icons::ACTIONS_DIALOG_OK : '';       
        
        $entryButtonAddress = $W->FlowItems(
        
            $W->Icon('', $iconClass),
            $W->VBoxItems(
                $W->Label($label)->addClass('crm-strong'),
                $addressWidget,
                $editButton
                )->setSizePolicy('widget-20em'),
            $actionsBox = $W->VBoxItems()
        )
        ->addClass(Func_Icons::ICON_TOP_32 . ' crm-listitem')
        ->setHorizontalSpacing(4, 'px')
        ->setVerticalAlign('top');
    
    
        if ($selected) {
            $entryButtonAddress->addClass('crm-selected');
        } else {
            $actionsBox->addItem($selectLink);
        }
        
        return $entryButtonAddress;
    }
    


    /**
     *
     * @param crm_Address $address
     * @param string $label
     *
     */
    protected function deliveryAddressFrame($address, $label, $selected = false, $invoiceaddress = false)
    {
        bab_functionality::includeOriginal('Icons');
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
        $cartSet = $Crm->ShoppingCartSet();
        $cartRecord = $this->cartRecord;
    
        $shippingamount = null;
        if (!$address->isEmpty())
        {
            $shippingamount = $cartRecord->computeShippingCost(
                $address->postalCode,
                $address->getCountry()->id
                );
        }
    
        $selectable = isset($shippingamount);
    
        if (!$selectable) {
            $selectLink = $W->Label($Crm->translate('We do not make delivery to this address'));
            $selected = false;
        } else {
            if (!$selected) {
                $selectLink = $this->getContactAddressButton($address);
            } else {
                $selectLink = $W->Label($Crm->translate('Selected address'));
            }
        }
    


        

        return $this->addressFrame(
            $label,
            $Ui->AddressFrame($address),
            $selected,
            $selectLink,
            $this->getAddressEditButton($address, $invoiceaddress)
        );
    }
    
    
    
    
    
}