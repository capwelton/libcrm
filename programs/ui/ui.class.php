<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * The crm_Ui class
 */
class crm_Ui extends crm_Object
{




    /**
     * Includes crm Ui helper functions definitions.
     */
    public function includeCampaign()
    {
        require_once FUNC_CRM_UI_PATH . 'campaign.ui.php';
    }



    /**
     * Includes crm Ui helper functions definitions.
     */
    public function includeBase()
    {
        require_once FUNC_CRM_UI_PATH . 'base.ui.php';
        require_once FUNC_CRM_UI_PATH . 'ui.helpers.php';
    }


    /**
     * Includes Address Ui helper functions definitions.
     */
    public function includeAddress()
    {
        require_once FUNC_CRM_UI_PATH . 'address.ui.php';
    }


    /**
     * Includes Classification Ui helper functions definitions.
     */
    public function includeClassification()
    {
        require_once FUNC_CRM_UI_PATH . 'classification.ui.php';
    }


    /**
     * Includes Contact Ui helper functions definitions.
     */
    public function includeContact()
    {
        require_once FUNC_CRM_UI_PATH . 'contact.ui.php';
    }

    /**
     * Includes MyContact Ui helper functions definitions.
     */
    public function includeMyContact()
    {
        require_once FUNC_CRM_UI_PATH . 'mycontact.ui.php';
    }


    /**
     * Includes Organization Ui helper functions definitions.
     */
    public function includeOrganization()
    {
        require_once FUNC_CRM_UI_PATH . 'organization.ui.php';
    }

    /**
     * Includes Tag Ui helper functions definitions.
     */
    public function includeTag()
    {
        require_once FUNC_CRM_UI_PATH . 'tag.ui.php';
    }


    /**
     * Includes Entry Ui helper functions definitions.
     */
    public function includeEntry()
    {
        require_once FUNC_CRM_UI_PATH . 'entry.ui.php';
    }


    /**
     * Includes Status Ui helper functions definitions.
     */
    public function includeStatus()
    {
        require_once FUNC_CRM_UI_PATH . 'status.ui.php';
    }


    /**
     * Includes Emblem Ui helper functions definitions.
     */
    public function includeEmblem()
    {
        require_once FUNC_CRM_UI_PATH . 'emblem.ui.php';
    }


    /**
     * Includes Task Ui helper functions definitions.
     */
    public function includeTask()
    {
        require_once FUNC_CRM_UI_PATH . 'task.ui.php';
    }


    /**
     * Includes Task Ui helper functions definitions.
     */
    public function includeNote()
    {
        require_once FUNC_CRM_UI_PATH . 'note.ui.php';
    }


    /**
     * Includes Email Ui helper functions definitions.
     */
    public function includeEmail()
    {
        require_once FUNC_CRM_UI_PATH . 'email.ui.php';
    }


    /**
     * Includes Email Signature Ui helper functions definitions.
     */
    public function includeEmailSignature()
    {
        require_once FUNC_CRM_UI_PATH . 'emailsignature.ui.php';
    }


    /**
     * Includes Deal Ui helper functions definitions.
     */
    public function includeDeal()
    {
        require_once FUNC_CRM_UI_PATH . 'deal.ui.php';
    }

    /**
     * Includes Team Ui helper functions definitions.
     */
    public function includeTeam()
    {
        require_once FUNC_CRM_UI_PATH . 'team.ui.php';
    }

    /**
     * Includes Article Ui helper functions definitions.
     */
    public function includeArticle()
    {
        require_once FUNC_CRM_UI_PATH . 'article.ui.php';
    }

    /**
     * Includes Shopping Cart Ui helper functions definitions.
     */
    public function includeShoppingCart()
    {
        require_once FUNC_CRM_UI_PATH . 'shoppingcart.ui.php';
    }

    /**
     * Includes Shopping Cart admin Ui helper functions definitions.
     */
    public function includeShoppingCartAdm()
    {
        require_once FUNC_CRM_UI_PATH . 'shoppingcartadm.ui.php';
    }


    /**
     * Includes Shipping Scale Ui helper functions definitions.
     */
    public function includeShippingScale()
    {
        require_once FUNC_CRM_UI_PATH . 'shippingscale.ui.php';
    }

    /**
     * Includes Article type Ui helper functions definitions.
     */
    public function includeArticleType()
    {
        require_once FUNC_CRM_UI_PATH . 'articletype.ui.php';
    }

    /**
     * Includes Article availability Ui helper functions definitions.
     */
    public function includeArticleAvailability()
    {
        require_once FUNC_CRM_UI_PATH . 'articleavailability.ui.php';
    }

    /**
     * Includes Packaging Ui helper functions definitions.
     */
    public function includePackaging()
    {
        require_once FUNC_CRM_UI_PATH . 'packaging.ui.php';
    }

    /**
     * Includes CustomField Ui helper functions definitions.
     */
    public function includeCustomField()
    {
        require_once FUNC_CRM_UI_PATH . 'customfield.ui.php';
    }


    /**
     * Includes online shop back-office Ui helper functions definitions.
     */
    public function includeShopAdmin()
    {
        require_once FUNC_CRM_UI_PATH . 'shopadmin.ui.php';
    }

    /**
     * Includes online shop front-office Ui helper functions definitions.
     */
    public function includeShopUser()
    {
        require_once FUNC_CRM_UI_PATH . 'shopuser.ui.php';
    }


    public function includePasswordToken()
    {
        require_once FUNC_CRM_UI_PATH . 'passwordtoken.ui.php';
    }


    public function includePrivateSell()
    {
        require_once FUNC_CRM_UI_PATH . 'privatesell.ui.php';
    }


    public function includeDiscount()
    {
        require_once FUNC_CRM_UI_PATH . 'discount.ui.php';
    }

    public function includeCoupon()
    {
        require_once FUNC_CRM_UI_PATH . 'coupon.ui.php';
    }


    public function includeVat()
    {
        require_once FUNC_CRM_UI_PATH . 'vat.ui.php';
    }


    public function includeMailBox()
    {
        require_once FUNC_CRM_UI_PATH . 'mailbox.ui.php';
    }


    /**
     * Get ArticleDisplay class name from ArticleUi.
     * The crm_CatalogItemDisplay class will herit from this classname
     */
    protected function ArticleDisplayClassName()
    {
        // try with current CRM prefix
        $className = $this->Crm()->classPrefix.'ArticleDisplay';

        if (class_exists($className))
        {
            return $className;
        }

        return 'crm_ArticleDisplay';
    }


    /**
     * Includes Catalog Item Ui helper functions definitions.
     */
    public function includeCatalogItem()
    {
        $this->includeArticle();
        if (!class_exists('crm_ArticleDisplayFinal')) {
            eval('class crm_ArticleDisplayFinal extends '.$this->ArticleDisplayClassName().' { }');
        }
        require_once FUNC_CRM_UI_PATH . 'catalogitem.ui.php';
    }

    /**
     * Includes Gift card Ui helper functions definitions.
     */
    public function includeGiftCard()
    {
        require_once FUNC_CRM_UI_PATH . 'giftcard.ui.php';
    }


    /**
     * Includes Catalog Ui helper functions definitions.
     */
    public function includeCatalog()
    {
        require_once FUNC_CRM_UI_PATH . 'catalog.ui.php';
    }

    /**
     * Includes Search Catalog Ui helper functions definitions.
     */
    public function includeSearchCatalog()
    {
        require_once FUNC_CRM_UI_PATH . 'searchcatalog.ui.php';
    }


    /**
     * Includes Comment Ui helper functions definitions.
     */
    public function includeComment()
    {
        require_once FUNC_CRM_UI_PATH . 'comment.ui.php';
    }

    /**
     *
     */
    public function includeCollectStore()
    {
        require_once FUNC_CRM_UI_PATH . 'collectstore.ui.php';
    }


    /**
     * Includes Import Ui helper functions definitions.
     */
    public function includeImport()
    {
        require_once FUNC_CRM_UI_PATH . 'import.ui.php';
    }

    /**
     * Includes Order Ui helper functions definitions.
     */
    public function includeOrder()
    {
        require_once FUNC_CRM_UI_PATH . 'order.ui.php';
    }


    /**
     * Includes Order Ui PDF helper functions definitions.
     * require the LibPdf addon
     */
    public function includeOrderPdf()
    {
        require_once FUNC_CRM_UI_PATH . 'orderpdf.ui.php';
    }

    /**
     * Includes delivery slip Ui PDF helper functions definitions.
     * require the LibPdf addon
     */
    public function includeDeliverySlipPdf()
    {
        require_once FUNC_CRM_UI_PATH . 'deliveryslippdf.ui.php';
    }


    /**
     * Includes History Ui functions definitions.
     */
    public function includeHistory()
    {
        require_once FUNC_CRM_UI_PATH . 'history.class.php';
    }


    /**
     * Includes File Ui functions definitions.
     */
    public function includeFile()
    {
        require_once FUNC_CRM_UI_PATH . 'file.ui.php';
    }


    /**
     * Includes Role Ui functions definitions.
     */
    public function includeRole()
    {
        require_once FUNC_CRM_UI_PATH . 'role.ui.php';
    }


    /**
     * Includes celendar Ui functions definitions.
     */
    public function includeCalendar()
    {
        require_once FUNC_CRM_UI_PATH . 'calendar.ui.php';
    }


    public function includeShopTrace()
    {
        require_once FUNC_CRM_UI_PATH . 'shoptrace.ui.php';
    }


    public function includePage()
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/page.class.php';
    }


    /**
     * Includes Criteria Ui helper functions definitions.
     */
    public function includeCriteria()
    {
        require_once FUNC_CRM_UI_PATH . 'criteria.ui.php';
    }

    /**
     * Return CRM page widget
     *
     * @return crm_Page
     */
    public function Page($id = null, Widget_Layout $layout = null)
    {
        $Crm = $this->Crm();

        $this->includePage();
        $page = new crm_Page($id, $layout);
        $page->setCrm($Crm);
        $page->setNumberFormat($Crm->numberFormat(1.1111111));
        $page->addMessageInfo();

        $currentController = crm_BreadCrumbs::getCurrentController();

        if (null !== $currentController && (false === $Crm->onlineShop || ($currentController instanceof crm_ShopAdminCtrl)) && !bab_isAjaxRequest())
        {
            $page->addToolbar($this->LocationBar()); // breadcrumbs location bar displayed for CRM and online shop back-office
        }

        return $page;
    }


    /**
     * Return CRM toolbar widget
     *
     * @return crm_Toolbar
     */
    public function Toolbar($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_CRM_UI_PATH . 'base.ui.php';
        $toolbar = new crm_Toolbar($id, $layout);

        return $toolbar;
    }


    /**
     * Return CRM toolbar widget with location from breadcrumbs
     * @return crm_Toolbar
     */
    public function LocationBar()
    {
        $toolbar = $this->Toolbar();
        $toolbar->addClass('crm-locationbar');

        $W = bab_Widgets();

        foreach (crm_BreadCrumbs::getLocationItems() as $crumb) {
            $toolbar->addItem($W->Link($W->Label($crumb->label), $crumb->getAction()->url()));
        }

        return $toolbar;
    }


    /**
     * A input widget to suggest organization
     * @return crm_SuggestOrganization | Widget_SuggestLineEdit
     */
    public function SuggestOrganization($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestorganization.class.php';
        $suggest = new crm_SuggestOrganization($this->Crm(), $id);
        $suggest->setSuggestAction($this->Crm()->Controller()->Organization()->suggest(), 'search');

        return $suggest;
    }


    /**
     * A input widget to suggest a parent for an organization.
     * The suggest does not propose the specified child organization or its descendants.
     *
     * @return crm_SuggestOrganization | Widget_SuggestLineEdit
     */
    public function SuggestParentOrganization($childOrganizationId = null, $id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestorganization.class.php';
        $suggest = new crm_SuggestOrganization($this->Crm(), $id);
        $suggest->setSuggestAction($this->Crm()->Controller()->Organization()->suggest(), 'search');
        if (isset($childOrganizationId)) {
            $suggest->excludeDescendantsOf($childOrganizationId);
        }

        return $suggest;
    }


    /**
     * @return crm_SuggestMember
     */
    public function SuggestMember($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestmember.class.php';
        $suggest = new crm_SuggestMember($id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestMember(), 'search');

        return $suggest;
    }

    /**
     * @return crm_SuggestContact
     */
    public function SuggestContact($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestcontact.class.php';
        $suggest = new crm_SuggestContact($id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Contact()->suggest(), 'search');

        return $suggest;
    }


    /**
     * @return crm_SuggestCollaborator
     */
    public function SuggestCollaborator($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestcollaborator.class.php';
        $suggest = new crm_SuggestCollaborator($id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Contact()->suggestCollaborator(), 'search');

        return $suggest;
    }

    /**
     * Suggest contact with collaborators from main organization and descendants
     * @return crm_SuggestContact
     */
    public function SuggestCollaborators($id = null)
    {
        $Crm = $this->Crm();
        $orgs = array();

        if ($mainOrganization = $Crm->getMainOrganization()) {

           // First choice: the main organization

           $orgSet = $Crm->OrganizationSet();
           $mainOrganization = $orgSet->get($mainOrganization);
           $descendantIds = array();
           $mainOrganization->getDescendants($descendantIds);
           $orgs[$mainOrganization->id] = $mainOrganization->name;

           $descendants = $orgSet->select($orgSet->id->in($descendantIds));

           foreach ($descendants as $descendant) {
               $orgs[$descendant->id] = $descendant->name;
           }

        } else {

            // Second choice: main organization of the current user

            if ($currentContact = $Crm->Access()->currentContact()) {
                $orgs = $currentContact->getRelatedOrganizations();
            }
        }

        $suggestCollaborator = $this->SuggestContact($id)
            ->setRelatedOrganization($orgs)
            ->addClass('widget-fullwidth')
            ->setMinChars(0)
            ->setSize(75);

        if (empty($orgs)) {
            $suggestCollaborator->disable();
            $suggestCollaborator->setTitle($Crm->translate('This field is disabled because you are not part of an organization in this application'));
        }

        return $suggestCollaborator;
    }


    /**
     * @return crm_SuggestTag
     */
    public function SuggestTag($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggesttag.class.php';
        $suggest = new crm_SuggestTag($this->Crm(), $id);
        $suggest->setSuggestAction($this->Crm()->Controller()->Tag()->suggest(), 'search');
        return $suggest;
    }


    /**
     * @return crm_SuggestEmail
     */
    public function SuggestEmail($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestemail.class.php';
        $suggest = new crm_SuggestEmail($this->Crm(), $id);

        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestEmail(), 'search');

        return $suggest;
    }


    /**
     * @return crm_SuggestPosition
     */
    public function SuggestPosition($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestposition.class.php';
        $suggest = new crm_SuggestPosition($this->Crm(), $id);

        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestPosition(), 'search');

        return $suggest;
    }

    /**
     * @return crm_SuggestAddressType
     */
    public function SuggestAddressType($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestaddresstype.class.php';
        $suggest = new crm_SuggestAddressType($this->Crm(), $id);

        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestAddressType(), 'search');

        return $suggest;
    }

    /**
     * @return crm_SuggestAddress
     */
    public function SuggestAddress($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestaddress.class.php';
        $suggest = new crm_SuggestAddress($this->Crm(), $id);

        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestAddress(), 'search');

        return $suggest;
    }

    /**
     * @return crm_SuggestEntry
     */
    public function SuggestEntry($category, $id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestentry.class.php';
        $suggest = new crm_SuggestEntry($category, $id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestEntry($category), 'search');
        return $suggest;
    }


    /**
     * @return widget_Select
     */
    public function SelectEntry($category, $id = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $select = $W->Select();
        $entrySet = $Crm->EntrySet();
        $entries = $entrySet->select($entrySet->category->is($category));
        $entries->orderAsc($entrySet->text);

        $select->addOption('', '');
        foreach ($entries as $entry) {
            $select->addOption($entry->id, $entry->text);
        }
        return $select;
    }


    /**
     * @return Widget_SuggestPostalCode
     */
    public function SuggestPostalCode($id = null)
    {
        $W = bab_Widgets();
        $suggest = $W->SuggestPostalCode($id);

        $testaction = $suggest->getMetadata('suggesturl');
        if (!isset($testaction['addon']))
        {
            // for backward compatibility with older version of ovidentia (< 7.8.90) and older versions of widgets
            $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestPostalCode(), 'search');
        }

        return $suggest;
    }




    /**
     * @return Widget_SuggestPlaceName
     */
    public function SuggestPlaceName($id = null)
    {
        $W = bab_Widgets();
        $suggest = $W->SuggestPlaceName($id);

        $testaction = $suggest->getMetadata('suggesturl');
        if (!isset($testaction['addon']))
        {
            // for backward compatibility with older version of ovidentia (< 7.8.90) and older versions of widgets
            $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestPlaceName(), 'search');
        }

        return $suggest;
    }




    /**
     * @return crm_SuggestArticle
     */
    public function SuggestArticle($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestarticle.class.php';
        $suggest = new crm_SuggestArticle($id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestArticle(), 'search');

        return $suggest;
    }




    /**
     * @return crm_SuggestArticlePackaging
     */
    public function SuggestArticlePackaging($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestarticlepackaging.class.php';
        $suggest = new crm_SuggestArticlePackaging($id);
        $suggest->setCrm($this->Crm());

        return $suggest;
    }




    /**
     * @return crm_SuggestCatalogItem
     */
    public function SuggestCatalogItem($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestcatalogitem.class.php';
        $suggest = new crm_SuggestCatalogItem($id);
        $suggest->setCrm($this->Crm());

        return $suggest;
    }


    /**
     * @return crm_SuggestClassification
     */
    public function SuggestClassification($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggestclassification.class.php';
        $suggest = new crm_SuggestClassification($id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestClassification(), 'search');

        return $suggest;
    }


    /**
     * @return crm_SuggestStatus
     */
    public function SuggestStatus($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/suggeststatus.class.php';
        $suggest = new crm_SuggestStatus($id);
        $suggest->setCrm($this->Crm());
        $suggest->setSuggestAction($this->Crm()->Controller()->Search()->suggestStatus(), 'search');

        return $suggest;
    }


    /**
     * @return crm_ClassificationsSelector
     */
    public function ClassificationsSelector($id = null)
    {
        require_once FUNC_CRM_PHP_PATH . 'widgets/classificationsselector.class.php';
        return new crm_ClassificationsSelector($id, $this->Crm());
    }




    /**
     * Generates and returns an image representing the specified contact.
     *
     * @param crm_Contact	$contact	The contact to generate a photo of.
     * @param int     		$width		Width in px.
     * @param int     		$height		Height in px.
     * @param bool    		$border		True to add a 1px light-grey border around the image.

     * @return Widget_Image
     */
    public function ContactPhoto(crm_Contact $contact = null, $width = 32, $height = 32, $border = false)
    {
        $this->includeContact();
        return crm_contactPhoto($contact, $width, $height, $border);
    }



    /**
     * Generates and returns an image representing the specified article.
     *
     * @param crm_Article	$article	The article to generate a photo of.
     * @param int     		$width		Width in px.
     * @param int     		$height		Height in px.
     *
     * @return Widget_Icon
     */
    public function ArticleMainPhoto(crm_Article $article, $width, $height)
    {
        $this->includeArticle();
        return crm_articleMainPhoto($article, $width, $height);
    }



    /**
     * Generates and returns an image representing the specified privatesell.
     *
     * @param crm_PrivateSell	crm_PrivateSell	The article to generate a photo of.
     * @param int     			$width			Width in px.
     * @param int     			$height			Height in px.
     *
     * @return Widget_Icon
     */
    public function PrivateSellMainPhoto(crm_PrivateSell $privatesell, $width, $height)
    {
        $this->includePrivateSell();
        return crm_privateSellMainPhoto($privatesell, $width, $height);
    }


    /**
     * Generates and returns an image representing the specified catalog.
     *
     * @param crm_Catalog	$catalog	The catalog to generate a photo of.
     * @param int     		$width		Width in px.
     * @param int     		$height		Height in px.
     *
     * @return Widget_Icon
     */
    public function CatalogPhoto(crm_Catalog $catalog, $width, $height)
    {
        $this->includeCatalog();
        return crm_catalogPhoto($catalog, $width, $height);
    }


    /**
     *
     * @param crm_ShoppingCart $shoppingcart
     * @return crm_ShoppingCartCardFrame
     */
    public function ShoppingCartCardFrame(crm_ShoppingCart $shoppingcart)
    {
        $this->includeShoppingCart();
        return new crm_ShoppingCartCardFrame($this->Crm(), $shoppingcart);
    }


    /**
     * Header frame for shopping cart products lists, visible by annonymous user
     * @param crm_ShoppingCart $shoppingcart
     * @return crm_ShoppingCartCardFrame
     */
    public function ShoppingCartHeaderFrame(crm_ShoppingCart $shoppingcart)
    {
        $this->includeShoppingCart();
        return new crm_ShoppingCartHeaderFrame($this->Crm(), $shoppingcart);
    }



    /**
     * A frame to display a contact without organization informations
     * ex : in organization page
     *
     * @param	crm_OrganizationPerson	$contact
     *
     * @return Widget_Frame
     */
    public function ContactCardFrame(crm_OrganizationPerson $contact, $attributes = 0xffff)
    {
        $this->includeContact();
        return new crm_ContactCardFrame($this->Crm(), $contact, $attributes);
    }




    /**
     * A frame to display all informations relative to a contact
     * ex : in contact visualisation page
     *
     * @param	crm_Contact	$contact
     *
     * @return Widget_Frame
     */
    public function ContactFullFrame(crm_Contact $contact, $view = '')
    {
        $this->includeContact();
        return new crm_ContactFullFrame($this->Crm(), $contact, $view);
    }


    /**
     * Display delivery address of one contact
     * @param	crm_Contact	$contact
     * @return Widget_Frame
     */
    public function AddressCardFrame(crm_Address $address, $title = null, $recipient = null)
    {
        $this->includeAddress();
        return new crm_AddressCardFrame($this->Crm(), $address, $title, $recipient);
    }



    /**
     * A frame to display all informations relative to the contact associated to current
     * ex : in visualisation page
     *
     * @return Widget_Frame
     */
    public function MyContactFullFrame()
    {
        $this->includeMyContact();
        return new crm_MyContactFullFrame($this->Crm());
    }


    /**
     * Task card frame
     * @param crm_Task $task
     * @return crm_TaskCardFrame
     */
    public function TaskCardFrame(crm_Task $task, $attributes = 0xFFFF)
    {
        $this->includeTask();
        return new crm_TaskCardFrame($this->Crm(), $task, $attributes);
    }

    /**
     * Task full frame
     * @param crm_Task $task
     * @return crm_TaskCardFrame
     */
    public function TaskFullFrame(crm_Task $task)
    {
        $this->includeTask();
        return new crm_TaskFullFrame($this->Crm(), $task);
    }

    /**
     * Task table view
     * @return crm_TaskTableView
     */
    public function TaskTableView()
    {
        $this->includeTask();
        return new crm_TaskTableView($this->Crm());
    }

    /**
     * Task table view
     * @return crm_TaskTableView
     */
    public function userTasksForCrmObject($object, $linkType = null)
    {
        $this->includeTask();
        return crm_userTasksForCrmObject($object, $linkType);
    }




    /**
     * Email signature table view
     * @return crm_EmailSignatureTableView
     */
    public function EmailSignatureTableView()
    {
        $this->includeEmailSignature();
        return new crm_EmailSignatureTableView($this->Crm());
    }

    /**
     * Generates and returns an image representing of the specified organization's logo.
     *
     * @param crm_Organization	$organization	The organization to generate a photo of.
     * @param int     			$width			Width in px.
     * @param int     			$height			Height in px.
     * @param bool    			$border			True to add a 1px light-grey border around the image.

     * @return Widget_Frame
     */
    public function OrganizationLogo(crm_Organization $organization, $width, $height, $border = false)
    {
        $this->includeOrganization();
        return crm_organizationLogo($organization, $width, $height, $border);
    }



    /**
     * A frame to display an organization
     *
     * @param	crm_Organization	$organization
     *
     * @return Widget_Frame
     */
    public function OrganizationCardFrame(crm_Organization $organization, $attributes = 0xffff)
    {
        $this->includeOrganization();
        return new crm_OrganizationCardFrame($this->Crm(), $organization, $attributes);
    }


    /**
     * A frame to display all informations relative to an organization
     *
     * @param	crm_Organization	$organization
     *
     * @return Widget_Frame
     */
    public function OrganizationFullFrame(crm_Organization $organization, $view = '')
    {
        $this->includeOrganization();
        return new crm_OrganizationFullFrame($this->Crm(), $organization, $view);
    }


    /**
     * A frame to display some informations relative to an order
     *
     * @return crm_OrderCardFrame
     */
    public function OrderCardFrame(crm_Order $order)
    {
        $this->includeOrder();
        $cardFrame = new crm_OrderCardFrame($this->Crm(), $order);
        $cardFrame->CardFrame();
        return $cardFrame;
    }



    /**
     * A frame to display all informations relative to an order
     *
     * @param	crm_Order	$order
     * @param	bool		$readonly		display or not the buttons to add items, taxes ...
     *
     * @return crm_OrderFullFrame
     */
    public function OrderFullFrame(crm_Order $order, $readOnly = false)
    {
        $this->includeOrder();
        $fullFrame = new crm_OrderFullFrame($this->Crm(), $order, $readOnly);
        $fullFrame->FullFrame();

        return $fullFrame;
    }


    /**
     * Download or save PDF file of order
     *
     * @return crm_OrderPdf
     */
    public function OrderPdf(crm_Order $order)
    {
        $this->includeOrderPdf();
        return new crm_OrderPdf($this->Crm(), $order);
    }


    /**
     * Download or save PDF file of a delivery slip
     *
     * @return crm_OrderPdf
     */
    public function DeliverySlipPdf(crm_Order $order)
    {
        $this->includeDeliverySlipPdf();
        return new crm_DeliverySlipPdf($this->Crm(), $order);
    }


    /**
     * Article display
     * @return crm_ArticleDisplay
     */
    public function ArticleDisplay(crm_Article $article)
    {
        $this->includeArticle();
        return new crm_ArticleDisplay($this->Crm(), $article);
    }


    /**
     * Private Sell display
     * @return crm_PrivateSellDisplay
     */
    public function PrivateSellDisplay(crm_PrivateSell $privatesell)
    {
        $this->includePrivateSell();
        return new crm_PrivateSellDisplay($this->Crm(), $privatesell);
    }


    /**
     * Discount display
     * @return crm_DiscountDisplay
     */
    public function DiscountDisplay(crm_Discount $discount)
    {
        $this->includeDiscount();
        return new crm_DiscountDisplay($this->Crm(), $discount);
    }

    /**
     * Coupon display
     * @return crm_CouponDisplay
     */
    public function CouponDisplay(crm_Coupon $coupon)
    {
        $this->includeCoupon();
        return new crm_CouponDisplay($this->Crm(), $coupon);
    }


    /**
     * A frame to display a card frame relative to an article
     *
     * @return Widget_Item
     */
    public function ArticleCardFrame(crm_Article $article)
    {
        return $this->ArticleDisplay($article)->getCardFrame();
    }


    /**
     * A frame to display all informations relative to an article
     *
     * @return Widget_Item
     */
    public function ArticleFullFrame(crm_Article $article)
    {
        return $this->ArticleDisplay($article)->getFullFrame();
    }


    /**
     * Catalog display
     *
     * @param	crm_Catalog | crm_SearchCatalog $catalog
     *
     * @return crm_CatalogDisplay
     */
    public function CatalogDisplay($catalog)
    {
        $this->includeCatalog();
        return new crm_CatalogDisplay($this->Crm(), $catalog);
    }


    /**
     * Catalog item display
     *
     * @return crm_CatalogItemDisplay
     */
    public function CatalogItemDisplay(crm_CatalogItem $item)
    {
        $this->includeCatalogItem();
        return new crm_CatalogItemDisplay($this->Crm(), $item);
    }


    /**
     * A frame to display all informations relative to an article
     * @param crm_CatalogItem $item
     * @return Widget_Item
     */
    public function CatalogItemFullFrame(crm_CatalogItem $item)
    {
        return $this->CatalogItemDisplay($item)->getFullFrame();
    }


    /**
     * A frame to display all informations relative to an article
     * @param crm_CatalogItem $item
     * @return Widget_Item
     */
    public function CatalogItemCardFrame(crm_CatalogItem $item)
    {
        return $this->CatalogItemDisplay($item)->getCardFrame();
    }




    /**
     * A frame to display gift card information on possibility to add a gift card to the shopping cart
     * @return Widget_Item
     */
    public function GiftCardFullFrame()
    {
        $this->includeGiftCard();
        return new crm_GiftCardFullFrame($this->Crm());
    }

    /**
     * @param crm_CollectStore $collectStore
     * @return crm_CollectStoreEditor
     */
    public function CollectStoreEditor(crm_CollectStore $collectStore = null)
    {
        $this->includeCollectStore();
        return new crm_CollectStoreEditor($this->Crm(), $collectStore);
    }


    /**
     * form to add/modify a comment with rating on article
     * @param bool			$private
     * @param crm_Comment 	$comment
     * @param crm_Record	$targetRecord
     */
    public function CommentEditor($private, $targetRecord = null, crm_Comment $comment = null)
    {
        $this->includeComment();
        $access_type = $private ? crm_CommentEditor::ACCESS_PRIVATE : crm_CommentEditor::ACCESS_PUBLIC;
        return new crm_CommentEditor($this->Crm(), $access_type, $targetRecord , $comment);
    }



    /**
     * form to confirm the waiting comments
     */
    public function WaitingCommentsEditor()
    {
        $this->includeComment();
        return new crm_WaitingCommentsEditor($this->Crm());
    }



    /**
     * A frame with the list of comments linked to relative record displayed for customer (online shop public access)
     * @param crm_Record $item
     */
    public function CommentHistory(crm_Record $record)
    {
        $this->includeComment();
        return new crm_CommentHistory($this->Crm(), $record);
    }


    /**
     * A frame to display some information relative to a team
     *
     * @param	crm_Team	$team
     *
     * @return Widget_Frame
     */
    public function TeamCardFrame(crm_Team $team)
    {
        $this->includeTeam();
        return new crm_TeamCardFrame($this->Crm(), $team);
    }



    /**
     * Generates and returns an image representing the specified team.
     *
     * @param crm_Team $team              The team to generate a photo of.
     * @param int      $width             Width in px.
     * @param int      $height            Height in px.
     * @param bool     $border            True to add a 1px light-grey border around the image.
     *
     * @return Widget_Image
     */
    public function TeamPhoto(crm_Team $team, $width, $height, $border = false)
    {
        $this->includeTeam();
        return crm_teamPhoto($team, $width, $height, $border);
    }



    /**
     * Generates and returns an image representing the specified deal.
     *
     * @param crm_Deal $deal              The deal to generate a photo of.
     * @param int      $width             Width in px.
     * @param int      $height            Height in px.
     * @param bool     $border            True to add a 1px light-grey border around the image.
     * @param bool     $defaultToLeadLogo True fallback to the deal's lead logo if the deal has
     *                                    no specific photo.
     *
     * @return Widget_Image
     */
    public function DealPhoto(crm_Deal $deal, $width, $height, $border = false, $defaultToLeadLogo = false)
    {
        $this->includeDeal();
        return crm_dealPhoto($deal, $width, $height, $border, $defaultToLeadLogo);
    }



    /**
     * A frame to display some information relative to a deal
     *
     * @param	crm_Deal	$deal
     *
     * @return Widget_Frame
     */
    public function DealCardFrame(crm_Deal $deal)
    {
        $this->includeDeal();
        return new crm_DealCardFrame($this->Crm(), $deal);
    }


    /**
     * A frame to display all informations relative to a deal
     *
     * @param	crm_Deal	$deal
     *
     * @return Widget_Frame
     */
    public function DealFullFrame(crm_Deal $deal, $view = '')
    {
        $this->includeDeal();
        return new crm_DealFullFrame($this->Crm(), $deal, $view);
    }


    /**
     * An extensive deal editor.
     *
     * @param	crm_Deal	$deal
     *
     * @return Widget_Frame
     */
    public function DealEditor(crm_Deal $deal = null, crm_Organization $organization = null)
    {
        $this->includeDeal();
        return new crm_DealEditor($this->Crm(), $deal, $organization);
    }


    /**
     * A frame to display files into a folder
     *
     *
     * @param crm_FileAttachment $file
     *
     * @return Widget_Frame
     */
    public function FileFullFrame(crm_FileAttachment $file)
    {
        $this->includeFile();
        return new crm_FileFullFrame($this->Crm(), $file);
    }



//     /**
//      * A frame with the list of contacts in organization,
//      * this frame should be inserted with addContextItem in the contex view
//      *
//      * @param	crm_Organization	$organization
//      * @param	int					$limit			max contacts to display
//      *
//      * @return Widget_Frame
//      */
//     public function OrganizationContacts(crm_Organization $organization, $limit = 50)
//     {
//         $this->includeOrganization();
//         $obj = new crm_organizationContacts($this->Crm());
//         return $obj->getFrame($organization, $limit);
//     }


    /**
     * Frame to reprent a note in the history view
     * @return crm_HistoryNote
     */
    public function HistoryNote(crm_Record $element)
    {
        $this->includeNote();
        return new crm_HistoryNote($element, $this->Crm());
    }



    public function MailBoxListItem(LibImap_Mail $email = null, $mode = null, $id = null)
    {
        $this->includeMailBox();
        $mailBoxListItem = new crm_MailBoxListItem($this->Crm(), $mode, $id);
        if (isset($email)) {
            $mailBoxListItem->setEmail($email);
        }
        return $mailBoxListItem;
    }


    /**
     * Creates a history element corresponding to $element
     *
     * @param crm_Record $element
     * @return crm_HistoryElement
     */
    public function HistoryElement(crm_Record $element, $mode = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $this->includeHistory();

        if ($element instanceof crm_Note) {
            $this->includeNote();
            return $this->HistoryNote($element, $mode);
        }

        if ($element instanceof crm_Comment) {
            $this->includeComment();
            return new crm_HistoryComment($element, $mode);
        }

        if ($element instanceof crm_Task) {
            $this->includeTask();
            return new crm_HistoryTask($element, $mode);
        }

        if ($element instanceof crm_Email) {
            $this->includeEmail();
            return new crm_HistoryEmail($element, $mode);
        }

        if ($element instanceof crm_Order) {
            $this->includeOrder();
            return new crm_HistoryOrder($element, $mode);
        }

        if ($element instanceof crm_Deal) {
            $this->includeDeal();
            $e = new crm_HistoryElement($this->Crm());

            $contactSet = $Crm->ContactSet();
            /* @var $sender crm_Contact */
            $creator = $contactSet->get($contactSet->user->is($element->createdBy));
            if ($creator) {
                $Crm->Ui()->includeContact();
                $creatorWidget = new crm_ContactLink($Crm, $creator);
            } else {
                $creatorWidget = $W->Label(bab_getUserName($element->createdBy, true));
            }

            $e->setElement($element);
            $e->addDefaultLinkedElements();

            if ($element->lead) {
                $lead = $element->lead();
                $e->addLinkedElements(
                    $W->Link(
                        $this->OrganizationLogo($lead, crm_HistoryElement::LINKED_ELEMENT_IMAGE_WIDTH, crm_HistoryElement::LINKED_ELEMENT_IMAGE_HEIGHT),
                        $Crm->Controller()->Organization()->display($lead->id)
                    )
                );
            }
            $e->addIcon(Func_Icons::STATUS_DIALOG_INFORMATION);
            $e->addTitle($W->Title($W->Link($element->name, $Crm->Controller()->Deal()->display($element->id)), 4));
            $e->addTitle(
                $W->FlowItems(
                    $W->Label($Crm->translate('Deal created by')),
                    $creatorWidget
                )->setHorizontalSpacing(1, 'ex')->addClass('crm-small')
            );
            $e->addTitle($W->Label(BAB_DateTimeUtil::relativePastDate($element->createdOn, true))->addClass('crm-small'));
            if (!empty($element->description)) {
                $e->addContent($W->RichText($element->description));
            }
            return $e;
        }

        if ($element instanceof crm_Contact) {
            $this->includeContact();
            $e = new crm_HistoryElement($this->Crm());

            $contactSet = $Crm->ContactSet();
            /* @var $sender crm_Contact */
            $creator = $contactSet->get($element->responsible);
            if ($creator) {
                $Crm->Ui()->includeContact();
                $creatorWidget = new crm_ContactLink($Crm, $creator);
            } else {
                $creatorWidget = $W->Label(bab_getUserName($element->createdBy, true));
            }

            $e->setElement($element);
            $e->addDefaultLinkedElements();

            $e->addIcon(Func_Icons::STATUS_DIALOG_INFORMATION);

            $title = $element->getFullName();
            if ($mainContactOrganization = $element->getMainContactOrganization()) {
                if (!empty($mainContactOrganization->position)) {
                    $title .= ' - ' . $mainContactOrganization->position;
                }
                if (!empty($mainContactOrganization->organization)) {
                    $title .= ' - ' . $mainContactOrganization->organization()->name;
                }
            }



            $e->addTitle($W->Title($W->Link($title, $Crm->Controller()->Contact()->display($element->id)), 4));
            $e->addTitle(
                $W->FlowItems(
                    $W->Label($Crm->translate('Contact added by')),
                    $creatorWidget
                )->setHorizontalSpacing(1, 'ex')->addClass('crm-small')
            );
            $e->addTitle($W->Label(BAB_DateTimeUtil::relativePastDate($element->createdOn, true))->addClass('crm-small'));

            $e->addLinkedElements(
                $W->Link(
                    $this->ContactPhoto($element, crm_HistoryElement::LINKED_ELEMENT_IMAGE_WIDTH, crm_HistoryElement::LINKED_ELEMENT_IMAGE_HEIGHT),
                    $Crm->Controller()->Contact()->display($element->id)
                )
            );
            return $e;
        }
    }


    /**
     * Creates a frame that represent any crm_record activity history.
     * this frame include the add note button if $showToolbar=true
     *
     * @return crm_History
     */
    public function HistoryFrame(crm_Record $record, $maxHistory = null, $skip = 0, $showToolbar = null, $title = null)
    {
        $history = $this->Crm()->Controller()->Search(false)->history($record->getRef(), $maxHistory, $skip, $showToolbar);
        if (null !== $title) {
            $history->setTitle($title);
        }
        return $history;
    }


    /**
     * Creates a frame that represent any crm_record activity history.
     *
     * @return crm_History
     */
    public function History(crm_Record $record)
    {
        $this->includeBase();
        $this->includeHistory();

        $history = new crm_History($record);

        return $history;
    }


    /**
     * Add note form
     * @param 	crm_Record 	$record
     * @return Widget_Item
     */
    public function AddTaskEditor(crm_Record $record)
    {
        $editor = $this->TaskEditor();
        $editor->associateTo($record);

        return $editor;
    }



    /**
     * Add note form
     * @param 	crm_Record 	$record
     * @return Widget_Item
     */
    public function AddNoteEditor(crm_Record $record, $withOptions = true, $withAttachements = true)
    {
        $W = bab_Widgets();
        $noteEditor = $this->NoteEditor();
        $noteEditor->addItem(
            $W->Hidden()->setName('for')->setValue($record->getRef())
        );
        $noteEditor->addNote();
        //$noteEditor->addFields($withOptions, $withAttachements);

        return $noteEditor;
    }



    /**
     * History of a record and a add note form at the top
     * @param 	crm_Record 	$record
     * @param	bool		$attachements
     * @return Widget_Item
     */
    public function AddNoteHistory(crm_Record $record)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $addNote = $W->Accordions();
        $addNote->addPanel($Crm->translate('Add a note'), $this->AddNoteEditor($record));

        return $W->VBoxItems(
            $addNote,
            $this->History($record)
        );
    }



    /**
     *
     * @return crm_CollectStoreTableView
     */
    public function CollectStoreTableView()
    {
        $this->includeCollectStore();
        return new crm_CollectStoreTableView($this->Crm());
    }


    /**
     * Contact table view, list contacts with a link to the contact display page
     * @return crm_ContactTableView
     */
    public function ContactTableView()
    {
        $this->includeContact();
        return new crm_ContactTableView($this->Crm());
    }


    /**
     * Contact table view, list contacts with column name defined to match outlook
     * @return crm_OutlookContactTableView
     */
    public function OutlookContactTableView()
    {
        $this->includeContact();
        return new crm_OutlookContactTableView($this->Crm());
    }


    /**
     * List of orders displayed on the mycontact page
     * @param	crm_Contact		$contact
     * @return crm_MyContactOrderTableView
     */
    public function MyContactOrderTableView(crm_Contact $contact)
    {
        $this->includeMyContact();
        return new crm_MyContactOrderTableView($this->Crm(), $contact);
    }


    /**
     * List of orders displayed on the mycontact page
     * @param	crm_Contact		$contact
     * @return crm_MyContactOrderTableView
     */
    public function MyContactShoppingCartTableView()
    {
        $this->includeMyContact();
        return new crm_MyContactShoppingCartTableView($this->Crm());
    }

    /**
     * Organization table view, list organization with a link to the organization display page
     * @return crm_OrganizationTableView
     */
    public function OrganizationTableView()
    {
        $this->includeOrganization();
        return new crm_OrganizationTableView($this->Crm());
    }


    /**
     * Deal table view, list deals with a link to the deal display page
     * @return crm_DealTableView
     */
    public function DealTableView()
    {
        $this->includeDeal();
        return new crm_DealTableView($this->Crm());
    }


    /**
     * Article table view, list articles with a link to the article display page
     * @return crm_ArticleTableView
     */
    public function ArticleTableView()
    {
        $this->includeArticle();
        return new crm_ArticleTableView($this->Crm());
    }


    /**
     * Private sell table view, list private sell with a link to the private sell display page
     * @return crm_ArticleTableView
     */
    public function PrivateSellTableView()
    {
        $this->includePrivateSell();
        return new crm_PrivateSellTableView($this->Crm());
    }


    /**
     * Discount table view, list discounts with a link to the discount display page
     * @return crm_DiscountTableView
     */
    public function DiscountTableView()
    {
        $this->includeDiscount();
        return new crm_DiscountTableView($this->Crm());
    }

    /**
     * Coupon table view, list coupon with a link to the coupon display page
     * @return crm_CouponTableView
     */
    public function CouponTableView()
    {
        $this->includeCoupon();
        return new crm_CouponTableView($this->Crm());
    }


    /**
     * Mail boxes table view
     * @return crm_MailBoxTableView
     */
    public function MailBoxTableView()
    {
        $this->includeMailBox();
        return new crm_MailBoxTableView($this->Crm());
    }


    /**
     * Article table view, list articles with a link to the article display page
     * @return crm_ArticleTableView
     */
    public function ArticleLinkTableView()
    {
        $this->includeArticle();
        return new crm_ArticleLinkTableView($this->Crm());
    }


    /**
     * Display shopping cart trace informations
     * @param crm_ShoppingCart $cart
     */
    public function ShoppingCartTraceFrame(crm_ShoppingCart $cartRecord)
    {
        $Crm = $this->Crm();

        if (!isset($Crm->ShopTrace))
        {
            return null;
        }

        $stSet = $Crm->ShopTraceSet();
        $shopTrace = $stSet->get($stSet->cart->is($cartRecord->id));

        if (!isset($shopTrace))
        {
            return null;
        }

        /*@var $shopTrace crm_ShopTrace */

        return $this->ShopTraceCardFrame($shopTrace);
    }

    /**
     * Display shop trace information with a link to the full shop trace
     * @param crm_ShopTrace $shopTrace
     */
    public function ShopTraceCardFrame(crm_ShopTrace $shopTrace)
    {
        $this->includeShopTrace();
        return new crm_ShopTraceCardFrame($this->Crm(), $shopTrace);

    }



    /**
     * Bouton de recalcul manuel pour le panier d'achat
     */
    public function ShoppingCartRecalculate()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $W->Button()->addItem($W->Label($Crm->translate('Recalculate')))->addClass('crm-shoppingcart-recalculate');
    }



    /**
     * Display shopping cart items in a widget table model view
     * This is the customer shopping cart with ID crm-shopping-cart used by javascript to update quantities
     *
     * @return crm_ShoppingCartItemTableView
     */
    public function ShoppingCartItemTableView(crm_ShoppingCart $shoppingCart)
    {
        $this->includeShoppingCart();
        $table = new crm_ShoppingCartItemTableView($this->Crm(), $shoppingCart, 'crm-shopping-cart');
        // TODO : the ID given as contructor parameter does not apply the ID to the tableModelView

        // temporary fix, apply ID after object construction
        $table->setId('crm-shopping-cart');
        return $table;
    }


    /**
     * List of payment errors or null if no errors for the shopping cart
     * @param crm_ShoppingCart $shoppingCart
     * @return crm_PaymentErrorTableView
     */
    public function PaymentErrorTableView(crm_ShoppingCart $shoppingCart)
    {
        $this->includeShoppingCartAdm();
        $table = new crm_PaymentErrorTableView($this->Crm(), $shoppingCart);

        $res = $table->getDataSource();

        if (0 === $res->count())
        {
            return null;
        }

        $set = $res->getSet();
        $table->addDefaultColumns($set);

        return $table;
    }

    /**
     * @return crm_ShopTraceTableView
     */
    public function ShopTraceTableView()
    {
        $this->includeShopTrace();
        return new crm_ShopTraceTableView($this->Crm());
    }

    /**
     *
     * @return crm_ShopTraceLogTableView
     */
    public function ShopTraceLogTableView()
    {
        $this->includeShopTrace();
        return new crm_ShopTraceLogTableView($this->Crm());
    }



    /**
     * Get values to display on shopping cart bottom
     * variables updated by ajax on shopping cart modification
     * @return array
     */
    public function ShoppingCartExtraRows(crm_ShoppingCart $shoppingCart)
    {
        $Crm = $this->Crm();

        $discountRows = $shoppingCart->getDiscountRows();

        $rows = array();

        $rows += array(
                'totaldf' 		=> array('title' => $Crm->translate('Total tax excl')							, 'value' => $shoppingCart->getItemSumDF())
        );

        if (!empty($discountRows) || $shoppingCart->getCouponTotal() > 0)
        {
            $rows += array(
                'sumti' 		=> array('title' => $Crm->translate('Total tax incl before reductions')				, 'value' => $shoppingCart->getItemSumTI())
            );
        }

        $rows += $discountRows;

        $rows += array(
                'totalrawti' 	=> array('title' => $Crm->translate('Total tax incl (without shipping cost)')	, 'value' => $shoppingCart->getRawTotalTI())
        );

        if ('' !== $shoppingCart->delivery)
        {
            $rows += array(
                    'freight'	=> array('title' => $Crm->translate('Shipping amount tax incl')					, 'value' => $shoppingCart->getFreight()),
                    'totalti' 	=> array('title' => $Crm->translate('Total tax incl')							, 'value' => $shoppingCart->getTotalTI())
            );
        }

        return $rows;
    }


    /**
     * Get coupons to display on shopping cart bottom
     * variables updated by ajax on shopping cart modification
     * @return array
     */
    public function CouponRows(crm_ShoppingCart $shoppingCart)
    {
        return $shoppingCart->getCouponRows();
    }


    /**
     * A widget with marketing information, no changes on shopping cart amount, displayed under the shopping cart
     * @return Widget_Displayable_Interface
     */
    public function ShoppingCartMarketing(crm_ShoppingCart $shoppingCart)
    {
        $W = bab_Widgets();
        return $W->Frame('shoppingcart_marketing');
    }



    /**
     * Get the object for steps management
     * get buttons, display list of steps wih link for allowed steps
     * @param crm_ShoppingCart $shoppingCart
     * @param int $step
     *
     * @return crm_ShoppingCartSteps
     */
    public function ShoppingCartSteps(crm_ShoppingCart $shoppingCart, $step)
    {
        $this->includeShoppingCart();
        return new crm_ShoppingCartSteps($this->Crm(), $shoppingCart, $step);
    }




    /**
     * Display import in a widget table model view
     * @return crm_ImportTableView
     */
    public function ImportTableView()
    {
        $this->includeImport();
        return new crm_ImportTableView($this->Crm());
    }


    /**
     * @return crm_CatalogTableView
     */
    public function CatalogTableView()
    {
        $this->includeCatalog();
        return new crm_CatalogTableView($this->Crm());
    }


    /**
     * @return crm_SearchCatalogTableView
     */
    public function SearchCatalogTableView()
    {
        $this->includeSearchCatalog();
        return new crm_SearchCatalogTableView($this->Crm());
    }


    /**
     * Display shopping cart link frame
     * shopping cart content for all pages with a link to shopping cart details
     * @return crm_ShoppingCartAdmTableView
     */
    public function ShoppingCartAdmTableView()
    {
        $this->includeShoppingCartAdm();
        return new crm_ShoppingCartAdmTableView($this->Crm());
    }

    /**
     * @return crm_ShoppingCartItemAdmTableView
     */
    public function ShoppingCartItemAdmTableView(crm_ShoppingCart $shoppingCart)
    {
        $this->includeShoppingCartAdm();
        return new crm_ShoppingCartItemAdmTableView($this->Crm(), $shoppingCart);
    }


    public function ShoppingCartAdmCardFrame(crm_ShoppingCart $shoppingCart)
    {
        $this->includeShoppingCartAdm();
        return new crm_ShoppingCartAdmCardFrame($this->Crm(), $shoppingCart);
    }


    /**
     * Display an item in shopping cart
     */
    public function ShoppingCartItemCardFrame(crm_ShoppingCartItem $cartitem)
    {
        if ($cartitem->catalogitem->id)
        {
            $display = $this->Crm()->Ui()->CatalogItemDisplay($cartitem->catalogitem);
            return $display->getShoppingCartLayout($cartitem);
        }

        $W = bab_Widgets();
        return $W->VBoxItems($W->Label($cartitem->name)->addClass('crm-strong'), $W->Label($cartitem->subtitle));
    }


    /**
     *
     * @param crm_ShoppingCart $shoppingcart
     * @return crm_Toolbar
     */
    public function ShoppingCartToolbar(crm_ShoppingCart $shoppingcart)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();
        $toolbar = new crm_Toolbar();
        $Controller = $Crm->Controller();

        // $toolbar->addButton($Crm->translate('Back to search results'), Func_Icons::ACTIONS_VIEW_LIST_TEXT, $this->Crm()->Controller()->CatalogItem()->displayList());

        if (!$shoppingcart->isEmpty())
        {
            if($access->canRecordShoppingCart())
            {
                $toolbar->addButton($Crm->translate('Record shopping cart'), Func_Icons::ACTIONS_DOCUMENT_SAVE, $Controller->ShoppingCart()->recordShoppingCart());
            }

            if (crm_ShoppingCart::EDIT === (int) $shoppingcart->status && $access->canSearchCatalogItems())
            {
                $toolbar->addButton($Crm->translate('Continue my shopping'), Func_Icons::ACTIONS_EDIT_FIND, $Controller->CatalogItem()->displayList());
            }

            if($access->canEmptyShoppingCart($shoppingcart))
            {
                $toolbar->addButton($Crm->translate('Empty shopping cart'), Func_Icons::ACTIONS_EDIT_DELETE, $Controller->ShoppingCart()->delete());
            }
        }

        return $toolbar;
    }


    /**
     * Display shopping cart link frame
     * shopping cart content for all pages with a link to shopping cart details
     * @return crm_UiObject
     */
    public function ShoppingLinkFrame()
    {
        $this->includeShoppingCart();
        return new crm_ShoppingCartLinkFrame($this->Crm());
    }



    /**
     * Home page of online shop back office
     * @return crm_UiObject
     */
    public function ShopAdminHomeFrame()
    {
        $this->includeShopAdmin();
        return new crm_ShopAdminHomeFrame($this->Crm());
    }




    /**
     * Order table view, list orders with a link to the order display page
     * @return crm_OrderTableView
     */
    public function OrderTableView()
    {
        $this->includeOrder();
        return new crm_OrderTableView($this->Crm());
    }


    /**
     * Order item table view, list orders items with a link to the order display page
     * @return crm_OrderItemTableView
     */
    public function OrderItemTableView()
    {
        $this->includeOrder();
        return new crm_OrderItemTableView($this->Crm());
    }


    /**
     * Table view of shipping cost scale
     * @return crm_ShippingScaleTableView
     */
    public function ShippingScaleTableView()
    {
        $this->includeShippingScale();
        return new crm_ShippingScaleTableView($this->Crm());
    }

    /**
     *
     * @param crm_ShippingScale $shippingscale
     * @return crm_ShippingScaleEditor
     */
    public function ShippingScaleEditor(crm_ShippingScale $shippingscale = null)
    {
        $this->includeShippingScale();
        return new crm_ShippingScaleEditor($this->Crm(), $shippingscale);
    }



    /**
     * Table view of packaging
     * @return crm_PackagingTableView
     */
    public function PackagingTableView()
    {
        $this->includePackaging();
        return new crm_PackagingTableView($this->Crm());
    }

    public function PackagingEditor(crm_Packaging $packaging = null)
    {
        $this->includePackaging();
        return new crm_PackagingEditor($this->Crm(), $packaging);
    }



    /**
     * Table view of article types
     * @return crm_ArticleTypeTableView
     */
    public function ArticleTypeTableView()
    {
        $this->includeArticleType();
        return new crm_ArticleTypeTableView($this->Crm());
    }

    public function ArticleTypeEditor(crm_ArticleType $articletype = null)
    {
        $this->includeArticleType();
        return new crm_ArticleTypeEditor($this->Crm(), $articletype);
    }



    /**
     * Table view of article availability
     * @return crm_ArticleAvailabilityTableView
     */
    public function ArticleAvailabilityTableView()
    {
        $this->includeArticleAvailability();
        return new crm_ArticleAvailabilityTableView($this->Crm());
    }

    public function ArticleAvailabilityEditor(crm_ArticleAvailability $articleavailability = null)
    {
        $this->includeArticleAvailability();
        return new crm_ArticleAvailabilityEditor($this->Crm(), $articleavailability);
    }



    /**
     * Table view of CustomField
     * @return crm_CustomFieldTableView
     */
    public function CustomFieldTableView()
    {
        $this->includeCustomField();
        return new crm_CustomFieldTableView($this->Crm());
    }

    public function CustomFieldEditor(crm_CustomField $customfield = null)
    {
        $this->includeCustomField();
        return new crm_CustomFieldEditor($this->Crm(), $customfield);
    }



    /**
     * Table view of tag rates
     * @return crm_TagTableView
     */
    public function TagTableView()
    {
        $this->includeTag();
        return new crm_TagTableView($this->Crm());
    }

    public function TagEditor(crm_Tag $tag = null)
    {
        $this->includeTag();
        return new crm_TagEditor($this->Crm(), $tag);
    }



    /**
     * Table view of vat rates
     * @return crm_VatTableView
     */
    public function VatTableView()
    {
        $this->includeVat();
        return new crm_VatTableView($this->Crm());
    }

    public function VatEditor(crm_Vat $vat = null)
    {
        $this->includeVat();
        return new crm_VatEditor($this->Crm(), $vat);
    }




    /**
     * When the manager switch a shopping cart from delivery status to shipped status
     * @param crm_ShoppingCart $shoppingcart
     * @return crm_ShoppingCartShippedCommentEditor
     */
    public function ShoppingCartShippedCommentEditor(crm_ShoppingCart $shoppingcart)
    {
        $this->includeShoppingCartAdm();
        return new crm_ShoppingCartShippedCommentEditor($this->Crm(), $shoppingcart);
    }


    /**
     * Organization editor
     * @return crm_AddressEdit
     */
    public function AddressEditor($withCountry = false, $withComplement = true, $withMapPreview = false)
    {
        $this->includeAddress();
        return crm_addressEditorFrame($this->Crm(), $withCountry, false, $withComplement, $withMapPreview);
    }


    /**
     * Mandatory address editor
     * Address editor used for a delivery address (online shop)
     * the country is mandatory
     *
     * @return crm_AddressEdit
     */
    public function MandatoryAddressEditor($withComplement = true, $withMapPreview = false, $withRecipient = false)
    {
        $this->includeAddress();

        // try to detect a default country, will be used only for creation form
        $country = 0;

        $Crm = $this->Crm();
        $countrySet = $Crm->CountrySet();
        if ($countryRecord = $countrySet->guessCountryFromBrowser('FR'))
        {
            $country = $countryRecord->id;
        }

        $addressEditor = crm_addressEditorFrame($this->Crm(), true, true, $withComplement, $withMapPreview);
        $addressEditor->showRecipient($withRecipient);
        $addressEditor->setCountrymandatory(true);
        $addressEditor->setDefaultCountry($country);
        $addressEditor->showInstructions();

        return $addressEditor;
    }


    /**
     * Organization editor
     * @return crm_OrganizationEditor
     */
    public function OrganizationEditor(crm_Organization $organization = null)
    {
        $this->includeOrganization();
        return new crm_OrganizationEditor($this->Crm(), $organization);
    }


    /**
     * Contact editor
     * @return crm_ContactEditor
     */
    public function ContactEditor(crm_Contact $contact = null, crm_Organization $organization = null)
    {
        $this->includeContact();
        return new crm_ContactEditor($this->Crm(), $contact, $organization);
    }


    /**
     * export options editor
     * @param array $contacts Filter and pagination informations
     * @return crm_ContactExportEditor
     */
    public function ContactExportEditor(Array $contacts = null)
    {
        $this->includeContact();
        return new crm_ContactExportEditor($this->Crm(), $contacts);
    }


    /**
     * Add contact to a mailing list
     * @param array $contacts
     */
    public function MailinglistEditor()
    {
        $this->includeContact();
        return new crm_MailinglistEditor($this->Crm());
    }


    /**
     * Contact of current user editor
     * @return crm_ContactEditor
     */
    public function MyContactEditor(crm_Contact $contact)
    {
        $this->includeMyContact();
        return new crm_MyContactEditor($this->Crm(), $contact);
    }

    /**
     * Registration form
     * @param	int	$context 	1 : shopping cart shop process
     * 							2 : save the shopping cart
     * @return crm_MyContactLoginCreateEditor
     */
    public function MyContactLoginCreateEditor($context = null)
    {
        $this->includeMyContact();
        return new crm_MyContactLoginCreateEditor($this->Crm(), $context);
    }


    /**
     * Merge contact editor
     * @return crm_ContactMergeEditor
     */
    public function ContactMergeEditor()
    {
        $this->includeContact();
        return new crm_ContactMergeEditor($this->Crm());
    }



    /**
     * Catalog editor
     * @return crm_CatalogEditor
     */
    public function CatalogEditor(crm_Catalog $catalog = null)
    {
        $this->includeCatalog();
        return new crm_CatalogEditor($this->Crm(), $catalog);
    }


    /**
     * Search Catalog editor
     * @return crm_SearchCatalogEditor
     */
    public function SearchCatalogEditor(crm_SearchCatalog $searchcatalog = null)
    {
        $this->includeSearchCatalog();
        return new crm_SearchCatalogEditor($this->Crm(), $searchcatalog);
    }


    /**
     *
     * @param crm_Article $article
     * @return crm_ArticlePackagingEditTableView
     */
    public function ArticlePackagingEditTableView(crm_Article $article = null)
    {
        $this->includeArticle();
        $table = new crm_ArticlePackagingEditTableView($this->Crm());
        $table->setArticle($article);

        return $table;
    }


    /**
     * Article editor (online shop back-office)
     * @param	crm_Article	$article
     * @return crm_ArticleEditor
     */
    public function ArticleEditor(crm_Article $article = null)
    {
        $this->includeArticle();
        return new crm_ArticleEditor($this->Crm(), $article);
    }


    public function ArticleCreateEditor(crm_Article $fromArticle = null)
    {
        $this->includeArticle();
        return new crm_ArticleCreateEditor($this->Crm(), $fromArticle);
    }


    /**
     * Article editor (online shop back-office)
     * @param	crm_PrivateSell	$privateSell
     * @return	crm_PrivateSellEditor
     */
    public function PrivateSellEditor(crm_PrivateSell $privateSell = null)
    {
        $this->includePrivateSell();
        return new crm_PrivateSellEditor($this->Crm(), $privateSell);
    }


    /**
     * Discount editor (online shop back-office)
     * @param	crm_Discount	$discount
     * @return	crm_DiscountEditor
     */
    public function DiscountEditor(crm_Discount $discount = null)
    {
        $this->includeDiscount();
        return new crm_DiscountEditor($this->Crm(), $discount);
    }


    /**
     * Coupon editor (online shop back-office)
     * @param	crm_Coupon	$coupon
     * @return	crm_CouponEditor
     */
    public function CouponEditor(crm_Coupon $coupon = null)
    {
        $this->includeCoupon();
        return new crm_CouponEditor($this->Crm(), $coupon);
    }


    /**
     * Upload files to import in article, photos or csv file
     * @return crm_ArticleImportEditor
     */
    public function ArticleImportEditor()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $form = $W->CsvImportForm();
        $form->addClass('crm-editor');

        $set = $Crm->ArticleSet();
        $set->joinHasOneRelations();
        $set->mainpackaging->packaging();

        $list = $this->ArticleTableView();
        $list->addDefaultColumns($set);
        $list->setDataSource($set->select());

        $arr = $list->getVisibleColumns();

        foreach($arr as $column)
        {
            /* @var $column widget_TableModelViewColumn */
            if (!$column->isExportable()) {
                continue;
            }

            bab_debug($column->getFieldPath());


            switch($column->getFieldPath()) {
                case 'createdOn':
                case 'modifiedOn':
                case '_unit_cost_tax_incl': // ignorer le champs de prix contenant une remise
                    continue 2;
            }

            $form->addImportField($column->getFieldPath(), $column->getDescription());
        }


        $form->setMandatory('reference');
        $form->setMandatory('name');

        return $form;
    }



    public function ArticleImportPhotosEditor()
    {
        $this->includeArticle();
        return new crm_ArticleImportPhotosEditor($this->Crm());
    }




    /**
     * Classify an article into catalogs (online shop back-office)
     */
    public function ArticleClassifyEditor(crm_Article $article)
    {
        $this->includeArticle();
        return new crm_ArticleClassifyEditor($this->Crm(), $article);
    }

    /**
     * Get catalog item search form
     * @return crm_CatalogItemSearchEditor
     */
    public function CatalogItemSearchEditor()
    {
        $this->includeCatalogItem();
        return new crm_CatalogItemSearchEditor($this->Crm());
    }

    /**
     * Editor used in dialog to set the options of a catalog item before the insertion in shopping cart
     *
     * @param	crm_CatalogItem			$catalogItem
     * @param 	crm_ArticlePackaging	$articlePackaging
     * @param	int						$quantity
     *
     * @return crm_catalogItemOptionsDialogEditor
     */
    public function catalogItemOptionsDialogEditor(crm_CatalogItem $catalogItem, crm_ArticlePackaging $articlePackaging, $quantity)
    {
        $this->includeCatalogItem();
        return new crm_catalogItemOptionsDialogEditor($this->Crm(), $catalogItem, $articlePackaging, $quantity);
    }

    /**
     * Editor used in dialog to save a shopping cart
     * @return crm_ShoppingCartRecordDialogEditor
     */
    public function ShoppingCartRecordEditor()
    {
        $this->includeShoppingCart();
        return new crm_ShoppingCartRecordEditor($this->Crm());
    }


    /**
     * Order editor
     * @return crm_OrderEditor
     */
    public function OrderEditor()
    {
        $this->includeOrder();
        return new crm_OrderEditor($this->Crm());
    }


    /**
     * Order item editor
     * @return crm_OrderItemEditor
     */
    public function OrderItemEditor()
    {
        $this->includeOrder();
        return new crm_OrderItemEditor($this->Crm());
    }


    /**
     * Order tax editor
     * @return crm_OrderTaxEditor
     */
    public function OrderTaxEditor()
    {
        $this->includeOrder();
        return new crm_OrderTaxEditor($this->Crm());
    }


    /**
     * Email editor
     * @return crm_EmailEditor
     */
    public function EmailEditor($signature = true)
    {
        $this->includeEmail();
        $editor = new crm_EmailEditor($this->Crm(), null, null, $signature);
        return $editor;
    }


    /**
     * Email signature editor
     * @return crm_EmailSignatureEditor
     */
    public function EmailSignatureEditor()
    {
        $this->includeEmailSignature();
        $editor = new crm_EmailSignatureEditor($this->Crm());
        return $editor;
    }


    /**
     * Note editor
     * @return crm_NoteEditor
     */
    public function NoteEditor()
    {
        $this->includeNote();
        $editor = new crm_NoteEditor($this->Crm());
        return $editor;
    }


    /**
     * Import editor
     * @param	crm_Import	$import
     * @return crm_ImportEditor
     */
    public function ImportEditor(crm_Import $import = null)
    {
        $this->includeImport();
        return new crm_ImportEditor($this->Crm(), $import);
    }


    /**
     * Task editor
     * @return crm_TaskEditor
     */
    public function TaskEditor(crm_Task $task = null)
    {
        $this->includeTask();
        return new crm_TaskEditor($this->Crm(), $task);
    }


    public function ShopConfigurationEditor()
    {
        $this->includeShopAdmin();
        return new crm_ConfigurationEditor($this->Crm());
    }

    /**
     *
     */
    public function shopTermsEditor()
    {
        $this->includeShopUser();
        return new crm_shopTermsEditor($this->Crm());
    }



    public function ForgotPasswordEditor()
    {
        $this->includePasswordToken();
        return new crm_ForgotPasswordEditor($this->Crm());
    }

    public function PasswordTokenEditor($uuid)
    {

        $this->includePasswordToken();
        return new crm_PasswordTokenEditor($this->Crm(), $uuid);
    }


    /**
     * @return crm_ShoppingCartCouponEditor
     */
    public function ShoppingCartCouponEditor()
    {
        $this->includeShoppingCart();
        return new crm_ShoppingCartCouponEditor($this->Crm());
    }


    /**
     *
     */
    public function ContactDirectoryEditor()
    {
        $this->includeContact();
        return new crm_ContactDirectoryEditor($this->Crm());
    }




    /**
     * Address edit
     * @return crm_AddressEdit
     */
    public function AddressEdit()
    {
        $this->includeAddress();
        return new crm_AddressEdit($this->Crm());
    }




    public function MailBoxEditor(crm_MailBox $mailbox = null, $userId = null)
    {
        $this->includeMailBox();
        return new crm_MailBoxEditor($this->Crm(), $mailbox, $userId);
    }



    /**
     * get euro char in ovidentia charset
     * @return string
     */
    public function Euro()
    {
        return bab_getStringAccordingToDataBase(chr(0xA4), 'ISO-8859-15');
    }

    /**
     * Get a non-breaking space character in ovidentia charset
     * @return string
     */
    public function Nbsp()
    {
        return bab_getStringAccordingToDataBase(chr(0xA0), 'ISO-8859-15');
    }


    public function Actions()
    {
        $this->includeBase();
        return new crm_Actions;
    }



    /**
     * Get a cardframe for a record if available
     * @param crm_Record $record
     * @return crm_CardFrame
     */
    public function getCardFrame(crm_Record $record)
    {
        $Crm = $this->Crm();
        $name = substr(get_class($record), strlen($Crm->classPrefix));

        $firstLetter = substr($name, 0, 1);
        $method = mb_strtolower($firstLetter).substr($name, 1).'CardFrame';

        if (!method_exists($this, $method)) {
            return null;
        }


        return $this->$method($record);
    }



    public function AddressFrame(crm_Address $address)
    {
        $this->includeAddress();
        return new crm_AddressFrame($address);
    }



    public function ShoppingCartDeliverySelect()
    {
        require_once FUNC_CRM_UI_PATH . 'delivery.ui.php';
        return new crm_ShoppingCartDeliverySelect($this->Crm());
    }


    /**
     * @param crm_Organization   $organization
     */
    public function OrganizationTimeline(crm_Organization $organization)
    {
        $this->includeOrganization();
        return crm_OrganizationTimeline($this->Crm(), $organization);
    }

    /**
     * @param crm_Contact   $contact
     */
    public function ContactTimeline(crm_Contact $contact)
    {
        $this->includeContact();
        return crm_ContactTimeline($this->Crm(), $contact);
    }

    /**
     * @param crm_Deal      $deal
     * @param string        $taskurl
     * @param string        $taskparam      Name of parameter in url for task ID
     */
    public function DealTimeline(crm_Deal $deal, $taskurl, $taskparam)
    {
        $this->includeDeal();
        return crm_DealTimeline($deal, $taskurl, $taskparam);
    }


    /**
     * @param crm_Deal      $deal
     * @param string        $taskurl
     * @param string        $taskparam      Name of parameter in url for task ID
     */
    public function DealPlanningList(crm_Deal $deal, $taskurl, $taskparam)
    {
        $this->includeDeal();
        return crm_dealPlanningList($deal, $taskurl, $taskparam);
    }

    /**
     * @return crm_ExportSelectEditor
     */
    public function ExportSelectEditor($id, crm_TableModelView $tableview, $filter = null)
    {
        require_once dirname(__FILE__).'/exportselect.ui.php';
        return new crm_ExportSelectEditor($this->Crm(), $id, $tableview, $filter);
    }

    /**
     * @param ORM_Criteria $criteria
     * @return crm_CriteriaEditor
     */
    public function CriteriaEditor(ORM_Criteria $criteria = null)
    {
        $this->includeCriteria();
        return new crm_CriteriaEditor($this->Crm(), $criteria);
    }


    /**
     * @param crm_Record $object
     * @param string $linkType
     * @return Widget_Displayable_Interface
     */
    public function NotesForCrmObject(crm_Record $object, $linkType = null, $limit = null)
    {
        $this->includeNote();
        return crm_notesForCrmObject($object, $linkType);
    }


    /**
     *
     * @param crm_Organization $organization
     * @return Widget_VBoxLayout
     */
    public function organizationContacts(crm_Organization $organization)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $contactSet =$Crm->ContactSet();

        $contacts = $contactSet->select($contactSet->organization->is($organization->id));

        $box = $W->VBoxItems();
        foreach ($contacts as $contact) {
            $box->addItem(
                $W->FlowItems(
                    $W->Link($contact->getFullName(), $Crm->Controller()->Contact()->display($contact->id))
                )->setSizePolicy('widget-list-element')
            );
        }

        return $box;
    }
}
