<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */







/**
 * @return widget_SimpleTreeView
 */
function crm_statusEditor(Func_Crm $Crm)
{

	$Crm->Ui()->includeDeal();
	$Crm->includeDeal();

	$W = bab_Widgets();


	$treeview = $W->SimpleTreeView();
	$treeview->addClass(Func_Icons::ICON_LEFT_16);

	$set = $Crm->StatusSet();
	$nodes = $set->select($set->id_parent->greaterThan('0'));

	$element = $treeview->createElement('1', '', '');

	$element->setItem($W->Label($Crm->translate('Statuses')));
	$treeview->appendElement($element, null);
	$element->addAction(
		'appendChild',
		$Crm->translate('Add substatus'),
		$GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
		$Crm->Controller()->Admin()->addStatus('1')->url(),
		''
	);

	/* !!! Statutes by default must not be deleted */

	foreach ($nodes as $node) {
		$element = $treeview->createElement(
			$node->id,
			'',
			$node->name,
			'',
			$Crm->Controller()->Admin()->editStatus($node->id)->url()
		);
//		$element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');
		$element->setItem(
			$W->FlowItems(
				$W->Frame()->setCanvasOptions(
					Widget_Item::Options()->backgroundColor('#' . $node->color)
				)->addClass('crm-color-preview'),
				$W->Icon($node->name, $node->icon)
			)->setVerticalAlign('middle')
			->setHorizontalSpacing(8, 'px')
		);

		$element->addAction(
			'appendChild',
			$Crm->translate('Add substatus'),
			$GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
			$Crm->Controller()->Admin()->addStatus($node->id)->url(),
			''
		);

		/* !!! Statutes by default must not be deleted */
		$statusCanBeDeleted = $node->isDeletable();

// 		if ($node->reference != '') {
// 			$statusCanBeDeleted = false;
//			foreach ($defaultStatus as $key => $value) {
//				if ($node->reference == $key) {
//					$statusCanBeDeleted = false;
//				}
//			}
// 		}
		if ($statusCanBeDeleted /* || bab_isUserAdministrator() */ ) {
			$element->addAction(
				'delete',
				$Crm->translate('Delete'),
				$GLOBALS['babSkinPath'] . 'images/Puces/delete.png',
				$Crm->Controller()->Status()->confirmDelete($node->id)->url(),
				''
			);
		}
		$parentId = $node->id_parent;
		$treeview->appendElement($element, $parentId);
	}

	return $treeview;
}





