<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



bab_Widgets()->includePhpClass('widget_TableView');


/**
 *
 *
 *
 */
class crm_CatalogItemTableView extends crm_TableModelView
{

	protected function handlePageSelector()
	{
		return null;
	}



	public function setDataSource(ORM_Iterator $iterator)
	{
		$this->addClass('icon-16x16')->addClass('icon-left')->addClass('icon-left-16');
		return parent::setDataSource($iterator);
	}



	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();

		if ($fieldPath == 'article/name') {
			$value = self::getRecordFieldValue($record, $fieldPath);
			return $W->Link($W->Icon($value, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), $this->Crm()->Controller()->CatalogItem()->display($record->id));
		}

		return parent::computeCellContent($record, $fieldPath);
	}

}









/**
 * Display list of recent modified catalog items
 * with default sort and columns
 */
class crm_RecentCatalogItemTableView extends crm_CatalogItemTableView
{

	public function setDataSource(ORM_Iterator $iterator)
	{
		$Crm = $this->Crm();
		$return = parent::setDataSource($iterator);

		$visibleColumns = array(
			'article/name' 			=> $Crm->translate('Name'),
			'article/modifiedOn'	=> $Crm->translate('Last modification')
		);

		$this->setVisibleColumns($visibleColumns);


		$set = $iterator->getSet();
		$iterator->orderDesc($set->article->modifiedOn);

		return $return;
	}

	/**
	 * Get recent article table from catalog
	 * init datasource with article in one catalog
	 *
	 * @param 	int 				$catalog
	 *
	 */
	public function initCatalog($catalog)
	{
		$this->setPageLength(10);

		$this->Crm()->includeArticleSet();
		$catalogItemSet = $this->Crm()->CatalogItemSet();
		$catalogItemSet->join('article');
		$iterator = $catalogItemSet->select($catalogItemSet->catalog->is($catalog));

		$this->setDataSource($iterator);
	}





}














/**
 * Display article from catalog item
 * Add access rights verification, price display and shopping cart link on article for public catalog
 * @see crm_ArticleDisplay
 * 
 * @property crm_Article $article
 */
class crm_CatalogItemDisplay extends crm_ArticleDisplayFinal {

	/**
	 *
	 * @var crm_CatalogItem
	 */
	protected $catalogItem = null;


	/**
	 *
	 * @param crm_CatalogItem $item
	 *
	 */
	public function __construct(Func_Crm $Crm, crm_CatalogItem $item)
	{
		$this->catalogItem = $item;
		parent::__construct($Crm, $item->article());
	}




	/**
	 * Add catalog item to shopping cart
	 * @return Widget_Action
	 */
	protected function getShoppingCartAction(crm_ShoppingCartItemSourceInterface $shoppingSource = null) {
		
		if (null === $shoppingSource)
		{
			$shoppingSource = $this->article->getMainArticlePackaging();
		}
		
		if (null === $shoppingSource)
		{
			return null;	
		}
		
		if ($shoppingSource instanceof crm_CatalogItem)
		{
			return $this->Crm()->Controller()->ShoppingCart()->addCatalogItem($shoppingSource->id);
		}
		
		if ($shoppingSource instanceof crm_ArticlePackaging)
		{
			return $this->Crm()->Controller()->ShoppingCart()->addCatalogItem($this->catalogItem->id, $shoppingSource->id);
		}
		
		throw new ErrorException(sprintf('the shoppingSource %s is not implemented', get_class($shoppingSource)));
	}





	/**
	 * Shopping cart link
	 * @return Widget_Link
	 */
	protected function getShoppingLink(crm_ShoppingCartItemSourceInterface $shoppingSource = null) {

		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		if (null === $shoppingSource)
		{
			$shoppingSource = $this->article->getMainArticlePackaging();
		}


		if (isset($shoppingSource)) {

			$cart = $Crm->ShoppingCartSet()->getMyCart(false);
			if ($cart instanceOf crm_ShoppingCart) {
				$set = $Crm->ShoppingCartItemSet();
				$res = $set->selectFromSource($cart->id, $shoppingSource);

				if ($res->count() > 0) {
					return $W->Label($Crm->translate('Allready in shopping cart'))->addClass('crm-shopping-link-disabled');
				}
			}
		}

		return $this->getAddToCartButton($shoppingSource)->addClass('crm-shopping-link');
	}


	/**
	 * The add to cart button
	 * @return Widget_Link
	 */
	protected function getAddToCartButton(crm_ShoppingCartItemSourceInterface $shoppingSource = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		bab_functionality::includefile('Icons');
		
		$link = $W->Link(
			$W->Icon($Crm->translate('Add to shopping cart'), Func_Icons::ACTIONS_LIST_ADD), 
			$this->getShoppingCartAction($shoppingSource)
		);
		
		$link->addAttribute('rel', 'nofollow');
		
		return $link;

	}



	/**
	 * Get similar catalog item as widget
	 * @return Widget_Item
	 */
	public function getSimilar()
	{
		$W = bab_Widgets();
		$I = $this->catalogItem->similarCatalogItems();

		if (null === $I) {
			return null;
		}

		if (0 === $I->count()) {
			return null;
		}

		$frame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em')->addClass('similar-catalog-items');

		$n = 0;
		foreach($I as $catalogItem)
		{
			$link = $this->getSimilarLink($catalogItem);
			$frame->addItem($link);

			$n++;

			if ($n > 10) {
				break;
			}
		}

		return $frame;
	}





	/**
	 *
	 * @param crm_Article $article
	 * @return Widget_Item | null
	 */
	protected function getSimilarLink(crm_CatalogItem $catalogItem)
	{
		$W = bab_Widgets();

		$access = $this->Crm()->Access();

		if ($access->viewCatalogItem($catalogItem)) {
			$display = $this->Crm()->Ui()->CatalogItemDisplay($catalogItem);
			return $display->getContextLink();
		}

		return null;
	}
	
	
	/**
	 * Display price in online shop tax included or tax excluded
	 * @return boolean
	 */
	protected function taxIncluded()
	{
		return true;
	}
	
	
	/**
	 * Get the label of the packaging
	 */
	protected function getMainPackaging()
	{
		$W = bab_Widgets();
		
		$packaging = $this->catalogItem->getPackagingName();
		
		if ('' === $packaging)
		{
			return null;
		}
		
		return $W->Label($packaging);
	}
	
	
	
	
	
	/**
	 * Display the price of catalog item with unit and the reduction
	 * @return Widget_Frame
	 */
	protected function getUnitCostFrame(crm_ShoppingCartItemSourceInterface $shoppingSource = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		/*@var $Crm Func_Crm */
		
		if (null === $shoppingSource)
		{
			$shoppingSource = $this->catalogItem->article->getMainArticlePackaging();
		}
		
		if (null === $shoppingSource)
		{
			return null;
		}
		
		if ($this->taxIncluded())
		{
			$unit_cost = $shoppingSource->getUnitCostTI();
			$instead_of = $shoppingSource->getRawUnitCostTI();
			
		} else {
		
			$unit_cost = $shoppingSource->getUnitCostDF();
			$instead_of = $shoppingSource->getRawUnitCostDF();
		}
		
		
		if ($unit_cost !== $instead_of)
		{
			$price = $this->amountEuro($unit_cost)->addClass('crm-promo-price');
			$disabled = $this->amountEuro($instead_of)->addClass('crm-disabled-price');
			
			$layout = $W->VBoxItems($price, $W->FlowItems($W->Label($Crm->translate('Instead of')), $disabled)->setHorizontalSpacing(.5,'em'));
		} else {

			$layout = $W->Layout()->additem($this->amountEuro($unit_cost)->addClass('crm-price'));
		}
		
		return $W->Frame(null, $layout)->addClass('crm-catalogitem-unitcost');
		
	}
	
	
	
	/**
	 * @param	float	$amount
	 * @param	bool	$tax_incl
	 * @return Widget_Displayable_Interface
	 * 
	 */
	protected function amountEuro($amount)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		/*@var $Crm Func_Crm */
		
		$price = $Crm->numberFormat($amount, 2).' '.$Crm->Ui()->Euro();
		
		if ($this->taxIncluded())
		{
			$suffix = $Crm->translate('incl tax');
		} else {
			$suffix = $Crm->translate('tax excl');
		}
		
		return $W->FlowItems($W->Label($price), $W->Label($suffix)->addClass('crm-price-suffix'));
	}
	
	
	
	/**
	 * 
	 * @param crm_ArticlePackaging $articlePackaging
	 * 
	 * @return 
	 */
	protected function getArticlePackagingFrame(crm_ArticlePackaging $articlePackaging)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		/*@var $Crm Func_Crm */
		
		$Access = $Crm->Access();
		
		$frame = $W->Frame(null, $W->Layout())->addClass('crm-articlepackaging-price');
		if ($Access->usePackaging())
		{
			$frame->addItem($W->Label($articlePackaging->getPackagingName()));
		}
		$hbox = $W->HBoxLayout()->setHorizontalSpacing(3,'em')->setVerticalAlign('top')->setSizePolicy(Widget_SizePolicy::MAXIMUM);
		$frame->addItem($hbox);
		
		
		$hbox->addItem($this->getUnitCostFrame($articlePackaging));
		
		if ($Crm->Access()->addCatalogItemToShoppingCart($this->catalogItem, $articlePackaging))
		{
			$hbox->addItem($this->getShoppingLink($articlePackaging));
		}
		
		return $frame;
	}
	
	
	
	
	
	/**
	 * The buy frame
	 * for multiple packaging and prices or for the main packaging only
	 * 
	 * @param	bool	$mainonly
	 * 
	 * @return Widget_Frame
	 */
	public function getBuyFrame($mainonly = false)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.6,'em'))->addClass('crm-buy-item');
		
		bab_functionality::includeOriginal('Icons');
		$frame->addClass(Func_Icons::ICON_LEFT_16);
		
		
		if (false === $mainonly)
		{
			$res = $this->article->selectPackagings();
			foreach($res as $articlePackaging)
			{
				$frame->addItem($this->getArticlePackagingFrame($articlePackaging));
			}
			
			return $frame->addClass('crm-buy-item-multi');
		}
		
		$articlePackaging = $this->article->getMainArticlePackaging();
		
		if (null === $articlePackaging)
		{
			return null;
		}
		
		
		$frame->addItem($this->getArticlePackagingFrame($articlePackaging));
		return $frame;
	}
	
	


	/**
	 * Article frame without the main photo, contain information displayed on the right of the main photo
	 * @return Widget_Frame
	 */
	protected function getTopFrame()
	{
		$W = bab_Widgets();
	
		$top = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'));

		if ($articletype = $this->articletype())
		{
			$top->addItem($articletype);
		}
		
		$top->additem($this->getBuyFrame());
	

	
		return $top;
	}
	
	
	/**
	 * Contain items displayed on full frame page
	 * after the HBox with photo
	 */
	public function getFullFrameItems()
	{
		$W = bab_Widgets();
	
		$frame = $W->Frame();

		$frame->addItem($this->catalogs());
		$frame->addItem($this->description());
		$frame->addItem($this->properties());
		$frame->addItem($this->attachments());

	
		return $frame;
	}
	
	/**
	 * @return Widget_Frame
	 */
	protected function getComments()
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$W = bab_Widgets();
		
		if (!isset($Crm->Comment) || !$Access->viewMyComments())
		{
			return null;
		}
		
		$frame = $W->Frame();
	
		if ($Access->createCommentOn($this->article))
		{
			$frame->additem($Crm->Ui()->CommentEditor(false, $this->article));
		}
		
		$frame->additem($Crm->Ui()->CommentHistory($this->article));
	
		return $frame;
	}
	
	
	
	/**
	 * Get linked articles in a flow layout
	 * 
	 * @param string $type
	 * 
	 * 		'I' => crm_translate('Up-sell'),		//ventes incitatives
	 *		'C' => crm_translate('Cross-sell'),		//ventes croisees
	 *  	'A' => crm_translate('Related product')	//apparentes
	 *  
	 *  @return Widget_Section
	 */
	public function getLinkedArticle($type, $method = 'getShopCardFrame')
	{
		return crm_getLinkedArticlesFrame($this->Crm(), array($this->article->id), $type, $method);
	}
	


	/**
	 * Full frame visible by customer
	 *
	 * @return Widget_Item
	 */
	public function getFullFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$frame = parent::getFullFrame();
		
		$access = $Crm->Access();
		/*@var $access crm_Access */
		if ($access->updateArticle($this->article))
		{
			$frame->addItem(
			    $W->Frame()->addClass(Func_Icons::ICON_LEFT_16)
			    ->addItem(
    			    $W->Link(
    			      $Crm->translate('Edit this article'), 
    			      $Crm->Controller()->Article()->edit($this->article->id)
    			    )
    			    ->addClass('icon')
    			    ->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT)
    			    ->addClass('crm-article-edit-link')
    		    )
			);
		}
		
		if ($comments = $this->getComments())
		{
			$frame->addItem($comments);
		}
		
		if ($linked = $this->getLinkedArticle('I'))
		{
			$frame->addItem($linked);
		}
		
		
		return $frame;
	}

	/**
	 *
	 * @return crm_Actions
	 */
	public function detailViewActions()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$actions = new crm_Actions();
		$actions
		 	->addItem($this->getShoppingLink())
			->addItem($W->Link($W->Icon($Crm->translate('Search results'), Func_Icons::ACTIONS_VIEW_LIST_TEXT ), $this->Crm()->Controller()->CatalogItem()->displayList()));
		return $actions;
	}
	
	
	/**
	 * @return Widget_Item
	 */
	protected function availability()
	{
		if (!$this->article->articleavailability)
		{
			return null;
		}
		
		if (!($this->article->articleavailability instanceof crm_ArticleAvailability))
		{
			throw new ErrorException('missing joint on ArticleAvailability');
		}
		
		$Crm = $this->Crm();
		$W = bab_Widgets();
		if ('' === $this->article->articleavailability->color)
		{
			$color = '#fff';
		} else {
			$color = '#'.$this->article->articleavailability->color;
		}
		$name = $this->article->articleavailability->name;
		
		$circle = $W->Label(bab_nbsp())->addClass('crm-circle');
		$circle->setCanvasOptions($circle->Options()->backgroundColor($color));
		
		return $W->HBoxItems($circle, $W->Label($name))->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle')->addClass('adv-availability');
	}
	

	
	
	/**
	 * @return Widget_Item
	 */
	protected function getArticleName()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$name = $W->Link($W->Title($this->article->name,3)->addClass('crm-article-name'), $this->catalogItem->getRewritenUrl());
		
		if ($this->article->subtitle)
		{
			$layout = $W->VBoxItems($name);
			$layout->addItem($W->Label($this->article->subtitle)->addClass('crm-small crm-article-subtitle'));
			
			return $layout;
		} else {
			return $name;
		}
	}
	
	
	/**
	 * CRM card frame
	 */
	public function getCardFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	

		$frame = $W->Frame(null, $W->VBoxLayout());
		$frame->addClass('crm-catalogitem-cardframe');
	
		$photo = $W->Link($this->Photo(64, 64), $this->catalogItem->getRewritenUrl())->addClass('photo-link');
		
	
		$frame->addItem($this->getArticleName());
		$frame->addItem($photo);
		$frame->addItem($this->getShoppingLink());
	
		return $frame;
	}
	
	/**
	 * result frame for online shop
	 * @param string $mode	user parameter 1:list | 0:card
	 */
	public function getShopResultFrame($mode)
	{
		switch((int) $mode)
		{
			case 1:
				return $this->getShopListFrame();
				break;
				
			default:
			case 0:
				return $this->getShopCardFrame();
				break;
		}
	}
	
	
	/**
	 * Online shop icon result Frame
	 * @param crm_ShoppingCartItemSourceInterface [$shoppingSource] optional shopping source, ex articlePackaging
	 * @return Widget_Frame
	 */
	public function getShopCardFrame(crm_ShoppingCartItemSourceInterface $shoppingSource = null)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$displayUrl = $this->catalogItem->getRewritenUrl();
		
		$frame = $W->Frame(null, $W->VBoxLayout());
		$frame->addClass('crm-catalogitem-cardframe');
		
		$photo = $W->Link($this->Photo(200, 150), $displayUrl)->addClass('photo-link');
		$name = $W->Link($W->Title(bab_abbr($this->article->name, BAB_ABBR_FULL_WORDS, 80),3), $displayUrl);
		$description = $W->RichText(bab_abbr($this->article->getShortDesc(), BAB_ABBR_FULL_WORDS, 70))->setRenderingOptions(BAB_HTML_ENTITIES);
		$readmore = $W->Frame()->addClass('readmore')->addItem($W->Link($Crm->translate('More').' >>', $displayUrl));
		
		$frame->addItem($photo);
		$frame->addItem($name);
		$frame->addItem($description);
		$frame->addItem($readmore);
		if ($unit_cost = $this->getUnitCostFrame($shoppingSource))
		{
			$frame->addItem($unit_cost);
		}
		$frame->addItem($this->getShoppingLink());
		
		return $frame;
	}
	
	
	protected function getShopListPhoto()
	{
	    $W = bab_Widgets();
	    $displayUrl = $this->catalogItem->getRewritenUrl();
	    return $W->Link($this->Photo(64, 64), $displayUrl)->addClass('crm-catalogitem-photocontainer-64');
	}
	
	
	/**
	 * Online shop list result frame
	 * @return Widget_Frame
	 */
	public function getShopListFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$displayUrl = $this->catalogItem->getRewritenUrl();

		$frame = $W->Frame(null, $W->HBoxLayout());
		$frame->addClass('crm-catalogitem-searchresult');

		$frame->addItem($this->getShopListPhoto());
		$frame->addItem($data = $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		$frame->addItem($this->getBuyFrame());
		
		
		$data->setSizePolicy(Widget_SizePolicy::MAXIMUM);
		$data->addItem($this->getArticleName());
		
		$description = $W->RichText($this->article->getShortDesc())->setRenderingOptions(BAB_HTML_ENTITIES);
		$readmore = $W->Frame()->addClass('readmore')->addItem($W->Link($Crm->translate('More').' >>', $displayUrl));
		
		$data->addItem($description);
		
		if ($availability = $this->availability())
		{
			$data->addItem($availability);
		}
		
		$data->addItem($readmore);
		
		return $frame;
	}
	
	
	/**
	 * Display the item in shopping cart
	 * @return Widget_Displayable_Interface
	 */
	public function getShoppingCartLayout(crm_ShoppingCartItem $shoppingCartItem)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$Access = $Crm->Access();
		
		$displayUrl = $this->catalogItem->getRewritenUrl();

		$photo = $W->Link($this->Photo(48, 48), $displayUrl);
		$name = $W->Link($shoppingCartItem->name, $displayUrl);
		if ($shoppingCartItem->subtitle)
		{
			$subtitle = $W->Label($shoppingCartItem->subtitle)->addClass('crm-small');
		} else {
			$subtitle = null;
		}
			
		return $W->HBoxItems(
			$photo->setSizePolicy(Widget_SizePolicy::MINIMUM), 
			$W->VBoxItems(
				$name, 
				$subtitle, 
				$Access->usePackaging() ? $W->Label($shoppingCartItem->packaging) : null
			)
		)->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle');
	}
	
	
	/**
	 * Display the item in the option dialog used to set quantity before adding to shopping cart
	 * @param	crm_ArticlePackaging		$articlePackaging
	 * @return Widget_Displayable_Interface
	 */
	public function getOptionDialogLayout(crm_ArticlePackaging $articlePackaging)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$Access = $Crm->Access();
		
		// TODO fix the bug with the regular ImageThumbnail widget
		// the thumnail is not loading because of the delayed loading of the dialog content
		// the thumbnailer is used directly to bypass the error
		
		$T = bab_functionality::get('Thumbnailer');
		/*@var $T Func_Thumbnailer */
		$path = $this->getMainPhotoPath();
		
		if (null === $path)
		{
			$addon = bab_getAddonInfosInstance('LibCrm');
			require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
			$path = new bab_Path($addon->getStylePath() . 'images/article-default.png');
		} else {
			$path = $this->getMainPhotoPath();
		}
		
		$T->setSourceFile($path);
		
		$photo = $W->Image($T->getThumbnail(64, 64));
		$name = $W->Title($this->article->name, 3);
		if ($this->article->subtitle)
		{
			$subtitle = $W->Label($this->article->subtitle)->addClass('crm-small');
		} elseif ($shortdesc = $this->article->getShortDesc()) {
			$subtitle = $W->Label($shortdesc)->addClass('crm-small');
		} else {
			$subtitle = null;
		}
		
		return $W->HBoxItems(
			$photo, 
			$W->VBoxItems(
				$W->VBoxItems($name, $subtitle), 
				$Access->usePackaging() ? $this->packaging($articlePackaging->packaging) : null
			)->setVerticalSpacing(.5,'em')
		)->setHorizontalSpacing(.5,'em')->setVerticalAlign('top');
	}
	
	
	
	/**
	 * @return Widget_Action
	 */
	protected function getAttachmentUrl($filename)
	{
	    $Crm = $this->Crm();
	    return $Crm->Controller()->CatalogItem()->downloadAttachment($this->catalogItem->id, $filename);
	}
}









class crm_CatalogItemSearchEditor extends crm_Editor
{
	public function __construct(Func_Crm $Crm, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($Crm, $id, $layout);
		
		$this->colon();
		$this->setName('search');
		$this->addClass('crm-catalogitem-search-editor');
		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->setReadOnly();
		
		$this->addFields();
		$this->addButtons();
	}

	protected function addFields()
	{
		$this->addItem($this->keywords());
		$this->addItem($this->catalog());
		$this->addItem($this->reduction());
	}
	
	
	/**
	 * The main search field
	 * @return 
	 */
	protected function keywords()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $this->labelledField(
				$Crm->translate('Search keywords'),
				$W->LineEdit()->setSize(50),
				__FUNCTION__
		);
	}
	
	
	protected function catalog()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $this->labelledField(
				$Crm->translate('Category'),
				$W->Select()->setOptions($Crm->CatalogSet()->getOptions()),
				__FUNCTION__
		);
	}
	
	
	public function reduction()
	{
		$Crm = $this->Crm();
		
		$W = bab_Widgets();
		return $this->labelledField(
				$Crm->translate('Only discounted products'),
				$W->Checkbox(),
				__FUNCTION__
		);
	} 
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$search = $W->SubmitButton()
			->setLabel($Crm->translate('Search'))
			->setAction($Crm->Controller()->CatalogItem()->displayList());
		
		$this->addItem($search);
	}
}





class crm_catalogItemOptionsDialogEditor extends crm_Editor
{
	/**
	 * @var crm_CatalogItem
	 */
	protected $catalogItem;
	
	/**
	 * @var int
	 */
	protected $defaultQuantity = 1;
	
	public function __construct(Func_Crm $Crm, crm_CatalogItem $catalogItem, crm_ArticlePackaging $articlePackaging, $quantity = null)
	{
		parent::__construct($Crm);
	
		$W = bab_Widgets();
		$this->colon();
		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->setHiddenValue('catalogitem', $catalogItem->id);
		$this->setHiddenValue('articlepackaging', $articlePackaging->id);
		//$this->setReadOnly();
		
		$this->catalogItem = $catalogItem;
		
		if (null !== $quantity)
		{
			$this->defaultQuantity = $quantity;
		}
		
		$display = $Crm->Ui()->CatalogItemDisplay($catalogItem);
	
		$this->addItem($display->getOptionDialogLayout($articlePackaging));
		$this->addItems();
		$this->addButtons();
	}
	
	
	protected function addItems()
	{
		$this->addItem($this->quantity());
	}
	
	
	protected function quantity()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $W->FlowItems(
			$W->Label($Crm->translate('Add the product in my shopping cart with the quantity'))->colon(),
			$W->LineEdit()->setSize(2)->setValue($this->defaultQuantity)->setName(__FUNCTION__)
		)->setSpacing(0,'em', .5,'em');
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		
		
		$actions = $W->FlowItems()->setSpacing(0,'em', .8,'em');
		$this->addItem($actions);
		
		
		
	
		$continue = $W->SubmitButton()
			->setLabel($Crm->translate('Continue shopping'))
			->setAction($Crm->Controller()->ShoppingCart()->addCatalogItem());
		
		$home = $Crm->getShopHomeAction();

		$continue->setSuccessAction($home);
		$continue->setFailedAction($home);
	

			
	
		$actions->addItem($continue);
		$actions->addItem($W->Label($Crm->translate('or')));
		
		$cart = $W->SubmitButton()
			->setLabel($Crm->translate('View my shopping cart'))
			->setAction($Crm->Controller()->ShoppingCart()->addCatalogItemEnd())
			->setSuccessAction($Crm->Controller()->ShoppingCart()->edit());
		
		if ($lastest = crm_BreadCrumbs::getLastest())
		{
			$cart->setFailedAction($lastest->getAction());
		}
		
		$actions->addItem($cart);
	}
}



/**
 * Get linked articles in a flow layout
 *
 * @param string $type
 *
 * 		'I' => crm_translate('Up-sell'),		//ventes incitatives
 *		'C' => crm_translate('Cross-sell'),		//ventes croisees
 *  	'A' => crm_translate('Related product')	//apparentes
 *
 *  @return Widget_Section
 */
function crm_getLinkedArticlesFrame(Func_Crm $Crm, Array $articles, $type, $method = 'getShopCardFrame')
{
	if (!isset($Crm->ArticleLink))
	{
		return null;
	}
	
	$Ui = $Crm->Ui();
	$W = bab_Widgets();
	$Access = $Crm->Access();
	$itemSet = $Crm->CatalogItemSet();
	$itemSet->article();
	$itemSet->article->joinHasOneRelations();
	

	$itemSet->catalog();

	$articleLinkSet = $Crm->ArticleLinkSet();
	$criteria = $articleLinkSet->article_from->in($articles)
	   ->_AND_($articleLinkSet->type->is($type))
	   ->_AND_($articleLinkSet->deleted->is(0));
	
	$catalogItems = $itemSet->select(
			$itemSet->article->id->in($criteria, 'article_to')
			->_AND_($itemSet->article->disabled->is(0))
			->_AND_($itemSet->catalog->getAccessibleCatalogs())
	);

	$frame = $W->Section(
			$Crm->translate('You should like'),
			$W->FlowLayout()->addClass('crm-linked-article')
			->setVerticalSpacing(1, 'em')
			->setHorizontalSpacing(1, 'em')
	);
	foreach($catalogItems as $catalogItem){
		$frame->addItem($Ui->CatalogItemDisplay($catalogItem)->$method());
	}

	if(!count($catalogItems)){
		return null;
	}

	return $frame;
	
}
