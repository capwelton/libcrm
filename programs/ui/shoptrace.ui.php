<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * 
 *
 */
class crm_ShopTraceTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_ShopTrace $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		switch ($fieldPath) {
			
			case '_view':
				return $W->Link($W->Icon($Crm->translate('View'), Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), $Crm->Controller()->ShopTrace()->display($record->trace));
				break;
		}
		
		

		return parent::computeCellContent($record, $fieldPath);
	}
	
	
	public function addDefaultColumns(crm_ShopTraceSet $set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn('_view', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		$this->addColumn(widget_TableModelViewColumn($set->traceCreatedOn, $Crm->translate('Created on')));
		$this->addColumn(widget_TableModelViewColumn($set->remote_ip, $Crm->translate('Remote IP address')));
		$this->addColumn(widget_TableModelViewColumn($set->user_agent, $Crm->translate('User Agent')));
		
	}
}






class crm_ShopTraceLogTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_ShopTrace $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		switch ($fieldPath) {
	
			case 'cart':
				return $W->Link($record->cart, $Crm->Controller()->ShoppingCartAdm()->display($record->cart));
				break;
		}
	
	
	
		return parent::computeCellContent($record, $fieldPath);
	}
	
	
	public function addDefaultColumns(crm_ShopTraceSet $set)
	{
		$Crm = $this->Crm();
		$this->addColumn(widget_TableModelViewColumn($set->createdOn, $Crm->translate('Created on')));
		$this->addColumn(widget_TableModelViewColumn($set->session, $Crm->translate('Session ID'))->setVisible(false));
		$this->addColumn(widget_TableModelViewColumn($set->shoppingcart_cookie, $Crm->translate('Shopping cart cookie'))->setVisible(false));
		$this->addColumn(widget_TableModelViewColumn($set->user_agent, $Crm->translate('User Agent'))->setVisible(false)->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->remote_ip, $Crm->translate('Remote IP address'))->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->url, $Crm->translate('Url')));
		$this->addColumn(widget_TableModelViewColumn($set->message, $Crm->translate('Message')));
		
		
		$this->addColumn(widget_TableModelViewColumn($set->cart, $Crm->translate('Shopping cart')));
	}
}




class crm_ShopTraceCardFrame extends crm_CardFrame
{
	protected $shopTrace;
	
	public function __construct(Func_Crm $Crm, crm_ShopTrace $shopTrace)
	{
		$this->shopTrace = $shopTrace;
		$W = bab_Widgets();
		
		parent::__construct($Crm, null, $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		
		$this->addClass('crm-shoptrace-cardframe')->addClass(Func_Icons::ICON_LEFT_16);
		$this->addItem($this->labelStr($Crm->translate('Remote IP address'), $W->Link($shopTrace->remote_ip, 'http://www.geoiptool.com/'.$GLOBALS['babLanguage'].'/?IP='.$shopTrace->remote_ip)->setOpenMode(Widget_Link::OPEN_POPUP)));
		
		/*
		if ($host = gethostbyaddr($shopTrace->remote_ip))
		{
			$this->addItem($this->labelStr($Crm->translate('Host name'), $host));
		}
		
		$this->addItem($this->labelStr($Crm->translate('User Agent'), $shopTrace->user_agent));
		*/
		
		if ($Crm->Access()->viewShopTrace()) {
		    $this->addItem($W->Link($W->Icon($Crm->translate('Message log in trace'), Func_Icons::ACTIONS_VIEW_HISTORY), $Crm->Controller()->ShopTrace()->display($shopTrace->trace)));
		}
		
	}
}
