<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Represent a file or folder attached to a crm_Record
 *
 * @throws Exception
 */
class crm_FileAttachment extends crm_Object
{
    /**
     * absolute path of file or folder
     * @var bab_Path
     */
    private $path;


    /**
     * relative path of file or folder from the record upload folder
     * can be null if object represent the root folder
     *
     * @see crm_TraceableRecord::uploadPath()
     * @var bab_Path
     */
    private $relativePath;


    /**
     * Object of attachemnt
     * @var crm_TraceableRecord
     */
    private $record;


    /**
     * Get secured path of file in parameter for a file or a folder
     *
     *
     * @param	crm_TraceableRecord	$record			object
     * @param	string				$relativePath	relative path to object
     *
     * @throws	Exception
     */
    public function __construct(Func_Crm $Crm, crm_TraceableRecord $record, $relativePath)
    {
        $this->path = $record->uploadPath();

        if ('' === $relativePath || null === $relativePath)
        {
            $this->record = $record;
            $this->relativePath = new bab_Path('');
            return;
        }



        $pathElements = explode('/', $relativePath);
        foreach ($pathElements as $pathElement) {
            if ($pathElement) {
                $this->path->push($pathElement);
            }
        }

        $basepath = $record->uploadPath()->getRealPath();
        $filepath = $this->path->getRealPath();

        if (0 !== strpos($filepath, $basepath))
        {
            $this->path = null;
            throw new Exception(sprintf($record->Crm()->translate('The path (%s) is not accessible'), $filepath));
        }


        $this->record = $record;
        $this->relativePath = new bab_Path(substr($filepath, strlen($basepath) + 1));

    }


    /**
     * Get full absolute path
     * @return bab_Path
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * Get secure relative path
     * @return bab_Path
     */
    public function getRelativePath()
    {
        return $this->relativePath;
    }

    /**
     *
     * @return crm_TraceableRecord
     */
    public function getRecord()
    {
        return $this->record;
    }

    /**
     * Get parent file attachment object or null if the current object is already root
     * @return crm_FileAttachment
     */
    public function getParent()
    {
        if (!$this->relativePath->toString())
        {
            return null;
        }

        $parent = clone $this->relativePath;
        $parent->pop();

        return $this->Crm()->FileAttachment($this->record, $parent->toString());
    }



    /**
     *
     * @return bool
     */
    public function isDir()
    {
        return $this->getPath()->isDir();
    }

    /**
     * Delete attachment
     * @return bool
     */
    public function delete()
    {
        $p = $this->getPath();
        if ($this->isDir())
        {
            return $p->deleteDir();
        } else {
            return unlink($p->toString());
        }
    }
}

