<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

include_once dirname(__FILE__).'/set/base.class.php';

/**
 * Use payment library
 *
 */
class crm_Payment extends crm_Object
{

	/**
	 * Returns the path of the payment system functionality configured for use with the shop.
	 * The returned string can be used in bab_getFunctionality().
	 *
	 * @return string
	 */
	protected function getPaymentSystemPath()
	{
		$Crm = $this->Crm();

		$addonname = $Crm->getAddonName();


		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		$value = $registry->getValue('payment_system');

		if (null === $value || '' === $value) {
			return null;
		}

		return 'Payment/' . $value;
	}





	/**
	 * Displays the payment page.
	 *
	 * This part is mainly managed by the payment system.
	 *
	 * @see Func_Payment::getPaymentRequestHtml()
	 *
	 * @throws ErrorException, Exception
	 *
	 */
	public function displayPaymentPage()
	{
		$Crm = $this->Crm();

		$paymentSystemPath = $this->getPaymentSystemPath();
		if (!isset($paymentSystemPath)) {
			$message = $Crm->translate('The payment system is not configured in online shop options');
			throw new Exception($message);
		}

		/* @var $PaymentSystem Func_Payment */
		$PaymentSystem = bab_functionality::get($paymentSystemPath);
		if (!isset($PaymentSystem)) {
		    $message = sprintf($Crm->translate('Unable to instanciate payment functionality (%s).'), $paymentSystemName);
			throw new Exception($message);
		}


		$set = $Crm->ShoppingCartSet();
		$cartRecord = $set->getMyCart();


		$W = bab_Widgets();

		if (!$cartRecord) {
			return;
		}

		if (crm_ShoppingCart::CONFIRMED !== (int) $cartRecord->status) {
		    $message = $Crm->translate('Your shopping cart is not confirmed');
			throw new Exception($message);
		}

		if (!$Crm->isContactLogged()) {
		    $message = $Crm->translate('You are not logged in as a regular customer');
			throw new Exception($message);
		}


		$set = $this->Crm()->ContactSet();
		$contact = $set->get($set->user->is($GLOBALS['BAB_SESS_USERID']));

		if (!$contact) {
			throw new Exception('Unexpected error, missing contact');
		}


		$contactEmail = trim($contact->getMainEmail());
		if (empty($contactEmail) || strpos($contactEmail, '@') === false || strpos($contactEmail, '.') === false) {
			throw new ErrorException($Crm->translate('Your email address is invalid'));
		}


		$amount = $cartRecord->getTotalTI();



		$payment = $PaymentSystem->newPayment();


		$payment->setAmount($amount);
		$payment->setCurrency(libpayment_Payment::CURRENCY_EUR);
		$payment->setPurchase($cartRecord);
		
		$text = '#'.$cartRecord->id." - ";
		foreach($cartRecord->getItems() as $scItem) {
		    /*@var $scItem crm_ShoppingCartItem */
		    $text .= $scItem->getDispQuantity().' '.$scItem->packaging.' '.$scItem->catalogitem->article->reference.", ";
		}
		
		$payment->setText(rtrim($text, ', '));
		
		$payment->setEmail($contactEmail);
		
		if(method_exists($PaymentSystem, 'setDoneUrl')){
			$PaymentSystem->setDoneUrl($GLOBALS['babUrl'].$Crm->Controller()->ShoppingCart()->paymentUserReturn($cartRecord->id)->url());
		}
		if(method_exists($PaymentSystem, 'setCancelUrl')){
			$PaymentSystem->setCancelUrl($GLOBALS['babUrl'].$Crm->Controller()->ShoppingCart()->edit()->url());
		}
		if(method_exists($PaymentSystem, 'setRefusedUrl')){
			$PaymentSystem->setRefusedUrl($GLOBALS['babUrl'].$Crm->Controller()->ShoppingCart()->edit()->url());
		}

		// save transaction token into shopping cart
		// the token must not be accessible from the user
		$cartRecord->paymentToken = $payment->getToken();
		$cartRecord->save();


		try {

			$page = $Crm->Ui()->Page();
			$html = $PaymentSystem->getPaymentRequestHtml($payment);
			if (stripos($html, '<html>') !== false) {
				die($html);
			}
			
			$page->addItem($scFrame = $W->Frame()->addClass('crm-shoppingprocess-frame'));
			$steps = $Crm->Ui()->ShoppingCartSteps($cartRecord, 3);
			$scFrame->addItem($steps);
 			$scFrame->addItem($W->Html($html));
 			
 			return $page;

			// TODO : we should instead return a special page object.


		} catch (ErrorException $e) {

			$page->addError($e->getMessage());
			return $page;
		}

	}


}
