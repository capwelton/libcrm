<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */







class crm_BreadCrumbs
{
	/**
	 * Do not cleanum if number of items lower than this number
	 * @var int
	 */
	const MAX_ITEM	= 50;

	/**
	 * Removes older traces if number of traces greater than this number
	 * @var int
	 */
	const MAX_TRACE = 5;


	/**
	 *
	 * @var crm_Controller
	 */
	private static $controller = null;


	private static function get()
	{
		if (isset($_SESSION['BreadCrumbs'])) {

			if (!is_string($_SESSION['BreadCrumbs']))
			{
				unset($_SESSION['BreadCrumbs']);
				$breadcrumbs = array();
			} else {

				$breadcrumbs = unserialize($_SESSION['BreadCrumbs']);
			}

		} else {
			$breadcrumbs = array();
		}

		return $breadcrumbs;
	}


	private static function set($breadcrumbs)
	{
		$_SESSION['BreadCrumbs'] = serialize($breadcrumbs);
	}

	/**
	 *
	 * @param crm_Controller $controller
	 */
	public static function setCurrentController(crm_Controller $controller)
	{
		self::$controller = $controller;
	}

	/**
	 *
	 * @return crm_Controller
	 */
	public static function getCurrentController()
	{
		return self::$controller;
	}


	/**
	 * limit number of traces
	 * @param	Array	$breadcrumbs
	 * @return Array
	 */
	private static function cleanup(Array $breadcrumbs)
	{
		if (count($breadcrumbs) <= self::MAX_ITEM)
		{
			// return unmodified if number of total items is low
			return $breadcrumbs;
		}


		$traces = array();

		foreach($breadcrumbs as $crumb)
		{
			/*@var $crumb crm_BreadCrumbItem */

			if (null === $crumb->getParent())
			{
				// root node of a trace
				$traces[] = $crumb;
			}
		}


		$number_of_traces = count($traces);

		if ($number_of_traces <= self::MAX_TRACE)
		{
			// return unmodified if number of traces is low (equal to number of opened tabs)
			return $breadcrumbs;
		}

		// sort traces traces by last usage date

		bab_Sort::sortObjects($traces, 'getTraceLastUsage');
		$delete_traces = array_slice($traces, 0, ($number_of_traces - self::MAX_TRACE));

		foreach($delete_traces as $crumb)
		{
			foreach($crumb->getTraceItems() as $uid)
			{
				unset($breadcrumbs[$uid]);
			}
		}


		return $breadcrumbs;
	}



	/**
	 * Method for controller to add all pages, before the setCurrentPosition
	 *
	 * @param	Widget_Action	$action
	 * @param	string			$previous		UID of previous page
	 *
	 * @return crm_BreadCrumbItem the new generated bread crumb item
	 */
	public static function addPage(Widget_Action $action, $previous = null)
	{
		$breadcrumbs = self::get();

		$breadcrumbs = self::cleanup($breadcrumbs);

		$actionUrl = new bab_url($action->url());
		$actionChecksum = $actionUrl->checksum();

		// try to detect previous UID from referer
		if (isset($_SERVER['HTTP_REFERER']) && null === $previous)
		{
			$refererend = mb_substr($_SERVER['HTTP_REFERER'], mb_strlen($GLOBALS['babUrl']));
			$referer = new bab_url($_SERVER['HTTP_REFERER']);
			$babrw = null;



			if (!empty($refererend) && false === mb_strpos($refererend, '?') && false === mb_strpos($refererend, '=') && $refererend !== basename($_SERVER['PHP_SELF']))
			{
				// rewrited with mod rewrite
				$babrw = $refererend;
			}

			if (null !== $referer->babrw)
			{
				// sitemap url without rewriting
				$babrw = $referer->babrw;
			}



			// process rewriten URL
			require_once $GLOBALS['babInstallPath'] . 'utilit/sitemap.php';
			if (null !== $babrw && false !== $arr = bab_siteMap::extractNodeUrlFromRewrite($babrw))
			{
				$referer->babrw = $babrw;
				foreach($arr as $parameter => $value)
				{
					$referer->$parameter = $value;
				}
			}


			$checksum = $referer->checksum();


			if (isset($_SESSION['BreadCrumbsCrc'][$checksum]))
			{
				$previous = $_SESSION['BreadCrumbsCrc'][$checksum];

			}
		}

		if (null !== $previous && isset($breadcrumbs[$previous]))
		{
			$parent = $breadcrumbs[$previous];
			/*@var $parent crm_BreadCrumbItem */

			if ($parent->checksum === $actionChecksum)
			{
				// do not add crumb if the previous page is the same,
				// move the previous page to last position for the setCurrentPosition() method

				unset($breadcrumbs[$parent->uid]);
				$breadcrumbs[$parent->uid] = $parent;
				self::set($breadcrumbs);
				return $parent;
			}

			if ($child = $parent->getChild($actionChecksum))
			{
				// do not add crumb if a child of parent allready exists,
				// move the child to last position for the setCurrentPosition() method

				unset($breadcrumbs[$child->uid]);
				$breadcrumbs[$child->uid] = $child;
				self::set($breadcrumbs);
				return $child;
			}

			$item = new crm_BreadCrumbItem;
			$item->setParent($parent);
		} else {

			// if a root node with same action allready exists, use it
			foreach($breadcrumbs as $crumb)
			{
				/*@var $crumb crm_BreadCrumbItem */
				if (null === $crumb->getParent() && $crumb->checksum === $actionChecksum)
				{
					// move the found root node to last position for the setCurrentPosition() method
					unset($breadcrumbs[$crumb->uid]);
					$breadcrumbs[$crumb->uid] = $crumb;
					self::set($breadcrumbs);
					return $crumb;
				}
			}


			$item = new crm_BreadCrumbItem;
		}

		$item->setAction($action);
		$breadcrumbs[$item->uid] = $item;

		self::set($breadcrumbs);

		return $item;
	}



	/**
	 * Add a label to current page in breadcrumb.
	 *
	 * @param Widget_Action $action 			this parameter exist for backward compatibility
	 * @param string $label
	 *
	 * @return void
	 */
	public static function setCurrentPosition(Widget_Action $action, $label)
	{
	    if (bab_isAjaxRequest()) {
	        return;
	    }
		$breadcrumbs = self::get();

		if (empty($breadcrumbs)) {
			throw new ErrorException('The breadcrumb must be initialized throw the controller');
		}
		$lastcrumb = end($breadcrumbs);
		$lastcrumb->label = $label;

		self::set($breadcrumbs);
	}


	/**
	 *
	 * @return string
	 */
	public static function asHtml()
	{
		$breadcrumbs = self::get();
		// find crumbs without parent

		$breadcrumbs = array_reverse($breadcrumbs);

		$html = '<table border="1" cellpadding="5"><tr>';

		foreach($breadcrumbs as $crumb)
		{
			if (null === $crumb->getParent())
			{
				$html .= '<td style="vertical-align:top">';
				$html .= sprintf('<h4>%s</h4>',bab_toHtml(bab_shortDate($crumb->time, true)));
				$html .= $crumb->asHtml();
				$html .= '</td>';
			}
		}

		$html .= '</tr></table>';

		return $html;
	}

	/**
	 * Clear all beadcrumb branchs
	 */
	public static function clearAll()
	{
		unset($_SESSION['BreadCrumbsCrc']);
		unset($_SESSION['BreadCrumbs']);
	}


	/**
	 * Clears all content in the breadcrumb in current branch only.
	 *
	 * @return void
	 */
	public static function clear($previous = null)
	{
		$breadcrumbs = self::get();



		if (isset($_SERVER['HTTP_REFERER']) && null === $previous)
		{

			$referer = new bab_url($_SERVER['HTTP_REFERER']);
			$checksum = $referer->checksum();

			if (isset($_SESSION['BreadCrumbsCrc'][$checksum]))
			{
				$previous = $_SESSION['BreadCrumbsCrc'][$checksum];
			}
		}


		if (null === $previous)
		{
			bab_debug('breadcrumb clear failed: no previous url found', DBG_ERROR, 'Breadcrumbs');
			return;
		}

		if (!isset($breadcrumbs[$previous]))
		{
			bab_debug('breadcrumb clear failed: previous action not found in breadcumbs', DBG_ERROR, 'Breadcrumbs');
			return;
		}



		$parent = $breadcrumbs[$previous];
		do {
			if (count($parent->getChildNodes()) < 2)
			{
				$next = $parent->getParent();

				$parent->__destruct();
				unset($breadcrumbs[$parent->uid]);


				if ($next)
				{
					$parent = $next;
				} else {
					$parent = null;
				}
			} else {
				$parent = null;
			}
		} while($parent);

		self::set($breadcrumbs);
	}


	public static function pop()
	{
		$breadcrumbs = self::get();
		$pop = array_pop($breadcrumbs);
		self::set($breadcrumbs);
		return $pop;
	}


	/**
	 * Returns the last labeled action present in the breadcrumbs stack.
	 *
	 * @return Widget_Action
	 */
	public static function lastAction($parameters = null)
	{
		$breadcrumbs = self::get();

		if (empty($breadcrumbs)) {
			return null;
		}

		$lastcrumb = end($breadcrumbs);
		do {

			if (null !== $lastcrumb->label)
			{
				$action = $lastcrumb->getAction();

				if (isset($parameters)) {
					foreach ($parameters as $name => $value) {
						$action->setParameter($name, $value);
					}
				}
				return $action;
			}
		} while($lastcrumb = $lastcrumb->getParent());

		return null;
	}



	/**
	 * Returns the last labeled action's url present in the breadcrumbs stack.
	 * return null if the url is the same as current url to prevent infinite loop, if the url is really needed with that similarity, use the lastAction method
	 *
	 *
	 * @return string
	 */
	public static function last($parameters = null)
	{
		$lastaction = self::lastAction($parameters);

		if (null === $lastaction)
		{
			return null;
		}


		$url = $lastaction->url();
		$obj = new bab_url($url);

		if ($obj->checksum() === bab_url::get_request_gp()->checksum())
		{
			return null;
		}

		return $url;
	}

	/**
	 * @return crm_BreadCrumbItem
	 */
	public static function getLastest()
	{
		$breadcrumbs = self::get();
		return end($breadcrumbs);
	}


	/**
	 * Returns an action based on its (relative) postion in the breadcrumbs stack.
	 *
	 * @param int $index If index is a positive integer the action with this index
	 *                   in the breadcrumb stack will be returned (index 0 is the
	 *                   oldest action in the breadcrumb).
	 *                   If index is a negative integer it is considered as an offset
	 *                   from the end of the stack (-1 is the most recent action)
	 * @return Widget_Action or null if he specified index was not found.
	 */
	public static function getPosition($index)
	{
		$breadcrumbs = self::get();

		if (empty($breadcrumbs)) {
			return null;
		}

		$breadcrumbs = array_values($breadcrumbs);
		if ($index < 0) {

			$pos = $index;
			$crumb = end($breadcrumbs);
			while($pos !== 0 && $crumb = $crumb->getParent()) {
				$pos++;
			}

			if ($crumb)
			{
				return $crumb->getAction();
			} else {

				// action without parent? try to get previous action in history by time

				$pos = (count($breadcrumbs) -1) + $index;
				if (isset($breadcrumbs[$pos]))
				{
					bab_debug(sprintf('crm_Breadcrumbs::getPosition(%s) action found using history by time, one of parent action is probably missing', $index));

					return $breadcrumbs[$pos]->getAction();
				}

				// no action found for this relative index
				return null;
			}
		}
		if (!isset($breadcrumbs[$index])) {
			return null;
		}
		return $breadcrumbs[$index]->getAction();
	}


	/**
	 * Get the last GET request
	 * This ignore only posts with a csrf token
	 */
	public static function lastGet($startPos = -1)
	{
	    do {
	        $action = self::getPosition($startPos);
	        $startPos--;
	    } while (($action instanceof Widget_Action) && $action->parameterExists('babCsrfToken'));

	    return $action;
	}


	/**
	 * Returns the most recent action in the breadcrumbs stack equivalent to the
	 * specified action.
	 *
	 * @param Widget_Action $action
	 *
	 * @return Widget_Action
	 */
	public function getAction(Widget $action)
	{
		$breadcrumbs = self::get();

		if (empty($breadcrumbs)) {
			return $action;
		}

		$crumb = end($breadcrumbs);
		do {

			if ($crumb->getAction()->getMethod() === $action->getMethod())
			{
				return $crumb->getAction();
			}

		} while($crumb = $crumb->getParent());

		return $action;
	}


	/**
	 * Returns the most recent item in the breadcrumbs stack equivalent to the
	 * specified action method.
	 *
	 * @param	array				$arr		list of nodes
	 * @param crm_BreadCrumbItem 	$crumb
	 *
	 * @return crm_BreadCrumbItem
	 */
	private static function getLastItem($arr, $pos, crm_BreadCrumbItem $item)
	{

		$i = count($arr) -1;
		do {
			if ($arr[$i]->getAction()->getMethod() === $item->getAction()->getMethod())
			{
				return $i;
			}

			$i--;

		} while($i >= $pos);

		return $i++;
	}


	/**
	 * get a list of crm_BreadCrumbItem
	 * @return array
	 */
	public static function getLocationItems()
	{
		$branch = array();
		$location = array();

		$breadcrumbs = self::get();

		if (empty($breadcrumbs)) {
			return array();
		}

		$crumb = end($breadcrumbs);
		do {
			if (null !== $crumb->label)
			{
				array_unshift($branch, $crumb);
			}
		} while($crumb = $crumb->getParent());

		$count = count($branch);
		for($i = 0; $i < $count; $i++)
		{
			$newpos = self::getLastItem($branch, $i, $branch[$i]);
			if ($newpos != $i && $i < $newpos)
			{
				$i = $newpos;
			}

			$location[] = $branch[$i];
		}


		// bab_debug(self::asHtml(), DBG_TRACE, 'Breadcrumbs');

		return $location;
	}



}









class crm_BreadCrumbItem
{
	/**
	 *
	 * @var string
	 */
	public $uid;

	/**
	 *
	 * @var crm_BreadCrumbItem
	 */
	private $parent;


	/**
	 *
	 * @var Widget_Action
	 */
	private $action;


	/**
	 *
	 * @var string
	 */
	public $label;

	/**
	 * checksum of action URL
	 * @var int
	 */
	public $checksum;

	/**
	 * for each nodes, references to child nodes
	 * @var array
	 */
	private $childnodes = array();


	/**
	 * Creation time
	 * @var int
	 */
	public $time;


	public function __construct()
	{
		$this->uid = uniqid();
		$this->time = time();
	}


	public function getParent()
	{
		return $this->parent;
	}

	public function setParent(crm_BreadCrumbItem $parent)
	{
		$this->parent = $parent;

		$parent->childnodes[] = $this;
	}


	public function getChildNodes()
	{
		return $this->childnodes;
	}


	/**
	 * remove parent of crumb item, this item will start a new branch from root
	 * @return unknown_type
	 */
	public function removeParent()
	{
		$this->parent = null;
	}


	public function setAction(Widget_Action $action)
	{
		$this->action = $action;

		$url = new bab_url($action->url());
		$this->checksum = $url->checksum();

		$_SESSION['BreadCrumbsCrc'][$this->checksum] = $this->uid;
	}

	/**
	 *
	 * @return Widget_Action
	 */
	public function getAction()
	{
	    if (isset($this->label)) {
	        $this->action->setTitle($this->label);
	    }

		return $this->action;
	}

	/**
	 * Search action in existing child
	 * @param Widget_Action | int $action			oaction or checksum
	 * @return crm_BreadCrumbItem
	 */
	public function getChild($action)
	{
		if ($action instanceof Widget_Action)
		{
			$url = new bab_url($action->url());
			$checksum = $url->checksum();
		} else {
			$checksum = $action;
		}

		foreach($this->childnodes as $crumb)
		{
			if ($crumb->checksum === $checksum)
			{
				return $crumb;
			}
		}

		return null;
	}


	/**
	 * get all childnodes UID recursively in a list
	 * @return array
	 */
	public function getTraceItems()
	{
		$list = array($this->uid);

		foreach($this->childnodes as $crumb)
		{
			$list = array_merge($list, $crumb->getTraceItems());
		}

		return $list;
	}


	/**
	 * Get last usage timstamp in all childnodes
	 * @return int
	 */
	public function getTraceLastUsage($last = null)
	{
		if (null === $last || $last < $this->time)
		{
			$last = $this->time;
		}

		foreach($this->childnodes as $crumb)
		{
			/*@var $crumb crm_BreadCrumbItem */

			$childtime = $crumb->getTraceLastUsage($last);

			if ($childtime > $last)
			{
				$last = $childtime;
			}
		}

		return $last;
	}



	/**
	 * this crumb item with the child nodes in a html tree view
	 * @return string
	 */
	public function asHtml()
	{
		return sprintf('<ul>%s</ul>',$this->HtmlListItem());
	}

	private function HtmlListItem()
	{

		if ($this->label)
		{
			$label = $this->checksum.' '.$this->label;
		} else {
			$label = $this->checksum.' '.$this->uid;
		}


		$html = sprintf('<li><a href="%s">%s</a></li>', bab_toHtml($this->getAction()->url()), bab_toHtml($label));
		if ($this->childnodes)
		{
			$html .= '<ul>';
			foreach($this->childnodes as $crumb)
			{
				$html .= $crumb->HtmlListItem();
			}
			$html .= '</ul>';
		}

		return $html;
	}




	public function __destruct()
	{
		// ce code produit un segmentation fault dans certains cas :

		/*if (isset($this->parent))
		{
			foreach($this->parent->getChildNodes() as $key => $child)
			{
				if ($child->uid === $this->uid)
				{
					unset($this->parent->childnodes[$key]);
				}
			}

			$this->parent = null;
		}*/
	}
}
