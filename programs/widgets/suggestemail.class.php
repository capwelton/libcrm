<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestTextEdit');






/**
 * Constructs a crm_SuggestEmail.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestEmail
 */
function crm_SuggestEmail($id = null)
{
	return new crm_SuggestEmail($id);
}


/**
 * A crm_SuggestEmail
 */
class crm_SuggestEmail extends Widget_SuggestTextEdit implements Widget_Displayable_Interface
{

	private $category = null;


	/**
	 * @param Func_Crm $crm		The category to select from.
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($crm, $id = null)
	{
		$this->crm = $crm;
		parent::__construct($id);
		$this->setLines(1);
		$this->setMultiple(',');
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-suggestemail';
		return $classes;
	}





	/**
	 * Send suggestions
	 */
	public function suggest()
	{

		$Crm = $this->crm;

		if (false !== $keyword = $this->getSearchKeyword()) {

			$contactSet = $Crm->ContactSet();

			$entries = $contactSet->select(
				$contactSet->email->isNot('')
					->_AND_(
						$contactSet->email->contains($keyword)
							->_OR_($contactSet->lastname->contains($keyword))
							->_OR_($contactSet->firstname->contains($keyword))
				)
			);
			$entries->orderAsc($contactSet->lastname);

			$i = 0;
			$emails = array();
			foreach ($entries as $contact) {

				$i++;
				if ($i > Widget_SuggestTextEdit::MAX) {
					break;
				}

				/* No duplication */
				if (isset($emails[$contact->email])) {
					break;
				} else {
					$emails[$contact->email] = $contact->email;
				}

				parent::addSuggestion(
					$contact->id,
					$contact->email,
					$contact->lastname.' '.$contact->firstname,
					''
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas)
	{
		$this->suggest();
		return parent::display($canvas);
	}

}