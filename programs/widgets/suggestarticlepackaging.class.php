<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');




/**
 * A crm_SuggestArticle
 */
class crm_SuggestArticlePackaging extends Widget_SuggestLineEdit implements Widget_Displayable_Interface, crm_Object_Interface
{

	private $crm = null;


	/**
	 * Get Crm object
	 * @return Func_Crm
	 */
	public function Crm()
	{
		return $this->crm;
	}

	/**
	 * Forces the Func_Crm object to which this object is 'linked'.
	 *
	 * @param Func_Crm	$crm
	 * @return crm_SuggestArticle
	 */
	public function setCrm(Func_Crm $crm = null)
	{
		$this->crm = $crm;
		return $this;
	}



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-suggestarticlepackaging';
		return $classes;
	}



	/**
	 * get separator beetween product name and packaging name
	 * @return string
	 */
	public function getSeparator()
	{
		return bab_nbsp().'/'.bab_nbsp();
	}




	/**
	 * Send suggestions
	 */
	private function suggest()
	{
	    $Crm = $this->Crm();

	    
	    
		$set = $Crm->ArticlePackagingSet();
		$set->article();
		$set->packaging();


		if (false !== $keyword = $this->getSearchKeyword()) {

			$k = explode($this->getSeparator(), $keyword);
			$criteria = $set->article->name->startsWith($k[0])->_OR_($set->article->reference->startsWith($k[0]));
			if (isset($k[1]))
			{
				$criteria = $criteria->_AND_($set->packaging->name->startsWith($k[1]));
			}

			$articles = $set->select($criteria);

			$i = 0;
			foreach ($articles as $articlePackaging) {
				/* @var $article crm_Article */

				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}

				$article = $articlePackaging->article;

				parent::addSuggestion(
					$articlePackaging->id,
					$article->getName().$this->getSeparator().$articlePackaging->packaging->name,
					empty($article->subtitle) ? $article->reference : $article->subtitle
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas) {

	    $Crm = $this->Crm();

	    if (!isset($Crm->ArticlePackaging)) {
	        return;
	    }
	    
		$this->setIdName('id');
		$this->suggest();
		return parent::display($canvas);
	}

}

