<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

if (!defined('FUNC_CRM_PHP_PATH')) {

    // in tests, the constant is defined

    $libcrm_addon = bab_getAddonInfosInstance('LibCrm');

    if (!$libcrm_addon) {
        // addon not installed
        return;
    }

    define('FUNC_CRM_PHP_PATH', $libcrm_addon->getPhpPath());
}

define('FUNC_CRM_SET_PATH', FUNC_CRM_PHP_PATH. 'set/');
define('FUNC_CRM_UI_PATH', FUNC_CRM_PHP_PATH . 'ui/');
define('FUNC_CRM_CTRL_PATH', FUNC_CRM_PHP_PATH . 'ctrl/');
define('FUNC_CRM_NOTIFY_PATH', FUNC_CRM_PHP_PATH . 'notify/');
define('FUNC_CRM_PORTLET_PATH', FUNC_CRM_PHP_PATH . 'portlet/');

require_once FUNC_CRM_PHP_PATH . 'utilit.php';
require_once FUNC_CRM_SET_PATH . 'base.class.php';
require_once FUNC_CRM_UI_PATH . 'base.ui.php';
require_once FUNC_CRM_UI_PATH . 'ui.helpers.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';



class crm_Exception extends Exception
{
}


/**
 * crm_NotFoundException are thrown when the user tries to
 * access a non-existent crm object using the request() method.
 *
 * @see crm_RecordSet::request()
 *
 * @since 1.0.18
 */
class crm_NotFoundException extends crm_Exception
{
    private $recordSet = null;

    private $id = null;

    /**
     * @param ORM_RecordSet $recordSet
     * @param string $id
     */
    public function __construct(ORM_RecordSet $recordSet, $id)
    {
        $this->recordSet = $recordSet;
        $this->id = $id;
    }

    /**
     * @return ORM_RecordSet
     */
    public function getRecordSet()
    {
        return $this->recordSet;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get the set description and translate
     * @return string
     */
    public function getObjectTitle()
    {
        $element = crm_translate('object');
        if ($description = $this->getRecordSet()->getDescription()) {
            $element = mb_strtolower(crm_translate($description));
        }
        
        return $element;
    }
}


/**
 * crm_DeletedRecordException are thrown when the user tries to
 * access a deleted crm object using the request() method.
 *
 * @see crm_RecordSet::request()
 *
 * @since 1.0.21
 */
class crm_DeletedRecordException extends crm_NotFoundException
{
    /**
     * @var ORM_Record
     */
    private $record = null;

    /**
     * @param ORM_Record $record
     */
    public function __construct(ORM_Record $record)
    {
        parent::__construct($record->getParentSet(), $record->id);
        $this->record = $record;
    }
    
    /**
     * @return string
     */
    public function getDeletedBy()
    {
        if (!$this->record->deletedBy) {
            return null;
        }
        
        $W = bab_Widgets();
        return $W->Label(sprintf(crm_translate('Deleted by: %s'), bab_getUserName($this->record->deletedBy)));
    }
    
    
    /**
     * @return string
     */
    public function getDeletedOn()
    {
        $set = $this->getRecordSet();
        if (!$set->deletedOn->isValueSet($this->record->deletedOn)) {
            return null;
        }
    
        $W = bab_Widgets();
        return $W->Label(sprintf(crm_translate('Deleted on: %s'), bab_shortDate(bab_mktime($this->record->deletedOn))));
    }
    
}


/**
 * crm_AccessException are thrown when the user tries to
 * perform an action or access a page that she is not
 * allowed to.
 */
class crm_AccessException extends crm_Exception
{
    /**
     * Require credential if the user is not logged in
     * @var bool
     */
    public $requireCredential = true;
}

/**
 * crm_EmptyResultException are thrown for empty search results
 *
 */
class crm_EmptyResultException extends crm_Exception
{
    public $page;

    public function __construct($message, Widget_Page $page)
    {
        $this->page = $page;
    }
}



/**
 * crm_SaveException are thrown in save methods of controller.
 * this will go back to previous page in breadcrumb history (the edit form)
 *
 */
class crm_SaveException extends crm_Exception
{
    /**
     * Relative position in breadcrumbs history
     * the save action is in position 0 (do not use position 0)
     * @var int
     */
    public $breadcrumbs_position = -1;

    /**
     * true : redirect to failed action
     * false : execute failed action directly without redirect
     * 		(can be used to redirect ot the same form and populate
     * 		 fields with posted data)
     *
     * @var bool
     */
    public $redirect = true;
}
