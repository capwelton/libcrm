<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


if (!class_exists('Widget_Action')) {
    bab_widgets()->includePhpClass('Widget_Action');
}


require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/json.php';
require_once dirname(__FILE__) . '/breadcrumbs.class.php';

/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function crm_translate($str)
{
    $translation = bab_translate($str, 'LibCrm');

    return $translation;
}

/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function crm_translateArray($arr)
{
    $newarr = $arr;

    foreach ($newarr as &$str) {
        $str = crm_translate($str);
    }
    return $newarr;
}


/**
 * Instanciates the widget factory.
 *
 * @return Func_Widgets
 */
function crm_Widgets()
{
    return bab_functionality::get('Widgets');
}



/**
 * Instanciates the Crm factory.
 *
 * @return Func_Crm
 */
function crm_Crm()
{
    return bab_functionality::get('Crm');
}


/**
 * Instanciates the Newsletter factory.
 *
 * @return Func_Newsletter
 */
/*function crm_Newsletter()
{
    return bab_functionality::get('Newsletter');
}*/


/**
 * Instanciates the MailingList factory.
 *
 * @return Func_MailingList
 */
function crm_MailingList()
{
    return bab_functionality::getOriginal('MailingList');
}



function crm_addPageInfo($message)
{
    $_SESSION['crm_msginfo'][] = $message;
}


/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 *
 * @param Widget_Action|string $url
 */
function crm_redirect($url, $message = null)
{
    if (null === $url) {
        $script = '<script type="text/javascript">';
        if (isset($message)) {
            $script .= 'alert("' . bab_toHtml($message, BAB_HTML_JS) . '");';
        }
        $script .= 'history.back();';
        $script .= '</script>';

        die($script);
    }

    if ($url instanceof Widget_Action) {
        $url = $url->url();
    }

    $babBody = bab_getBody();

    $lines = array();
    if (!empty($babBody->msgerror)) {
        $lines = array_merge($lines, explode("\n", $babBody->msgerror));
    }
    if (!empty($babBody->messages)) {
        $lines = array_merge($lines, $babBody->messages);
    }
    if (isset($message)) {
        $lines = explode("\n", $message);
    }
    foreach ($lines as $line) {
        crm_addPageInfo($line);
    }

    header('Location: ' . $url);
    die;
}


/**
 * @param string	$labelText
 * @param string	$inputWidget
 * @param string	$descriptionText
 * @return Widget_Item
 */
function crm_FormField($labelText, Widget_Displayable_Interface $inputWidget, $descriptionText = null)
{
    $W = bab_Widgets();

    $formField = $W->VBoxLayout()->setVerticalSpacing(0.2, 'em');

    if ($inputWidget instanceof Widget_CheckBox) {
        $formField->addItem(
            $W->HBoxItems(
                $inputWidget,
                $W->Label($labelText)->setAssociatedWidget($inputWidget)
            )->setVerticalAlign('middle')
        );
    } else {
        $formField->addItem($W->Label($labelText)->setAssociatedWidget($inputWidget));
        $formField->addItem($inputWidget);
    }
    if (isset($descriptionText)) {
        $formField->addItem($W->Label($descriptionText)->addClass('crm-field-description'));
    }

    return $formField;
}







/**
 * An a empty form.
 * @return crm_BaseForm
 */
function crm_BaseForm($id = null)
{
    return new crm_BaseForm($id);
}


bab_Widgets()->includePhpClass('Widget_Frame');


/**
 * An a empty form with a button box.
 */
class crm_BaseForm extends Widget_Frame
{
    private $outerLayout = null;
    private $innerLayout = null;
    private $buttonRow = null;

    public function __construct($id)
    {
        parent::__construct($id);

        $W = bab_Widgets();

        $this->outerLayout = $W->VBoxLayout()
            ->addClass('crm-form-layout')
            ->setVerticalSpacing(2, 'em');
        $this->setLayout($this->outerLayout);

        $this->innerLayout = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'em');

        $this->outerLayout->addItem($this->innerLayout);

        $this->buttonRow = $W->HBoxLayout()
            ->setHorizontalSpacing(1, 'em')
            ->addClass('crm-form-button-row');

        $this->outerLayout->addItem($this->buttonRow);
    }

    /**
     * Adds an item to the top part of the form.
     *
     * @param Widget_Item	$item
     *
     * @return $this
     */
    public function addItem(Widget_Displayable_Interface $item = null)
    {
        $this->innerLayout->addItem($item);

        return $this;
    }

    /**
     * Adds a button to button box of the form.
     *
     * @return crm_BaseForm
     */
    public function addButton(Widget_SubmitButton $button)
    {
        $this->buttonRow->addItem($button);

        return $this;
    }
}







/**
 * @param	mixed	$value
 * @return	string
 * @deprecated Use bab_json_encode()
 */
function crm_json_encode($a)
{
	return bab_json_encode($a);
}



/**
 *
 * @return array
 */
function crm_getCalendarCategories()
{
    require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';

    static $eventCategories = null;
    if (!isset($eventCategories)) {
        $eventCategories = array();
        $categories = bab_calGetCategories();
        foreach ($categories as $category) {
            $eventCategories[$category['id']] = $category;
        }
    }
    return $eventCategories;
}


/**
 * create or validate a rewriten name
 * @param string $name
 */
function crm_getRewriteName($rewriteName)
{
    $rewriteName = bab_removeDiacritics($rewriteName);
    $rewriteName = strtolower($rewriteName);
    $rewriteName = str_replace(array(' ', '/'), '-', $rewriteName);
    $rewriteName = preg_replace('/[^-0-9A-Za-z]/', '', $rewriteName);
    $rewriteName = preg_replace('/[-]+/', '-', $rewriteName);
    $rewriteName = trim($rewriteName, '- ');

    return $rewriteName;
}



/**
 * Upload file error to string
 * @return string
 */
function crm_fileUploadError($error)
{
    switch($error) {
        case UPLOAD_ERR_OK:
            return null;

        case UPLOAD_ERR_INI_SIZE:
            return crm_translate('The uploaded file exceeds the upload_max_filesize directive.');

        case UPLOAD_ERR_FORM_SIZE:
            return crm_translate('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');

        case UPLOAD_ERR_PARTIAL:
            return crm_translate('The uploaded file was only partially uploaded.');

        case UPLOAD_ERR_NO_FILE:
            return crm_translate('No file was uploaded.');

        case UPLOAD_ERR_NO_TMP_DIR: //  since PHP 4.3.10 and PHP 5.0.3
            return crm_translate('Missing a temporary folder.');

        case UPLOAD_ERR_CANT_WRITE: //  since php 5.1.0
            return crm_translate('Failed to write file to disk.');

        default :
            return crm_translate('Unknown File Error.');
    }
}





/**
 * Fix datetime submited from a separated datePicker and timePicker
 * @param array $dateTime
 * @param bool $ignoreTime
 *
 * @return string
 */
function crm_fixDateTime(Array $dateTime, $ignoreTime)
{
    $W = bab_Widgets();
    $date = $W->DatePicker()->getISODate($dateTime['date']);

    if (!$date || '0000-00-00' === $date) {
        return '0000-00-00 00:00:00';
    }

    $hour = $dateTime['time'];

    if (5 === mb_strlen($hour)) {
        $hour .= ':00';
    }

    if ($ignoreTime || empty($hour) || 8 !== mb_strlen($hour)) {
        $hour = '00:00:00';
    }

    return $date.' '.$hour;
}
