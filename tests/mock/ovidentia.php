<?php

$GLOBALS['babInstallPath'] = dirname(__FILE__).'/../../vendor/capwelton/core/programs/';


require_once $GLOBALS['babInstallPath'].'utilit/dbutil.php';
require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/utilit.php';
require_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/functionality.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';



// create babDB

$GLOBALS['babDBHost'] = getenv('babDBHost') !== false ? getenv('babDBHost') : '127.0.0.1';
$GLOBALS['babDBLogin'] = getenv('babDBLogin') !== false ? getenv('babDBLogin') : 'test';
$GLOBALS['babDBPasswd'] = getenv('babDBPasswd') !== false ? getenv('babDBPasswd') : '';
$GLOBALS['babDBName'] = getenv('babDBName') !== false ? getenv('babDBName') : 'test';

$GLOBALS['babDB'] = $babDB = new babDatabase();


// exec('mysql -u test -Nse "show tables" test | while read table; do mysql -u test -e "drop table $table" test; done');

exec('mysql -u ' . $GLOBALS['babDBLogin'] . ' ' . $GLOBALS['babDBName'] . ' < vendor/capwelton/core/install/babinstall.sql 2>/dev/null');

$babDB->db_query("INSERT INTO bab_addons(title) VALUES ('widgets')");
$babDB->db_query("INSERT INTO bab_addons(title) VALUES ('jquery')");

$GLOBALS['babLanguage'] = 'en';

if (!defined('FUNC_WIDGETS_PHP_PATH')) {
    define('FUNC_WIDGETS_PHP_PATH', realpath(dirname(__FILE__) . '/../../vendor/capwelton/widgets/programs/widgets') . '/');
}

// if (!defined('FUNC_WIDGETS_JS_PATH')) {
//     define('FUNC_WIDGETS_JS_PATH', realpath(dirname(__FILE__) . '/../../programs/skins/ovidentia/templates') . '/');
// }


$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */
$session->setStorage(new bab_SessionMockStorage());


$functionalities = new bab_functionalities();
$functionalities->register('Widgets', dirname(__FILE__) . '/../../vendor/capwelton/widgets/programs/widgets.php');
$functionalities->register('jquery', dirname(__FILE__) . '/../../vendor/ovidentia/jquery/programs/jquery.php');
$functionalities->register('Icons'						, $GLOBALS['babInstallPath'].'utilit/icons.php');
$functionalities->register('Icons/Default'				, $GLOBALS['babInstallPath'].'utilit/icons.php');

