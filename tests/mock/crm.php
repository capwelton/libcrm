<?php

define('FUNC_CRM_PHP_PATH', dirname(__FILE__).'/../../programs/');


// initialize ORM


// Initialize mysql ORM backend.

require_once dirname(__FILE__).'/ovidentia.php';

require_once dirname(__FILE__).'/../../vendor/capwelton/liborm/programs/orm.class.php';

$ORM = new Func_LibOrm();
$ORM->initMySql();
$mysqlbackend = new ORM_MySqlBackend($babDB);
ORM_MySqlRecordSet::setBackend($mysqlbackend);
