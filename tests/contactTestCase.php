<?php

require_once dirname(__FILE__).'/mock/crmTestCase.php';

abstract class crm_ContactTestCase extends crm_TestCase
{
    
    
    /**
     * @return crm_Contact
     */
    public function getMockContact()
    {
        $set = $this->Crm()->ContactSet();
        
        $contact = $set->newRecord();
        $contact->lastname = 'Lastname';
        $contact->firstname = 'Firstname';
        $contact->email = 'test@example.com';
        
        return $contact;
    }
    
    
    public function testSave()
    {
        $contact = $this->getMockContact();
        $this->assertTrue($contact->save());
        $this->assertTrue($contact->id > 0);
    }
    
    
    public function testGetMailEmail()
    {
        $contact = $this->getMockContact();
        $this->assertEquals('test@example.com', $contact->getMainEmail());
    }
    
    /*
    public function testUserCreate()
    {
        $contact = $this->getMockContact();
        $id_user = $contact->createUser('testcrm', 'secret');
        
        $this->assertTrue($id_user > 0);
    }
    */
}